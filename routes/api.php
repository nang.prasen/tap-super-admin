<?php

use App\Http\Controllers\API\ArticleApiController;
use App\Http\Controllers\API\ArticleComment;
use App\Http\Controllers\API\BannerController;
use App\Http\Controllers\API\CatalogController;
use App\Http\Controllers\API\BookController;
use App\Http\Controllers\API\BookPopulerController;
use App\Http\Controllers\API\DistributionController;
use App\Http\Controllers\API\Register\RegisterController;
use App\Http\Controllers\API\VideoBookController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('komen-artikelya', [CommentArticleApi::class,'userPostComment']);

Route::apiResource('register', RegisterController::class);
//catalog
Route::get('buku/{id}', [CatalogController::class,'recomendedbook']);
Route::get('catalog/buku', [CatalogController::class,'recomendedbook']);
Route::get('catalog/buku/detail/{id}', [CatalogController::class,'bookDetail']);
Route::get('catalog/buku/count', [CatalogController::class,'categoryCountBook']);
Route::get('catalog/buku/rekomendasi-baru', [CatalogController::class,'recomendLastBook']);
Route::get('catalog/buku/rekomendasi-paket', [CatalogController::class,'recomendPacketBook']);
Route::get('catalog/category/{category}', [CatalogController::class,'listCategoryBook']);
Route::get('catalog/search/{keyword}', [CatalogController::class,'searchingAsset']);
// catalog videobook-tidakjadi
Route::get('catalog/videbook', [VideoBookController::class,'list']);
Route::get('catalog/videbook/all', [VideoBookController::class,'listAll']);
Route::get('catalog/videbook/detail/{id}', [VideoBookController::class,'detail']);
//catalog audiobook
Route::get('catalog/audiobook', [CatalogController::class,'audiobookAll']);
//catalog videobook
Route::get('catalog/list-all/videobook', [CatalogController::class,'videobookAll']);

//artikel
Route::get('article/list',[ArticleApiController::class,'index']);

//banner
Route::get('banner/dashboard', [BannerController::class,'bannerDashboard']);
Route::get('banner/catalog', [BannerController::class,'bannerCatalog']);
Route::get('banner/user', [BannerController::class,'bannerUser']);
Route::get('sub-category/{category}',[BookController::class,'subCategory']);
Route::post('distribution/pembayaran',[DistributionController::class,'pembayaran']);
Route::post('distribution/pembayaran/bukti',[DistributionController::class,'buktiPembayaran']);
// book
Route::prefix('book')
->group(function(){
    Route::get('list', [BookController::class,'list']);
});


