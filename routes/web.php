<?php

use App\Http\Controllers\Admin\AgencyController;
use App\Http\Controllers\Admin\AgentController;
use App\Http\Controllers\Admin\ArticleControlle;
use App\Http\Controllers\Admin\BannerController;
use App\Http\Controllers\Admin\BookController;
use App\Http\Controllers\Admin\CatalogController;
use App\Http\Controllers\Admin\DistribusiSegera;
use App\Http\Controllers\Admin\DistributionController;
use App\Http\Controllers\Admin\LocationController;
use App\Http\Controllers\Admin\PublisherController;
use App\Http\Controllers\Admin\RabAdminController;
use App\Http\Controllers\Admin\RecapitulationController;
use App\Http\Controllers\Admin\TransactionController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Manajemen\ManajemenController;
use App\Http\Controllers\Mitra\MitraController;
use App\Http\Controllers\Mitra\RabController;
use App\Http\Controllers\Perwakilan\PerwakilanController;
use App\Http\Controllers\Publisher\AudioBookController;
use App\Http\Controllers\Publisher\BookControllerPublisher;
use App\Http\Controllers\Publisher\EpubController;
use App\Http\Controllers\publisher\ProfileControllerPublisher;
use App\Http\Controllers\Publisher\RekapControllerPublisher;
use App\Http\Controllers\Publisher\VideoBookPublisher;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;

use function Clue\StreamFilter\fun;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [App\Http\Controllers\HomeController::class, 'registerInstansi'])->name('register.instansi');
Route::get('/assets', [App\Http\Controllers\HomeController::class, 'assets']);
Route::post('/register/instansi', [App\Http\Controllers\HomeController::class, 'actionRegister'])->name('create.register');
Route::get('/debug-sentry', function () {
    throw new Exception('My first Sentry error!');
});
//lokasi
Route::get('provinces', [LocationController::class, 'provinces'])->name('provinces');
Route::get('cities', [LocationController::class, 'cities'])->name('cities');
Route::get('districts', [LocationController::class, 'districts'])->name('districts');
Route::get('villages', [LocationController::class, 'villages'])->name('villages');

Auth::routes();

Route::group(['middleware' => ['IsSuperAdmin','auth'] ], function()
{
    Route::get('/home/admin', [App\Http\Controllers\HomeController::class, 'index'])->name('home.admin');
    Route::get('/user', [App\Http\Controllers\Admin\UserController::class, 'index'])->name('user');
    Route::get('/user-edit', [App\Http\Controllers\Admin\UserController::class, 'edit'])->name('user-edit');
    Route::get('/user-create', [App\Http\Controllers\Admin\UserController::class, 'create'])->name('user-create');
    Route::post('/user-insert', [App\Http\Controllers\Admin\UserController::class, 'store'])->name('user-store');
    Route::get('/user-detail', [App\Http\Controllers\Admin\UserController::class, 'show'])->name('user-detail');
    Route::get('/setting', [App\Http\Controllers\Admin\SettingController::class, 'index'])->name('setting');
    Route::get('/profile', [App\Http\Controllers\Admin\ProfileController::class, 'index'])->name('profile');
    Route::get('/profile-edit', [App\Http\Controllers\Admin\ProfileController::class, 'edit'])->name('profile-edit');

    // publisher
    Route::get('/publisher', [App\Http\Controllers\Admin\PublisherController::class, 'index'])->name('publisher');
    Route::get('/detail-publisher/{id}', [App\Http\Controllers\Admin\PublisherController::class, 'show'])->name('publisher.detail');
    Route::get('/create-publisher', [App\Http\Controllers\Admin\PublisherController::class, 'create'])->name('publisher-create');
    Route::post('/create-publisher/insert', [App\Http\Controllers\Admin\PublisherController::class, 'store'])->name('publisher.store');
    Route::get('/publisher/register-user', [PublisherController::class,'registerUser'])->name('publisher.user');
    Route::post('/publisher/register-user/insert', [PublisherController::class,'actionRegisterUser'])->name('publisher.user.store');
    Route::get('/publisher/rekapilutasi/{id}', [App\Http\Controllers\Admin\PublisherController::class, 'rekapitulasi'])->name('publisher.rekapitulasi');

    // Book
    Route::resource('book', BookController::class);
    Route::get('/distribusi', [BookController::class, 'distribution'])->name('book.distribution');
    Route::post('distribusi/insert', [BookController::class, 'insertDistribution'])->name('book.distribution.store');
    Route::get('/book/download', [BookController::class, 'getDownloadImageBook'])->name('book.download');
    Route::get('/book/export/publisher/{id}', [BookController::class, 'exportBookPublisher'])->name('book.exportdownload');
    Route::get('book/read/{id}', [BookController::class, 'readBook'])->name('baca-buku');

    //Mitra dan Perwakilan
    Route::resource('agent', AgentController::class);
    Route::get('agent/create-user/{id}', [AgentController::class, 'createUserAgent'])->name('agent.create-user');
    Route::post('agent/create-user/', [AgentController::class, 'insertUserAgent'])->name('agent.insert-user');
    Route::get('agent/mitra/create/{id}', [AgentController::class, 'mitra'])->name('agent.mitra');
    Route::post('agent/mitra/insert', [AgentController::class, 'insertmitra'])->name('agent.insert.mitra');
    Route::get('agent/mitra/edit/{id}', [AgentController::class, 'editMitra'])->name('agent.edit.mitra');
    Route::put('agent/mitra/update/{id}', [AgentController::class, 'updatemitra'])->name('agent.update.mitra');

    //Rekapitulasi
    Route::get('/recapitulation/agent', [RecapitulationController::class, 'rekapAgen'])->name('rekap.agent');
    Route::get('/detail-agency/{id}', [RecapitulationController::class, 'detail'])->name('rekap.detail');
    Route::get('/edit-agency/{id}', [RecapitulationController::class, 'agencyEdit'])
     ->name('rekap.agency.edit')
     ->missing(function (Request $request) {
         return Redirect::route('rekap.agent');
     });
    Route::put('/edit-agency/{id}', [RecapitulationController::class, 'agencyUpdate'])
     ->name('rekap.agency.update')
     ->missing(function (Request $request) {
         return Redirect::route('rekap.agent');
     });
     Route::get('/member/agen', [RecapitulationController::class,'getAgenMember'])->name('agent.member');
     Route::get('/recapitulation/mostread', [RecapitulationController::class, 'mostRead'])->name('rekap.seringdibaca');
     Route::get('/recapitulation/mostdemand', [RecapitulationController::class, 'mostDemand'])->name('rekap.bukulaku');
     Route::get('/recapitulation/mostdemand/detail/{id}', [RecapitulationController::class, 'detailBookmostDemand'])->name('rekap.bukulaku.detail');
     Route::get('/recapitulation/userinstansi', [RecapitulationController::class, 'userInstansi'])->name('rekap.userinstansi');
     Route::get('/recapitulation/userinstansi/{id}', [RecapitulationController::class, 'userInstansiDetail'])->name('rekap.userinstansi.detail');
     Route::get('/recapitulation/bookdistribution', [RecapitulationController::class, 'bookDistribute'])->name('rekap.bookdistribution');
    
    //transaksi
    Route::resource('transaction', TransactionController::class);
    
    // Master RAB
    Route::resource('master-rab', RabAdminController::class);
    Route::post('/master-rab/insert/rab',[RabAdminController::class, 'insertbudgetplan'])->name('master-rab.insert');
    Route::get('/master-rab/insert/add-item/{id}',[RabAdminController::class, 'additem'])->name('master-rab.additem');
    Route::post('/master-rab/insert/list-item',[RabAdminController::class, 'additem'])->name('master-rab.list.item');
    Route::post('/master-rab/move/temp-distribute',[RabAdminController::class, 'addToTempDistribute'])->name('master-rab.move.detail');

    //Distribusi Buku
    Route::resource('distribution', DistributionController::class);
    Route::get('/distribution/banned/{id}', [DistributionController::class, 'banned'])->name('distribution.banned');
    // publish
    Route::get('/distribution/publish/{id}', [DistributionController::class, 'publish'])->name('distribution.publish');
    
    Route::get('/distribution/book/publisher', [DistributionController::class, 'createajax'])->name('distribution-book');
    Route::post('/distribution/book/publisher', [DistributionController::class, 'createajaxPost'])->name('distribution-book.post');
    Route::post('/distribution/list-distribute', [DistributionController::class, 'listDistribute'])->name('list-destribute');
    Route::post('/distribution/post-distribute', [DistributionController::class, 'postDistribute'])->name('post-destribute');
    
    //segera tanpa endpoint
    Route::get('/distribusi/segera', [DistribusiSegera::class, 'create'])->name('sebar-buku');
    Route::post('/distribusi/segera/post', [DistribusiSegera::class, 'postDistribute'])->name('sebar-buku.post');
    Route::post('/distribusi/instansi/get', [DistribusiSegera::class, 'getAgency'])->name('sebar-buku.instansi.get');
    // Route::get('/distribution/filter/publisher/{filter_publisher}', [DistributionController::class, 'json'])->name('distribution.filter');

    // Route::get('/create-book', [App\Http\Controllers\Admin\BookController::class, 'create'])->name('book-create');
    Route::get('/detail-book', [App\Http\Controllers\Admin\BookController::class, 'show'])->name('book-detail');
    Route::get('/inventory-book', [App\Http\Controllers\Admin\BookController::class, 'inventory'])->name('book-inventory');
    Route::get('/inventory-recapitulation', [App\Http\Controllers\Admin\BookController::class, 'recapitulation'])->name('recapitulation-inventory');

    // Instansi
    Route::get('/agency', [App\Http\Controllers\Admin\AgencyController::class, 'index'])->name('agency');
    Route::get('/agency/detail/{id}', [App\Http\Controllers\Admin\AgencyController::class, 'show'])->name('agency-detail');
    Route::get('/agency/edit/{id}/edit', [App\Http\Controllers\Admin\AgencyController::class, 'edit'])->name('agency.edit');
    Route::put('/agency/update/{id}', [App\Http\Controllers\Admin\AgencyController::class, 'update'])->name('agency.update');
    Route::put('/agency/active/{id}', [App\Http\Controllers\Admin\AgencyController::class, 'active'])->name('agency.active');
    Route::put('/agency/onprogres/{id}', [App\Http\Controllers\Admin\AgencyController::class, 'onprogres'])->name('agency.onprogres');
    Route::put('/agency/banned/{id}', [App\Http\Controllers\Admin\AgencyController::class, 'banned'])->name('agency.banned');
    
    Route::get('/download/logo/{logo}', function ($logo) {
        // $name = $logo;
        $file = $logo;
        // $file = Storage::disk('image')->get("logo/".$logo);
        $headers  = array(
            'Content-Type: image/png,image/jpg,image/jpeg',
        );

        return Response::download($file, $headers);

    });

    // Article
    Route::resource('article', ArticleControlle::class);
    //Banner
    Route::resource('banner', BannerController::class);
    
});

Route::group(['middleware' => ['IsPenerbit','auth'] ], function()
    {
        Route::get('/home/penerbit', [App\Http\Controllers\HomeController::class, 'penerbit'])->name('home.penerbit');
        Route::resource('book-penerbit', BookControllerPublisher::class);
        Route::resource('videobook-penerbit', VideoBookPublisher::class);
        Route::get('/videobook-penerbit-satuan',[VideoBookPublisher::class,'CreateSatuan'])->name('videobook-penerbit.satuan');
        Route::post('/videobook-penerbit-satuan',[VideoBookPublisher::class,'PostSatuan'])->name('videobook-penerbit-satuan.post');
        Route::get('/videobook-penerbit-chapter/{id}',[VideoBookPublisher::class,'CreateChapter'])->name('videobook-penerbit.chapter');
        Route::post('/videobook-penerbit-chapter',[VideoBookPublisher::class,'PostChapter'])->name('videobook-penerbit.chapterpost');
        Route::put('/videobook-penerbit-chapter',[VideoBookPublisher::class,'PutChapter'])->name('videobook-penerbit.chapterput');
        Route::resource('audiobook-penerbit', AudioBookController::class);
        Route::resource('epub-penerbit', EpubController::class);
        Route::get('/category/sub',[VideoBookPublisher::class,'getSubCategory'])->name('getsubcategory');
        Route::get('/book-penerbit/rekapitulasi/{id}',[BookControllerPublisher::class,'bookRekap'])->name('book-penerbit.rekap');
        Route::resource('profile-penerbit', ProfileControllerPublisher::class);
        Route::get('/book-rekap', [RekapControllerPublisher::class,'index'])->name('book-rekap.index');


        Route::get('/videobook-baru-satuan',[VideoBookPublisher::class,'createNewSatuan'])->name('videobook-baru.satuan');
        Route::post('/videobook-post-satuan',[VideoBookPublisher::class,'postNewSatuan'])->name('videobook-baru.satuanpost');
        
    });

Route::group(['middleware' => ['IsMitra','auth'] ], function()
    {
        Route::get('/home/mitra', [App\Http\Controllers\HomeController::class, 'mitra'])->name('home.mitra');
        Route::get('/catalog', [CatalogController::class, 'index'])->name('catalog.mitra');
        Route::get('/catalog/detail/{id}', [CatalogController::class, 'detailCatalog'])->name('catalog.detail');
        Route::get('/catalog/category/{slug}', [CatalogController::class, 'catalogCategory'])->name('catalog.show.category');

        //rab
        Route::get('/rab', [RabController::class, 'index'])->name('rab.index');
        Route::get('/rab/cart', [RabController::class, 'cart'])->name('rab.cart');
        Route::get('/rab/create', [RabController::class, 'create'])->name('rab.create');
        Route::post('/rab/insert', [RabController::class, 'insert'])->name('rab.insert');
        Route::get('/rab/detail/{id}', [RabController::class, 'detailrab'])->name('rab.detail');
        Route::get('/rab/get-books', [RabController::class, 'getBooks'])->name('get.books');
        Route::get('/rab/export/{id}', [RabController::class, 'exportRab'])->name('rab.export');
        
        Route::resource('mitra', MitraController::class);
        Route::get('/transaksi', [MitraController::class, 'transaction'])->name('transaction');
        Route::get('/profile/mitra', [MitraController::class, 'profile'])->name('profile.mitra');
        Route::get('/profile/mitra/edit/{id}', [MitraController::class, 'profileEdit'])->name('profile.mitra.edit');
        Route::get('/profile/mitra/update/{id}', [MitraController::class, 'profileUpdate'])->name('profile.mitra.update');
    });

Route::group(['middleware' => ['IsPerwakilan','auth'] ], function()
{
    Route::get('/home/perwakilan', [HomeController::class, 'perwakilan'])->name('home.perwakilan');
    Route::resource('perwakilan', PerwakilanController::class);
    Route::get('/profile/saya', [PerwakilanController::class, 'profile'])->name('perwakilan.profile');
    Route::get('/perwakilan/mitra/list', [PerwakilanController::class, 'mitraPerwakilan'])->name('perwakilan.mitra');
    Route::get('/perwakilan/mitra/detail/{id}', [PerwakilanController::class, 'detailMitraPerwakilan'])->name('perwakilan.mitra.detail');
    
});

Route::group(['middleware' => ['IsSekolah','auth'] ], function()
{
    Route::get('/home/sekolah', [HomeController::class, 'sekolah'])->name('home.sekolah');
    Route::get('/profile/sekolah', [HomeController::class, 'profileSekolah'])->name('profile.sekolah');
    
});

Route::group(['middleware' => ['IsManajemen','auth'] ], function()
{
    Route::get('/home/manajemen', [HomeController::class, 'manajemen'])->name('home.manajemen');
    Route::get('/setting', [App\Http\Controllers\Admin\SettingController::class, 'index'])->name('setting');
    Route::get('/profile', [App\Http\Controllers\Admin\ProfileController::class, 'index'])->name('profile');
    Route::get('/profile-edit', [App\Http\Controllers\Admin\ProfileController::class, 'edit'])->name('profile-edit');

    /**
     * master data
     */
    // publisher
    Route::get('/publisher/manajemen', [ManajemenController::class, 'publisher']);
    Route::get('publisher/detail-publisher/{id}', [ManajemenController::class, 'detailPublisher'])->name('manajemen.publisher.detail');
    //mitra
    Route::get('/mitra/manajemen/data', [ManajemenController::class, 'mitra']);
    Route::get('/mitra/manajemen/data/detail/{id}', [ManajemenController::class, 'detailMitra']);
    /**
     * end master data
     */

    //rekapitulasi
    Route::get('/manajemen/rekapitulasi/buku-paling-laku',[ManajemenController::class,'rekapitulasiBukuPalingLaku']);
    Route::get('/manajemen/rekapitulasi/buku-paling-laku/detail/{id}',[ManajemenController::class,'DetailrekapitulasiBukuPalingLaku'])->name('manajemen.rekapitulasi.buku-paling-laku.detail');
    Route::get('/manajemen/rekapitulasi/buku-distribusi',[ManajemenController::class,'rekapitulasiBukuTerdistribusi']);
    Route::get('/manajemen/rekapitulasi/data-user',[ManajemenController::class,'rekapitulasiDataUser']);
    Route::get('/manajemen/rekapitulasi/data-user/detail/{id}',[ManajemenController::class,'DetailrekapDataUser'])->name('manajemen.rekapitulasi.data-user.detail');
    Route::get('/manajemen/rekapitulasi/data-instansi',[ManajemenController::class,'rekapitulasiDataInstansi']);
    Route::get('/manajemen/rekapitulasi/data-instansi/detail/{id}',[ManajemenController::class,'DetailRekapDataInstansi']);

    // Route::get('/create-book', [App\Http\Controllers\Admin\BookController::class, 'create'])->name('book-create');
    Route::get('/detail-book', [App\Http\Controllers\Admin\BookController::class, 'show'])->name('book-detail');
    Route::get('/inventory-book', [App\Http\Controllers\Admin\BookController::class, 'inventory'])->name('book-inventory');
    Route::get('/inventory-recapitulation', [App\Http\Controllers\Admin\BookController::class, 'recapitulation'])->name('recapitulation-inventory');

    // Instansi
    Route::get('/agency/manajemen', [App\Http\Controllers\Admin\AgencyController::class, 'agencyForManajemen']);

});

Route::get('distribution-detail/{id}', [DistributionController::class,'detailDistribution'])->name('distribution.detail');
