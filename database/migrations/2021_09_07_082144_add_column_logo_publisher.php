<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnLogoPublisher extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('publishers', function (Blueprint $table) {
            $table->string('logo')->after('name');
            $table->string('publisherId')->after('logo');
            $table->string('phone')->after('publisherId');
            $table->string('email')->after('phone');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('publishers', function (Blueprint $table) {
            $table->dropColumn('logo');
            $table->dropColumn('publisherId');
            $table->dropColumn('phone');
            $table->dropColumn('email');
        });
    }
}
