<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeNullableTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->bigInteger('preorder')->nullable()->change();
            $table->date('spk_date')->nullable()->change();
            $table->date('rab_date')->nullable()->change();
            $table->string('status')->nullable()->change();
            $table->date('delivery_packet_date')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->bigInteger('preorder')->change();
            $table->date('spk_date')->change();
            $table->date('rab_date')->change();
            $table->string('status')->change();
            $table->date('delivery_packet_date')->change();
        });
    }
}
