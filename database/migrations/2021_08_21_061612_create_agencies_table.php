<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agencies', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('nameapp');
            $table->string('address');
            $table->string('phone');
            $table->string('npwp');
            $table->string('person_responsible');
            $table->string('departement_responsible');
            $table->string('organizer_app');
            $table->string('organizer_phone');
            $table->boolean('registration');
            $table->text('description');
            $table->string('logo');
            $table->string('statement_letter_work')->nullable();
            $table->unsignedBigInteger('book_id')->nullable();
            $table->foreign('book_id')
                ->references('id')->on('books')
                ->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agencies');
    }
}
