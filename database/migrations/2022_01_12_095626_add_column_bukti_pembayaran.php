<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnBuktiPembayaran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('temp_distribute_plans', function (Blueprint $table) {
            // $table->dropColumn('endpoint');
            $table->string('bukti_pembayaran')->nullable()->after('status');
            $table->integer('agensi_id')->nullable()->after('bukti_pembayaran');
            $table->integer('id_asal_agensi')->nullable()->after('agensi_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('temp_distribute_plans', function (Blueprint $table) {
            $table->dropColumn('bukti_pembayaran');
            $table->dropColumn('agensi_id');
            $table->dropColumn('id_asal_agensi');
        });
    }
}
