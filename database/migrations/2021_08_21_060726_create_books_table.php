<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->id();
            $table->string('cover');
            $table->string('title');
            $table->string('isbn');
            $table->integer('page');
            $table->year('publication_year');
            $table->string('language')->nullable();
            $table->text('description')->nullable();
            $table->integer('category_id');
            $table->float('rate')->nullable();
            $table->string('pdf')->nullable();
            $table->integer('qty')->nullable();
            $table->string('price')->nullable();
            $table->string('slug')->nullable();
            $table->integer('author_id')->nullable();
            $table->integer('publisher_id')->nullable();
            $table->boolean('flag_packet')->default(0)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
