<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class EndpointNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('agencies', function (Blueprint $table) {
            $table->dropColumn('endpoint');
            // 
        });
        Schema::table('agencies', function (Blueprint $table) {
            $table->string('endpoint')->nullable()->after('book_id');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('agencies', function (Blueprint $table) {
            // $table->dropColumn('endpoint');
            $table->dropColumn('endpoint');
        });
        Schema::table('agencies', function (Blueprint $table) {
            // $table->dropColumn('endpoint');
            $table->string('endpoint')->nullable()->after('book_id');
        });
    }
}
