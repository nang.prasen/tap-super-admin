<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EnchanmentBookChapter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('book_chapters', function (Blueprint $table) {
            $table->dropColumn('percent');
            $table->integer('no_chapters')->nullable()->after('duration');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('book_chapters', function (Blueprint $table) {
            $table->integer('percent')->nullable();
            $table->dropColumn('no_chapters');
        });
    }
}
