<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnMitradllToAgencies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('agencies', function (Blueprint $table) {
            $table->date('registration_date')->after('registration')->nullable();
            $table->date('delivery_app_date')->after('statement_letter_work')->nullable();
            $table->string('domain')->after('nameapp')->nullable();
            $table->string('status_extension_app')->after('status')->nullable();
            $table->string('link_app')->after('domain')->nullable();
            $table->string('version_app')->after('link_app')->nullable();
            $table->string('status_progress_app')->after('status_extension_app')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('agencies', function (Blueprint $table) {
            $table->dropColumn('registration_date');
            $table->dropColumn('delivery_app_date');
            $table->dropColumn('domain');
            $table->dropColumn('status_extension_app');
            $table->dropColumn('link_app');
            $table->dropColumn('version_app');
            $table->dropColumn('status_progress_app');
        });
    }
}
