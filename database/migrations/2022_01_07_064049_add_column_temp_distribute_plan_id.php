<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnTempDistributePlanId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('temp_distributes', function (Blueprint $table) {
            $table->integer('temp_distribute_plans_id')->after('flag_bagi')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('temp_distributes', function (Blueprint $table) {
            // 1 untuk pdf
            $table->dropColumn('temp_distribute_plans_id');
        });
    }
}
