<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BudgetPlanDetail extends Model
{
    use HasFactory,SoftDeletes;
    protected $table = 'budget_plan_details';
    protected $fillable = [
        'budget_plan_id',
        'book_id',
    ];

    public function book()
    {
        return $this->belongsTo(Book::class,'book_id');
    }
    public function budget()
    {
        return $this->belongsTo(BudgetPlan::class,'budget_plan_id');
    }


}
