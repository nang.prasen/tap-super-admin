<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Agencies extends Model
{
    use HasFactory,SoftDeletes;

    protected $table = 'agencies';
    protected $fillable = [
            'name',
            'nameapp',
            'email',
            'address',
            'province_id',
            'city_id',
            'district_id',
            'village_id',
            'phone',
            'npwp',
            'person_responsible',
            'departement_responsible',
            'organizer_app',
            'organizer_phone',
            'registration',
            'description',
            'logo',
            'students',
            'endpoint',
            'degree',
            'count_user',
            // 'statement_letter_work',

    ];

    protected $hidden = [];

    // public function bookagency()
    // {
    //     return $this->belongsToMany(Book::class);
    // }

    public function bookagency()
    {
        return $this->hasMany(BookAgency::class);
    }

    public function agency()
    {
        return $this->hasMany(TempDistribute::class,'distribute_target_id');
    }

    public function agentmember()
    {
        return $this->hasMany(AgentMember::class,'id','agent_member_id');
    }

    public function transaction()
    {
        return $this->hasMany(Transaction::class,'agency_id');
    }
    
}
