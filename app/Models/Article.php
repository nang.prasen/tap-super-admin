<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Article extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = 'articles';
    protected $fillable = [
        'title',
        'content',
        'category',
        'thumbnail',
        'status',
        'user_id'

    ];

    protected $hidden = [];

    public function getCreatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['created_at'])
            ->format('d, M Y H:i');
    }

    public function comment()
    {
        return $this->hasMany(ArticleComment::class, 'article_id');
    }
}
