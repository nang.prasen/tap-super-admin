<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Publisher extends Model
{
    use HasFactory,SoftDeletes;
    protected $table = 'publishers';
    protected $fillable = [
        'name',
        'logo',
        'address'
    ];

    protected $hidden = [];

    public function book()
    {
        return $this->hasMany(Book::class, 'publisher_id', 'id');
    }

    public function agencybook()
    {
        return $this->hasMany(BookAgency::class);
    }
}
