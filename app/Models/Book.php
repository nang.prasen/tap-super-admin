<?php

namespace App\Models;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
class Book extends Model
{
    use HasFactory, SoftDeletes;
    public const BASE = '';

    protected $table = 'books';
    protected $fillable = [
        'title',
        'isbn',
        'description',
        'language',
        'rate',
        'pdf',
        'cover',
        'publication_year',
        // 'qty',
        'page',
        'slug',
        'writer',
        // 'author_id',
        'price',
        'publisher_id',
        'category_id',
        'flag_packet',
        'sub_category_id',
        'type',
        'duration',
        'chapter'
    ];

    public static $url_cover = "https://video-tap.sgp1.digitaloceanspaces.com/book/cover/";

    public function publisher()
    {
        return $this->belongsTo(Publisher::class,'publisher_id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class)->select('id','name');
    }

    public function author()
    {
        return $this->belongsTo(Author::class);
    }

    // public function bookagency()
    // {
    //     return $this->belongsToMany(Agencies::class);
    // }

    public function bookagency()
    {
        return $this->hasMany(BookAgency::class);
    }

    public function getDescriptionLimitAttribute()
    {
        return Str::words($this->description, '125');
    }
   

    static function addVideoBook($request,$penerbit){
        // dd($request);
        // dd($request->file('cover'));

        $video = new Book();
        $video->title = $request->title;
        $video->isbn = $request->isbn;
        $video->description = $request->description;
        $video->rate = $request->rate;
        $video->pdf = $request->video;
        $video->cover = $request->cover;
        $video->publication_year = $request->publication_year;
        $video->duration = $request->duration;
        $video->slug = Str::slug($request->title);
        $video->writer = $request->writer;
        $video->publisher_id = $penerbit->id;
        $video->price = $request->price;
        $video->category_id = $request->category_id;
        $video->flag_packet = 0;
        $video->type = 2; //video
        $video->chapter = $request->chapter; //video chapter
        $video->sub_category_id=$request->sub_category_id;
        
         if($request->file('cover')){
            $file = $request->file('cover');
            $name = $file->getClientOriginalName();
            // $extension = $request->file('cover')->extension();
            // $fullname=$name.'.'.$extension;
            $fullname = self::BASE. $name;
            $dd = $request->file('cover')->storeAs('video/cover', $fullname, 'digitalocean', 'public');
            Storage::disk('digitalocean')->setVisibility('video/cover/' . $fullname, 'public');
            $disk = Storage::disk('digitalocean')->url($dd);
            $video->cover=$disk;
        }

           if($request->file('video')){
            $file = $request->file('video');
            $name = $file->getClientOriginalName();
            // $extension = $request->file('cover')->extension();
            // $fullname=$name.'.'.$extension;
            $fullname = self::BASE. $name;
            $dd = $request->file('video')->storeAs('video/mp4', $fullname, 'digitalocean', 'public');
           
            Storage::disk('digitalocean')->setVisibility('video/mp4/' . $fullname, 'public');
            $disk = Storage::disk('digitalocean')->url($dd);
    
            $video->pdf=$disk;
            }


        $video->save();
        return $video;
    }


}
