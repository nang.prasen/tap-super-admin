<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TempDistribute extends Model
{
    use HasFactory;
    public function book()
    {
        return $this->belongsTo(Book::class,'book_id');
    }
    public function agency()
    {
        return $this->belongsTo(Agencies::class,'distribute_target_id','id');
    }
  
}
