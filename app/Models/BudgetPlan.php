<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BudgetPlan extends Model
{
    use HasFactory,SoftDeletes;
    protected $table = 'budget_plans';
    protected $fillable = [
        'name',
        'user_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
    public function detail()
    {
        return $this->hasMany(BudgetPlanDetail::class);
    }

    
}
