<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class AgentMember extends Model
{
    use HasFactory,SoftDeletes;
    protected $table = 'agent_members';
    protected $fillable = [
            'name',
            'address',
            'agent_id',

    ];

    public function agent()
    {
        return $this->belongsTo(Agent::class,'agent_id');
    }
    public function agency()
    {
        return $this->belongsTo(Agencies::class,'agent_member_id','id');
    }

    public function getId()
    {
        return $getId = DB::table('agent_members')->orderBy('id','desc')->take(1)->get();
    }

}
