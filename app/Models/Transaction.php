<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use HasFactory,SoftDeletes;
    protected $table = 'transactions';
    protected $fillable = [
            'preorder',
            'agency_id',
            'spk_date',
            'rab_date',
            'status',
            'delivery_packet_date',
    ];

    public function agency()
    {
        return $this->belongsTo(Agencies::class);
    }

    public function getSpkDateAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['spk_date'])->translatedFormat('l, d F Y');
    }
    public function getRabDateAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['rab_date'])->translatedFormat('l, d F Y');
    }
    public function getDeliveryPacketDateAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['delivery_packet_date'])->translatedFormat('l, d F Y');
    }

}
