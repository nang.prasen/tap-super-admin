<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BookAgency extends Model
{
    use HasFactory,SoftDeletes;
    protected $table = 'agencies_book';
    protected $fillable = [
        'book_id',
        'agencies_id',
        'qty',
        'publisher_id',
    ];
    protected $casts = [
        'qty' => 'array',
    ];

    public function book()
    {
        return $this->belongsTo(Book::class);
    }

    public function agencies()
    {
        return $this->belongsTo(Agencies::class);
    }
    

    public function publisher()
    {
        return $this->belongsTo(Publisher::class);
    }
}
