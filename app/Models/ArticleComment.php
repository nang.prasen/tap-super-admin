<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ArticleComment extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'article_coments';

    protected $guarded = [];

    public function article()
    {
        return $this->belongsTo(Article::class,'article_id');
    }
}
