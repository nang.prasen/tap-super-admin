<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CountBookPopular extends Model
{
    use HasFactory;
    protected $fillable = [
        'isbn',
        'flag',
        'total',
        'book_id',
        'title',
        'endpoint'
    ];
}
