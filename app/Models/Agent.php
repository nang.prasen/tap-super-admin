<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Agent extends Model
{
    use HasFactory,SoftDeletes;
    protected $table = 'agents';
    protected $fillable = [
            'name',
            'agentId',
            'address'
    ];

    public function member()
    {
        return $this->hasMany(AgentMember::class,'agent_id','id');
    }

    public function agency()
    {
        return $this->belongsTo(Agencies::class,'agent_id');
    }
}
