<?php

namespace App\Exports;

use App\Models\BudgetPlan;
use App\Models\BudgetPlanDetail;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class RabExport implements FromCollection, WithHeadings, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $id;

    function __construct($id) {
        $this->id = $id;
    }

    public function headings(): array
    {
        return [
            "title" ,
            "isbn" ,
            "page" ,
            "publication_year" ,
            "language" ,
            "description" ,
            "price" ,
            "slug" ,
        ];
        // return $this->columns;
    }

    public function getCsvSettings(): array
    {
        return [
            'delimiter' => ';'
        ];
    }

    public function rab()
    {
        $rab = BudgetPlanDetail::with('book')->where('budget_plan_id',$this->id)->get();
        // $rab = BudgetPlan::with('detail.book')->get();
        return $rab;
    }

    public function map($rab):array
    {
        return [
            $rab->book->title,
            $rab->book->isbn,
            $rab->book->page,
            $rab->book->publication_year,
            $rab->book->language,
            $rab->book->description,
            $rab->book->price,
        ];
    }

    public function collection()
    {
        return $this->rab();
    }
}
