<?php

namespace App\Exports;

use App\Models\Book;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Maatwebsite\Excel\Concerns\WithHeadings;

class BookExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $id;

    function __construct($id) {
        $this->id = $id;
    }

    public function getCsvSettings(): array
    {
        return [
            'delimiter' => ';'
        ];
    }

    public function headings(): array
    {
        return [
            "id" ,
            "cover" ,
            "title" ,
            "isbn" ,
            "page" ,
            "publication_year" ,
            "language" ,
            "description" ,
            "category_id" ,
            "rate" ,
            "pdf" ,
            "qty" ,
            "price" ,
            "slug" ,
            "author" ,
            "author_id" ,
            "publisher_id",
            "flag_packet",
            "deleted_at",
            "created_at",
            "updated_at",
        ];
        // return $this->columns;
    }

    public function collection()
    {
        return Book::where('publisher_id',$this->id)->get();
    }
}
