<?php
namespace App\Traits;
 
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
 
trait ImageUpload
{
    public function UserImageUpload($query) // Taking input image as parameter
    {
        $image_name = Str::random(20);
        $ext = strtolower($query->getClientOriginalExtension()); // You can use also getClientOriginalName()
        $image_full_name = $image_name.'.'.$ext;
        $upload_path = Storage::disk('pdf');    //Creating Sub directory in Public folder to put image
        $image_url = $upload_path.$image_full_name;
        $success = $query->move(public_path($upload_path,$image_full_name) );
 
        return $success; // Just return image
    }
    public function PdfUpload($pdf) // Taking input image as parameter
    {
        $image_name = Str::random(20);
        $ext = strtolower($pdf->getClientOriginalExtension()); // You can use also getClientOriginalName()
        $image_full_name = $image_name.'.'.$ext;
        $upload_path = Storage::disk('pdf'); 
        // $upload_path = Storage::disk('pdf')->assertExists($image_full_name); 
           //Creating Sub directory in Public folder to put image
        $pdf_url = $upload_path.$image_full_name;
        $success_pdf = $pdf->move(public_path($upload_path,$image_full_name) );
 
        return $success_pdf; // Just return image
    }
}

?>