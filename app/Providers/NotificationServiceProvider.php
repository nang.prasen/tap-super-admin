<?php

namespace App\Providers;

use App\Models\Notification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;


class NotificationServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public $menuItems;

    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        
        // if(Auth::check()){
        view()->composer('components.navbar', function($view)
        {
            $notif=Notification::where('target_id',Auth::user()->id)->latest()->limit(3)->get();
        //    dd($notif);
            $view->with('notification', $notif);
        });
        // }
       


       
        
    }
}
