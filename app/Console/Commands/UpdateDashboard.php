<?php

namespace App\Console\Commands;

use App\Models\Agencies;
use App\Models\CountBookPopular;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class UpdateDashboard extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dashboard:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update dashboard popular dan paling sering dibaca';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $agen = Agencies::select('id','name','endpoint')
                            ->where('endpoint',"!=",null)
                            ->get();
       
        $this->getPopular($agen);
        $this->getRead($agen);
        $this->info('Selesai melakukan update dashboard.');
        return 0;
    }
    private function getPopular($agen)
    {
        foreach ($agen as $a) {
            // untuk popular
            $url = $a->endpoint."/book-popular";
            $response = Http::get($url);
            if($response->status()==200){
                // dd($response->json());
                if(isset($response->json()['data'])){
                    $resp= $response->json()['data'];
                    // cek apakah ada atau tidak dengan endpoint
                    $cbp = CountBookPopular::where('endpoint',$a->endpoint)
                                            ->where('flag','popular')
                                            ->first();
                    if($cbp){
                        // jika ada
                        $cbp->isbn=$resp['isbn'];
                        $cbp->total=$resp['total_borrow'];
                        $cbp->book_id=$resp['id'];
                        $cbp->title=$resp['title'];
                        $cbp->save();
                        
                    }else{
                        // jika tidak ada
                        $cbp = CountBookPopular::create([
                            'isbn'=>$resp['isbn'],
                            'flag'=>'popular',
                            'total'=>$resp['total_borrow'],
                            'book_id'=>$resp['id'],
                            'title'=>$resp['title'],
                            'endpoint'=>$a->endpoint
                        ]);
                    }

                    // array_push($temp, $tempArr);
                }   
            }
            
        }
    }
    private function getRead($agen)
    {
        foreach ($agen as $a) {
            // untuk popular
            $url = $a->endpoint."/book-read";
            $response = Http::get($url);
            if($response->status()==200){
                // dd($response->json());
                if(isset($response->json()['data'])){
                    $resp= $response->json()['data'];
                    // cek apakah ada atau tidak dengan endpoint
                    $cbp = CountBookPopular::where('endpoint',$a->endpoint)
                                            ->where('flag','read')
                                            ->first();
                    if($cbp){
                        // jika ada
                        $cbp->isbn=$resp['isbn'];
                        $cbp->total=$resp['total_read'];
                        $cbp->book_id=$resp['id'];
                        $cbp->title=$resp['title'];
                        $cbp->save();
                        
                    }else{
                        // jika tidak ada
                        $cbp = CountBookPopular::create([
                            'isbn'=>$resp['isbn'],
                            'flag'=>'read',
                            'total'=>$resp['total_read'],
                            'book_id'=>$resp['id'],
                            'title'=>$resp['title'],
                            'endpoint'=>$a->endpoint
                        ]);
                    }

                    // array_push($temp, $tempArr);
                }   
            }
            
        }
    }
}
