<?php

namespace App\Console\Commands;

use App\Models\Agencies;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class UpdateCountAgencies extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'agencies:countuser';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Count User From Agencies';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $agen = Agencies::select('id','name','endpoint')
                        ->where('endpoint',"!=",null)
                        ->get();
        foreach ($agen as $a) {
            // untuk popular
            $url = $a->endpoint."/count-user";
            $response = Http::get($url);
            if ($response->status()==200) {
                $count= $response->json()['data'];
                $a->count_user=$count;
                $a->save();
            }
        }
        $this->info('Successfully update user agencies.');

        return 0;
    }
}
