<?php

namespace App\Mail;

use App\Models\Agencies;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Crypt;

class EmailRegister extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    //  public $agencies;
     public $user;
    public function __construct(User $user)
    {
        //
        // $this->agencies = $agencies;
        $this->user = $user;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // $logo = Storage::path($this->agencies->logo);
        $pass = $this->user->password;
        // $password = 'pass1234';

        $encryptedPassword = encrypt($pass);
        $decryptedPassword = decrypt($encryptedPassword);


        return $this->from('mail@gmail.com')
                    ->markdown('home.email-register')
                    ->with([
                        // 'name'          => $this->agencies->name,
                        // 'nameapp'          => $this->agencies->nameapp,
                        // 'address'          => $this->agencies->address,
                        // 'phone'          => $this->agencies->phone,
                        // 'status'          => $this->agencies->status,
                        // 'students'          => $this->agencies->students,
                        'name'          => $this->user->name,
                        'email'          => $this->user->email,
                        'password'          => $pass == $decryptedPassword,
                    ]);
                    // ->attach($logo);
    }
}
