<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterInstansiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:5',
            'email' => 'required|string|unique:agencies,email',
            'nameapp' => 'required|string|min:5',
            'domain' => 'required|string|min:3',
            'address' => 'required|string|min:10',
            'phone' => 'required|string|min:10|max:20',
            'npwp' => 'required|string|min:15',
            'person_responsible' => 'required|string|min:5',
            'departement_responsible' => 'required|string|min:5',
            'organizer_app' => 'required|string|min:5',
            'organizer_phone' => 'required|string|min:5',
            'registration' => 'required|boolean|min:1',
            'students' => 'required|min:1',
            // 'description' => 'required|string|min:50',
            // 'statement_letter_work' => 'required|mimes:pdf|max:4096',
            'logo'        =>  'required|image|mimes:jpeg,png,jpg|max:2048',
            'province_id' =>  'required',
            'city_id'     =>  'required',
            'district_id' =>  'required',
            'village_id'  =>  'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Nama Sekolah harus diisi',
            'email.required' => 'Email sekolah harus diisi',
            'email.unique' => 'Email sekolah sudah terdaftar',
            'nameapp.required' => 'Nama aplikasi perpustakaan sekolah harus diisi',
            'domain.required' => 'Alamat domain perpustakaan sekolah harus diisi',
            'domain.min:3' => 'Alamat domain minimal 3 karakter',
            'address.required' => 'Alamat sekolah harus diisi',
            'phone.required' => 'Nomer telephone sekolah harus diisi',
            'npwp.required' => 'NPWP sekolah harus diisi',
            'person_responsible.required' => 'Nama penanggungjawab sekolah harus diisi',
            'departement_responsible.required' => 'Jabatan penanggungjawab sekolah harus diisi',
            'organizer_app.required' => 'Nama pengelola aplikasi harus diisi',
            'organizer_phone.required' => 'Nomer telephone pengelola aplikasi harus diisi',
            'registration.required' => 'Harap memilih',
            'students.required' => 'Jumlah siswa di sekolah harus diisi',
            'logo.required' => 'Logo sekolah harus diisi/upload',
            'province_id.required' => 'Provinsi harus diisi',
            'city_id.required' => 'Kabupaten/kota harus diisi',
            'district_id.required' => 'Kecamatan harus diisi',
            'village_id.required' => 'Desa harus diisi',
            
        ];
    }
}
