<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookChapterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'             => 'required|string',
            'descirption'       => 'required|string',
            'chapter'           => 'required|mimes:mp4,mov,ogg,qt | max:2000000',
            'chapter_thumbnail' => 'required|mimes:jpeg,png,jpg | max:2048',
            'duration'          => 'required',
        ];
    }
    public function messages()
    {
        return [
            'title.required' => 'Judul harus diisi',
            'descirption.required' => 'Deskripsi harus diisi',
            'chapter.required' => 'Asset harus diisi/upload',
            'chapter.max:2000000' => 'Ukuran maksimal 200MB',
            'chapter_thumbnail.required' => 'Cover Asset harus diisi/upload',
            'chapter_thumbnail.2048' => 'Cover Asset maksimal 2MB',
            'duration.required' => 'Durasi harus diisi',
        ];
    }
}
