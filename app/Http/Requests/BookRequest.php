<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'           => 'required|string',
            'isbn'            => 'required|string|unique:books,isbn',
            'description'     => 'required|string',
            'pdf'             => 'required|mimes:pdf,epub | max:2000000',
            'cover'           => 'required|mimes:jpeg,png,jpg | max:2048',
            'publication_year' => 'required',
            'page'            => 'required|numeric',
            'writer'          => 'required',
            'price'          => 'required',
            'category_id'     => 'required',
            'sub_category_id'     => 'required',
            'flag_packet'     => 'required',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Judul harus diisi',
            'isbn.required' => 'ISBN harus diisi',
            'isbn.unique' => 'ISBN sudah terdaftar',
            'description.required' => 'Deskripsi harus diisi',
            'pdf.required' => 'Asset harus diisi/upload',
            'pdf.max:2000000' => 'Ukuran maksimal 200MB',
            'cover.required' => 'Cover Asset harus diisi/upload',
            'cover.2048' => 'Cover Asset maksimal 2MB',
            'publication_year.required' => 'Tahun terbit harus diisi',
            'page.required' => 'Halaman harus diisi',
            'writer.required' => 'Pengarang harus diisi',
            'price.required' => 'Harga harus diisi',
            'category_id.required' => 'Kategori harus diisi',
            'sub_category_id.required' => 'Sub kategori harus diisi',
            'flag_packet.required' => 'Ini buku paket apa bukan? harus diisi',
        ];
    }
}
