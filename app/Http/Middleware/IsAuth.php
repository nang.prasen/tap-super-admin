<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class IsAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $auth = Auth::user();
        if(empty($auth)){
            return $next($request);
        }else{
            switch ($auth->role) {
                case 'SuperAdmin':
                        return redirect()->route('home.admin');
                    break;
                case 'Penerbit':
                    return redirect()->route('home.penerbit');
                    break;
                default:
                        return '/login'; 
                    break;
            }
        }
    }
}
