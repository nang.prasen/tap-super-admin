<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = RouteServiceProvider::HOME;
    protected $idcard;

    public function redirectTo(){
        $role = Auth::user()->role; 
        switch ($role) {
            case 'SuperAdmin':
                    return '/home/admin';
                break;
            case 'Penerbit':
                return '/home/penerbit';
                break;
            case 'Mitra':
                return '/home/mitra';
                break;
            case 'Perwakilan':
                return '/home/perwakilan';
                break;
            case 'Sekolah':
                return '/home/sekolah';
                break;
            case 'Manajemen':
                return '/home/manajemen';
                break;
            default:
                    return '/login'; 
                break;
        }
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->idcard = $this->findIdCard();
    }
    public function findIdCard()
    {
        $login = request()->input('login');
        // dd($login);

        $fieldType = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'idcard';
 
        request()->merge([$fieldType => $login]);
 
        return $fieldType;
    }
 
    /**
     * Get username property.
     *
     * @return string
     */
    public function idCard()
    {
        return $this->idcard;
    }
    public function login(Request $request)
    {
        $this->validate($request, [
            'login'    => 'required',
            'password' => 'required',
        ]);

        $login_type = filter_var($request->input('login'), FILTER_VALIDATE_EMAIL ) 
            ? 'email' 
            : 'idcard';

        $request->merge([
            $login_type => $request->input('login')
        ]);

        if (Auth::attempt($request->only($login_type, 'password'))) {
            return redirect()->intended($this->redirectPath());
        }

        return redirect()->back()
            ->withInput()
            ->withErrors([
                'login' => 'These credentials do not match our records.',
            ]);
        } 

   
}
