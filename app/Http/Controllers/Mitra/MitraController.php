<?php

namespace App\Http\Controllers\Mitra;

use App\Http\Controllers\Controller;
use App\Models\Agencies;
use App\Models\AgentMember;
use App\Models\Transaction;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MitraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mitra = AgentMember::where('agent_member_id',Auth::user()->idcard)->first();
        $rekap = Agencies::with(['agentmember','agentmember.agent'])->where('agent_member_id',$mitra->id)->get();
        return view('mitra.rekap.index',compact('rekap'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $detailtransaksi = Agencies::with('transaction')->findOrFail($id);
        // dd($detailtransaksi);
        return view('mitra.rekap.detail',compact('detailtransaksi'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function transaction()
    {
        $mitra = AgentMember::where('agent_member_id',Auth::user()->idcard)->first();
        $distribution=DB::table('temp_distributes')
            ->join('agencies', 'temp_distributes.distribute_target_id', '=', 'agencies.id')
            ->join('temp_distribute_plans', 'temp_distributes.temp_distribute_plans_id', '=', 'temp_distribute_plans.id')
            ->select('temp_distributes.*', 'agencies.name','agencies.agent_member_id','temp_distribute_plans.kode','temp_distribute_plans.status',DB::raw('SUM(temp_distributes.qty) as total'))
            ->where('agencies.agent_member_id',$mitra->id)
            ->groupBy('temp_distributes.temp_distribute_plans_id')
            ->get();
            // dd($distribution);
            // dd($transaksi);
        return view('mitra.transaksi.data',compact('distribution'));
    }

    public function profile()
    {
        $user = Auth::user()->idcard;
        $datamitra = AgentMember::with('agent')->where('agent_member_id',$user)->first();
        return view('mitra.profile.index',compact('datamitra'));
    }
    public function profileEdit($id)
    {
        $mitra = AgentMember::findOrFail($id);
        return view('mitra.profile.edit',compact('mitra'));
    }

    public function profileUpdate(Request $request, $id)
    {
        $data = $request->all();
        // dd($data);
        $data = $request->except(['agent_member_id', 'agent_id']);
        try {
            $item = AgentMember::findOrFail($id);
            $item->update($data);
            return redirect()->route('profile.mitra')->with('status', true);
        } catch (Exception $e) {
            // return $e;
            return view('mitra.profile.edit')->with('status', 'Data tidak boleh kosong');
        }
        
    }
}
