<?php

namespace App\Http\Controllers\Mitra;

use App\Exports\RabExport;
use App\Http\Controllers\Controller;
use App\Models\Book;
use App\Models\BudgetPlan;
use App\Models\BudgetPlanDetail;
use App\Models\Publisher;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\Calculation\MathTrig\Sum;
use Yajra\DataTables\Facades\DataTables;


use function Aws\filter;

class RabController extends Controller
{
    public function index()
    {

        $rabmitra = BudgetPlan::with(['detail.book' => function($q){
            $q->sum('price');
        }])->where('user_id',Auth::user()->id)->get();

        // $querysum = BudgetPlan::selectRaw('budget_plans.*,SUM(books.price) as total_rab')
        //                         ->has('detail')
        //                         ->join('budget_plan_details','budget_plans.id','=','budget_plan_details.budget_plan_id')
        //                         ->join('books','budget_plan_details.book_id','=','books.id')
        //                         // ->where('user_id',Auth::user()->id);
        //                         ->where([
        //                             ['user_id',Auth::user()->id],
        //                             ['budget_plan_details.budget_plan_id','budget_plans.id']
        //                         ]);
        // $getquery = $querysum->get();

        // $rabmitra = DB::table('budget_plans')
        // dd($rabmitra);
        return view('mitra.rab.index',compact('rabmitra'));
    }

    public function detailrab($id)
    {
        $rabmitradetail = BudgetPlan::with('detail.book')->findOrFail($id);
        // dd($rabmitradetail);
        return view('mitra.rab.detail-rab',compact('rabmitradetail'));
    }

    public function getBooks(Request $request)
    {
        // $data = Book::with(['publisher'])->get();
        // return Datatables::of($data)->make(true);
        if ($request->ajax()) {
            $data = Book::with(['publisher'])->get();
            return DataTables::eloquent($data)
                                ->addIndexColumn()
                                ->filter(function($instance) use ($request){
                                    if ($request->has('publisher_id') && $request->filter_publisher!=null) {
                                        $instance->where('publisher_id',$request->filter_publisher);
                                    }
                                    if (!empty($request->get('search'))) {
                                        $instance->where(function($w)use($request){
                                            $search = $request->get('seacrh');
                                            $w->orWhere('title','like','%$search%')
                                                ->orWhere('isbn','like','%$search%')
                                                ->orWhere('price','like','%$search%');

                                        });
                                    }
                                })
                                // ->make(true);
                                ->toJson();
        }
    }

    public function create(Request $request)
    {
        
        // $publisher = Publisher::pluck('name', 'id');
        // return view('mitra.rab.create',compact('publisher'));
        $books = Book::with(['publisher'])->get();
        return view('mitra.rab.create',compact('books'));
    }

    public function insert(Request $request)
    {
        // dd($request->all());
        $name = $request->input('name');
        $budget_plan = new BudgetPlan();
        $budget_plan->name = $name;
        $budget_plan->user_id = Auth::user()->id;
        $budget_plan->save();


        $rab = [];
        $book_ids = $request->input('book_ids');
        foreach ($book_ids as $bookid) {
            array_push($rab,[
                'budget_plan_id' => $budget_plan->id,
                'book_id' => $bookid,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }

        // dd($rab);
        BudgetPlanDetail::insert($rab);
        return redirect()->route('home.mitra');
    }

    public function exportRab(Request $request)
    {
        return Excel::download(new RabExport($request->id), 'rab.xlsx');
    }

    public function cart()
    {
        return view('mitra.rab.cart');
    }
}
