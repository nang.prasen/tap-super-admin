<?php

namespace App\Http\Controllers\Perwakilan;

use App\Http\Controllers\Controller;
use App\Models\Agent;
use App\Models\AgentMember;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PerwakilanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $perwakilan = Agent::findOrFail($id);
        return view('perwakilan.profile.edit',compact('perwakilan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        // dd($data);
        $data = $request->except(['agentId']);
        try {
            $item = Agent::findOrFail($id);
            $item->update($data);
            return redirect()->route('perwakilan.profile')->with('status', true);
        } catch (Exception $e) {
            // return $e;
            return view('perwakilan.profile.data')->with('status', 'Data tidak boleh kosong');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function profile()
    {
        $user = Auth::user()->idcard;
        $perwakilan = Agent::where('agentId',$user)->first();
        // dd($perwakilan);
        return view('perwakilan.profile.data',compact('perwakilan'));
    }

    public function mitraPerwakilan()
    {
        $user = Auth::user()->idcard;
        // dd($user);
        $agent = Agent::with('member')->where('agentId',$user)->get();
        // dd($agent);
        return view('perwakilan.mitra.data',compact('agent'));
    }

    public function detailMitraPerwakilan($id)
    {
        
    }
}
