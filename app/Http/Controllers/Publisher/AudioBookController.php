<?php

namespace App\Http\Controllers\Publisher;

use App\Http\Controllers\Controller;
use App\Http\Requests\AudioBookRequest;
use App\Models\Book;
use App\Models\Category;
use App\Models\Publisher;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class AudioBookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::pluck('name', 'id');
        return view('publisher.asset.audiobook.create',compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    private function base()
    {
        $base_path = '';
    }
    public function store(AudioBookRequest $request)
    {
        // $user = User::where('publisherId',Auth::user()->publisherId)->first();
        $penerbit = Publisher::where('publisherId', Auth::user()->publisherId)->first();
        $book = new Book();
        $book->title = $request->title;
        $book->isbn = $request->isbn;
        $book->description = $request->description;
        $book->rate = $request->rate;
        $book->pdf = $request->pdf;
        $book->cover = $request->cover;
        $book->publication_year = $request->publication_year;
        $book->slug = Str::slug($request->title);
        $book->writer = $request->writer;
        $book->publisher_id = $penerbit->id;
        $book->price = $request->price;
        $book->category_id = $request->category_id;
        $book->sub_category_id=$request->sub_category_id;
        $book->type=3; //audiobook

        if ($request->file('cover')) {
            $name = $request->file('cover')->getClientOriginalName();
            $extension = $request->file('cover')->extension();
            // $fullname = $name.'.'.$extension;
            $fullname = $this->base() . $name;
            $dd = $request->file('cover')->storeAs('audiobook/cover', $fullname, 'digitalocean', 'public');
            Storage::disk('digitalocean')->setVisibility('audiobook/cover/' . $fullname, 'public');
            $disk = Storage::disk('digitalocean')->url($dd);
            // dd($disk);
            $book->cover = $disk;
        }
        if ($request->file('pdf')) {

            $name = $request->file('pdf')->getClientOriginalName();
            $extension = $request->file('pdf')->extension();
            // $fullname = $name.'.'.$extension;
            $fullname = $this->base() . $name ;
            $dd = $request->file('pdf')->storeAs('audiobook/audio',$fullname, 'digitalocean', 'public');
            Storage::disk('digitalocean')->setVisibility('audiobook/audio/'.$fullname, 'public');
            $disk = Storage::disk('digitalocean')->url($dd);
            // dd($disk);
            $book->pdf=$disk;
        }
        // dd($book);
        $book->save();
        return redirect()->route('book-penerbit.index')->with('status', 'Data berhasil disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
