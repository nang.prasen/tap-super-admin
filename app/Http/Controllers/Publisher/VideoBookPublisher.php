<?php

namespace App\Http\Controllers\Publisher;

use App\Http\Controllers\Controller;
use App\Http\Requests\BookChapterRequest;
use App\Http\Requests\VideoBookRequest;
use App\Models\Book;
use App\Models\BookChapter;
use App\Models\Category;
use App\Models\Publisher;
use App\Models\SubCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class VideoBookPublisher extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::pluck('name', 'id');
        return view('publisher.asset.videobook.create',compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    private function base()
    {
        $base_path = '';
    }
    public function store(Request $request)
    {
    //   dd($request->all());
        $penerbit = Publisher::where('publisherId', Auth::user()->publisherId)->first();
        if($request->chapter == 0){
           $book= Book::addVideoBook($request,$penerbit);
            return redirect()->route('book-penerbit.index')->with('status', "sukses add video store");
        }else{
           
            $book= Book::addVideoBook($request,$penerbit);
            $i=1;
            foreach($request->judulChapter as $key => $value){
                // dd();
                $chapter = new BookChapter();
                $chapter->book_id=$book->id;
                $chapter->title=$request->judulChapter[$key];
                $cover = $request->coverChapter[$key];
                $video = $request->videoChapter[$key];
                $chapter->descirption=$request->deskripsiChapter[$key];
                $chapter->duration=$request->durasiChapter[$key];
                $nameCover = $cover->getClientOriginalName();
                $fullname = $this->base(). $nameCover;
                $dd = $cover->storeAs('video/chapters/cover', $fullname, 'digitalocean', 'public');
                Storage::disk('digitalocean')->setVisibility('video/chapters/cover/' . $fullname, 'public');
                $disk = Storage::disk('digitalocean')->url($dd);
                $chapter->chapter_thumbnail=$disk;

                $nameVideo = $video->getClientOriginalName();
                $fullname = $this->base(). $nameVideo;
                $dd = $video->storeAs('video/chapters/video', $fullname, 'digitalocean', 'public');
                Storage::disk('digitalocean')->setVisibility('video/chapters/video/' . $fullname, 'public');
                $disk = Storage::disk('digitalocean')->url($dd);
                $chapter->chapter=$disk;
                $chapter->no_chapters=$i;
                
                $chapter->save();
                $i++;
                
            }
        }
        return redirect()->route('book-penerbit.index')->with('status', "sukses add video store");
        // dd($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getSubCategory(Request $request)
    {
        $subcategory = SubCategory::where('category_id',$request->category_id)->get();
        // return response()->json($agenmember);
        return json_encode($subcategory);
    }
    public function CreateSatuan()
    {
        // dd("Halo");
        $category = Category::pluck('name', 'id');
        return view('publisher.asset.videobook.createchapter',compact('category'));
    }
    public function PostSatuan(VideoBookRequest $request)
    {
        $penerbit = Publisher::where('publisherId', Auth::user()->publisherId)->first();
        $book = Book::addVideoBook($request,$penerbit);
        if($request->chapter == 0){
            return redirect()->route('home.penerbit')->with('status', 'video berhasil ditambahkan');
        }else{
            return redirect()->route('videobook-penerbit.chapter',$book->id)->with('status', 'video berhasil ditambahkan');
        }
    }
    public function CreateChapter($id)
    {
        $book = Book::find($id);
        $chapters = BookChapter::where('book_id',$id)->orderBy('no_chapters')->get();
        return view('publisher.asset.videobook.addchapter',compact('book','chapters'));
        // dd($book);
    }
    public function PostChapter(BookChapterRequest $request)
    {
        // dd($request->all());
        
        $bookchapter=BookChapter::where('book_id',$request->book_id)->count();
        $chapter = new BookChapter();
        $chapter->book_id=$request->book_id;
        $chapter->title=$request->judul;
        $chapter->chapter_thumbnail = $request->cover;
        $chapter->chapter= $request->video;
        $chapter->descirption=$request->deskripsi;
        $chapter->duration=$request->durasi;
        if ($request->file('cover')) {
            
            $nameCover = $request->file('cover')->getClientOriginalName();
            $fullname = $this->base(). $nameCover;
            $dd = $request->file('cover')->storeAs('video/chapters/cover', $fullname, 'digitalocean', 'public');
            Storage::disk('digitalocean')->setVisibility('video/chapters/cover/' . $fullname, 'public');
            $disk = Storage::disk('digitalocean')->url($dd);
            $chapter->chapter_thumbnail=$disk;
        }
        if ($request->file('video')) {
            $nameVideo = $request->file('video')->getClientOriginalName();
            $fullname = $this->base(). $nameVideo;
            $dd = $request->file('video')->storeAs('video/chapters/video', $fullname, 'digitalocean', 'public');
            Storage::disk('digitalocean')->setVisibility('video/chapters/video/' . $fullname, 'public');
            $disk = Storage::disk('digitalocean')->url($dd);
            $chapter->chapter=$disk;
        }
        dd($chapter);
        
        
        $chapter->no_chapters=$bookchapter+1;
        $chapter->save();
        return redirect()->route('videobook-penerbit.chapter',$request->book_id)->with('status', 'chapter berhasil ditambahkan');
    }

    public function PutChapter(Request $request)
    {
        $chapter = BookChapter::find($request->chapter_id);
        $chapter->title = $request->judul;
        $chapter->descirption=$request->deskripsi;
        $chapter->duration=$request->durasi;
        if($request->file('cover')){
                $file = $request->file('cover');
                $name = $file->getClientOriginalName();
                $fullname = $this->base() . $name;
                $dd = $request->file('cover')->storeAs('video/chapters/cover', $fullname, 'digitalocean', 'public');
                Storage::disk('digitalocean')->setVisibility('video/chapters/cover' . $fullname, 'public');
                $disk = Storage::disk('digitalocean')->url($dd);
                $chapter->chapter_thumbnail=$disk;

        }
        if($request->file('video')){
            $file = $request->file('video');
            $name = $file->getClientOriginalName();
            $fullname = $this->base() . $name;
            $dd = $request->file('video')->storeAs('video/chapters/video', $fullname, 'digitalocean', 'public');
            Storage::disk('digitalocean')->setVisibility('video/chapters/video' . $fullname, 'public');
            $disk = Storage::disk('digitalocean')->url($dd);
            $chapter->chapter=$disk;
        }
        $chapter->save();
        return redirect()->route('videobook-penerbit.chapter',$request->book_id)->with('status', 'chapter berhasil diupdate');

    }

    public function createNewSatuan()
    {
        $category = Category::pluck('name', 'id');
        return view('publisher.asset.video.new',compact('category'));
    }

    public function postNewSatuan(Request $request)
    {
        // dd($request->all());
        $penerbit = Publisher::where('publisherId', Auth::user()->publisherId)->first();
        $book = new Book();
        $book->title = $request->title;
        $book->isbn = $request->isbn;
        $book->description = $request->description;
        $book->pdf = $request->pdf;
        $book->cover = $request->cover;
        $book->publication_year = $request->publication_year;
        $book->slug = Str::slug($request->title);
        $book->writer = $request->writer;
        $book->publisher_id = $penerbit->id;
        $book->price = $request->price;
        $book->category_id = $request->category_id;
        $book->sub_category_id=$request->sub_category_id;
        $book->type=2; //video
        $book->chapter = $request->chapter; //video chapter

        if ($request->file('cover')) {
            $name = $request->file('cover')->getClientOriginalName();
            $fullname = $this->base() . $name;

            $dd = $request->file('cover')->storeAs('video/cover', $fullname, 'digitalocean', 'public');
            Storage::disk('digitalocean')->setVisibility('video/cover/' . $fullname, 'public');
            $disk = Storage::disk('digitalocean')->url($dd);
            // dd($disk);
            $book->cover = $disk;
        }
        if ($request->file('pdf')) {

            $name = $request->file('pdf')->getClientOriginalName();
            $fullname = $this->base() . $name ;
            $dd = $request->file('cover')->storeAs('video/mp4', $fullname, 'digitalocean', 'public');
            Storage::disk('digitalocean')->setVisibility('video/mp4/' . $fullname, 'public');
            $disk = Storage::disk('digitalocean')->url($dd);
            // dd($disk);
            $book->pdf=$disk;
            
        }
        // dd($book);
        $book->save();
        return redirect()->route('book-penerbit.index')->with('status', 'Data berhasil disimpan');
    }
}
