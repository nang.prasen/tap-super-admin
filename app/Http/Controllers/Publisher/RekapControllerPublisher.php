<?php

namespace App\Http\Controllers\Publisher;

use App\Http\Controllers\Controller;
use App\Models\BookAgency;
use App\Models\Publisher;
use App\Models\TempDistribute;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RekapControllerPublisher extends Controller
{
    public function index()
    {
        $penerbit = Publisher::where('publisherId',Auth::user()->publisherId)->first();
        // $rekap = BookAgency::with(['agencies','book'])->where('publisher_id',$penerbit->id)->get();
        $rekap = TempDistribute::with(['agency','book'])->where('publisher_id',$penerbit->id)->get();
        // dd($penerbit);
        // dd($rekap);
        return view('publisher.rekap.index',compact('rekap'));
    }
}
