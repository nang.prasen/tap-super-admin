<?php

namespace App\Http\Controllers\Publisher;

use App\Http\Controllers\Controller;
use App\Models\Book;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helpers\ImageUploader;
use App\Http\Requests\BookRequest;
use App\Models\BookAgency;
use App\Models\Publisher;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class BookControllerPublisher extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $penerbit = Publisher::where('publisherId', Auth::user()->publisherId)->first();
        $book = Book::with('category')->where('publisher_id', $penerbit->id)->get();
        // dd($book);
        return view('publisher.asset.index', compact('book'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::pluck('name', 'id');
        return view('publisher.asset.create', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    private function base()
    {
        $base_path = '';
    }
    public function store(BookRequest $request, ImageUploader $imageUploader)
    {
        
        // $user = User::where('publisherId',Auth::user()->publisherId)->first();
        $penerbit = Publisher::where('publisherId', Auth::user()->publisherId)->first();
        $book = new Book();
        $book->title = $request->title;
        $book->isbn = $request->isbn;
        $book->description = $request->description;
        $book->rate = $request->rate;
        $book->pdf = $request->pdf;
        $book->cover = $request->cover;
        $book->publication_year = $request->publication_year;
        $book->page = $request->page;
        $book->slug = Str::slug($request->title);
        $book->writer = $request->writer;
        $book->publisher_id = $penerbit->id;
        $book->price = $request->price;
        $book->category_id = $request->category_id;
        $book->flag_packet = $request->flag_packet;
        $book->sub_category_id=$request->sub_category_id;
        $book->type=1; //buku
        $book->chapter = 0; //video chapter

        if ($request->file('cover')) {
            $name = $request->file('cover')->getClientOriginalName();
            $extension = $request->file('cover')->extension();
            // $fullname = $name.'.'.$extension;
            $fullname = $this->base() . $name;
            $dd = $request->file('cover')->storeAs('book/cover', $fullname, 'digitalocean', 'public');
            Storage::disk('digitalocean')->setVisibility('book/cover/' . $fullname, 'public');
            $disk = Storage::disk('digitalocean')->url($dd);
            // dd($disk);
            $book->cover = $disk;
        }
        if ($request->file('pdf')) {

            $name = $request->file('pdf')->getClientOriginalName();
            $extension = $request->file('pdf')->extension();
            // $fullname = $name.'.'.$extension;
            $fullname = $this->base() . $name ;
            $dd = $request->file('pdf')->storeAs('book/pdf',$fullname, 'digitalocean', 'public');
            Storage::disk('digitalocean')->setVisibility('book/pdf/'.$fullname, 'public');
            $disk = Storage::disk('digitalocean')->url($dd);
            // dd($disk);
            $book->pdf=$disk;
        }
        // dd($book);
        $book->save();
        return redirect()->route('book-penerbit.index')->with('status', 'Data berhasil disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $link = Book::$url_cover;
        $book = Book::with('category')->findOrFail($id);
        // dd($book);
        return view('publisher.asset.detail', compact('book', 'link'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function bookRekap($id)
    {
        $book = Book::findOrFail($id);
        $rekap = BookAgency::with('agencies')
            ->where('book_id', $id)
            ->get();
        // dd($rekap);
        return view('publisher.asset.rekapitulasi', compact('book', 'rekap'));
    }
}
