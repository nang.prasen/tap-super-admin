<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Publisher;
use App\Helpers\ImageUploader;
use App\Models\Agencies;
use App\Models\Book;
use App\Models\BookAgency;
use App\Models\User;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class PublisherController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // $penerbit = Publisher::with('book')->get();
        $userPublisher = User::where('role','Penerbit')->get();
        
        $penerbit = DB::table('books')
                    ->rightjoin('publishers', 'publishers.id', '=', 'books.publisher_id')
                    ->select('publishers.*','books.publisher_id', DB::raw('count(*) as jumlah_buku'))
                    ->groupBy('publishers.id')
                    ->get();
        // dd($penerbit);
        return view('pages.publishers.index', compact('penerbit','userPublisher'));
    }
    public function indexManajemen()
    {
        $penerbit = Publisher::all();
        $userPublisher = User::where('role','Penerbit')->get();
        return view('manajemen.masterdata.publisher', compact('penerbit','userPublisher'));
    }

    public function create()
    {
        return view('pages.publishers.create');
    }

    public function store(Request $request, ImageUploader $imageUploader)
    {
        $this->validate($request, [
            'name' => 'required|string|min:5',
            'address' => 'required|string|min:10',
            'phone' => 'required|string|min:10|max:20',
            'publisherId' => 'required|string|min:4|unique:publishers,publisherId',
            'email' => 'required|string|min:5|unique:publishers,email',
            'logo'        =>  'required|image|mimes:jpeg,png,jpg|max:2048'
        ]);

        $data = new Publisher;
        $data->name = $request->name;
        $data->address = $request->address;
        $data->phone = $request->phone;
        $data->publisherId = $request->publisherId;
        $data->email = $request->email;
        $data->logo = $request->logo;
        if ($request->file('logo')) {
            $image_path = $request->file('logo')->path();
            $extension = $request->file('logo')->extension();
            $image = base64_encode(file_get_contents($image_path));
            $image_url = $imageUploader->upload($image, $extension);
            $data->logo = $image_url;
        }
        // dd($data);
        $data->save();
        return redirect()->route('publisher')->with('status', 'Data berhasil di upload');
        // return redirect()->back();
    }

    public function show($id)
    {
        $penerbit = Publisher::with('book')->findOrFail($id);
        // dd($penerbit);
        return view('pages.publishers.detail', compact('penerbit'));
    }

    public function rekapitulasi($id)
    {
        // $rekap = Book::with('author')
        //                 ->whereHas('bookagency', function($q) use ($id){
        //                 $q->where('id',$id);
        //             })->get();
        // $rekap = Book::with('bookagency','author')->findOrFail($id);
        // $rekap = BookAgency::with(['agencies','book','book.author'])
        //                     ->whereHas('book',function($q) use ($id){
        //                         $q->where('book_id',$id);
        //                     })->get();
        $rekap = Book::with('author')->findOrFail($id);
        $pitulasi = BookAgency::with('agencies')->where('book_id', $id)->get();
        // $agen = Agencies::where('id',$pitulasi->agencies_id)->get();
        // return $rekap;
        // return $pitulasi;
        // return response($agen);
        // dd($rekap,$pitulasi);
        return view('pages.publishers.recapitulation', compact('rekap', 'pitulasi'));
    }

    public function registerUser()
    {
        return view('pages.publishers.register-user');
    }

    public function actionRegisterUser(Request $request)
    {
        $this->validate($request, [
            'email'            => 'required|string|email|unique:users',
            'password'         => 'required|string|min:8|confirmed',
            'publisherId'      => 'required|string|unique:users',
        ]);


        $countPenerbit = Publisher::where('publisherId', $request->publisherId)->count();
        $idpenerbit = Publisher::where('publisherId', $request->publisherId)->get();
        foreach ($idpenerbit as $val) {
            $penerbit = Publisher::findOrFail($val->id);
        }
        if ($countPenerbit >= 1) {
            $user = new User();
            $user->name = $penerbit->name;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->role = 'Penerbit';
            $user->publisherId = $request->publisherId;
            // dd($user);
            $user->save();
            return redirect()->back()->status('Success', 'User Penerbit berhasil ditambahkan');
        }else {
            return redirect()->back()->with('Error', 'Maaf User ini tidak terdaftar sebagai Penerbit!');
        }
    }
}
