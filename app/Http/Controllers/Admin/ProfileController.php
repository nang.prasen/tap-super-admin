<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('pages.profiles.index');
    }

    public function create()
    {
        return view('pages.profiles.create');
    }

    public function show()
    {
        return view('pages.profiles.detail');
    }

    public function edit()
    {
        return view('pages.profiles.edit');
    }

}
