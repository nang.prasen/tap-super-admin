<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helpers\ImageUploader;
use Illuminate\Support\Facades\Storage;

class ArticleControlle extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $article = Article::all();
        return view('pages.article.index',compact('article'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.article.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    private function base()
    {
        $base_path = '';
    }
    public function store(Request $request ,ImageUploader $imageUploader)
    {
        // $this->validate($request, [
        //     'title' => 'required|string|min:5',
        //     'content' => 'required|string',
        //     'category' => 'required',
        //     'thumbnail' => 'required|image|mimes:jpeg,png,jpg|max:2048',
        //     'status' => 'required',
        //     'user_id' => 'required'
        // ]);

        // $data = new Article();
        // $data->title = $request->title;
        // $data->content = $request->content;
        // $data->category = $request->category;
        // $data->thumbnail = $request->thumbnail;
        // $data->status = $request->status;
        // $data->user_id = Auth::user()->id;
        // $data->save();
        $data = $request->all();
        $data['user_id'] = Auth::user()->id;
        if ($request->file('thumbnail')) {
            $name = $request->file('thumbnail')->getClientOriginalName();
            $extension = $request->file('thumbnail')->extension();
            // $fullname = $name.'.'.$extension;
            $fullname = $this->base() . $name;
            $dd = $request->file('thumbnail')->storeAs('image/artikel/image', $fullname, 'digitalocean', 'public');
            Storage::disk('digitalocean')->setVisibility('image/artikel/image/' . $fullname, 'public');
            $disk = Storage::disk('digitalocean')->url($dd);
            // dd($disk);
            $data['thumbnail'] = $disk;
        }
        // dd($data);
        Article::create($data);
        return redirect()->route('article.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $article = Article::findOrFail($id);
        return view('pages.article.show',compact('article'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article_edit = Article::findOrFail($id);
        return view('pages.article.edit',compact('article_edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        dd($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
