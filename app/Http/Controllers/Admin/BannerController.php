<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Banner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private function base()
    {
        $base_path = '';
    }
    public function index()
    {
        $banner1 = Banner::where('type','dashboard_instansi')->get();
        $banner2 = Banner::where('type','katalog_instansi')->get();
        $banner3 = Banner::where('type','user_instansi')->get();
        return view('pages.banner.index',compact('banner1','banner2','banner3'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = array(
            'name'           => 'required|string',
            'type'            => 'required|string',
            'image'           => 'required|mimes:jpeg,png,jpg | max:2048',
        );
        $error = Validator::make($request->all(), $validate);
        if ($error->fails()) {
            return response()->json([
                'error' => $error->errors()->all()
            ]);
        }
        $banner = new Banner();
        $banner->name=$request->name;
        $banner->type=$request->type;
        $banner->link = $request->link;
        
        if ($request->file('image')) {
            $name = $request->file('image')->getClientOriginalName();
            $extension = $request->file('image')->extension();
            // $fullname = $name.'.'.$extension;
            $fullname = $this->base() . $name;
            $dd = $request->file('image')->storeAs('image/banner', $fullname, 'digitalocean', 'public');
            Storage::disk('digitalocean')->setVisibility('image/banner/' . $fullname, 'public');
            $disk = Storage::disk('digitalocean')->url($dd);
            // dd($disk);
            $banner->image=$disk;
        }
        $banner->save();
        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $banner = Banner::findOrFail($id);
        $banner->delete();
        return redirect()->route('banner.index')->with('success','Berhasil di hapus');
    }
}
