<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Book;
use App\Models\Publisher;
use App\Models\Author;
use App\Helpers\ImageUploader;
use App\Models\Agencies;
use App\Models\BookAgency;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\BookExport;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.books.index');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $author = Author::pluck('name', 'id');
        $publisher = Publisher::pluck('name', 'id');
        // $category = Category::pluck('name', 'id');
        return view('pages.books.create', compact('author', 'publisher'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, ImageUploader $imageUploader)
    {
        $validate = array(
            'title'           => 'required|string',
            'isbn'            => 'required|string',
            'description'     => 'required|string',
            'rate'            => 'required|string',
            'pdf'             => 'required|mimes:pdf,epub | max:200000',
            'cover'           => 'required|mimes:jpeg,png,jpg | max:2048',
            'publication_year' => 'required',
            'qty'             => 'required|numeric',
            'page'            => 'required|numeric',
            'author_id'       => 'required',
            'publisher_id'    => 'required',
            'flag_packet'     => 'required',
        );
        $error = Validator::make($request->all(), $validate);
        if ($error->fails()) {
            return response()->json([
                'error' => $error->errors()->all()
            ]);
        }
        $book = new Book();
        $book->title = $request->title;
        $book->isbn = $request->isbn;
        $book->description = $request->description;
        $book->rate = $request->rate;
        $book->pdf = $request->pdf;
        $book->cover = $request->cover;
        $book->publication_year = $request->publication_year;
        $book->qty = $request->qty;
        $book->page = $request->page;
        $book->slug = Str::slug($request->title);
        $book->author_id = $request->author_id;
        $book->publisher_id = $request->publisher_id;
        $book->category_id = 1;
        $book->flag_packet = $request->flag_packet;

        if ($request->file('cover')) {
            $image_path = $request->file('cover')->path();
            $extension = $request->file('cover')->extension();
            $image = base64_encode(file_get_contents($image_path));
            $image_url = $imageUploader->upload($image, $extension);
            $book->cover = $image_url;
        }

        if ($request->file('pdf')) {
            $image_path = $request->file('pdf')->path();
            $extension = $request->file('pdf')->extension();
            $image = base64_encode(file_get_contents($image_path));
            $image_url = $imageUploader->uploadPdf($image, $extension);
            $book->pdf = $image_url;
        }
        // dd($book);
        $book->save();
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $book = Book::findOrFail($id);
        // dd($book);
        return view('pages.books.detail', compact('book'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function inventory()
    {
        return view('pages.books.inventory');
    }
    public function recapitulation()
    {
        return view('pages.books.recapitulation');
    }

    public function distribution()
    {
        $books = Book::with('publisher')->get();
        $instansi = Agencies::all();
        // dd($books);
        return view('pages.books.distribution', compact('books', 'instansi'));
    }

    public function insertDistribution(Request $request)
    {
        // dd($request->all());
        $checked_array = $request->book_id;
        foreach ($request->qty as $key => $value) {
            if (in_array($request->qty[$key], $checked_array)) {
                $distribution = new BookAgency();
                $distribution->book_id = $request->book_id[$key];
                $distribution->agencies_id = $request->agencies_id[$key];
                $distribution->publisher_id = implode(',', (array) $request->publisher_id[$key]);
                $distribution->qty = $request->qty[$key];
                $distribution->save();
            }
        }

        return response()->json(['success' => 'Data berhasil ditambahkan']);


        // $data = $request->all();

        // $data['book_id'] = $request->input('book_id');
        // $data['agencies_id'] = $request->input('agencies_id');
        // // $data['publisher_id'] = $request->input('publisher_id');
        // // dd($request->all());
        // // dd($data);

        // $distribution = [];

        // foreach($book_id as $book){
        //     foreach($agencies_id as $agency){
        //         foreach($publisher_id as $publisher){
        //             foreach($qty as $q){
        //                 array_push($distribution,[
        //                     'book_id' => $book,
        //                     'agencies_id' => $agency,
        //                     'publisher_id' => $publisher,
        //                     'qty' => $q,
        //                 ]);
        //             }
        //         }
        //     }
        // }

        // BookAgency::insert($distribution);
        return redirect()->route('book.distribution');
        // dd($request->all());
    }

    public function getDownloadImageBook($id)
    {
        $attachment = Book::find($id);

        $ch = curl_init();
        $source = $attachment->cover;;
        curl_setopt($ch, CURLOPT_URL, $source);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $data = curl_exec($ch);
        curl_close($ch);

        $destination = "/download/afile.zip";
        $file = fopen($destination, "w+");
        fputs($file, $data);
        fclose($file);


        // $attachment = Book::find($id);
        // $headers = [
        //     'Content-Type'        => 'application/jpeg',
        //     'Content-Disposition' => 'attachment; filename="' . $attachment->name . '"',
        // ];
        // return Response::make(Storage::disk('digitalocean')->get($attachment->cover), 200, $headers);
    }

    public function exportBookPublisher(Request $request)
    {
        return Excel::download(new BookExport($request->id), 'book.xlsx');
    }

    public function readBook($id)
    {
        $book = Book::findOrFail($id);
        return view('pages.books.read', compact('book'));
    }
}
