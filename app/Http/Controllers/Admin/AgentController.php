<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Agent;
use App\Models\AgentMember;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AgentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $agent = Agent::all();
        return view('pages.agent.index',compact('agent'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tgl = date('dmY');
        $cek = Agent::count();
        if ($cek == 0) {
            $urut = 100001;
            $kode = 'PWK'.$tgl.'-'.$urut;
        } else {
            $kolom = Agent::all()->last();
            $urut = (int) substr($kolom->agentId,-6)+1; 
            $kode = 'PWK'.$tgl.'-'.$urut;
        }
        return view('pages.agent.create',compact('kode'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $data = $request->all();
        Agent::create($data);

        $userpwk = new User();
        $userpwk->name = $request->name;
        $userpwk->email = $request->email;
        $userpwk->password = Hash::make($request->password);  
        $userpwk->role = 'Perwakilan';
        $userpwk->idcard = $request->agentId;
        $userpwk->save();

        return redirect()->route('agent.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $agentdetail = Agent::with('member')->findOrFail($id);
        $user = User::where('idcard',$agentdetail->agentId)->where('role','Perwakilan')->first();

        return view('pages.agent.detail',compact('agentdetail','user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $agentedit = Agent::findOrFail($id);
        return view('pages.agent.edit',compact('agentedit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Agent::findOrFail($id);
        $data->name = $request->name;
        $data->agentId = $request->agentId;
        $data->address = $request->address;
            
        $data->update();
        return redirect()->route('agent.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function createUserAgent($id)
    {
        $agent = Agent::findOrFail($id);
        return view('pages.agent.create-user',compact('agent'));
    }

    public function insertUserAgent(Request $request)
    {
        // dd($request->all());
        $userpwk = new User();
        $userpwk->name = $request->name;
        $userpwk->email = $request->email;
        $userpwk->password = Hash::make($request->password);  
        $userpwk->role = 'Perwakilan';
        $userpwk->idcard = $request->agentId;
        $userpwk->save();
        return redirect()->route('agent.index');
    }

    public function mitra($id)
    {
        $agent = Agent::findOrFail($id);
        $tgl = date('dmY');
        $cek = AgentMember::count();
        if ($cek == 0) {
            $urut = 100001;
            $kode = 'MITRA'.$tgl.'-'.$urut;
        } else {
            $kolom = AgentMember::all()->last();
            $urut = (int) substr($kolom->agent_member_id,-6)+1; 
            $kode = 'MITRA'.$tgl.'-'.$urut;
        }
        return view('pages.mitra.create',compact('agent','kode'));
    }

    public function insertmitra(Request $request)
    {
        // dd($request->all());

        $usermitra = new User();
        $usermitra->name = $request->name;
        $usermitra->email = $request->email;
        $usermitra->password = Hash::make($request->password);  
        $usermitra->role = 'Mitra';
        $usermitra->idcard = $request->agent_member_id;
        $usermitra->save();

        $new = new AgentMember();
        $new->name= $request->name;
        $new->agent_member_id= $request->agent_member_id;
        $new->address = $request->address;
        $new->agent_id = $request->agent_id;
        // $data = $request->all();
        // dd($new);
        $new->save();
        return redirect()->route('agent.index');
    }

    public function editMitra($id)
    {
        $agent = Agent::pluck('name','id');
        $editmitra = AgentMember::with('agent')->findOrFail($id);
        // dd($editmitra);
        return view('pages.agent.edit-mitra',compact('editmitra','agent'));
    }

    public function updatemitra(Request $request, $id)
    {
        $data = AgentMember::findOrFail($id);
        $data->name = $request->name;
        $data->agent_member_id = $request->agent_member_id;
        $data->address = $request->address;
        $data->agent_id = $request->agent_id;
            
        $data->update();
        return redirect()->route('agent.index');
    }
}
