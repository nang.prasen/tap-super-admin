<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Book;
use App\Models\BudgetPlan;
use App\Models\BudgetPlanDetail;
use App\Models\Publisher;
use App\Models\TempDistribute;
use Aws\Crypto\Cipher\CipherMethod;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class RabAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rabmitra = BudgetPlan::with(['detail.book' => function($q){
            $q->sum('price');
        }])->where('user_id',Auth::user()->id)->get();
        return view('pages.rab.data',compact('rabmitra'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create(Request $request)
    {
        // 
        // return view('pages.rab.create',compact('publisher'));
        return view('pages.rab.create-rab');
    }

    public function insertbudgetplan(Request $request)
    {
        $name = $request->input('name');
        $budget_plan = new BudgetPlan();
        $budget_plan->name = $name;
        $budget_plan->user_id = Auth::user()->id;
        $budget_plan->save();
        return redirect()->route('master-rab.index');
    }

    public function additem(Request $request, $id)
    {
        $budget_plan_item = BudgetPlan::findOrFail($id);
        if (request()->ajax()) {
                if (!empty($request->filter_publisher)) {
                    $data = DB::table('books')
                        ->join('publishers', 'publishers.id', '=', 'books.publisher_id')
                        ->select('books.id as bookId', 'books.title', 'books.isbn', 'books.qty', 'publishers.name','publishers.id as publisherId')
                        ->where('publisher_id', $request->filter_publisher)
                        ->get();
                    } else {
                        $data = DB::table('books')
                        ->join('publishers', 'publishers.id', '=', 'books.publisher_id')
                        ->select('books.id as bookId', 'books.title', 'books.isbn', 'books.qty', 'publishers.name','publishers.id as publisherId')
                        ->get();
                    }
                    // dd($data);
    
                return datatables()->of($data)->make(true);
            }
    
            $publisher = Publisher::pluck('name', 'id');
        return view('pages.rab.additem-rab',compact('publisher','budget_plan_item'));
    }

    public function listitem(Request $request)
    {
        // $budget = BudgetPlan::findOrFail($id);
        $result = DB::table('budget_plan_details')
                        ->select('budget_plan_details.*','books.title','books.isbn','publishers.name as penerbit')
                        ->join('books','books.id','budget_plan_details.book_id')
                        ->join('publishers','publishers.id','books.publisher_id')
                        ->where("budget_plan_details.budget_plan_id",$request->id)

                        ->get();
                        // dd($result);
        return response()->json([
            "status"=>"success",
            "data"=>$result
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $result =[];
        foreach($request->data as $d){
            $temp['book_id']=$d['book_api_id'];
            $temp['qty']=$d['value'];
            $temp['budget_plan_id']=$request->budgetplan_id;
            //   ambil publisher_id
            // $book= Book::find($d['book_api_id']);
            // $temp['publisher_id']=$book->publisher_id;
            $temp['created_at']=Carbon::now();
            $temp['updated_at']=Carbon::now();
          array_push($result,$temp);
        }
        // dd($result);
        // dd($result);
        $test = BudgetPlanDetail::insert($result);
        if($test==true){
            return response()->json([
                "status"=>"sukses menambahkan ke rab anda",
            ]);
        }else{
            return response()->json([
                "status"=>"error",
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $rabmitradetail = BudgetPlan::with('detail.book')->findOrFail($id);
        // dd($rabmitradetail);

        return view('pages.rab.detail',compact('rabmitradetail'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function listAddKeranjang()
    {
        $result = DB::table('budget_plan_details')
                        ->select('budget_plan_details.*','books.title','books.isbn','publishers.name as penerbit')
                        ->join('books','books.id','budget_plan_details.book_id')
                        ->join('publishers','publishers.id','books.publisher_id')

                        ->latest();
                        // dd($result);
        return response()->json([
            "status"=>"success",
            "data"=>$result
        ]);
    }

    public function addToTempDistribute(Request $request)
    {
        $result =[];
        foreach($request->data as $d){
            $temp['book_id']=$d['book_id'];
            $temp['qty']=$d['qty'];
            $temp['distribute_target_id']=$request->instansi;
            //   ambil publisher_id
            $book= Book::find($d['book_id']);
            $temp['publisher_id']=$book->publisher_id;
            $temp['created_at']=Carbon::now();
          array_push($result,$temp);
        }
        // dd($result);
        $test = TempDistribute::insert($result);
        if($test==true){
            return response()->json([
                "status"=>"simpan sukses",
            ]);
        }else{
            return response()->json([
                "status"=>"error",
            ]);
        }
    }

    public function edit($id)
    {
        $editrab = BudgetPlan::with('detail.book')->findOrFail($id);
        // dd($editrab);
        return view('pages.rab.edit',compact('editrab'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $item = BudgetPlan::findOrFail($id);
        $detail = BudgetPlanDetail::findOrFail($id);
        // dd($detail);
        $updatedetail = DB::table('budget_plan_details')
                            ->where('id',$detail->id)
                            ->update([
                                'qty' => $request->qty,
                                'updated_at' => Carbon::now()
                            ]);

                            // return redirect()->back()->with('status', 'Sukses mengubah data');
        if($updatedetail==true){
            return response()->json([
                "status"=>"sukses mengubah data",
            ]);
        }else{
            return response()->json([
                "status"=>"error",
            ]);
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
