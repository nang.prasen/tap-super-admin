<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Agencies;
use App\Models\Book;
use App\Models\BookAgency;
use App\Models\Publisher;
use App\Models\TempDistribute;
use App\Models\TempDistributePlan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Redirect;

// use DB;

class DistributionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $distribution = TempDistribute::with('agency')
        //                                 ->groupBy('distribute_target_id')
        //                                 ->selectRaw('*,sum(qty) as jumlah')
        //                                 ->get();
        $distribution=DB::table('temp_distributes')
            ->join('agencies', 'temp_distributes.distribute_target_id', '=', 'agencies.id')
            ->join('temp_distribute_plans', 'temp_distributes.temp_distribute_plans_id', '=', 'temp_distribute_plans.id')
            ->select('temp_distributes.*', 'agencies.name','temp_distribute_plans.kode','temp_distribute_plans.status','temp_distribute_plans.bukti_pembayaran',DB::raw('SUM(temp_distributes.qty) as total'))
            ->groupBy('temp_distributes.temp_distribute_plans_id')
            ->get();
        // dd($distribution);
        return view('pages.distribution.index',compact('distribution'));
    }
    public function banned($id)
    {
        $dis = TempDistribute::where('temp_distribute_plans_id',$id)
                                ->get();
        $a = TempDistribute::where('temp_distribute_plans_id',$id)
            ->first();
        $agen = Agencies::find($a->distribute_target_id);
        // dd($agen);
        if($agen==null){
            return Redirect::back()->withErrors(['msg' => 'Agensi tidak ditemukan']);
        }
        if($agen->endpoint==null){
            return Redirect::back()->withErrors(['msg' => 'Agensi tidak memiliki endpoint']);
        }
        foreach($dis as $d){
            // dd($dis);
            $book = Book::find($d->book_id);
            // dd($book);
            $url = $agen->endpoint."/book/status_po";
            // dd($url);
            $response = Http::post($url, [
                "isbn"=>$book->isbn,
                "slug"=>$book->slug,
                "status_po"=>0,
                "qty"=>$d->qty,
            ]);
            $hasil = $response->json();
            // dd($hasil);
            if($hasil["status"] != "success"){
                return Redirect::back()->withErrors(['msg' => $hasil["status"]." In book $book->title"]);
            }
        }
        // get book
         
        return Redirect::back()->with('success','Berhasil ganti status');
    }

    public function publish($id)
    {
        // $dis = TempDistribute::find($id);
        $dis = TempDistribute::where('temp_distribute_plans_id',$id)
                                ->get();
        $a = TempDistribute::where('temp_distribute_plans_id',$id)
            ->first();
        $agen = Agencies::find($a->distribute_target_id);
        if($agen==null){
            return Redirect::back()->withErrors(['msg' => 'Agensi tidak ditemukan']);
        }
        if($agen->endpoint==null){
            return Redirect::back()->withErrors(['msg' => 'Agensi tidak memiliki endpoint']);
        }
        foreach($dis as $d){
            $book = Book::find($d->book_id);
            // dd($book);
            $url = $agen->endpoint."/book/status_po";
            // dd($url);
            $response = Http::post($url, [
                "isbn"=>$book->isbn,
                "slug"=>$book->slug,
                "status_po"=>1,
                "qty"=>$d->qty,
            ]);
            $hasil = $response->json();
            if($hasil["status"] != "success"){
                return Redirect::back()->withErrors(['msg' => $hasil["status"]." In book $book->title"]);
            }
        }
        // get book
         
        return Redirect::back()->with('success','Berhasil ganti status');
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if (request()->ajax()) {
            if (!empty($request->filter_publisher)) {
                $data = DB::table('books')
                    ->join('publishers', 'publishers.id', '=', 'books.publisher_id')
                    ->select('books.id as bookId', 'books.title', 'books.isbn', 'books.qty', 'publishers.name','publishers.id as publisherId')
                    ->where('publisher_id', $request->filter_publisher)
                    ->get();
                } else {
                    $data = DB::table('books')
                    ->join('publishers', 'publishers.id', '=', 'books.publisher_id')
                    ->select('books.id as bookId', 'books.title', 'books.isbn', 'books.qty', 'publishers.name','publishers.id as publisherId')
                    ->get();
                }
                // dd($data);

            return datatables()->of($data)->make(true);
        }

        $publisher = Publisher::pluck('name', 'id');
        $agencies = Agencies::all();
        return view('pages.distribution.createajax', compact('publisher', 'agencies'));
    }

    public function createajax(Request $request)
    {
        if (request()->ajax()) {
            if (!empty($request->filter_publisher)) {
                $data = DB::table('books')
                    ->join('publishers', 'publishers.id', '=', 'books.publisher_id')
                    ->select('books.id as bookId', 'books.title', 'books.isbn', 'books.qty', 'publishers.name','publishers.id as publisherId')
                    ->where('publisher_id', $request->filter_publisher)
                    ->get();
                } else {
                    $data = DB::table('books')
                    ->join('publishers', 'publishers.id', '=', 'books.publisher_id')
                    ->select('books.id as bookId', 'books.title', 'books.isbn', 'books.qty', 'publishers.name','publishers.id as publisherId')
                    ->get();
                }
                // dd($data);

            return datatables()->of($data)->make(true);
        }

        $publisher = Publisher::pluck('name', 'id');
        $agencies = Agencies::all();
        return view('pages.distribution.createajax', compact('publisher', 'agencies'));
    }

    public function createajaxPost(Request $request)
    {
        // dd($request->all());
        $result =[];
        foreach($request->data as $d){
            $temp['book_id']=$d['book_api_id'];
            $temp['qty']=$d['value'];
            $temp['distribute_target_id']=$request->instansi;
            //   ambil publisher_id
            $book= Book::find($d['book_api_id']);
            $temp['publisher_id']=$book->publisher_id;
            $temp['created_at']=Carbon::now();
          array_push($result,$temp);
        }
        // dd($result);
        // dd($result);
        $test = TempDistribute::insert($result);
        if($test==true){
            return response()->json([
                "status"=>"success",
            ]);
        }else{
            return response()->json([
                "status"=>"error",
            ]);
        }
        
    }
    public function listDistribute(Request $request)
    {
        // $id=1;
        // if($request->has('id')){
        //     $id=$request->get('id');
        // }
                $result = DB::table('temp_distributes')
                        ->select('temp_distributes.*','books.title','books.isbn','publishers.name as penerbit')
                        ->join('books','books.id','temp_distributes.book_id')
                        ->join('publishers','publishers.id','books.publisher_id')
                        ->where("temp_distributes.distribute_target_id",$request->id)
                        ->where('temp_distributes.flag_bagi',0)

                        ->get();
                        // dd($result);
        return response()->json([
            "status"=>"success",
            "data"=>$result
        ]);
    }

    public function postDistribute(Request $request)
    {
     
        $distribute = TempDistribute::where('distribute_target_id',$request->id)->where('flag_bagi',0)->get();
        // dd(count($distribute));
        // cek apakah ada yg dibagikan
        if(count($distribute)==0){
            return response()->json([
                "status"=>"error data tidak ditemukan",
                "data"=>$distribute
            ],400);
        }
        // cek apakah endpoint sudah ada
        $agencies = Agencies::find($request->id);
        if($agencies->endpoint == null){
            return response()->json([
                "status"=>"instansi pengguna belum memiliki endpoint",
                "data"=>$distribute
            ],400);
        }

        $tgl = date('dmY');
        $cek = TempDistributePlan::count();
        if ($cek == 0) {
            $urut = 100001;
            $kode = 'INV'.$tgl.'-'.$urut;
        } else {
            $kolom = TempDistributePlan::all()->last();
            $urut = (int) substr($kolom->kode,-6)+1; 
            $kode = 'INV'.$tgl.'-'.$urut;
        }
        $tdp=new TempDistributePlan();
        $tdp->kode=$kode;
        $tdp->save();


        $url = $agencies->endpoint."/recive/book";
        // $temp=[];
        foreach($distribute as $d){
            // dd($d);
            // array_push($temp,$d->book_id);
            $book = Book::find($d->book_id);
            // dd($book);
            $response = Http::post($url, [
                'qty_distribute' => $d->qty,
                'cover'=>$book->cover,
                'title'=>$book->title,
                'isbn'=>$book->isbn,
                'page'=>$book->page,
                'publication_year'=>$book->publication_year,
                'language'=>$book->language,
                'description'=>$book->description,
                'category_id'=>$book->category_id,	
                'rate'=>$book->rate,	
                'pdf'=>$book->pdf,
                'price'=>$book->price,	
                'slug'=>$book->slug,
                'writer'=>$book->writer,
                'author_id'=>$book->author_id,
                'publisher_id'=>$book->publisher_id,
                'flag_packet'=>$book->flag_packet,
                'sub_category_id'=>$book->sub_category_id,
                'type'=>$book->type,
                'chapter'=>$book->chapter,
                'duration'=>$book->duration,
            ]);
            // dd($response);
            // dd($url);
            if($response->status()==200){
                $d->flag_bagi=1;
                $d->temp_distribute_plans_id=$tdp->id;
                $d->save();
                // dd($d);
            }   
        }
        

        return response()->json([
            "status"=>"buku selesai diblas",
        ],200);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $validate = array(
        //     'book_id'          => 'required|integer|exists:books,id',
        //     'agencies_id'      => 'required|integer|exists:agencies,id',
        //     'publisher_id'     => 'required|integer|exists:publishers,id',
        //     'qty'              => 'required|integer',
        // );
        // $error = Validator::make($request->all(), $validate);
        // if ($error->fails()) {
        //     return response()->json([
        //         'error' => $error->errors()->all()
        //     ]);
        // }
        // dd($request->all());
        $bookid = $request->bookid;
        $distribution = [];
        foreach ($bookid as $key => $bokid) {
            array_push($distribution, [
                'book_id' => $bokid,
                'agencies_id' => $request->agencies_id,
                'publisher_id' => $request->publisher_id[$key],
                'qty' => $request->qty[$key],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }

        // dd($distribution);
        // DB::table('agencies_book')->insert($distribution);
        BookAgency::insert($distribution);
        return redirect()->route('home.admin');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $detailrab = TempDistribute::whereHas('book', function($q) use($id){
        //     $q->where('distribute_target_id',$id);
        // })->get();
        $sekolah = TempDistribute::with('agency')->findOrFail($id);
        $detailrab = TempDistribute::with('book')->where('distribute_target_id',$id)->get();
        // dd($detailrab,$sekolah);
        return view('pages.distribution.detail',compact('detailrab','sekolah'));
    }
    public function detailDistribution($id)
    {
        
        // $sekolah = TempDistribute::with('agency')->findOrFail($id);
        $tdp = TempDistributePlan::find($id);
        $detailrab = TempDistribute::with('book')->where('temp_distribute_plans_id',$tdp->id)->get();
        $ambilsatu = TempDistribute::with('book')->where('temp_distribute_plans_id',$tdp->id)->first();
        $sekolah=Agencies::find($ambilsatu->distribute_target_id);
        return view('pages.distribution.detail',compact('tdp','detailrab','sekolah'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
