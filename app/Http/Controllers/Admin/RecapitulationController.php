<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Agencies;
use App\Models\Agent;
use App\Models\AgentMember;
use App\Models\Book;
use App\Models\Province;
use App\Models\TempDistribute;
use App\Models\Transaction;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Laravolt\Indonesia\Models\City;
use Laravolt\Indonesia\Models\District;
use Laravolt\Indonesia\Models\Village;

class RecapitulationController extends Controller
{
    public function rekapAgen()
    {
        $rekap = Agencies::with(['agentmember.agent','transaction'])->get();
        // dd($rekap);
        //menampilkan agent yang ada di instansi/agency
        // $rekap = Agencies::all()->map->agent->unique(); 
        
        // $rekap = DB::table('agencies')
        //             ->join('agents','agents.id', '=', 'agencies.agent_member_id')
        //             ->join('agent_members','agent_members.agent_id','=','agents.id')
        //             ->select(['agencies.*','agents.name as name_agent','agent_members.name as agent_member','agent_members.id as id_agent_member'])
        //             ->get();
        // dd($rekap);
        return view('pages.recapitulation.index', ['rekap' => $rekap]);
    }

    public function detail($id)
    {
        $detailtransaksi = Agencies::with(['transaction','agentmember','agentmember.agent'])->findOrFail($id);
        // dd($detailtransaksi);
        return view('pages.recapitulation.detail',compact('detailtransaksi'));
    }

    public function agencyEdit($id)
    {
        $edit = Agencies::findOrFail($id);
        $agent = Agent::pluck('name','id');
        $wilayah = Province::pluck('name', 'id');
        $agentmember = AgentMember::pluck('name','id');
        $city = City::pluck('name','id');
        $village = Village::pluck('name','id');
        $district = District::pluck('name','id');
        // $wilayah_kota = Province::pluck('name', 'id');

        // dd($edit);
        return view('pages.recapitulation.edit-agency',compact('edit','agent','wilayah','agentmember','city','village','district'));
    }

    public function agencyUpdate(Request $request, $id)
    {
        // $data = $request->all();
        // $data = $request->except(['logo','statement_letter_work','registration']);
        
        try {
            $data = Agencies::findOrFail($id);
            $data->name = $request->name;
            $data->nameapp = $request->nameapp;
            $data->domain = $request->domain;
            $data->link_app = $request->link_app;
            $data->version_app = $request->version_app;
            $data->address = $request->address;
            $data->province_id = $request->provinsi;
            $data->city_id = $request->kota;
            $data->district_id = $request->kecamatan;
            $data->village_id = $request->desa;
            $data->registration_date = $request->registration_date;
            $data->delivery_app_date = $request->delivery_app_date;
            $data->status_extension_app = $request->status_extension_app;
            $data->status_progress_app = $request->status_progress_app;
            $data->agent_member_id = $request->agentmember;
            $data->update();
            // return redirect()->route('agency')->with('status', 'Data berhasil di perbarui');
            return redirect()->route('rekap.agent')->with('status', true);
        } catch (Exception $e) {
            // return $e;
            $agency = Agencies::findOrFail($id);
            return view('pages.recapitulation.edit-agency',compact('agency'))->with('status', 'Data tidak boleh kosong');;
            // return redirect()->route('agency')->with('status', $e);
        }
        dd($data);
    }

    public function getAgenMember(Request $request)
    {
        // $agenmember = AgentMember::where('agent_id',$id)->pluck('name','id');
        $agenmember = AgentMember::where('agent_id',$request->agentID)->pluck('id','name');
        // return response()->json($agenmember);
        return json_encode($agenmember);
    }

    public function mostRead()
    {
        return view('pages.recapitulation.mostread');
    }

    public function mostDemand()
    {
        $laku = DB::table('temp_distributes')
                    ->join('books','books.id','=','temp_distributes.book_id')
                    ->select('temp_distributes.qty as qty_distribusi','books.*',
                            DB::raw('count(book_id) as jumlah_laku')
                        )
                    ->groupBy('books.id')
                    ->get();
        // dd($laku);
        return view('pages.recapitulation.mostdemand',compact('laku'));
    }

    public function detailBookmostDemand($id)
    {
        $detail = Book::findOrFail($id);
        // dd($detail);
        return view('pages.recapitulation.mostdeman-detail',compact('detail'));
    }

    public function userInstansi()
    {
        // $hostname = env("SUPERADMIN_URL")."/banner/dashboard";
        

        $instansi = Agencies::all();

        return view('pages.recapitulation.userinstansi',compact('instansi'));
    }

    public function userInstansiDetail($id)
    {
        $user_instansi = Agencies::findOrFail($id);
        // dd($user_instansi);
        if ($user_instansi->endpoint == null) {
            return response()->json([
                "status"=>"instansi pengguna belum memiliki endpoint",
                "data"=>$user_instansi
            ],400);
        }
        $url = $user_instansi->endpoint."/list-user";
        $response = Http::get($url);
        if ($response->status()==200) {
            if (isset($response->json()['data'])) {
                $user= $response->json()['data'];
                // dd($user);
                return view('pages.recapitulation.listuser',compact('user'));
            }else{
                return response()->json([
                    "status"=>"data tidak ditemukan",
                ],400);
            }
        }else{
            return response()->json([
                "status"=>"error api dengan code ".$response->status(),
            ],400);
        }

        
       

        

        
    }

    public function bookDistribute()
    {
        $bookdist = TempDistribute::with(['book','agency'])->get();
        // dd($bookdist);
        return view('pages.recapitulation.bookdistribution',compact('bookdist'));
    }

    //manajemen
    public function InstansiForManajemen()
    {
        $rekap = Agencies::with(['agentmember.agent','transaction'])->get();
        return view('manajemen.rekap.instansi', ['rekap' => $rekap]);
    }

}
