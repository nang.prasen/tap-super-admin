<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Agencies;
use App\Models\Province;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Laravolt\Indonesia\Models\City;
use Laravolt\Indonesia\Models\District;
use Laravolt\Indonesia\Models\Village;
use Yajra\DataTables\Contracts\DataTable;
use Yajra\DataTables\Facades\DataTables;

class AgencyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {

       

        if ($request->ajax()) {
            $agency = Agencies::select('*');
            $dataya = DataTables::of($agency)
                ->addIndexColumn()
                ->addColumn('aksi', function ($agency) {
                    $c = csrf_field();
                    $mp = method_field('PUT');
                    $btn = '<div class="btn-group dropleft">
                <button type="button" class="btn btn-secondary fa fa-ellipsis-v"
                    data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false"></button>
                <div class="dropdown-menu">
                    <a class="dropdown-item "
                        href="' . route('agency-detail', $agency->id) . '"> <img
                            src="super-admin/images/icon/profile.svg" alt=""/> Detail
                        Instansi</a>
                    <a class="dropdown-item"
                        href="' . route('agency.edit', $agency->id) . '"><img
                            src="super-admin/images/icon/edit.svg" alt=""> Edit</a>
                    <form
                        action="' . route('agency.onprogres', ['id' => $agency->id]) . '"
                        method="POST" style="display: inline-block;">
                        ' . $mp . '
                        ' . $c . '
                        <button type="submit" class="dropdown-item" value="Delete"
                        onclick=" return confirm(\'Proses Instansi ini\')">
                            <i class="fas fa-spinner"></i> On Progres
                        </button>
                    </form>
                    <form action="' . route('agency.active', ['id' => $agency->id]) . '"
                        method="POST" style="display: inline-block;">
                        ' . $mp . '
                        ' . $c . '
                        <button type="submit" class="dropdown-item" value="Delete"
                        onclick=" return confirm(\'Proses Instansi ini\')">
                            <i class="fas fa-check"></i> Aktif
                        </button>
                    </form>
                    <form action="' . route('agency.banned', ['id' => $agency->id]) . '"
                        method="POST" style="display: inline-block;">
                        ' . $mp . '
                        ' . $c . '
                        <button type="submit" class="dropdown-item" value="Delete"
                        onclick=" return confirm(\'Proses Instansi ini\')">
                            <img src="super-admin/images/icon/banned-active.svg" alt="">
                            Banned
                        </button>
                    </form>
                    <a class="dropdown-item" href="#"><img src="super-admin/images/icon/trash.svg" alt=""> Hapus</a>

                </div>';
                    return $btn;
                })
                ->filter(function ($instance) use ($request) {
                    if ($request->province_id!=0) {
                        $instance->where('province_id', $request->province_id);
                    }
                    if ($request->city_id!=0) {
                        $instance->where('city_id', $request->city_id);
                    }
                    if ($request->district_id!=0) {
                        $instance->where('district_id', $request->district_id);
                    }
                    if ($request->degree!=0) {
                        $instance->where('degree', $request->degree);
                    }
                   
                })
                
                ->rawColumns(['aksi'])
                ->make(true);
            return $dataya;
            // return datatables()->of($data)->make(true);
        }
        // $agency = Agencies::all();
        $wilayah = Province::pluck('name', 'id');
        return view('pages.agencies.index', compact('wilayah'));
    }

    public function show($id)
    {
        $agency = Agencies::findOrFail($id);
        $logo = $agency->logo;
        return view('pages.agencies.detail', compact('agency'));
    }

    public function edit($id)
    {
        $agency = Agencies::findOrFail($id);
        $wilayah = Province::pluck('name', 'id');
        $city = City::pluck('name','id');
        $village = Village::pluck('name','id');
        $district = District::pluck('name','id');
        return view('pages.agencies.edit', compact('agency', 'wilayah','city','village','district'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        // dd($data);
        $data = $request->except(['logo', 'statement_letter_work', 'registration']);

        try {
            $item = Agencies::findOrFail($id);
            $item->update($data);
            // return redirect()->route('agency')->with('status', 'Data berhasil di perbarui');
            return redirect()->route('agency')->with('status', true);
        } catch (Exception $e) {
            // return $e;
            // $agency = Agencies::findOrFail($id);
            return view('pages.agencies.edit', compact('agency'))->with('status', 'Data tidak boleh kosong');
            // return redirect()->route('agency')->with('status', $e);
        }
    }

    public function onprogres($id)
    {
        $instansi = Agencies::findOrFail($id);
        $instansi->status = 'ONPROGRES';
        $instansi->save();
        return redirect()->back();
    }
    public function active($id)
    {
        $instansi = Agencies::findOrFail($id);
        $instansi->status = 'ACTIVE';
        $instansi->save();
        return redirect()->back();
    }
    public function banned($id)
    {
        $instansi = Agencies::findOrFail($id);
        $instansi->status = 'BANNED';
        $instansi->save();
        return redirect()->back();
    }

    public function downloadLogo($id)
    {
        $agencylogo = Agencies::findOrFail($id);
        try {
            return Storage::disk('image')->download($agencylogo->logo);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function agencyForManajemen(Request $request)
    {
        if ($request->ajax()) {
            $agency = Agencies::select('*');
            $dataya = DataTables::of($agency)
                ->addIndexColumn()
                ->addColumn('aksi', function ($agency) {
                    $c = csrf_field();
                    $mp = method_field('PUT');
                    $btn = '<div class="btn-group dropleft">
                <button type="button" class="btn btn-secondary fa fa-ellipsis-v"
                    data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false"></button>
                <div class="dropdown-menu">
                    <a class="dropdown-item "
                        href="' . route('agency-detail', $agency->id) . '"> <img
                            src="super-admin/images/icon/profile.svg" alt=""/> Detail
                        Instansi</a>
                    

                </div>';
                    return $btn;
                })
                ->filter(function ($instance) use ($request) {
                    if ($request->province_id!=0) {
                        $instance->where('province_id', $request->province_id);
                    }
                    if ($request->city_id!=0) {
                        $instance->where('city_id', $request->city_id);
                    }
                    if ($request->district_id!=0) {
                        $instance->where('district_id', $request->district_id);
                    }
                    if ($request->degree!=0) {
                        $instance->where('degree', $request->degree);
                    }
                   
                })
                
                ->rawColumns(['aksi'])
                ->make(true);
            return $dataya;
            // return datatables()->of($data)->make(true);
        }
        // $agency = Agencies::all();
        $wilayah = Province::pluck('name', 'id');
        return view('manajemen.masterdata.instansi', compact('wilayah'));
    }
}
