<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Agencies;
use App\Models\Book;
use App\Models\BookAgency;
use App\Models\Publisher;
use App\Models\TempDistribute;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class DistribusiSegera extends Controller
{
    public function create(Request $request)
    {
        if (request()->ajax()) {
            if (!empty($request->filter_publisher)) {
                $data = DB::table('books')
                    ->join('publishers', 'publishers.id', '=', 'books.publisher_id')
                    ->select('books.id as bookId', 'books.title', 'books.isbn', 'books.qty', 'publishers.name','publishers.id as publisherId')
                    ->where('publisher_id', $request->filter_publisher)
                    ->get();
                } else {
                    $data = DB::table('books')
                    ->join('publishers', 'publishers.id', '=', 'books.publisher_id')
                    ->select('books.id as bookId', 'books.title', 'books.isbn', 'books.qty', 'publishers.name','publishers.id as publisherId')
                    ->get();
                }
                // dd($data);

            return datatables()->of($data)->make(true);
        }

        $publisher = Publisher::pluck('name', 'id');
        $agencies = Agencies::all();

        return view('pages.distribution.segera.create',compact('publisher', 'agencies'));
    }

    public function createajaxPost(Request $request)
    {
        $result =[];
        foreach($request->data as $d){
          $temp['book_id']=$d['book_api_id'];
          $temp['qty']=$d['value'];
          $temp['distribute_target_id']=$request->instansi;
          array_push($result,$temp);
        }
        // dd($result);
        // dd($result);
        $test = TempDistribute::insert($result);
        if($test==true){
            return response()->json([
                "status"=>"success",
            ]);
        }else{
            return response()->json([
                "status"=>"error",
            ]);
        }
        
    }

    public function listDistribute(Request $request)
    {
        // $id=1;
        // if($request->has('id')){
        //     $id=$request->get('id');
        // }
                $result = DB::table('temp_distributes')
                        ->select('temp_distributes.*','books.title','books.isbn','publishers.name as penerbit','books.publisher_id as idp')
                        ->join('books','books.id','temp_distributes.book_id')
                        ->join('publishers','publishers.id','books.publisher_id')
                        ->where("temp_distributes.distribute_target_id",$request->id)
                        ->where('temp_distributes.flag_bagi',0)

                        ->get();
                        // dd($result);
        return response()->json([
            "status"=>"success",
            "data"=>$result
        ]);
    }

    public function postDistribute(Request $request)
    {
        
        $distribute = TempDistribute::where('distribute_target_id',$request->id)->where('flag_bagi',0)->get();
        // cek apakah ada yg dibagikan
        if(count($distribute)==0){
            return response()->json([
                "status"=>"error data tidak ditemukan",
                "data"=>$distribute
            ],400);
        }
        // cek apakah endpoint sudah ada
        // $agencies = Agencies::find($request->id);
        // if($agencies->endpoint == null){
        //     return response()->json([
        //         "status"=>"instansi pengguna belum memiliki endpoint",
        //         "data"=>$distribute
        //     ],400);
        // }
        // $temp=[];
        foreach($distribute as $d){
            // dd($d);
            // array_push($temp,$d->book_id);
            $book = Book::find($d->book_id);
            // dd($book);
        //    Http::post($agencies->endpoint, [
        //         'book' => $book,
        //         'qty' => $d->qty,
        //     ]);
            $d->flag_bagi=1;
            $d->save();
            // dd($response->status());
           
            
        }
        
        

        return response()->json([
            "status"=>"buku berhasil diblas",
        ],200);
        
    }

    public function getAgency(Request $request)
    {
        $search = $request->search;
        if ($search == '') {
            $agency = Agencies::orderby('name','desc')->select('id','name')->limit(5)->get();
        }else{
            $agency = Agencies::orderby('name','asc')
                                ->select('id','name')
                                ->where('name','like','%'.$search.'%')
                                ->limit(6)
                                ->get();
        }

        $response = array();
        foreach($agency as $agn){
            $response[]=array(
                "id" =>$agn->id,
                "text" =>$agn->name,
            );
        }

        return response()->json($response);
    }
}
