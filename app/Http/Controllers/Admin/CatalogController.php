<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Book;
use App\Models\Category;
use Illuminate\Http\Request;

class CatalogController extends Controller
{
    public function index()
    {
        $catalogs = Book::with('category')->orderBy('created_at','desc')->limit(6)->get();
        $books = Book::with('category')->where('flag_packet',1)->orderBy('created_at','desc')->limit(6)->get();
        $category = Category::withCount('book')->get();
        return view('mitra.rab.catalog',compact('books','category','catalogs'));
    }

    public function detailCatalog($id)
    {
        $catalogdetail = Book::with(['author','publisher'])
                            ->findOrFail($id);
        return view('mitra.rab.detail-catalog', compact('catalogdetail'));
    }

    public function catalogCategory($slug)
    {
        $bookCategory = Category::where('slug',$slug)->with('book')->first();
        dd($bookCategory);
        return view('instansi.pages.catalog.showCategory', compact('bookCategory'));
    }
}
