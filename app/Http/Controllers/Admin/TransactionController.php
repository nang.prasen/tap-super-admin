<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Agencies;
use App\Models\Transaction;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.transaction.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $data = $request->all();
        Transaction::create($data);
        return redirect()->route('rekap.agent');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $agency = Agencies::with(['agentmember','agentmember.agent'])->findOrFail($id);
        return view('pages.transaction.create',compact('agency'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $agency = Agencies::with(['transaction','agentmember','agentmember.agent'])->findOrFail($id);
        $edit_trx = Transaction::with('agency')->findOrFail($id);
        // dd($edit_trx);
        return view('pages.transaction.edit',compact('edit_trx'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $transaction = Transaction::find($id);
        $transaction->preorder = $request->preorder;
        $transaction->agency_id = $request->agency_id;
        $transaction->spk_date = $request->spk_date;
        $transaction->rab_date = $request->rab_date;
        $transaction->status = $request->status;
        $transaction->delivery_packet_date = $request->delivery_packet_date;
        $transaction->save();

        return redirect()->route('rekap.agent');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
