<?php

namespace App\Http\Controllers\API\Register;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Agencies;
use App\Helpers\ImageUploader;
use App\Helpers\ResponseFormatter;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $agency = Agencies::orderBy('id', 'desc')->get();
        return ResponseFormatter::success(
            $agency,
            'List instansi ditampilkan'
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, ImageUploader $imageUploader)
    {
        
        $this->validateRequest();

        try {
            $data = new Agencies;
            $data->name = $request->name;
            $data->nameapp = $request->nameapp;
            $data->email = $request->email;
            $data->address = $request->address;
            $data->phone = $request->phone;
            $data->npwp = $request->npwp;
            $data->person_responsible = $request->person_responsible;
            $data->organizer_app = $request->organizer_app;
            $data->organizer_phone = $request->organizer_phone;
            $data->registration = $request->registration;
            $data->description = $request->description;
            $data->departement_responsible = $request->departement_responsible;
            $data->logo = $request->logo;
            $data->endpoint = 0;
            $data->statement_letter_work = $request->statement_letter_work;
            $data->status = 'PENDING';

            if ($request->file('logo')) {
                $image_path = $request->file('logo')->path();
                $extension = $request->file('logo')->extension();
                $image = base64_encode(file_get_contents($image_path));
                $image_url = $imageUploader->upload($image, $extension);
                $data->logo = $image_url;
            }
            if ($request->file('statement_letter_work')) {
                $image_path = $request->file('statement_letter_work')->path();
                $extension = $request->file('statement_letter_work')->extension();
                $image = base64_encode(file_get_contents($image_path));
                $image_url = $imageUploader->uploadPdf($image, $extension);
                $data->statement_letter_work = $image_url;
            }
            $data->save();
        } catch (\Throwable $th) {
            //throw $th;
        }


        return ResponseFormatter::success(
            $data,
            'Data berhasil ditambahkan'
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function validateRequest()
    {

        $validator = Validator::make(request()->all(),[
            'name' => 'required|string|min:5',
            'email' => 'required|string|unique:agencies,email',
            'nameapp' => 'required|string|min:5',
            'address' => 'required|string|min:10',
            'phone' => 'required|string|min:10|max:20',
            'npwp' => 'required|string|min:15',
            'person_responsible' => 'required|string',
            'departement_responsible' => 'required|string|min:5',
            'organizer_app' => 'required|string|min:5',
            'organizer_phone' => 'required|string|min:5',
            'registration' => 'required|boolean|min:1',
            'description' => 'required|string|min:50',
            // 'statement_letter_work' => 'required|mimes:pdf|max:4096',
            'logo'        =>  'required|image|mimes:jpeg,png,jpg|max:2048'
        ]);
        

        if($validator->fails()) {
            response()->json($validator->messages(), 422)->send();
            exit;
        }
    }
}
