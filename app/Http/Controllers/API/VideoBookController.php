<?php

namespace App\Http\Controllers\API;

use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Models\Book;
use App\Models\BookChapter;
use Illuminate\Http\Request;

class VideoBookController extends Controller
{
    public function list()
    {
        $videobook = Book::orderBy('created_at','desc')
                                    ->with('category')
                                    ->where('type',2)
                                    ->paginate(6);
        $temp['videobook']=$videobook;
        return ResponseFormatter::success($temp,'List buku rekomendasi buku paket ditampilkan');
    }
    public function listAll()
    {
        $videobook = Book::orderBy('created_at','desc')
                                    ->with('category')
                                    ->where('category_id',2)
                                    ->get();
        $temp['videobook']=$videobook;
        return ResponseFormatter::success($temp,'List buku rekomendasi buku paket ditampilkan');
    }
    public function detail($id)
    {
        $videobook = Book::find($id);
        $category = Book::where('category_id',$videobook->category_id)->where('type',2)->get();
        $chapter = null;
        if($videobook->chapter == 1){
            $chapter = BookChapter::where('book_id',$id)->get();
        }
        $temp['videobook']=$videobook;
        $temp['recomended']=$category;
        $temp['chapter']=$chapter;
        return ResponseFormatter::success($temp,'List buku rekomendasi buku paket ditampilkan');
    }
}
