<?php

namespace App\Http\Controllers\API;

use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Models\Article;
use Illuminate\Http\Request;

class ArticleApiController extends Controller
{
    public function index()
    {
        $article = Article::with('comment')->get();
        return ResponseFormatter::success(
            $article,
            'Artikel untuk di mobile ditampilkan',
        );
    }
}
