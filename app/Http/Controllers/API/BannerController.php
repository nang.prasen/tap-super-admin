<?php

namespace App\Http\Controllers\API;

use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Models\Banner;
use Illuminate\Http\Request;

class BannerController extends Controller
{
    public function bannerDashboard()
    {
        $bannerDashboard = Banner::where('type','dashboard_instansi')->latest()->get();
        // $temp['bannerDashboard']=$bannerDashboard;
        return ResponseFormatter::success(
            $bannerDashboard,
            'Banner untuk di dashboard ditampilkan',
        );
    }

    public function bannerCatalog()
    {
        $bannerCatalog = Banner::where('type','katalog_instansi')->latest()->get();
        return ResponseFormatter::success(
            $bannerCatalog,
            'Banner untuk di katalog ditampilkan',
        );
    }
    public function bannerUser()
    {
        $bannerUser = Banner::where('type','user_instansi')->latest()->get();
        return ResponseFormatter::success(
            $bannerUser,
            'Banner untuk di user ditampilkan',
        );
    }
}
