<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Book;
use Illuminate\Http\Request;
use App\Helpers\ResponseFormatter;
use App\Models\Category;

class CatalogController extends Controller
{
    public function recomendedbook()
    {
        
        $recomendlastbook = Book::orderBy('created_at','desc')->with('category')->paginate(6);
        $recomendpacketbook = Book::orderBy('created_at','desc')
                                    ->with('category')
                                    ->where('flag_packet',1)
                                    ->paginate(6);
                                    // dd($recomendpacketbook);
        $videobook = Book::orderBy('created_at','desc')
                                    ->with('category')
                                    ->where('type',2)
                                    ->paginate(6);
        $audiobook = Book::orderBy('created_at','desc')
                                    ->with('category')
                                    ->where('type',4)
                                    ->paginate(6);
        $bukupdf = Book::orderBy('created_at','desc')
                                    ->with('category')
                                    ->where('type',1)
                                    ->where('type',3)
                                    ->paginate(6);
        $temp['recomendlastbook']=$recomendlastbook;
        $temp['recomendpacketbook']=$recomendpacketbook;
        $temp['videobook']=$videobook;
        $temp['audiobook']=$audiobook;
        $temp['bukupdf']=$bukupdf;
        return ResponseFormatter::success($temp,'List buku rekomendasi buku paket ditampilkan');
    }

    public function categoryCountBook()
    {
        $categorycount = Category::withCount('book')->get();
        return ResponseFormatter::success(
            $categorycount,
            'Count buku category ditampilkan',
        );
    }

    public function recomendLastBook()
    {
        $recomendlastbook = Book::orderBy('created_at','desc')
                                    ->with('category')
                                    ->paginate(20);
        return ResponseFormatter::success(
            $recomendlastbook,
            'Buku rekomendasi terbaru ditampilkan',
        );
    }

    public function recomendPacketBook()
    {
        $recomendpacketbook = Book::orderBy('created_at','desc')
                                    ->where('flag_packet',1)
                                    ->with('category')
                                    ->paginate(20);
        return ResponseFormatter::success(
            $recomendpacketbook,
            'Buku rekomendasi terbaru ditampilkan',
        );
    }

    public function bukuPdfAll()
    {
        $bukupdf = Book::orderBy('created_at','desc')
                                    ->where('type',1)
                                    ->where('type',3)
                                    ->with('category')
                                    ->paginate(20);
        return ResponseFormatter::success(
            $bukupdf,
            'Buku bukupdf ditampilkan',
        );
    }
    public function audiobookAll()
    {
        $audiobook = Book::orderBy('created_at','desc')
                                    ->where('type',4)
                                    ->with('category')
                                    ->paginate(20);
        return ResponseFormatter::success(
            $audiobook,
            'Buku audiobook ditampilkan',
        );
    }

    public function videobookAll()
    {
        $videobook = Book::orderBy('created_at','desc')
                                    ->where('type',2)
                                    ->with('category')
                                    ->paginate(20);
        return ResponseFormatter::success(
            $videobook,
            'Buku videobook ditampilkan',
        );
    }


    public function bookDetail($id)
    {
       $book = Book::where('id',$id)->with('publisher','category','author')->first();
       return ResponseFormatter::success(
            $book,
            "Buku detail  ditampilkan",
        );
    }
    public function listCategoryBook($cat)
    {
        $category = Category::where('slug',$cat)->first();
        // dd($category);
        $book = Book::where('category_id',$category->id)->with('publisher','category','author')->paginate(10);
       return ResponseFormatter::success(
            $book,
            "List buku dengan kategori",
        );
    }

    public function searchingAsset(Request $request, $keyword)
    {
        $search = Book::where(function($query) use ($keyword){
            $query->where('title', 'like', '%'.$keyword.'%')
                  ->orWhere('description', 'like', '%'.$keyword.'%');
            })
            ->get();
        return ResponseFormatter::success(
        $search, 'Pencarian data artikel ditemukan');
    }
}
