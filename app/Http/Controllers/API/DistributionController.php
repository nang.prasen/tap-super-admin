<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Agencies;
use App\Models\Book;
use App\Models\Notification;
use App\Models\TempDistribute;
use App\Models\TempDistributePlan;
use App\Models\User;
use Illuminate\Http\Request;

class DistributionController extends Controller
{
    public function pembayaran(Request $request)
    {
    //    return response()->json($request->data);
    // cek agensi dulu
    $agency = Agencies::where('endpoint',$request->agency)
                        ->join('users','users.id','=','agencies.agent_member_id')
                        ->select('agencies.*','users.id as user_id')
                        ->first();
                        // 
    if(!$agency){
        return response()->json([
            'status'=>'error',
            'data'=>'agensi tidak ditemukan di server'
        ]);
    }
    //    create ke temp distribute  plan
    $tgl = date('dmY');
    $cek = TempDistributePlan::count();
    if ($cek == 0) {
        $urut = 100001;
        $kode = 'INV'.$tgl.'-'.$urut;
    } else {
        $kolom = TempDistributePlan::all()->last();
        $urut = (int) substr($kolom->kode,-6)+1; 
        $kode = 'INV'.$tgl.'-'.$urut;
    }
    $tdp=new TempDistributePlan();
    $tdp->kode=$kode;
    $tdp->status="pending";
    $tdp->agensi_id=$agency->id;
    $tdp->id_asal_agensi = $request->id_asal_agensi;
    $tdp->save();
   
    // crete ke temp distribute
    // cari bukunya dulu
    foreach($request->data as $d){
        $book = Book::find($d['book_id']);
        $temp = new TempDistribute();
        $temp->book_id=$book->id;
        $temp->distribute_target_id=$agency->id;
        $temp->qty=$d['qty'];
        $temp->publisher_id=$book->publisher_id;
        $temp->flag_bagi=0;
        $temp->temp_distribute_plans_id=$tdp->id;
        $temp->save();
    }
   

    // BUAT NOTIFIKASI untuk superadmin
    $superadmin = User::select('id')->where('role','SuperAdmin')->get();
    foreach($superadmin as $s){
        $n = new Notification();
        $n->target_id=$s->id;
        $n->pesan = "Agensi $agency->name melakukan pembayaran";
        $n->temp_distributes_plans_id=$tdp->id;
        $n->flag_baca=0;
        $n->save();
        // return response()->json($s->id);
    }
    // BUAT NOTIFIKASI untuk mitra
    $n = new Notification();
    $n->target_id=$agency->user_id;
    $n->pesan = "Agensi $agency->name melakukan pembayaran";
    $n->temp_distributes_plans_id=$tdp->id;
    $n->flag_baca=0;
    $n->save();
    

    return response()->json([
        'status'=>'success'
    ]);
    
    }

    public function buktiPembayaran(Request $request)
    {
        $agency = Agencies::where('endpoint',$request->agency)
                        ->join('users','users.id','=','agencies.agent_member_id')
                        ->select('agencies.*','users.id as user_id')
                        ->first();
                        // 
        if(!$agency){
            return response()->json([
                'status'=>'error',
                'data'=>'agensi tidak ditemukan di server'
            ]);
        }
        $temp = TempDistributePlan::where('id_asal_agensi',$request->id_asal_agensi)
                                    ->where('agensi_id',$agency->id)
                                    ->first();
                                    // return response()->json($temp);
        if(!$temp){
            return response()->json([
                'status'=>'error',
                'data'=>'Data pengajuan tidak ditemukan diserver'
            ]);
        }
        $temp->bukti_pembayaran=$request->link_bukti;
        $temp->status='bayar';
        $temp->save();


        // BUAT NOTIFIKASI untuk superadmin
        $superadmin = User::select('id')->where('role','SuperAdmin')->get();
        foreach($superadmin as $s){
            $n = new Notification();
            $n->target_id=$s->id;
            $n->pesan = "Agensi $agency->name mengupload bukti pembayaran";
            $n->temp_distributes_plans_id=$temp->id;
            $n->flag_baca=0;
            $n->save();
            // return response()->json($s->id);
        }
        // BUAT NOTIFIKASI untuk mitra
        $n = new Notification();
        $n->target_id=$agency->user_id;
        $n->pesan = "Agensi $agency->name mengupload bukti pembayaran";
        $n->temp_distributes_plans_id=$temp->id;
        $n->flag_baca=0;
        $n->save();
        return response()->json([
            'status'=>'success'
        ]);

    }
}
