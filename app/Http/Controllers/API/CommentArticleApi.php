<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\ArticleComment;
use Illuminate\Http\Request;

class CommentArticleApi extends Controller
{
    public function userPostComment(Request $request)
    {
        $data = $request->all();
        dd($data);
        // $article = Article::where('id',$data['article_id'])->first();

        $comment = new ArticleComment();
        $comment->comments = $data['comments'];
        $comment->article_id = $data['article_id'];
        $comment->user_id = $data['user_id'];
        $comment->email = $data['email'];
        $comment->registration_number = $data['registration_number'];
        $comment->avatar = $data['avatar'];
        $comment->gender = $data['gender'];
        $comment->address = $data['address'];
        $comment->agency_name = $data['agency_name'];
        $comment->agency_address = $data['agency_address'];
        $comment->save();

        return response()->json([
            "status"=>"success"
        ]);



    }
}
