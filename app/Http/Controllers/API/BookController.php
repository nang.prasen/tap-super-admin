<?php

namespace App\Http\Controllers\API;

use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Models\Book;
use App\Models\SubCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class BookController extends Controller
{
    public function list(Request $request)
    {
        // dd($req)
       $req=$request->get('id');
       if(!$req){
        return ResponseFormatter::error(
            null,
            "list is not array",
        );
       }

       $ex =explode(",",$req);
       $temp =[];
       foreach($ex as $e){
           $book =Book::select('id','title','isbn','price')->where('id',$e)->first();
           if($book!=null){
            array_push($temp,$book);
           }
          
       }
        // $list = $request->li
        // $book = Book::select('id','title','isbn','price')->whereIn('id',$ex)->get();
        return ResponseFormatter::success(
             $temp,
             "List buku ditampilkan",
         );
    }
    public function subCategory($category_id)
    {
        $sc = SubCategory::where('category_id',$category_id)->get();
        return ResponseFormatter::success(
            $sc,
            "List sub category",
        );
    }
}
