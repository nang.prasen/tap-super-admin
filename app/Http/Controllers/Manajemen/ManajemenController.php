<?php

namespace App\Http\Controllers\Manajemen;

use App\Http\Controllers\Controller;
use App\Models\Agencies;
use App\Models\Agent;
use App\Models\Book;
use App\Models\Publisher;
use App\Models\TempDistribute;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class ManajemenController extends Controller
{
    /**
     * menu master data.
     *
     * @return void
     */
    public function publisher()
    {
        $penerbit = DB::table('books')
                    ->rightjoin('publishers', 'publishers.id', '=', 'books.publisher_id')
                    ->select('publishers.*','books.publisher_id', DB::raw('count(*) as jumlah_buku'))
                    ->groupBy('publishers.id')
                    ->get();
        $userPublisher = User::where('role','Penerbit')->get();
        return view('manajemen.masterdata.publisher', compact('penerbit','userPublisher'));
    }

    public function detailPublisher($id)
    {
        $penerbit = Publisher::with('book')->findOrFail($id);
        // dd($penerbit);
        return view('manajemen.masterdata.detail-publisher', compact('penerbit'));
    }

    public function mitra()
    {
        $agent = Agent::all();
        // return view('pages.agent.index',compact('agent'));
        return view('manajemen.masterdata.mitra', compact('agent'));
    }

    public function detailMitra($id)
    {
        $agentdetail = Agent::with('member')->findOrFail($id);
        $user = User::where('idcard',$agentdetail->agentId)->where('role','Perwakilan')->first();

        // return view('pages.agent.detail',compact('agentdetail','user'));
        return view('manajemen.masterdata.detail-mitra', compact('agentdetail','user'));
    }

    /**
     * menu rekapitulasi
     */
    public function rekapitulasiDataUser()
    {
        $instansi = Agencies::all();

        // return view('pages.recapitulation.userinstansi',compact('instansi'));
        return view('manajemen.rekap.rekapitulasi-data-user',compact('instansi'));
    }

    public function DetailrekapDataUser($id)
    {
        $user_instansi = Agencies::findOrFail($id);
        // dd($user_instansi);
        if ($user_instansi->endpoint == null) {
            return response()->json([
                "status"=>"instansi pengguna belum memiliki endpoint",
                "data"=>$user_instansi
            ],400);
        }
        $url = $user_instansi->endpoint."/list-user";
        $response = Http::get($url);
        if ($response->status()==200) {
            if (isset($response->json()['data'])) {
                $user= $response->json()['data'];
                // dd($user);
                return view('manajemen.rekap.rekapitulasi-data-user-detail',compact('user'));
            }else{
                return response()->json([
                    "status"=>"data tidak ditemukan",
                ],400);
            }
        }else{
            return response()->json([
                "status"=>"error api dengan code ".$response->status(),
            ],400);
        }
    }

    public function rekapitulasiDataInstansi()
    {
        $rekap = Agencies::with(['agentmember.agent','transaction'])->get();
        return view('manajemen.rekap.rekapitulasi-data-instansi',compact('rekap'));
    }

    public function DetailRekapDataInstansi($id)
    {
        $detailtransaksi = Agencies::with(['transaction','agentmember','agentmember.agent'])->findOrFail($id);
        // dd($detailtransaksi);
        // return view('pages.recapitulation.detail',compact('detailtransaksi'));
        return view('manajemen.rekap.rekapitulasi-data-instansi-detail',compact('detailtransaksi'));
    }

    public function rekapitulasiBukuTerdistribusi()
    {
        $bookdist = TempDistribute::with(['book','agency'])->get();
        // dd($bookdist);
        // return view('pages.recapitulation.bookdistribution',compact('bookdist'));
        return view('manajemen.rekap.rekapitulasi-buku-terdistribusi',compact('bookdist'));
    }

    public function rekapitulasiBukuPalingLaku()
    {
        $laku = DB::table('temp_distributes')
                    ->join('books','books.id','=','temp_distributes.book_id')
                    ->select('temp_distributes.qty as qty_distribusi','books.*',
                            DB::raw('count(book_id) as jumlah_laku')
                        )
                    ->groupBy('books.id')
                    ->get();
        // dd($laku);
        return view('manajemen.rekap.rekapitulasi-bukupaling-laku',compact('laku'));
    }
    public function DetailrekapitulasiBukuPalingLaku($id)
    {
        $detail = Book::findOrFail($id);
        return view('manajemen.rekap.rekapitulasi-bukupaling-laku-detail',compact('detail'));
    }
    
}
