<?php

namespace App\Http\Controllers;

use App\Models\Agencies;
use App\Helpers\ImageUploader;
use App\Http\Requests\RegisterInstansiRequest;
use App\Mail\EmailRegister;
use App\Models\AgentMember;
use App\Models\Book;
use App\Models\CountBookPopular;
use App\Models\Notification;
use App\Models\Publisher;
use App\Models\TempDistribute;
use App\Models\Transaction;
use App\Models\User;
use App\Traits\ImageUpload;
use Illuminate\Http\Request;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Laravolt\Indonesia\Models\City;
use Laravolt\Indonesia\Models\District;
use Laravolt\Indonesia\Models\Province;
use Laravolt\Indonesia\Models\Village;

class HomeController extends Controller
{
    use ImageUpload;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware(['auth','IsAuth'])->except('registerInstansi', 'createRegister','actionRegister');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $totinstansi = Agencies::count();
        // dd($totinstansi);
        $totdistribusi = TempDistribute::count();
        $totpublisher = Publisher::count();
        $totmitra = AgentMember::count();
        $totaset = Book::count();
        $notification = Notification::orderBy('created_at','asc')->get();

        // ambil buku paling sering dibaca
        $agen = Agencies::select('id','name','endpoint')
                            ->where('endpoint',"!=",null)
                            // ->where('endpoint','!=',null)
                            ->get();
        $popular = CountBookPopular::where('flag','popular')->orderBy('total')->get();
        // dd($popular);
        $read = CountBookPopular::where('flag','read')->orderBy('total')->get();
        // dd($read);

        return view('pages.dashboard',compact('totinstansi','totdistribusi','totpublisher','totaset','totmitra','popular', 'read','notification'));
    }


    public function penerbit()
    {
        $penerbit = Publisher::where('publisherId', Auth::user()->publisherId)->first();
        $countbook = Book::where('publisher_id',$penerbit->id)->count();
        $countdistribusi = TempDistribute::where('publisher_id',$penerbit->id)->count();
        // dd($countbook);
        return view('publisher.dashboard',compact('countbook','countdistribusi'));
    }

    public function perwakilan()
    {
        return view('perwakilan.dashboard');
    }

    public function mitra()
    {
        $mitra = AgentMember::where('agent_member_id',Auth::user()->idcard)->first();
        $countinstansi = Agencies::where('agent_member_id',$mitra->id)->count();
        // dd($countinstansi);
        $totpo = DB::table('transactions')
                    ->rightjoin('agencies','transactions.agency_id','=','agencies.id')
                    ->where('agencies.agent_member_id',$mitra->id)
                    ->sum('transactions.preorder');
        // dd('total po',$totpo);
        return view('mitra.dashboard',compact('countinstansi','totpo'));
    }

    public function sekolah()
    {
        $instansi = Agencies::where('email',Auth::user()->email)->first();
        // dd($instansi);
        // return view('school.dashboard',compact('instansi'));
        return view('school.panelschool',compact('instansi'));
    }

    public function manajemen()
    {
        $totinstansi = Agencies::count();
        // dd($totinstansi);
        $totdistribusi = TempDistribute::count();
        $totpublisher = Publisher::count();
        $totmitra = AgentMember::count();
        $totaset = Book::count();
        $notification = Notification::orderBy('created_at','asc')->get();

        // ambil buku paling sering dibaca
        $agen = Agencies::select('id','name','endpoint')
                            ->where('endpoint',"!=",null)
                            // ->where('endpoint','!=',null)
                            ->get();
        $popular = CountBookPopular::where('flag','popular')->orderBy('total')->get();
        // dd($popular);
        $read = CountBookPopular::where('flag','read')->orderBy('total')->get();
        return view('manajemen.dashboard',compact('totinstansi','totdistribusi','totpublisher','totaset','totmitra','popular', 'read','notification'));
    }

    public function profileSekolah()
    {
        $instansi = Agencies::where('email',Auth::user()->email)->first();
        return view('school.profile.data',compact('instansi'));
    }

    public function registerInstansi()
    {
        $wilayah = Province::pluck('name', 'id');
        $city = City::pluck('name','id');
        $village = Village::pluck('name','id');
        $district = District::pluck('name','id');
        return view('home.register',compact('wilayah','city','village','district'));
    }

    public function actionRegister(RegisterInstansiRequest $request, ImageUploader $imageUploader)
    {
        $data = new Agencies;
        $data->name = $request->name;
        $data->nameapp = $request->nameapp;
        $data->email = $request->email;
        $data->domain = $request->domain;
        $data->address = $request->address;
        $data->province_id = $request->province_id;
        $data->city_id = $request->city_id;
        $data->district_id = $request->district_id;
        $data->village_id = $request->village_id;
        $data->degree = $request->degree;
        $data->phone = $request->phone;
        $data->npwp = $request->npwp;
        $data->person_responsible = $request->person_responsible;
        $data->organizer_app = $request->organizer_app;
        $data->organizer_phone = $request->organizer_phone;
        $data->registration = $request->registration;
        $data->description = $request->description;
        $data->students = $request->students;
        $data->departement_responsible = $request->departement_responsible;
        $data->logo = $request->logo;
        $data->endpoint = 0;
        $data->statement_letter_work = $request->statement_letter_work;
        $data->status = 'PENDING';


        if ($request->input('agent_member_id')) {
            $mitra = AgentMember::where('agent_member_id',$request->input('agent_member_id'))->first();
            $data->agent_member_id = $mitra->id;
        }elseif($request->input('agent_member_id') == null){
            $data->agent_member_id = 39; //mitra dg nama troyadigital
        }

        if($request->file('logo')){
            $image_path = $request->file('logo')->path();
            $extension = $request->file('logo')->extension();
            $image = base64_encode(file_get_contents($image_path));
            $image_url = $imageUploader->upload($image, $extension);
            $data->logo = $image_url;
        }
        if($request->file('statement_letter_work')){
            $image_path = $request->file('statement_letter_work')->path();
            $extension = $request->file('statement_letter_work')->extension();
            $image = base64_encode(file_get_contents($image_path));
            $image_url = $imageUploader->uploadPdf($image, $extension);
            $data->statement_letter_work = $image_url;
        }
        
        // dd($data);
        $data->save();
        $isi = Str::random(8);
        $secret = Crypt::encrypt($isi);
        $decrypted_secret = Crypt::decrypt($secret);

        $user = new User();
        $user->email = $request->email;
        $user->name = $request->name;
        $user->role = 'Sekolah';
        $user->password = Hash::make($isi);
        $user->save();

        // $subdomain_clone = json_encode($data->domain);

        // // dd($subdomain_clone);
        // $url = env("CLONING_URL")."/jobs";

        // $client = new Client(['verify' => false]);
        

        // $response = $client->request('POST', $url, [
        //     'form_params' => [
        //         'subdomain' => 'smasatuya',
        //     ]
        // ]);
        
        // var_dump($response->getBody());
        // echo $response->getStatusCode();
        // if ($response == true) {
        //     return json_decode($response->getBody()->getContents(), true);
        //     // dd($response);
        // } else {
        //     # code...
        //     return response()->json([
        //         "status"=>"error cloning app",
        //     ],400);
        // }
        
        // dd($res);

        // Mail::to($data->email)->send(new EmailRegister($user));
        
        // dd($data,$user,$decrypted_secret);

        return redirect()->back()->with('status', 'Data berhasil di upload '.$decrypted_secret.' passowrd');
        // dd($data);
    }

    public function assets()
    {
        return view('home.assets');
    }
}
