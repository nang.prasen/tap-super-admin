<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />

    <title>TAP - Troya Academic Platform</title>

    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet" />
    <link href="{{ url('super-admin/style/main.css') }}" rel="stylesheet" />

    <link rel="stylesheet" href="{{ url('super-admin/vendor/datatables/Buttons-1.7.0/css/buttons.bootstrap4.css') }}">
    <!-- Icon -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Date Picker -->

</head>

<body>
    <div id="page-content-wrapper">
        <!--Content -->
        <div class="page-auth" data-aos="fade-up" data-aos-delay="500">
            <div class="section-store-auth" data-aos="fade-up">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-6">
                            <img src="{{ asset('super-admin/images/ilustrasi.png') }}" alt="">
                        </div>
                        <div class="col-lg-2"></div>
                        <div class="col-lg-4 ">
                            <form action="{{ route('login') }}" method="post">
                                @csrf
                                <h2> Selamat Datang </h2>
                                <p>Masukkan email dan password Anda dengan benar</p>
                                <form class=" justify-content-center">
                                    <div class="form-group">
                                        <label>Email Address</label>
                                        <input type="email" name="email"
                                            class="form-control w-75 @error('email') is-invalid placeholder
                                    @enderror"
                                            required="" id="email" placeholder="Enter email">
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>Password</label>
                                        <div class="input-group-append">
                                            <input type="password" name="password"
                                            class="form-control w-75 @error('password') is-invalid placeholder
                                            @enderror"
                                            required="" id="password" placeholder="Enter password">
                                        <span class="" style=" margin: auto 0 auto -35px"
                                            onclick="password_show_hide();">
                                            <i class="fa fa-eye" id="show_eye"></i>
                                            <i class="fa fa-eye-slash d-none" id="hide_eye"></i>
                                        </span>
                                        </div>
                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <button type="submit" class="btn btn-blue btn-block w-75 mt-4">
                                        Masuk
                                    </button>
                                </form>
                                <img class="icon" style="min-height: 80px; margin-top: 120px;"
                                    src="{{ asset('super-admin/images/powered_vector.svg') }}" alt="">
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>



    <!-- Bootstrap core JavaScript -->
    <!--Jquery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!--Boostrap -->
    <!-- <script src="/vendor/jquery/jquery.slim.min.js "></script> -->
    <script src="{{ url('instansi/vendor/bootstrap/js/bootstrap.bundle.min.js') }} "></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js "></script>
    <script>
        AOS.init();
    </script>
    <script>
        function password_show_hide() {
            var x = document.getElementById('password')
            var show_eye = document.getElementById('show_eye')
            var hide_eye = document.getElementById('hide_eye')
            hide_eye.classList.remove('d-none')
            if (x.type === 'password') {
                x.type = 'text'
                show_eye.style.display = 'none'
                hide_eye.style.display = 'block'
            } else {
                x.type = 'password'
                show_eye.style.display = 'block'
                hide_eye.style.display = 'none'
            }
        }
    </script>

</body>

</html>
