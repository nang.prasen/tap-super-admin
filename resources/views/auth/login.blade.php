<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />

    <title>TAP - Troya Academic Platform</title>

    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet" />
    <link href="https://daftar.troyaacademic.com/super-admin/style/main.css" rel="stylesheet" />

    <link rel="stylesheet"
        href="https://daftar.troyaacademic.com/super-admin/vendor/datatables/Buttons-1.7.0/css/buttons.bootstrap4.css" />
    <!-- Icon -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <!-- Date Picker -->
</head>

<body data-aos-easing="ease" data-aos-duration="400" data-aos-delay="0" style="
      background-image: url('super-admin/images/pattern.svg');
      background-repeat: no-repeat;
      background-position: right bottom;
    ">
    <div id=" page-content-wrapper">
        <!--Content -->
        <div class="page-auth aos-init aos-animate" data-aos="fade-up" data-aos-delay="500">
            <div class="section-store-auth aos-init aos-animate" data-aos="fade-up">
                <div class="container-fluid row justify-content-center" style="height: 110vh">
                    <div class="col-lg-12" style="height: 150px;">
                        <img src="{{asset('super-admin/images/logotap-login.svg')}}" alt="" class="ml-5"
                            style="max-width: 312px; height: 200px; position:absolute" />
                        <img class="icon" src="{{asset('super-admin/images/powerd.svg')}}" alt="" style="margin-top: 210px; margin-left: 150px;">

                    </div>

                    <div class="offset-1 col-lg-4">
                        <div class="container">
                            <form action="{{ route('login') }}" method="post">
                                @csrf
                                <h2 class="text-center" style="padding-top: 0;">Selamat Datang</h2>
                                <br>

                                <div class="form-group">
                                    <label>Alamat Email</label>
                                    <input type="text"
                                    class="form-control{{ $errors->has('idcard') || $errors->has('email') ? ' is-invalid' : '' }}"
                                    name="login" value="{{ old('idcard') ?: old('email') }}" required autofocus placeholder="Masukan Email/ID Anda">
                                    @error('idcard')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                     
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>Kata Sandi</label>
                                    <div class="input-group-append">
                                        <input type="password" name="password" class="form-control @error('password') is-invalid placeholder
                                        @enderror" required id="password" placeholder="Masukkan Kata Sandi Anda" />
                                        <span class="input-group-text" onclick="password_show_hide();">
                                            <i class="fa fa-eye" id="show_eye"></i>
                                            <i class="fa fa-eye-slash d-none" id="hide_eye"></i>
                                        </span>
                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <a href="#"> Reset Kata Sandi </a>
                                <button type="submit" class="btn btn-blue btn-block mt-4">
                                    Masuk
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript -->
    <!--Jquery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!--Boostrap -->
    <!-- <script src="/vendor/jquery/jquery.slim.min.js "></script> -->
    <script src="https://daftar.troyaacademic.com/instansi/vendor/bootstrap/js/bootstrap.bundle.min.js "></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js "></script>
    <script>
        AOS.init();

        function password_show_hide() {
            var x = document.getElementById('password')
            var show_eye = document.getElementById('show_eye')
            var hide_eye = document.getElementById('hide_eye')
            hide_eye.classList.remove('d-none')
            if (x.type === 'password') {
                x.type = 'text'
                show_eye.style.display = 'none'
                hide_eye.style.display = 'block'
            } else {
                x.type = 'password'
                show_eye.style.display = 'block'
                hide_eye.style.display = 'none'
            }
        }
    </script>
</body>

</html>