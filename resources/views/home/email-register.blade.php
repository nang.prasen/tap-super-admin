@component('mail::message')
# Introduction

Terimakasih sudah {{$name}} melakukan pendaftaran di Troya Academic Platform
berikut adalah data Anda

Email login {{$email}}
Password {{$password}}

@component('mail::button', ['url' => ''])
Button Text
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
