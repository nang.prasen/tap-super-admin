<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />

    <title>TAP - Troya Academic Platform</title>
    <link rel="icon" href="{{ asset('super-admin/images/logo-tap.svg') }}" type="image/x-icon">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet" />
    <link href="style/main.css" rel="stylesheet" />

    <link rel="stylesheet" href="/vendor/datatables/Buttons-1.7.0/css/buttons.bootstrap4.css" />
    <!-- Icon -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <!-- Date Picker -->

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <!-- Datatabeles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css" />

    <!-- Google-Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;300;500;600&display=swap" rel="stylesheet">


</head>

<body>
    <div class="page-dashboard" id="page-content-wrapper" >
        <div class="container my-5" style="background-color: white;" >
            <div class="text-center">
                <img src="{{ asset('super-admin/images/logo-tap.svg') }}" class="mx-auto" />
            </div>
            <h4 class="mb-2" style="font-size: 20; font-weight: 600;">Data Instansi</h4>
            <hr />
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <table class="table table-striped">
                <form action="{{ route('create.register') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <tr class="    d-flex" style="background-color: #d0e3f3
                    ">
                        <th class="col-4" style="font-size: 14px; font-weight: 400;">Kode Mitra </th>
                        <td class="col-8" style="font-size: 14px; font-weight: 400;">
                            <input type="text" name="agent_member_id" class="form-control @error('agent_member_id') is-invalid @enderror"
                                value="{{ old('agent_member_id') }}" placeholder="Masukan Kode Mitra Anda" />
                            @error('agent_member_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <hr>
                        </th>
                    </tr>
                    <tr class="d-flex">
                        <th class="col-4" style="font-size: 14px; font-weight: 400;">Nama Instansi</th>
                        <td class="col-8" style="font-size: 14px; font-weight: 400;">
                            <input type="text" name="name" class="form-control @error('name') is-invalid @enderror"
                                value="{{ old('name') }}" placeholder="Masukan nama instansi" />
                                <small style="color: red">* Harus diisi</small>
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </td>
                        

                    </tr>
                    <tr class="d-flex">
                        <th class="col-4" style="font-size: 14px; font-weight: 400;">Alamat Domain</th>
                        <td class="col-8" style="font-size: 14px; font-weight: 400;">
                            <input type="text" name="domain" class="form-control @error('domain') is-invalid @enderror"
                                value="{{ old('domain') }}" placeholder="Masukan nama alamat domain (contoh:sman1gamping)" />
                                <small style="color: red">* Harus diisi</small>
                            @error('domain')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </td>
                    </tr>

                    <tr class="d-flex">
                        <th class="col-4" style="font-size: 14px; font-weight: 400;">Email Instansi</th>
                        <td class="col-8" style="font-size: 14px; font-weight: 400;">
                            <input type="email" name="email" class="form-control @error('email') is-invalid @enderror"
                                value="{{ old('email') }}" placeholder="Masukan akun email valid" />
                                <small style="color: red">* Harus diisi</small>
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </td>
                    </tr>
                    <tr class="d-flex">
                        <th class="col-4" style="font-size: 14px; font-weight: 400;">Nama Instansi Pada
                            Aplikasi</th>
                        <td class="col-8" style="font-size: 14px; font-weight: 400;">
                            <input type="text" name="nameapp"
                                class="form-control @error('nameapp') is-invalid @enderror"
                                value="{{ old('nameapp') }}"
                                placeholder="Masukkan nama yang akan di gunakan di aplikasi" />
                                <small style="color: red">* Harus diisi</small>
                            @error('nameapp')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </td>
                    </tr>
                    <tr class="d-flex">
                        <th class="col-4" style="font-size: 14px; font-weight: 400;">Jenjang Sekolah</th>
                        <td class="col-8" style="font-size: 14px; font-weight: 400;">
                            <select name="degree" id="degree" class="form-control">
                                <option>---Pilih Jenjang---</option>
                                <option value="1">SD</option>
                                <option value="2">SMP</option>
                                <option value="3">SMA</option>
                                <option value="4">SMK</option>
                                <option value="5">PERGURUAN TINGGI</option>
                                <option value="6">SLB</option>
                            </select>
                            <small style="color: red">* Harus diisi</small>
                        </td>
                    </tr>
                    <tr class="d-flex">
                        <th class="col-4" style="font-size: 14px; font-weight: 400;">Alamat Lengkap</th>
                        <td class="col-8" style="font-size: 14px; font-weight: 400;">
                            <input type="text" name="address"
                                class="form-control @error('address') is-invalid @enderror"
                                value="{{ old('address') }}" placeholder="Masukan alamat instansi" />
                                <small style="color: red">* Harus diisi</small>
                            @error('address')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </td>
                    </tr>
                    <tr class="d-flex">
                        <th class="col-4" style="font-size: 14px; font-weight: 400;">Provinsi</th>
                        <td class="col-8" style="font-size: 14px; font-weight: 400;">
                            <select class="form-control" name="province_id" id="provinsi" required>
                                <option>---Pilih Provinsi---</option>
                                @foreach ($wilayah as $id => $name)
                                    <option value="{{ $id}}">{{ $name }}</option>
                                @endforeach
                            </select>
                            <small style="color: red">* Harus diisi</small>
                        </td>
                    </tr>
                    <tr class="d-flex">
                        <th class="col-4" style="font-size: 14px; font-weight: 400;">Kabupaten/Kota</th>
                        <td class="col-8" style="font-size: 14px; font-weight: 400;">
                            <select class="form-control" name="city_id" id="kota" required>
                                <option>---Pilih Kab/Kota---</option>
                                @foreach ($city as $id => $name)
                                        <option value="{{ $id}}">{{ $name }}</option>
                                @endforeach
                            </select>
                            <small style="color: red">* Harus diisi</small>
                        </td>
                    </tr>
                    <tr class="d-flex">
                        <th class="col-4" style="font-size: 14px; font-weight: 400;">Kecamatan</th>
                        <td class="col-8" style="font-size: 14px; font-weight: 400;">
                            <select class="form-control" name="district_id" id="kecamatan" required>
                                <option>---Pilih Kecamatan---</option>
                                @foreach ($district as $id => $name)
                                    <option value="{{ $id}}">{{ $name }}</option>
                                 @endforeach
                            </select>
                            @error('district_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            <small style="color: red">* Harus diisi</small>
                        </td>
                    </tr>
                    <tr class="d-flex">
                        <th class="col-4" style="font-size: 14px; font-weight: 400;">Desa</th>
                        <td class="col-8" style="font-size: 14px; font-weight: 400;">
                            <select class="form-control" name="village_id" id="desa" required>
                                <option>---Pilih Desa---</option>
                                @foreach ($village as $id => $name)
                                    <option value="{{ $id}}">{{ $name }}</option>
                                @endforeach
                            </select>
                            @error('village_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            <small style="color: red">* Harus diisi</small>
                        </td>
                    </tr>
                    <tr class="d-flex">
                        <th class="col-4" style="font-size: 14px; font-weight: 400;">No. Telepon</th>
                        <td class="col-8" style="font-size: 14px; font-weight: 400;">
                            <input type="tel" name="phone" class="form-control @error('phone') is-invalid @enderror"
                                value="{{ old('phone') }}" placeholder="Masukan no telepon instansi" />
                                <small style="color: red">* Harus diisi</small>
                            @error('phone')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </td>
                    </tr>
                    <tr class="d-flex">
                        <th class="col-4" style="font-size: 14px; font-weight: 400;">NPWP</th>
                        <td class="col-8" style="font-size: 14px; font-weight: 400;">
                            <input type="text" name="npwp" class="form-control @error('npwp') is-invalid @enderror"
                                value="{{ old('npwp') }}" placeholder="Masukan NPWP instansi" />
                                <small style="color: red">* Harus diisi</small>
                            @error('npwp')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </td>
                    </tr>
                    <tr class="d-flex">
                        <th class="col-4" style="font-size: 14px; font-weight: 400;">Nama Penanggung Jawab</th>
                        <td class="col-8" style="font-size: 14px; font-weight: 400;">
                            <input type="text" name="person_responsible"
                                class="form-control @error('person_responsible') is-invalid @enderror"
                                value="{{ old('person_responsible') }}"
                                placeholder="Masukan nama penanggung jawab instansi" />
                                <small style="color: red">* Harus diisi</small>
                            @error('person_responsible')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </td>
                    </tr>
                    <tr class="d-flex">
                        <th class="col-4" style="font-size: 14px; font-weight: 400;">Jabatan Penanggung Jawab
                        </th>
                        <td class="col-8" style="font-size: 14px; font-weight: 400;">
                            <input type="text" name="departement_responsible"
                                class="form-control @error('departement_responsible') is-invalid @enderror"
                                value="{{ old('departement_responsible') }}"
                                placeholder="masukan Jabatan penanggung jawab" />
                                <small style="color: red">* Harus diisi</small>
                            @error('departement_responsible')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </td>
                    </tr>
                    <tr class="d-flex">
                        <th class="col-4" style="font-size: 14px; font-weight: 400;">Nama Pengelola Aplikasi
                        </th>
                        <td class="col-8" style="font-size: 14px; font-weight: 400;">
                            <input type="text" name="organizer_app"
                                class="form-control @error('organizer_app') is-invalid @enderror"
                                value="{{ old('organizer_app') }}" placeholder="Masukan nama pengelola aplikasi" />
                                <small style="color: red">* Harus diisi</small>
                            @error('organizer_app')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </td>
                    </tr>
                    <tr class="d-flex">
                        <th class="col-4" style="font-size: 14px; font-weight: 400;">Nomor Telepon Pengelola
                        </th>
                        <td class="col-8" style="font-size: 14px; font-weight: 400;">
                            <input type="tel" name="organizer_phone"
                                class="form-control @error('organizer_phone') is-invalid @enderror"
                                value="{{ old('organizer_phone') }}"
                                placeholder="Masukan nomor telepon pengelola aplikasi" />
                                <small style="color: red">* Harus diisi</small>
                            @error('organizer_phone')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </td>
                    </tr>
                    <!-- End -->

                    <tr class="d-flex">
                        <th class="col-4" style="font-size: 14px; font-weight: 400;">Fitur Pendaftaran Online
                            di Aplikasi</th>
                        <td class="col-8" style="font-size: 14px; font-weight: 400;">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="registration" id="inlineRadio1"
                                value="1" />
                                <label class="form-check-label" for="inlineRadio1">Ya</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="registration" id="inlineRadio1"
                                value="0" />
                                <label class="form-check-label" for="inlineRadio1">Tidak</label>
                            </div>
                            <small style="color: red">* Harus diisi</small>
                        </td>
                    </tr>

                    <tr class="d-flex">
                        <th class="col-4" style="font-size: 14px; font-weight: 400;">Jumlah Siswa</th>
                        <td class="col-8" style="font-size: 14px; font-weight: 400;">
                            <input type="number" name="students"
                                class="form-control @error('students') is-invalid @enderror"
                                value="{{ old('students') }}" placeholder="Jumlah siswa/pengguna di Instansi" />
                                <small style="color: red">* Harus diisi</small>
                            @error('students')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </td>
                    </tr>

                    <tr class="d-flex">
                        <th class="col-4" style="font-size: 14px; font-weight: 400;">Kata Sambutan (Tentang
                            Instansi)</th>
                        <td class="col-8" style="font-size: 14px; font-weight: 400;">
                            <textarea name="description" class="form-control @error('description') is-invalid @enderror"
                                value="{{ old('description') }}"
                                placeholder="Masukan kata sambutan / tentang instansi"></textarea>
                                <small style="color: red">* Harus diisi</small>
                            @error('description')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </td>
                    </tr>
                    <tr class="d-flex">
                        <th class="col-4" style="font-size: 14px; font-weight: 400;">Logo (Resolusi Tinggi)
                        </th>
                        <td class="col-8" style="font-size: 14px; font-weight: 400;">
                            <input type="file" name="logo"
                                class="form-control form-control-file @error('logo') is-invalid @enderror"
                                value="{{ old('logo') }}" accept="image/jpeg,image/png" />
                                <small style="color: red">* Harus diisi</small>
                            <small id="passwordHelpBlock" class="form-text text-muted">
                                Format gambar .jpeg atau .png
                                <br>
                                Gambar min. 800 pixel
                            </small>
                            @error('logo')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </td>
                    </tr>
                    <tr class="d-flex">
                        <th class="col-4" style="font-size: 14px; font-weight: 400;">Surat Perjanjian</th>
                        <td class="col-8" style="font-size: 14px; font-weight: 400;">
                            <input type="file" name="statement_letter_work"
                                class="form-control form-control-file @error('statement_letter_work') is-invalid @enderror"
                                value="{{ old('statement_letter_work') }}" accept="application/pdf" />
                            <small id="passwordHelpBlock" class="form-text text-muted">
                                Format dokumen PDF
                            </small>
                            @error('statement_letter_work')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </td>
                    </tr>
                    <tr class="d-flex" style="text-align: right">
                        <td rowspan="2" class="col-12">
                            <input type="submit" class="btn btn-primary mt-4" value="Selesai" name="submit" />
                        </td>
                    </tr>
                </form>
            </table>
            <div class="text-center mt-5">
                <img src="images/dashboard-text-troya.svg" alt="" />
            </div>
        </div>
    </div>

</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    $(document).ready(function() {
            $('#provinsi').on('change',function(){
            var idkota = $(this).val();
            console.log(idkota);
            if (idkota) {
                $.ajax({
                    type: "get",
                    url: '{{ route('cities') }}',
                    data: {id:idkota},
                    dataType: "json",
                    success: function (response) {
                        console.log('a');
                        if (response) {
                            // console.log(response);
                            $('#kota').empty();
                            $('#kota').append(
                                    '<option>---Pilih Kab/Kota---</option>');
                            $.each(response, function(name, id) {
                                    $('#kota').append('<option name="city_id" value ="'+name+'">'+id+'</option>')
                            });
                        }
                    }
                });
            } else {
                $('#kota').empty();
            }
        });


        $('#kota').on('change',function(){
            var idkec = $(this).val();
            console.log(idkec);
            if (idkec) {
                $.ajax({
                    type: "get",
                    url: '{{ route('districts') }}',
                    data: {id:idkec},
                    dataType: "json",
                    success: function (response) {
                        console.log('a');
                        if (response) {
                            // console.log(response);
                            $('#kecamatan').empty();
                            $('#kecamatan').append(
                                    '<option>---Pilih Kecamatan---</option>');
                            $.each(response, function(name, id) {
                                    $('#kecamatan').append('<option name="district_id" value ="'+name+'">'+id+'</option>')
                            });
                        }
                    }
                });
            } else {
                $('#kecamatan').empty();
            }
        });

        $('#kecamatan').on('change',function(){
            var iddesa = $(this).val();
            console.log(iddesa);
            if (iddesa) {
                $.ajax({
                    type: "get",
                    url: '{{ route('villages') }}',
                    data: {id:iddesa},
                    dataType: "json",
                    success: function (response) {
                        console.log('a');
                        if (response) {
                            // console.log(response);
                            $('#desa').empty();
                            $('#desa').append(
                                    '<option>---Pilih Desa---</option>');
                            $.each(response, function(name, id) {
                                    $('#desa').append('<option name="village_id" value ="'+name+'">'+id+'</option>')
                            });
                        }
                    }
                });
            } else {
                $('#desa').empty();
            }
        });
        });
</script>

</html>
