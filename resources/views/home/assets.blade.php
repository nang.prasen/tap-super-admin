<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />
    <meta name="description" content="" />
    <meta name="author" content="" />

    <title>TAP - Troya Academic Platform</title>
    <link rel="icon" href="{{asset('super-admin/images/logo-tap.svg')}}" type="image/x-icon">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet" />
    <link href="style/main.css" rel="stylesheet" />

    <link
      rel="stylesheet"
      href="/vendor/datatables/Buttons-1.7.0/css/buttons.bootstrap4.css"
    />
    <!-- Icon -->
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
    />
    <!-- Date Picker -->

    <link
      rel="stylesheet"
      href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"
    />
    <!-- Datatabeles -->
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css"
    />
    <link
      rel="stylesheet"
      href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css"
    />

    <!-- Google-Fonts -->
<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;300;500;600&display=swap" rel="stylesheet">


  </head>
  <body>
    <div class="page-dashboard" id="page-content-wrapper">
      <div class="container my-5">
      <div class="text-center">
        <img src="{{asset('super-admin/images/logo-tap.svg')}}" class="mx-auto" />
      </div>
      <br>
      <br>
      <div class="text-center">

          <h4 class="mb-2" style="font-size: 20; font-weight: 600;">Data Assets Instansi</h4>
      </div>
      <hr />
      @if(session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
      @endif
      
      <table class="table table-striped">
        <form action="{{route('create.register')}}" method="POST" enctype="multipart/form-data"">
          @csrf
          <tr class="d-flex">
            <th class="col-4" style="font-size: 14px; font-weight: 400;">Klik Tombol</th>
            <td class="col-8" style="font-size: 14px; font-weight: 400;">
                <a href="https://www.youtube.com/" target="_blank" class="btn btn-primary btn-sm">Asset Saya</a>
            </td>
          </tr>
        </form>
      </table>
      <div class="text-center mt-5">
        <img src="images/dashboard-text-troya.svg" alt="" />
      </div>
    </div>
    </div>
    
  </body>
</html>
