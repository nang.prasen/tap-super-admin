{{-- <div class="border-right" id="sidebar-wrapper"> --}}
<div class="" id="sidebar-wrapper">
    <div class="list-group list-group-flush">
        <span class="right-nav-text">MENU</span>
        
        @if (Auth::user()->role == 'SuperAdmin')
            <a href="{{url('home/admin')}}" class="list-group-item list-group-item-action {{set_active('home.admin')}}"> <img src="{{asset('super-admin/images/icon/dashboard.svg')}}" alt="" style="margin-right: 18px;"> Dashboard </a>
            <a href="{{route('distribution.index')}}" class="list-group-item list-group-item-action {{set_active(['distribution.index','distribution.create'])}}"><img src="{{asset('super-admin/images/icon/schools.svg')}}" alt="" style="margin-right: 18px;">Distribusi Buku </a>
            <a href="#collapseOne" class="list-group-item list-group-item-action drop-down-class" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="collapseOne"> <img src="https://daftar.troyaacademic.com/super-admin/images/icon/katalog.svg" alt="" style="margin-right: 18px" />Rekapitulasi<i class="fa fa-angle-down" style="margin-left: 60px;"></i></a>
            <div id="collapseOne" class="collapse">
                <a href="#" class=" list-group-item list-group-item-action {{set_active('rekap.seringdibaca')}}">
                    <img style="margin-right: 27px" /> Paling Sering Dibaca
                </a>
                <a href="{{route('rekap.bukulaku')}}" class=" list-group-item list-group-item-action {{set_active(['rekap.bukulaku','rekap.bukulaku.detail'])}}">
                    <img style="margin-right: 27px" /> Buku Paling Laku
                </a>
                <a href="{{route('rekap.userinstansi')}}" class=" list-group-item list-group-item-action {{set_active('rekap.userinstansi')}}">
                    <img style="margin-right: 27px" /> Data User
                </a>
                <a href="{{route('rekap.bookdistribution')}}" class=" list-group-item list-group-item-action {{set_active('rekap.bookdistribution')}}">
                    <img style="margin-right: 27px" /> Buku Terdistribusi
                </a>
                <a href="{{route('rekap.agent')}}" class=" list-group-item list-group-item-action {{set_active('rekap.agent')}}">
                    <img style="margin-right: 27px" /> Instansi
                </a>
            </div>
            <a href="#collapsedata" class="list-group-item list-group-item-action drop-down-class {{set_active(['publisher','publisher-create','publisher.detail','agency','agent.index'])}}" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="collapseOne"> <img src="https://daftar.troyaacademic.com/super-admin/images/icon/berita.svg" alt="" style="margin-right: 18px" />Master Data <i class="fa fa-angle-down" style="margin-left: 60px;"></i></a>
            <div id="collapsedata" class="collapse">
                <a href="{{url('agency')}}" class=" list-group-item list-group-item-action {{set_active('agency')}}">
                    <img style="margin-right: 27px" /> Data Instansi
                </a>
                <a href="{{url('publisher')}}" class=" list-group-item list-group-item-action {{set_active(['publisher','publisher-create','publisher.detail'])}}">
                    <img style="margin-right: 27px" /> Data Penerbit
                </a>
                <a href="{{route('agent.index')}}" class=" list-group-item list-group-item-action {{set_active(['agent'])}}">
                    <img style="margin-right: 27px" /> Data Mitra
                </a>
            </div>
            
            {{-- <a href="{{route('sebar-buku')}}" class="list-group-item list-group-item-action {{set_active(['sebar-buku'])}}"><img src="{{asset('super-admin/images/icon/book.svg')}}" alt="" style="margin-right: 18px; color:red;"> Blas Buku Penerbit</a> --}}
            <a href="#collapsepengaturan" class="list-group-item list-group-item-action drop-down-class" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="collapseOne"> <img src="{{asset('super-admin/images/icon/setting.svg')}}" alt="" style="margin-right: 18px" />Pengaturan <i class="fa fa-angle-down" style="margin-left: 60px;"></i></a>
            <div id="collapsepengaturan" class="collapse">
                <a href="{{route('banner.index')}}" class=" list-group-item list-group-item-action {{set_active('banner.index')}}">
                    <img style="margin-right: 27px" /> Banner
                </a>
                <a href="{{route('article.index')}}" class=" list-group-item list-group-item-action {{set_active('article.index')}}">
                    <img style="margin-right: 27px" /> Artikel
                </a>
                <a href="#" class=" list-group-item list-group-item-action ">
                    <img style="margin-right: 27px" /> Tutorial Instansi
                </a>
                <a href="{{route('user')}}" class=" list-group-item list-group-item-action ">
                    <img style="margin-right: 27px" /> User
                </a>
            </div>
            
        @endif
        @if (Auth::user()->role == 'Manajemen')
            <a href="{{url('home/manajemen')}}" class="list-group-item list-group-item-action {{set_active('home.admin')}}"> <img src="{{asset('super-admin/images/icon/dashboard.svg')}}" alt="" style="margin-right: 18px;"> Dashboard </a>
            <a href="#collapseOne" class="list-group-item list-group-item-action drop-down-class" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="collapseOne"> <img src="https://daftar.troyaacademic.com/super-admin/images/icon/katalog.svg" alt="" style="margin-right: 18px" />Rekapitulasi<i class="fa fa-angle-down" style="margin-left: 60px;"></i></a>
            <div id="collapseOne" class="collapse">
                <a href="#" class=" list-group-item list-group-item-action {{set_active('rekap.seringdibaca')}}">
                    <img style="margin-right: 27px" /> Paling Sering Dibaca
                </a>
                <a href="{{url('/manajemen/rekapitulasi/buku-paling-laku')}}" class=" list-group-item list-group-item-action {{set_active(['rekap.bukulaku','rekap.bukulaku.detail'])}}">
                    <img style="margin-right: 27px" /> Buku Paling Laku
                </a>
                <a href="{{url('/manajemen/rekapitulasi/data-user')}}" class=" list-group-item list-group-item-action {{set_active('rekap.userinstansi')}}">
                    <img style="margin-right: 27px" /> Data User
                </a>
                <a href="{{url('/manajemen/rekapitulasi/buku-distribusi')}}" class=" list-group-item list-group-item-action {{set_active('rekap.bookdistribution')}}">
                    <img style="margin-right: 27px" /> Buku Terdistribusi
                </a>
                <a href="{{url('/manajemen/rekapitulasi/data-instansi')}}" class=" list-group-item list-group-item-action {{set_active('rekap.agent')}}">
                    <img style="margin-right: 27px" /> Instansi
                </a>
            </div>
            <a href="#collapsedata" class="list-group-item list-group-item-action drop-down-class {{set_active(['publisher','publisher-create','publisher.detail','agency','agent.index'])}}" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="collapseOne"> <img src="https://daftar.troyaacademic.com/super-admin/images/icon/berita.svg" alt="" style="margin-right: 18px" />Master Data <i class="fa fa-angle-down" style="margin-left: 60px;"></i></a>
            <div id="collapsedata" class="collapse">
                <a href="{{url('agency/manajemen')}}" class=" list-group-item list-group-item-action {{set_active('agency')}}">
                    <img style="margin-right: 27px" /> Data Instansi
                </a>
                <a href="{{url('/publisher/manajemen')}}" class=" list-group-item list-group-item-action {{set_active(['publisher','publisher-create','publisher.detail'])}}">
                    <img style="margin-right: 27px" /> Data Penerbit
                </a>
                <a href="{{url('/mitra/manajemen/data')}}" class=" list-group-item list-group-item-action {{set_active(['agent'])}}">
                    <img style="margin-right: 27px" /> Data Mitra
                </a>
            </div>
            
        @endif
        @if (Auth::user()->role == 'Penerbit')
            <a href="{{route('home.penerbit')}}" class="list-group-item list-group-item-action {{set_active('home.penerbit')}}"> <img src="{{asset('super-admin/images/icon/dashboard.svg')}}" alt="" style="margin-right: 18px;"> Dashboard </a>
            <a href="{{route('book-penerbit.index')}}" class="list-group-item list-group-item-action {{set_active(['book-penerbit.index','book-penerbit.create','book-penerbit.show','book-penerbit.rekap'])}} "><img src="{{asset('super-admin/images/icon/book.svg')}}" alt="" style="margin-right: 18px;"> Daftar Asset </a>
            <a href="{{route('book-rekap.index')}}" class="list-group-item list-group-item-action {{set_active('book-rekap.index')}}"><img src="{{asset('super-admin/images/icon/katalog.svg')}}" alt="" style="margin-right: 18px;"> Rekapitulasi </a>
        @endif
        @if (Auth::user()->role == 'Mitra')
            <a href="{{url('home/mitra')}}" class="list-group-item list-group-item-action {{set_active('home.mitra')}}"> <img src="{{asset('super-admin/images/icon/dashboard.svg')}}" alt="" style="margin-right: 18px;"> Dashboard </a>
            <a href="{{route('rab.index')}}" class="list-group-item list-group-item-action {{set_active('rab.index')}}"> <img src="{{asset('super-admin/images/icon/berita.svg')}}" alt="" style="margin-right: 18px;"> Buat RAB </a>
            <a href="{{route('mitra.index')}}" class="list-group-item list-group-item-action {{set_active('mitra.index')}}"> <img src="{{asset('super-admin/images/icon/book.svg')}}" alt="" style="margin-right: 18px;"> Progress </a>
            <a href="{{route('transaction')}}" class="list-group-item list-group-item-action {{set_active('transaction')}}"> <img src="{{asset('super-admin/images/icon/book-detail.svg')}}" alt="" style="margin-right: 18px;"> Transaksi </a>
            
        @endif
        @if (Auth::user()->role == 'Perwakilan')
            <a href="{{url('home/perwakilan')}}" class="list-group-item list-group-item-action {{set_active('home.perwakilan')}}"> <img src="{{asset('super-admin/images/icon/dashboard.svg')}}" alt="" style="margin-right: 18px;"> Dashboard </a>
            <a href="{{route('perwakilan.mitra')}}" class="list-group-item list-group-item-action {{set_active('perwakilan.mitra')}}"> <img src="{{asset('super-admin/images/icon/berita.svg')}}" alt="" style="margin-right: 18px;"> Mitra </a>
            
        @endif
        @if (Auth::user()->role == 'Sekolah')
            <a href="{{route('home.sekolah')}}" class="list-group-item list-group-item-action {{set_active('home.mitra')}}"> <img src="{{asset('super-admin/images/icon/dashboard.svg')}}" alt="" style="margin-right: 18px;"> Dashboard </a>
        @endif
        <span class="right-nav-text">AKUN</span>
        @if (Auth::user()->role == 'Penerbit')    
            <a href="{{route('profile-penerbit.index')}}" class="list-group-item list-group-item-action {{set_active('profile-penerbit.index')}} "> <img src="{{asset('super-admin/images/icon/user.svg')}}" style="margin-right: 18px;" alt=""> Profile</a>
        @endif
        @if (Auth::user()->role == 'SuperAdmin')    
            <a href="{{url('profile')}}" class="list-group-item list-group-item-action {{set_active('profile')}} "> <img src="{{asset('super-admin/images/icon/user.svg')}}" style="margin-right: 18px;" alt=""> Profile</a>
        @endif
        @if (Auth::user()->role == 'Manajemen')    
            <a href="{{url('profile')}}" class="list-group-item list-group-item-action {{set_active('profile')}} "> <img src="{{asset('super-admin/images/icon/user.svg')}}" style="margin-right: 18px;" alt=""> Profile</a>
        @endif
        @if (Auth::user()->role == 'Perwakilan')    
            <a href="{{route('perwakilan.profile')}}" class="list-group-item list-group-item-action {{set_active('profile')}} "> <img src="{{asset('super-admin/images/icon/user.svg')}}" style="margin-right: 18px;" alt=""> Profile</a>
        @endif
        @if (Auth::user()->role == 'Mitra')    
            <a href="{{route('profile.mitra')}}" class="list-group-item list-group-item-action {{set_active('profile')}} "> <img src="{{asset('super-admin/images/icon/user.svg')}}" style="margin-right: 18px;" alt=""> Profile</a>
        @endif
        @if (Auth::user()->role == 'Sekolah')    
        <a href="{{route('profile.sekolah')}}" class="list-group-item list-group-item-action {{set_active('profile')}} "> <img src="{{asset('super-admin/images/icon/user.svg')}}" style="margin-right: 18px;" alt=""> Profile</a>
        @endif
        {{-- <a href="#" class="list-group-item list-group-item-action "> <img src="{{asset('super-admin/images/icon/setting.svg')}}" style="margin-right: 18px;" alt=""> Pengaturan</a> --}}
        <img src="super-admin/images/dashboard-troya.svg" alt="">

        
    </div>
</div>