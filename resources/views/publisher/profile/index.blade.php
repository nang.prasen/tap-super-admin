@extends('layouts.super-admin')
@section('content')
<div id="page-content-wrapper">
    <!--Content Dashboard-->
    <!-- ........................ -->
    <div class="section-content section-dashboard-bukuterbaru" data-aos="fade-up" data-aos-delay="500">
        <div class="container-fluid">
       <!-- Breandcrumb -->
               <section class="store-breadcrumbs">
                   <div class="row">
                       <div class="col-lg-12">
                           <nav aria-label="breadcrumb">
                               <ol class="breadcrumb">
                                   <li class="breadcrumb-item"><a href="/dashboard.html">Profile</a></li>
                                   <li class="breadcrumb-item active" aria-current="page">Data Admin</li>
                               </ol>
                           </nav>
                       </div>
                   </div>
               </section>
           <!-- Judul -->
           <div class="row justify-content-between">
              
           </div>
           <!--Aksi Tambah Pengguna-->
           <br>
           <div class="container-fluid table" >
               <div class="">
                   <h5 class=" content-title data" >
                   Data Penerbit
                   {{-- <a href="{{url('profile-edit')}}"><button class="btn btn-white data" ><img src="super-admin/images/icon/pen_blue.svg" aria-hidden="true"> Edit</button></a> --}}
                   
               </h5> 
                  
               </div>
                      
               <hr>
               <table class="table table-striped ">
                   <tbody class="body-table">
                       <tr class="d-flex">
                           <th class="col-3">Avatar</th>
                           <td class="col-5"> 
                              <img class="rounded-circle mr-2 profile-picture" src="super-admin/images/icon-user.png" alt="">
                           </td>
                           <td class="col-4"> </td>
                       </tr>
                       <tr class="d-flex">
                           <th  class="col-3">Nama Lengkap</th>
                           <td class="col-5">
                               <p> {{Auth::user()->name}}</p>      
                           </td>
                           <td> </td>
                       </tr>
                       <tr class="d-flex">
                        <th class="col-3">E-mail</th>
                        <td class="col-5">
                            {{Auth::user()->email}}
                        </td>
                        <td> </td>
                    </tr>
                    <tr class="d-flex">
                        <th class="col-3">Sebagai</th>
                        <td class="col-5"> {{Auth::user()->role}}</p>
                        </td>
                        <td> </td>
                    </tr>
                   </tbody>
               </table>
               <br>
           </div>
           <br>
           <hr>
       <!-- <div class="container-fluid battom-nav">
           <button class="btn btn-white" ><img src="super-admin/images/icon/stop_blue.svg" aria-hidden="true"> Batal</button>
           <button class="btn btn-disabled" ><img src="super-admin/images/icon/plus_white.svg" aria-hidden="true"> Tambah  Pengguna</button>
            -->
           <!-- jika aktif -->
           <!-- <button class="btn btn-blue" ><img src="super-admin/images/icon/plus_white.svg" aria-hidden="true"> Tambah  Pengguna</button>
                                                       
       </div> -->
   </div>
   </div>
</div>
@endsection