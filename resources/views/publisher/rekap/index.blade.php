@extends('layouts.super-admin')
@section('content')
<div id="page-content-wrapper">
    <!--Content Dashboard-->
    <!-- ........................ -->
    <div class="section-content" data-aos="fade-up" data-aos-delay="500">
        <div class="container-fluid">
        <br>
            <div class="row">
                <div class="col-lg-12">
                    <div class="row justify-content-between">
                        <div class="col-lg-6">
                            <div class="dashboard-heading">
                                <h5 class="dashboard-title">
                                    Rekapitulasi Buku
                                </h5>
                                
                                <p class="dashboard-subtitle">
                                    Daftar buku yang terdistribusi
                                </p>
                            </div>
                        </div>
                    </div>
    
                    <div class="dashboard-heading">
                        <div class="container-fluid table" >
                            <br>
                            <table class="table table-striped " id="table-tambah-pengguna">
                                <thead class="head-table">
                                    <tr>
                                        <th>No</th>
                                        <th>Judul Buku</th>
                                        <th>Jumlah Eksemplar</th>
                                        <th>Instansi</th>
                                        <th>Harga</th>
                                    </tr>
                                </thead>
                                <tbody class="body-table">
                                    @foreach ($rekap as $item)
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{ isset ($item->book->title) ? $item->book->title : ''}}</td>
                                        <td>{{$item->qty}} Eksemplar</td>
                                        <td>{{ isset($item->agency->name) ? $item->agency->name : '' }}</td>
                                        <td>{{isset($item->book->price) ? $item->book->price : ''}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
