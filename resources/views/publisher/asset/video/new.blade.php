@extends('layouts.super-admin')
@section('content')
<form action="{{ route('videobook-baru.satuanpost') }}" method="post" enctype="multipart/form-data" class="needs-validation" novalidate>
    @csrf
    <div id="page-content-wrapper">
        <!--Content Dashboard-->
        <!-- ........................ -->
        <div class="section-content section-dashboard-bukuterbaru" data-aos="fade-up" data-aos-delay="500">
            <div class="container-fluid">
                <!-- Breandcrumb -->
                <section class="store-breadcrumbs">
                    <div class="row">
                        <div class="col-lg-12">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('book-penerbit.index') }}">Aset Baru</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Tambah Aset Videobook Baru</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </section>
                <!-- Judul -->
                <div class="row justify-content-between">
                    <div class="col-lg-6">
                        <div class="dashboard-heading">
                            <h5 class="dashboard-title">
                                Tambah Videobook Baru
                            </h5>
                            <p class="dashboard-subtitle">
                                Lakukan pengisian data pengguna dengan teliti
                            </p>
                        </div>
                    </div>
                </div>
               
                    @csrf
                    <div class="container-fluid table">
                        <h5 class=" content-title">
                            Data Videobook
                        </h5>
                        @if($errors->any())
                            <div class="alert alert-danger" role="alert">
                                @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                                @endforeach
                            </div>
                          @endif
                        <hr>
                        <table class="table table-striped " id="form-videobook">
                            <tbody class="body-table">
                                <tr class="d-flex">
                                    <th class="col-3">Cover</th>
                                    <td class="col-5">
                                        <input type="file" name="cover" class="form-control-file"
                                            id="exampleFormControlFile1" accept="image/jpg,image/png">
                                        @error('cover')
                                            <span class="help-block" style="color: red"> {{ $message }} </span>
                                        @enderror
                                    </td>
                                    <td class="col-4"> </td>
                                </tr>
                                <tr class="d-flex">
                                    <th class="col-3">Judul</th>
                                    <td class="col-5">
                                        <input type="text" name="title"
                                            class="form-control @error('title') is invalid @enderror"
                                            value="{{ old('title') }}" id="NamaLengkap" aria-describedby="nama"
                                            placeholder="Masukan nama videobook">
                                        @error('title')
                                            <span class="help-block" style="color:red"> {{ $message }} </span>
                                        @enderror
                                    </td>
                                    <td> </td>
                                </tr>
                                <tr class="d-flex">
                                    <th class="col-3">ISBN</th>
                                    <td class="col-5">
                                        <input type="text" name="isbn"
                                            class="form-control @error('isbn') is invalid @enderror"
                                            value="{{ old('isbn') }}" id="NoKaryawan" aria-describedby="nokaryawan"
                                            placeholder="ISBN">
                                        @error('isbn')
                                            <span class="help-block" style="color:red"> {{ $message }} </span>
                                        @enderror
                                    </td>
                                    <td> </td>
                                </tr>
                                <tr class="d-flex">
                                    <th class="col-3">Deskripsi</th>
                                    <td class="col-5">
                                        <textarea name="description" class="form-control @error('description') is invalid @enderror"
                                            value="{{ old('description') }}" id="TanggalLahir" cols="30"
                                            rows="10"></textarea>
                                        @error('description')
                                            <span class="help-block" style="color:red"> {{ $message }} </span>
                                        @enderror
                                    </td>
                                    <td> </td>

                                </tr>
                                {{-- <tr class="d-flex">
                                    <th class="col-3">Video</th>
                                    <td class="col-5"> 
                                        <input type="file" name="video" class="form-control @error('pdf') is invalid @enderror" value="{{old('pdf')}}" 
                                              id="upload-video" 
                                              aria-describedby="ttl"
                                              accept="video/mp4">
                                        @error('pdf')
                                            <span class="help-block" style="color:red"> {{$message}} </span>
                                        @enderror
                                    </td>
                                    <td> </td>
                                    
                                </tr> --}}
                                <tr class="d-flex">
                                    <th class="col-3">Video</th>
                                    <td class="col-5">
                                        <input type="file" name="pdf"
                                            class="form-control @error('pdf') is invalid @enderror"
                                            value="{{ old('pdf') }}" aria-describedby="ttl"
                                            accept="video/mp4">
                                        @error('pdf')
                                            <span class="help-block" style="color:red"> {{ $message }} </span>
                                        @enderror
                                    </td>
                                    <td> </td>

                                </tr>
                                <tr class="d-flex">
                                    <th class="col-3">Tahun Terbit</th>
                                    <td class="col-5">
                                        <input type="number" name="publication_year"
                                            class="form-control @error('publication_year') is invalid @enderror"
                                            value="{{ old('publication_year') }}" id="TanggalLahir" aria-describedby="ttl">
                                        @error('publication_year')
                                            <span class="help-block" style="color:red"> {{ $message }} </span>
                                        @enderror
                                    </td>
                                    <td> </td>

                                </tr>
                                <tr class="d-flex">
                                    <th class="col-3">Duration</th>
                                    <td class="col-5">
                                        <input type="time" name="duration"
                                            class="form-control @error('duration') is invalid @enderror"
                                            value="{{ old('duration') }}" id="duration" aria-describedby="duration">
                                        @error('duration')
                                            <span class="help-block" style="color:red"> {{ $message }} </span>
                                        @enderror
                                    </td>
                                    <td> </td>

                                </tr>

                                <tr class="d-flex">
                                    <th class="col-3">Harga</th>
                                    <td class="col-5">
                                        <input type="number" name="price" id="price"
                                            class="form-control @error('price') is invalid @enderror"
                                            value="{{ old('price') }}" id="TanggalLahir" aria-describedby="ttl">
                                        @error('price')
                                            <span class="help-block" style="color:red"> {{ $message }} </span>
                                        @enderror
                                    </td>
                                    <td> </td>

                                </tr>

                                <tr class="d-flex">
                                    <th class="col-3">Pengarang</th>
                                    <td class="col-5">
                                        <input type="text" name="writer"
                                            class="form-control @error('writer') is invalid @enderror"
                                            value="{{ old('writer') }}" id="TanggalLahir" aria-describedby="ttl">
                                        @error('writer')
                                            <span class="help-block" style="color:red"> {{ $message }} </span>
                                        @enderror
                                    </td>
                                    <td> </td>

                                </tr>

                                <tr class="d-flex">
                                    <th class="col-3">Kategori</th>
                                    <td class="col-5">
                                        <select name="category_id" id="category_id"
                                            class="form-control @error('category_id') is invalid @enderror" tabindex="1">
                                            <option>-- Pilih Kategori --</option>
                                            @foreach ($category as $id => $name)
                                                <option value="{{ $id }}">{{ $name }}</option>
                                            @endforeach
                                        </select>
                                        @error('category_id')
                                            <span class="help-block" style="color:red"> {{ $message }} </span>
                                        @enderror

                                    </td>
                                    <td> </td>

                                </tr>

                                <tr class="d-flex">
                                    <th class="col-3">Sub Kategori</th>
                                    <td class="col-5">
                                        <select name="sub_category_id" id="sub_category_id"
                                            class="form-control @error('sub_category_id') is invalid @enderror"
                                            tabindex="1">
                                            <option class="subcategory">-- Pilih Sub Kategori --</option>

                                        </select>
                                        @error('sub_category_id')
                                            <span class="help-block" style="color:red"> {{ $message }} </span>
                                        @enderror

                                    </td>
                                    <td> </td>

                                </tr>
                                <tr class="d-flex">
                                    <th class="col-3">Ada Chapter</th>
                                    <td class="col-5">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input @error('chapter') is invalid @enderror"
                                                type="radio" name="chapter" id="chp1" value="1">
                                            <label class="form-check-label" for="inlineRadio1">Ya</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input @error('chapter') is invalid @enderror"
                                                type="radio" name="chapter" id="chp0" value="0" checked>
                                            <label class="form-check-label" for="inlineRadio2">Tidak</label>
                                        </div>

                                        @error('chapter')
                                            <span class="help-block" style="color:red"> {{ $message }} </span>
                                        @enderror
                                    <td> </td>

                                </tr>
                            </tbody>
                        </table>
                        <br>
                    </div>
                    <hr>
                    

                    <div class="container-fluid battom-nav text-right">
                        <button type="button" class="btn btn-white"><img src="{{ asset('super-admin/images/icon/stop_blue.svg') }}"
                                aria-hidden="true"> Batal</button>
                        <!-- jika aktif -->
                        <button type="submit" class="btn btn-blue"><img
                                src="{{ asset('super-admin/images/icon/plus_white.svg') }}" aria-hidden="true">
                            Simpan</button>
                    </div>
                    <br>
               

            </div>
        </div>

    </div>
</form>

@push('after-script')
    <script>
        $(document).ready(function() {
            $('#category_id').on('change', function() {
                var category = $(this).val();
                console.log(category);
                $.ajax({
                    type: "GET",
                    url: "/category/sub?category_id="+category,
                    dataType: "json",
                    success: function (response) {
                        // console.log(response);
                        if (response) {
                            $('#sub_category_id').empty();
                            $('#sub_category_id').append('<option>---Pilih Sub Category---</option>');

                            $.each(response, function(index, value) {
                            // console.log(value);
                            $('#sub_category_id').append('<option name="sub_category_id" value ="'+value.id+'">' + value.name + '</option>')});
                        } else {
                            $('#sub_category_id').empty();
                        }
                        
                    }
                });
            });

        });

        // $("form-videobook tbody").on("click", "#chp1", function() {
        //     $("#formChapter").toggle('slow');
        // });
        // $("form-videobook tbody").on("click", "#chp0", function() {
        //     $("#formChapter").hide('slow');
        // });

       

        
        var myVideos = [];

        window.URL = window.URL || window.webkitURL;
        document.getElementById('upload-video').onchange = setFileInfo;
        function setFileInfo() {
        var files = this.files;
        myVideos.push(files[0]);
        var video = document.createElement('video');
        video.preload = 'metadata';

        video.onloadedmetadata = function() {
            window.URL.revokeObjectURL(video.src);
            var duration = video.duration;
            var dur = duration.toString()
            var result = dur.replaceAll('.', '');
            console.log(result)
            console.log()
            $("#duration").val(msToTime(result))
          
        }

        video.src = URL.createObjectURL(files[0]);;
        }



        $(".videoChapter").on("change",function(e){
            var file = this.files[0]; // Get uploaded file
            validateFile(file) // Validate Duration
        })

        function validateFile(file) {

        var video = document.createElement('video');
        video.preload = 'metadata';
        video.onloadedmetadata = function() {
            window.URL.revokeObjectURL(video.src);
            if (video.duration < 1) {
                console.log("Invalid Video! video is less than 1 second");
                return;
            }
            methodToCallIfValid();
        }
        video.src = URL.createObjectURL(file);
        }


    
        function msToTime(duration) {
            var milliseconds = Math.floor((duration % 1000) / 100),
                seconds = Math.floor((duration / 1000) % 60),
                minutes = Math.floor((duration / (1000 * 60)) % 60),
                hours = Math.floor((duration / (1000 * 60 * 60)) % 24);

            hours = (hours < 10) ? "0" + hours : hours;
            minutes = (minutes < 10) ? "0" + minutes : minutes;
            seconds = (seconds < 10) ? "0" + seconds : seconds;

            return hours + ":" + minutes + ":" + seconds;
        }
        // console.log(msToTime(300000))


    </script>
@endpush
@endsection
