@extends('layouts.super-admin')
@section('content')
<div id="page-content-wrapper">
    <!--Content Dashboard-->
    <!-- ........................ -->
    <div class="section-content section-dashboard-bukuterbaru" data-aos="fade-up" data-aos-delay="500">
         <div class="container-fluid">
        <!-- Breandcrumb -->
                <section class="store-breadcrumbs">
                    <div class="row">
                        <div class="col-lg-12">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{route('book-penerbit.index')}}">Aset</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Tambah Aset Buku</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </section>
            <!-- Judul -->
            <div class="row justify-content-between">
                <div class="col-lg-6">
                    <div class="dashboard-heading">
                        <h5 class="dashboard-title">
                            Tambah Buku
                        </h5>
                        <p class="dashboard-subtitle">
                            Lakukan pengisian data pengguna dengan teliti
                        </p>
                    </div>
                </div>
            </div>
            <!--Aksi Tambah Pengguna-->
            <br>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form action="{{ route('book-penerbit.store')}}" method="post" enctype="multipart/form-data" class="needs-validation" novalidate>
                @csrf
                <div class="container-fluid table" >
                    <h5 class=" content-title" >
                        Data Buku
                    </h5> 
                    <hr>
                    <table class="table table-striped ">
                        <tbody class="body-table">
                            <tr class="d-flex">
                                <th class="col-3">Cover</th>
                                <td class="col-5"> 
                                    <input type="file" name="cover" class="form-control-file" id="exampleFormControlFile1" accept="image/jpg,image/png">
                                    
                                    @error('cover')
                                        <span class="help-block" style="color:red"> {{$message}} </span>
                                    @enderror
                                </td>
                                <td class="col-4"> </td>
                            </tr>
                            <tr class="d-flex">
                                <th  class="col-3">Judul</th>
                                <td class="col-5">
                                    <input type="text" name="title" 
                                            class="form-control @error('title') is invalid @enderror" value="{{old('title')}}" 
                                            id="NamaLengkap" 
                                            aria-describedby="nama" 
                                            placeholder="Masukan nama buku">
                                    @error('title')
                                        <span class="help-block" style="color:red"> {{$message}} </span>
                                    @enderror
                                </td>
                                <td> </td>
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">ISBN</th>
                                <td class="col-5">
                                    <input type="text" name="isbn" class="form-control @error('isbn') is invalid @enderror" value="{{old('isbn')}}" 
                                            id="NoKaryawan" 
                                            aria-describedby="nokaryawan" 
                                            placeholder="ISBN">
                                    @error('isbn')
                                        <span class="help-block" style="color:red"> {{$message}} </span>
                                    @enderror
                                </td>
                                <td> </td>
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">Deskripsi</th>
                                <td class="col-5"> 
                                    <textarea name="description" class="form-control @error('description') is invalid @enderror" value="{{old('description')}}" 
                                            id="TanggalLahir" 
                                            cols="30" rows="10"></textarea>
                                    @error('description')
                                        <span class="help-block" style="color:red"> {{$message}} </span>
                                    @enderror
                                </td>
                                <td> </td>
                                
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">PDF</th>
                                <td class="col-5"> 
                                    <input type="file" name="pdf" class="form-control @error('pdf') is invalid @enderror" value="{{old('pdf')}}" 
                                          id="TanggalLahir" 
                                          aria-describedby="ttl"
                                          accept="application/pdf,application/epub">
                                    @error('pdf')
                                        <span class="help-block" style="color:red"> {{$message}} </span>
                                    @enderror
                                </td>
                                <td> </td>
                                
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">Tahun Terbit</th>
                                <td class="col-5"> 
                                    <input type="number" name="publication_year" class="form-control @error('publication_year') is invalid @enderror" value="{{old('publication_year')}}" 
                                            id="TanggalLahir" 
                                            aria-describedby="ttl">
                                    @error('publication_year')
                                        <span class="help-block" style="color:red"> {{$message}} </span>
                                    @enderror
                                </td>
                                <td> </td>
                                
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">Halaman</th>
                                <td class="col-5"> 
                                    <input type="number" name="page" class="form-control @error('page') is invalid @enderror" value="{{old('page')}}"
                                            id="TanggalLahir" 
                                            aria-describedby="ttl">
                                    @error('page')
                                        <span class="help-block" style="color:red"> {{$message}} </span>
                                    @enderror
                                </td>
                                <td> </td>
                                
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">Harga</th>
                                <td class="col-5"> 
                                    <input type="number" name="price" id="price" class="form-control @error('price') is invalid @enderror" value="{{old('price')}}"
                                            id="TanggalLahir" 
                                            aria-describedby="ttl">
                                    @error('price')
                                        <span class="help-block" style="color:red"> {{$message}} </span>
                                    @enderror
                                </td>
                                <td> </td>
                                
                            </tr>

                            <tr class="d-flex">
                                <th class="col-3">Pengarang</th>
                                <td class="col-5"> 
                                    <input type="text" name="writer" class="form-control @error('writer') is invalid @enderror" value="{{old('writer')}}"
                                            id="TanggalLahir" 
                                            aria-describedby="ttl">
                                    @error('writer')
                                        <span class="help-block" style="color:red"> {{$message}} </span>
                                    @enderror
                                </td>
                                <td> </td>
                                
                            </tr>
                            
                            <tr class="d-flex">
                                <th class="col-3">Kategori</th>
                                <td class="col-5"> 
                                    <select name="category_id" id="category" class="form-control @error('category_id') is invalid @enderror" tabindex="1">
                                        <option>-- Pilih Kategori --</option>
                                        @foreach ($category as $id => $name)
                                            <option value="{{$id}}">{{$name}}</option>
                                        @endforeach
                                    </select>
                                    @error('category_id')
                                            <span class="help-block" style="color:red"> {{$message}} </span>
                                    @enderror
                                    
                                </td>
                                <td> </td>
                                
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">Sub Kategori</th>
                                <td class="col-5"> 
                                    <select name="sub_category_id" id="sub_category_id" class="form-control @error('sub_category_id') is invalid @enderror" tabindex="1">
                                        <option class="delete-sub-category">-- Pilih Sub Kategori --</option>
                                    </select>
                                    @error('sub_category_id')
                                            <span class="help-block" style="color:red"> {{$message}} </span>
                                    @enderror
                                    
                                </td>
                                <td> </td>
                                
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">Buku Paket</th>
                                <td class="col-5"> 
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input @error('flag_packet') is invalid @enderror" type="radio" name="flag_packet" id="inlineRadio1" value="1">
                                        <label class="form-check-label" for="inlineRadio1">Ya</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input @error('flag_packet') is invalid @enderror" type="radio" name="flag_packet" id="inlineRadio2" value="0" checked>
                                        <label class="form-check-label" for="inlineRadio2">Tidak</label>
                                    </div>

                                    @error('flag_packet')
                                      <span class="help-block" style="color:red"> {{$message}} </span>
                                    @enderror
                                </td>
                                <td> </td>
                                
                            </tr>
                        </tbody>
                    </table>
                    <br>
                </div>
                <hr>
                <div class="container-fluid battom-nav text-right" >
                    <button class="btn btn-white" ><img src="{{ asset('super-admin/images/icon/stop_blue.svg')}}" aria-hidden="true"> Batal</button>
                    <!-- jika aktif -->
                    <button type="submit" class="btn btn-blue" ><img src="{{ asset('super-admin/images/icon/plus_white.svg')}}" aria-hidden="true"> Tambah Buku</button>                                                
                </div>
                <br>
            </form>
        </div>
    </div>
   
</div>
@push('after-script')
    <script>
        $(document).ready(function(){
            $("#category").on("change",function(){
                // alert( this.value );
                $(".delete-sub-category").remove();
                let id = this.value
                $.ajax({
                    type: "GET",
                    // url: "{{ route('agent.member') }}"+'/'+agentID,
                    // url: '/agent-member/'+agentID,
                    url: `/api/sub-category/${id}`,
                    dataType: "json",
                    success: function(res) {
                        // console.log(res);
                        $.each(res.data, function(index, value){
                            console.log(value)
                            $("#sub_category_id").append(`<option class="delete-sub-category" value="${value.id}">${value.name}</option>`)
                        })
                    }
                });

            });

        });

        
    </script>
@endpush
@endsection

