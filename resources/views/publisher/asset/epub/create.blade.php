@extends('layouts.super-admin')
@section('content')
<div id="page-content-wrapper">
    <!--Content Dashboard-->
    <!-- ........................ -->
    <div class="section-content section-dashboard-bukuterbaru" data-aos="fade-up" data-aos-delay="500">
         <div class="container-fluid">
        <!-- Breandcrumb -->
                <section class="store-breadcrumbs">
                    <div class="row">
                        <div class="col-lg-12">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{route('book-penerbit.index')}}">Aset</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Tambah Aset Epub</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </section>
            <!-- Judul -->
            <div class="row justify-content-between">
                <div class="col-lg-6">
                    <div class="dashboard-heading">
                        <h5 class="dashboard-title">
                            Tambah Epub
                        </h5>
                        <p class="dashboard-subtitle">
                            Lakukan pengisian data pengguna dengan teliti
                        </p>
                    </div>
                </div>
            </div>
            <!--Aksi Tambah Pengguna-->
            <br>
            <form action="{{ route('videobook-penerbit.store')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="container-fluid table" >
                    <h5 class=" content-title" >
                        Data Buku
                    </h5> 
                    <hr>
                    <table class="table table-striped ">
                        <tbody class="body-table">
                            <tr class="d-flex">
                                <th class="col-3">Cover</th>
                                <td class="col-5"> 
                                    <input type="file" name="cover" class="form-control-file" id="exampleFormControlFile1" accept="image/jpg,image/png">
                                    @error('cover')
                                        <span class="help-block"> {{$message}} </span>
                                    @enderror
                                </td>
                                <td class="col-4"> </td>
                            </tr>
                            <tr class="d-flex">
                                <th  class="col-3">Judul</th>
                                <td class="col-5">
                                    <input type="text" name="title" 
                                            class="form-control @error('title') is invalid @enderror" value="{{old('title')}}" 
                                            id="NamaLengkap" 
                                            aria-describedby="nama" 
                                            placeholder="Masukan nama buku">
                                    @error('title')
                                        <span class="help-block"> {{$message}} </span>
                                    @enderror
                                </td>
                                <td> </td>
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">ISBN</th>
                                <td class="col-5">
                                    <input type="text" name="isbn" class="form-control @error('isbn') is invalid @enderror" value="{{old('isbn')}}" 
                                            id="NoKaryawan" 
                                            aria-describedby="nokaryawan" 
                                            placeholder="ISBN">
                                    @error('isbn')
                                        <span class="help-block"> {{$message}} </span>
                                    @enderror
                                </td>
                                <td> </td>
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">Deskripsi</th>
                                <td class="col-5"> 
                                    <textarea name="description" class="form-control @error('description') is invalid @enderror" value="{{old('description')}}" 
                                            id="TanggalLahir" 
                                            cols="30" rows="10"></textarea>
                                    @error('description')
                                        <span class="help-block"> {{$message}} </span>
                                    @enderror
                                </td>
                                <td> </td>
                                
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">Video</th>
                                <td class="col-5"> 
                                    <input type="file" name="pdf" class="form-control @error('pdf') is invalid @enderror" value="{{old('pdf')}}" 
                                          id="TanggalLahir" 
                                          aria-describedby="ttl"
                                          accept="video/mp4">
                                    @error('pdf')
                                        <span class="help-block"> {{$message}} </span>
                                    @enderror
                                </td>
                                <td> </td>
                                
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">Tahun Terbit</th>
                                <td class="col-5"> 
                                    <input type="number" name="publication_year" class="form-control @error('publication_year') is invalid @enderror" value="{{old('publication_year')}}" 
                                            id="TanggalLahir" 
                                            aria-describedby="ttl">
                                    @error('publication_year')
                                        <span class="help-block"> {{$message}} </span>
                                    @enderror
                                </td>
                                <td> </td>
                                
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">Duration</th>
                                <td class="col-5"> 
                                    <input type="time" name="duration" class="form-control @error('duration') is invalid @enderror" value="{{old('duration')}}"
                                            id="duration" 
                                            aria-describedby="duration" required>
                                    @error('duration')
                                        <span class="help-block"> {{$message}} </span>
                                    @enderror
                                </td>
                                <td> </td>
                                
                            </tr>
                            
                            <tr class="d-flex">
                                <th class="col-3">Harga</th>
                                <td class="col-5"> 
                                    <input type="number" name="price" id="price" class="form-control @error('price') is invalid @enderror" value="{{old('price')}}"
                                            id="TanggalLahir" 
                                            aria-describedby="ttl">
                                    @error('price')
                                        <span class="help-block"> {{$message}} </span>
                                    @enderror
                                </td>
                                <td> </td>
                                
                            </tr>

                            <tr class="d-flex">
                                <th class="col-3">Pengarang</th>
                                <td class="col-5"> 
                                    <input type="text" name="author" class="form-control @error('author') is invalid @enderror" value="{{old('author')}}"
                                            id="TanggalLahir" 
                                            aria-describedby="ttl">
                                    @error('author')
                                        <span class="help-block"> {{$message}} </span>
                                    @enderror
                                </td>
                                <td> </td>
                                
                            </tr>
                            
                            <tr class="d-flex">
                                <th class="col-3">Kategori</th>
                                <td class="col-5"> 
                                    <select name="category_id" id="category_id" class="form-control @error('category_id') is invalid @enderror" tabindex="1">
                                        <option>-- Pilih Kategori --</option>
                                        @foreach ($category as $id => $name)
                                            <option value="{{$id}}">{{$name}}</option>
                                        @endforeach
                                    </select>
                                    @error('category_id')
                                            <span class="help-block"> {{$message}} </span>
                                    @enderror
                                    
                                </td>
                                <td> </td>
                                
                            </tr>

                            <tr class="d-flex">
                                <th class="col-3">Sub Kategori</th>
                                <td class="col-5"> 
                                    <select name="sub_category_id" id="sub_category_id" class="form-control @error('sub_category_id') is invalid @enderror" tabindex="1">
                                        <option>-- Pilih Sub Kategori --</option>
                                       
                                    </select>
                                    @error('sub_category_id')
                                            <span class="help-block"> {{$message}} </span>
                                    @enderror
                                    
                                </td>
                                <td> </td>
                                
                            </tr>
                        </tbody>
                    </table>
                    <br>
                </div>
                <hr>
                <div class="container-fluid battom-nav text-right" >
                    <button class="btn btn-white" ><img src="{{ asset('super-admin/images/icon/stop_blue.svg')}}" aria-hidden="true"> Batal</button>
                    <!-- jika aktif -->
                    <button type="submit" class="btn btn-blue" ><img src="{{ asset('super-admin/images/icon/plus_white.svg')}}" aria-hidden="true"> Tambah Buku</button>                                                
                </div>
                <br>
            </form>
        </div>
    </div>
   
</div>
@endsection
@push('after-script')
    <script>
        $(document).ready(function () {
            $('#category_id').on('change',function(){
                var category = $(this).val();
                console.log(category);
                if (category) {
                    $.ajax({
                        type : "GET",
                        url : "/category/sub?category_id="+category,
                        json : "json",
                        success:function(data){
                            console.log('oke')
                            if (data) {
                                console.log(data)
                                $('#sub_category_id').empty();
                                $('#sub_category_id').append(
                                    '<option>---Pilih Sub Category---</option>');
                                $.each(data, function(name, id) {
                                    $('#sub_category_id').append('<option name="sub_category_id" value ="'+id+'">'+name+'</option>')
                                });
                            } else {
                                $('#sub_category_id').empty();
                            }
                        }
                    });
                } else {
                    $('#sub_category_id').empty();
                }
            });
        });
    </script>
@endpush

