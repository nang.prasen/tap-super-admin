@extends('layouts.super-admin')
@section('content')
<div class="section-content" data-aos="fade-up" data-aos-delay="500">
    <div class="container-fluid">
    <br>
        <div class="row">
            <div class="col-lg-12">
            
                <div class="row justify-content-between">
                    <div class="col-lg-6">
                        <div class="dashboard-heading">
                            <h5 class="dashboard-title">
                            
                                Daftar Buku 
                            </h5>
                            <p class="dashboard-subtitle">
                                Daftar buku penerbit
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-6" style="text-align: right;">
                        <div class="dashboard-heading">
                            <div class="btn-group dropleft">
                                <button type="button" class="btn btn-blue" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="super-admin/images/icon/plus_white.svg" aria-hidden="true"> Tambah Aset</button>
                                <div class="dropdown-menu">
                                    <!-- Dropdown menu links -->
                                    <a class="dropdown-item " href="{{route('book-penerbit.create')}}"> <img src="https://daftar.troyaacademic.com/super-admin/images/icon/katalog.svg" alt=""> Buku</a>
                                    <a class="dropdown-item" href="{{route('videobook-penerbit.create')}}"><img src="super-admin/images/icon/edit.svg" alt=""> Videobook</a>
                                    {{-- <a class="dropdown-item" href="{{route('videobook-penerbit.satuan')}}"><img src="super-admin/images/icon/edit.svg" alt=""> Videobook Satuan</a> --}}
                                    <a class="dropdown-item" href="{{route('videobook-baru.satuan')}}"><img src="super-admin/images/icon/edit.svg" alt=""> Videobook Satuan Baru</a>
                                    <a class="dropdown-item" href="{{route('audiobook-penerbit.create')}}"><i class="fa fa-volume-up"></i> Audio</a>
                                    <a class="dropdown-item" href="{{route('epub-penerbit.create')}}"><i class="fa fa-leanpub"></i> Epub</a>
                                    <a class="dropdown-item" href="#"><i class="fa fa-gamepad"></i> Game</a>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="dashboard-heading">
                    <div class="container-fluid table" >
                        <br>
                        <table class="table table-striped " id="table-tambah-pengguna">
                            <thead class="head-table">
                                <tr>
                                    <th>No</th>
                                    <th>Judul</th>
                                    <th>ISBN</th>
                                    <th>Harga</th>
                                    <th>Jumlah Halaman</th>
                                    <th>Kategori</th>
                                    <th>Tipe</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody class="body-table">
                                @foreach ($book as $item)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$item->title}}</td>
                                    <td>{{$item->isbn}}</td>
                                    <td>@currency($item->price) </td>
                                    <td>{{$item->page}} halaman</td>
                                    <td>{{$item->category->name}}</td>
                                    <td>
                                        @if ($item->type == 1)
                                            <p>PDF</p>
                                        @elseif($item->type == 2)
                                            @if ($item->chapter ==1)
                                            <p>Chapter</p>
                                            @endif
                                            <p>Videobook</p>
                                        @elseif($item->type == 3)
                                            <p>Audiobook</p>
                                        @elseif($item->type == 4)
                                            <p>Epub</p>
                                        @endif
                                    </td>

                                    <td>
                                        <!-- Default dropleft button -->
                                        <div class="btn-group dropleft">
                                            <button type="button" class="btn btn-secondary fa fa-ellipsis-v" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                            <div class="dropdown-menu">
                                                <!-- Dropdown menu links -->
                                                @if ($item->chapter == 1)
                                                    <a class="dropdown-item" href="{{route('videobook-penerbit.chapter', $item->id)}}"><i class="fa fa-edit"></i> Tambah Chapter</a>
                                                    <a class="dropdown-item " href="{{route('book-penerbit.show',$item->id)}}"> <img src="super-admin/images/icon/profile.svg" alt=""> Detail</a>
                                                    <a class="dropdown-item" href="{{route('book-penerbit.rekap',$item->id)}}"><img src="super-admin/images/icon/edit.svg" alt=""> Rekapitulasi</a>
                                                    
                                                @else
                                                    
                                                <a class="dropdown-item " href="{{route('book-penerbit.show',$item->id)}}"> <img src="super-admin/images/icon/profile.svg" alt=""> Detail</a>
                                                <a class="dropdown-item" href="{{route('book-penerbit.rekap',$item->id)}}"><img src="super-admin/images/icon/edit.svg" alt=""> Rekapitulasi</a>
                                                @endif

                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('after-script')
    <script>
        $(function() {
            var show_modal = "{{ session()->pull('status') }}";

            if (typeof show_modal !== 'undefined' && show_modal) {
                $('#modalWarning').modal('show');
                // This will open up the modal if the variable is present in session as true
                // OR you can simply show an alert message!
                // alert("Data has been submitted");
            }
        });
    </script>
@endpush
<div class="modal fade bd-example-modal-sm" id="modalWarning" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Peringatan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Berhasil menambahkan asset
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
