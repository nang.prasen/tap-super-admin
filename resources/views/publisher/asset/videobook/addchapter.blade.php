@extends('layouts.super-admin')
@section('content')
<div class="section-content" data-aos="fade-up" data-aos-delay="500">
    <div class="container-fluid">
        <br>
        <div class="row">
            <div class="col-lg-12">

                <div class="row justify-content-between">
                    <div class="col-lg-6">
                        <div class="dashboard-heading">
                            <h5 class="dashboard-title">

                                Daftar Chapter
                            </h5>
                            <p class="dashboard-subtitle">
                                Buku {{$book->title}}
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-6" style="text-align: right;">
                        <div class="dashboard-heading">
                            <div class="btn-group dropleft">

                                <button type="button" class="btn btn-blue" data-toggle="modal"
                                    data-target="#exampleModal1">
                                    <img src="super-admin/images/icon/plus_white.svg" aria-hidden="true"> Tambah Chapter
                                </button>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="dashboard-heading">
                    <div class="container-fluid table">
                        <br>
                        <table class="table table-striped ">
                            <thead class="head-table">
                                <tr>
                                    <th>No</th>
                                    <th>Judul Chapter</th>
                                    <th>Link Chapter</th>
                                    <th>Deskripsi</th>
                                    <th>Durasi</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody class="body-table">
                                @foreach ($chapters as $c)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$c->title}}</td>
                                    <td> <a href="{{$c->chapter}}">Lihat Video</a> </td>
                                    <td>{{$c->descirption}}</td>
                                    <td>{{$c->duration}}</td>
                                    <td> 
                                        <button type="button" class="btn btn-blue button_edit" data-toggle="modal"
                                    data-target="#modalEdit" data-id="{{$c->id}}" data-judul="{{$c->title}}" data-deskripsi="{{$c->descirption}}" data-duration="{{$c->duration}}">
                                     Edit
                                </button>
                                        <a href="" class="btn btn-blue">Delete</a> </td>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('after-script')
<script>
    
    var myVideos = [];

    window.URL = window.URL || window.webkitURL;
    document.getElementById('uploadVideo').onchange = setFileInfo;
    function setFileInfo() {
        var files = this.files;
        myVideos.push(files[0]);
        var video = document.createElement('video');
        video.preload = 'metadata';

        video.onloadedmetadata = function() {
            window.URL.revokeObjectURL(video.src);
            var duration = video.duration;
            var dur = duration.toString()
            var result = dur.replaceAll('.', '');
            console.log(result)
            console.log()
            $("#durasi").val(msToTime(result))
        
        }

        video.src = URL.createObjectURL(files[0]);;
    }

    document.getElementById('video_edit').onchange = setFileInfo;
    function setFileInfo() {
        var files = this.files;
        myVideos.push(files[0]);
        var video = document.createElement('video');
        video.preload = 'metadata';

        video.onloadedmetadata = function() {
            window.URL.revokeObjectURL(video.src);
            var duration = video.duration;
            var dur = duration.toString()
            var result = dur.replaceAll('.', '');
            console.log(result)
            console.log()
            $("#duration_edit").val(msToTime(result))
        
        }

        video.src = URL.createObjectURL(files[0]);;
    }




    function msToTime(duration) {
        var milliseconds = Math.floor((duration % 1000) / 100),
            seconds = Math.floor((duration / 1000) % 60),
            minutes = Math.floor((duration / (1000 * 60)) % 60),
            hours = Math.floor((duration / (1000 * 60 * 60)) % 24);

        hours = (hours < 10) ? "0" + hours : hours;
        minutes = (minutes < 10) ? "0" + minutes : minutes;
        seconds = (seconds < 10) ? "0" + seconds : seconds;

        return hours + ":" + minutes + ":" + seconds;
    }

  
    $(".button_edit").click(function(){
        let id = $(this).attr("data-id");
        let judul = $(this).attr("data-judul");
        // let video = $(this).attr("data-video");
        // let chpater = $(this).attr("data-chpater");
        let deskripsi = $(this).attr("data-deskripsi");
        let duration = $(this).attr("data-duration");
        let cover = $(this).attr("data-cover");
        $("#chapter_id").val(id)
        // $("#cover_edit").val(cover)
        $("#judul_edit").val(judul)
        $("#deskripsi_edit").text(deskripsi)
        // $("#video_edit").val(video)
        $("#duration_edit").val(duration)
        
    });


</script>
@endpush


<div class="modal fade bd-example-modal-lg" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Chapter</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('videobook-penerbit.chapterpost') }}" method="post" enctype="multipart/form-data">
            <div class="modal-body">
                    @csrf

                    <div class="container-fluid table" id="formChapter">
                        <h5 class=" content-title">
                            Chapter
                        </h5>
                        <table class="table table-striped">
                            <tbody class="body-table">
                                <tr class="d-flex">
                                    <th class="col-3">Thumbnail Chapter</th>
                                    <td class="col-5">
                                        <input type="hidden" name="book_id" value="{{$book->id}}">
                                        <input type="file" name="cover" class="form-control-file"
                                            id="exampleFormControlFile1" accept="image/jpg,image/png" required>
                                        @error('cover')
                                        <span class="help-block" style="color: red"> {{ $message }} </span>
                                        @enderror
                                    </td>
                                    <td class="col-4"> </td>    
                                </tr>
                                <tr class="d-flex">
                                    <th class="col-3">Judul</th>
                                    <td class="col-5">
                                        <input type="text" name="judul"
                                            class="form-control @error('judul') is invalid @enderror"
                                            value="{{ old('judul') }}" id="NamaLengkap" aria-describedby="nama"
                                            placeholder="Masukan nama judul chapter videobook">
                                        @error('judul')
                                        <span class="help-block" style="color:red"> {{ $message }} </span>
                                        @enderror
                                    </td>
                                    <td> </td>
                                </tr>
                                <tr class="d-flex">
                                    <th class="col-3">Deskripsi</th>
                                    <td class="col-5">
                                        <textarea name="deskripsi"
                                            class="form-control @error('deskripsi') is invalid @enderror"
                                            value="{{ old('deskripsi') }}" id="TanggalLahir" cols="30"
                                            rows="10"></textarea>
                                        @error('deskripsi')
                                        <span class="help-block" style="color:red"> {{ $message }} </span>
                                        @enderror
                                    </td>
                                    <td> </td>

                                </tr>
                                <tr class="d-flex">
                                    <th class="col-3">Video Chapter</th>
                                    <td class="col-5">
                                        <input type="file" name="video"
                                            class="form-control videoChapter @error('video') is invalid @enderror"
                                            value="{{ old('video') }}" id="uploadVideo" aria-describedby="ttl"
                                            accept="video/mp4">
                                        @error('video')
                                        <span class="help-block" style="color:red"> {{ $message }} </span>
                                        @enderror
                                    </td>
                                    <td> </td>

                                </tr>
                                <tr class="d-flex">
                                    <th class="col-3">Duration</th>
                                    <td class="col-5">
                                        <input type="time"  name="durasi"
                                            class="form-control @error('durasi') is invalid @enderror"
                                            value="{{ old('durasi') }}" id="durasi" aria-describedby="durasi">
                                        @error('durasi')
                                        <span class="help-block" style="color:red"> {{ $message }} </span>
                                        @enderror
                                    </td>
                                    <td> </td>

                                </tr>
                            </tbody>
                            <br>
                        </table>
                    </div>
                

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
        </div>
    </div>
</div>



{{-- modal edit --}}
<div class="modal fade bd-example-modal-lg" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Chapter</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('videobook-penerbit.chapterput') }}" method="post" enctype="multipart/form-data"
                  >
                    @csrf
                    @method('PUT')

                    <div class="container-fluid table" id="formChapter">
                        <h5 class=" content-title">
                            Chapter
                        </h5>
                        <table class="table table-striped">
                            <tbody class="body-table">
                                <tr class="d-flex">
                                    <th class="col-3">Thumbnail Chapter</th>
                                    <td class="col-5">
                                        <input type="hidden" name="book_id" value="{{$book->id}}">
                                        <input type="hidden" name="chapter_id" id="chapter_id">
                                        <input type="file" name="cover" id="cover_edit" class="form-control-file"
                                            id="exampleFormControlFile1" accept="image/jpg,image/png">
                                        @error('cover')
                                        <span class="help-block" style="color: red"> {{ $message }} </span>
                                        @enderror
                                    </td>
                                    <td class="col-4"> </td>    
                                </tr>
                                <tr class="d-flex">
                                    <th class="col-3">Judul</th>
                                    <td class="col-5">
                                        <input type="text" name="judul"
                                            class="form-control @error('judul') is invalid @enderror"
                                            value="{{ old('judul') }}" id="judul_edit" aria-describedby="nama"
                                            placeholder="Masukan nama judul chapter videobook">
                                        @error('judul')
                                        <span class="help-block" style="color:red"> {{ $message }} </span>
                                        @enderror
                                    </td>
                                    <td> </td>
                                </tr>
                                <tr class="d-flex">
                                    <th class="col-3">Deskripsi</th>
                                    <td class="col-5">
                                        <textarea name="deskripsi"
                                            class="form-control @error('deskripsi') is invalid @enderror"
                                            value="{{ old('deskripsi') }}" id="deskripsi_edit" cols="30"
                                            rows="10"></textarea>
                                        @error('deskripsi')
                                        <span class="help-block" style="color:red"> {{ $message }} </span>
                                        @enderror
                                    </td>
                                    <td> </td>

                                </tr>
                                <tr class="d-flex">
                                    <th class="col-3">Video Chapter</th>
                                    <td class="col-5">
                                        <input type="file" name="video"
                                            class="form-control videoChapter @error('video') is invalid @enderror"
                                            value="{{ old('video') }}" id="video_edit" aria-describedby="ttl"
                                            accept="video/mp4">
                                        @error('video')
                                        <span class="help-block" style="color:red"> {{ $message }} </span>
                                        @enderror
                                    </td>
                                    <td> </td>

                                </tr>
                                <tr class="d-flex">
                                    <th class="col-3">Duration</th>
                                    <td class="col-5">
                                        <input  name="durasi"
                                            class="form-control @error('durasi') is invalid @enderror"
                                            value="{{ old('durasi') }}" id="duration_edit" aria-describedby="durasi" readonly>
                                        @error('durasi')
                                        <span class="help-block" style="color:red"> {{ $message }} </span>
                                        @enderror
                                    </td>
                                    <td> </td>

                                </tr>
                            </tbody>
                            <br>
                        </table>
                    </div>
                

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
        </div>
    </div>
</div>


