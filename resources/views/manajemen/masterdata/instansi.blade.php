@extends('layouts.super-admin')
@section('content')
    <div class="section-content" data-aos="fade-up" data-aos-delay="500">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row justify-content-between">
                        <div class="col-lg-6">
                            <div class="dashboard-heading">
                                <h5 class="dashboard-title">
                                    Daftar Instansi
                                </h5>
                                <p class="dashboard-subtitle">
                                    Daftar Instansi Pengguna TAP
                                </p>

                            </div>
                        </div>

                    </div>
                    {{-- Alert berhasil --}}
                    {{-- @if (session('status'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>Sukses!</strong> {{ session('status') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                    </div>
                @endif --}}

                    <div class="dashboard-heading">
                        <div class="container">
                            <h5 class="dashboard-title">
                                Filter Lokasi
                            </h5>
                            <hr>
                            <!-- Table Bootstrap -->
                            {{-- <form method="POST" id="filter-form" class="form-inline"> --}}
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="filter">Provinsi</label>
                                        <select class="form-control" name="province_id" id="provinsi" required>
                                            <option value="0">---Pilih Provinsi---</option>
                                            @foreach ($wilayah as $id => $name)
                                                <option name="province_id" value="{{ $id }}">{{ $name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="filter">Kab/Kota</label>
                                        <select class="form-control" name="city_id" id="kota" required>
                                            <option value="0">---Pilih Kab/Kota---</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="filter">Kecamatan</label>
                                        <select class="form-control" name="district_id" id="kecamatan" required>
                                            <option value="0">---Pilih Kecamatan---</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="filter">Jenjang</label>
                                        <select name="degree" id="degree" class="form-control">
                                            <option value="0">---Pilih Jenjang---</option>
                                            <option value="1">SD</option>
                                            <option value="2">SMP</option>
                                            <option value="3">SMA</option>
                                            <option value="4">SMK</option>
                                            <option value="5">PERGURUAN TINGGI</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <button type="submit" class="btn btn-blue" id="btn-filter">Filter</button>
                                        <button type="button" class="btn btn-blue" id="btn-reset-filter">Reset</button>
                                    </div>
                                </div>
                            {{-- </form> --}}
                            
                            <br>


                        </div>
                        <div class="container-fluid table">

                            <br>
                            <table class="table table-striped tabelmanajemen">
                                <thead class="head-table">
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Instansi </th>
                                        <th>Alamat</th>
                                        <th>Penanggung Jawab</th>
                                        <th>No Pengelola</th>
                                        <th>Status</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody class="body-table">
                                    {{-- @foreach ($agency as $item)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $item->name }}</td>
                                            <td>{{ $item->address }}</td>
                                            <td>{{ $item->person_responsible }}</td>
                                            <td>{{ $item->organizer_phone }}</td>
                                            <td>
                                                @if ($item->status == 'PENDING')
                                                    <span class="badge badge-warning">Pending</span>
                                                @elseif($item->status == 'ONPROGRES')
                                                    <span class="badge badge-primary">On Progres</span>
                                                @elseif($item->status == 'ACTIVE')
                                                    <span class="badge badge-success">Aktif</span>
                                                @else
                                                    <span class="badge badge-danger">Banned</span>
                                                @endif
                                            </td>
                                            <td>
                                                <!-- Default dropleft button -->
                                                <div class="btn-group dropleft">
                                                    <button type="button" class="btn btn-secondary fa fa-ellipsis-v"
                                                        data-toggle="dropdown" aria-haspopup="true"
                                                        aria-expanded="false"></button>
                                                    <div class="dropdown-menu">
                                                        <!-- Dropdown menu links -->
                                                        <a class="dropdown-item "
                                                            href="{{ route('agency-detail', $item->id) }}"> <img
                                                                src="super-admin/images/icon/profile.svg" alt=""> Detail
                                                            Instansi</a>
                                                        <a class="dropdown-item"
                                                            href="{{ route('agency.edit', $item->id) }}"><img
                                                                src="super-admin/images/icon/edit.svg" alt=""> Edit</a>
                                                        <form
                                                            action="{{ route('agency.onprogres', ['id' => $item->id]) }}"
                                                            method="POST" style="display: inline-block;">
                                                            @method('put')
                                                            @csrf
                                                            <button type="submit" class="dropdown-item" value="Delete"
                                                                onclick="return confirm('Proses Instansi ini {{ $item->name }} ?')">
                                                                <i class="fas fa-spinner"></i> On Progres
                                                            </button>
                                                        </form>
                                                        <form action="{{ route('agency.active', ['id' => $item->id]) }}"
                                                            method="POST" style="display: inline-block;">
                                                            @method('put')
                                                            @csrf
                                                            <button type="submit" class="dropdown-item" value="Delete"
                                                                onclick="return confirm('Proses Instansi ini {{ $item->name }} ?')">
                                                                <i class="fas fa-check"></i> Aktif
                                                            </button>
                                                        </form>
                                                        <form action="{{ route('agency.banned', ['id' => $item->id]) }}"
                                                            method="POST" style="display: inline-block;">
                                                            @method('put')
                                                            @csrf
                                                            <button type="submit" class="dropdown-item" value="Delete"
                                                                onclick="return confirm('Proses Instansi ini {{ $item->name }} ?')">
                                                                <img src="super-admin/images/icon/banned-active.svg" alt="">
                                                                Banned
                                                            </button>
                                                        </form>
                                                        <a class="dropdown-item" href="#"><img src="super-admin/images/icon/trash.svg" alt=""> Hapus</a>

                                                    </div>
                                            </td>
                                        </tr>
                                    @endforeach --}}


                                </tbody>
                            </table>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('after-script')
    <script>
        $(document).ready(function() {
            load();
            $('#provinsi').on('change', function() {
                var idkota = $(this).val();
                console.log(idkota);
                if (idkota) {
                    $.ajax({
                        type: "get",
                        url: '{{ route('cities') }}',
                        data: {
                            id: idkota
                        },
                        dataType: "json",
                        success: function(response) {
                            console.log('a');
                            if (response) {
                                // console.log(response);
                                $('#kota').empty();
                                $('#kota').append(
                                    '<option>---Pilih Kab/Kota---</option>');
                                $.each(response, function(name, id) {
                                    $('#kota').append(
                                        '<option name="city_id" value ="' + name +
                                        '">' + id + '</option>')
                                });
                            }
                        }
                    });
                } else {
                    $('#kota').empty();
                }
            });


            $('#kota').on('change', function() {
                var idkec = $(this).val();
                console.log(idkec);
                if (idkec) {
                    $.ajax({
                        type: "get",
                        url: '{{ route('districts') }}',
                        data: {
                            id: idkec
                        },
                        dataType: "json",
                        success: function(response) {
                            console.log('a');
                            if (response) {
                                // console.log(response);
                                $('#kecamatan').empty();
                                $('#kecamatan').append(
                                    '<option>---Pilih Kecamatan---</option>');
                                $.each(response, function(name, id) {
                                    $('#kecamatan').append(
                                        '<option name="district_id" value ="' +
                                        name + '">' + id + '</option>')
                                });
                            }
                        }
                    });
                } else {
                    $('#kecamatan').empty();
                }
            });

            $('#kecamatan').on('change', function() {
                var iddesa = $(this).val();
                console.log(iddesa);
                if (iddesa) {
                    $.ajax({
                        type: "get",
                        url: '{{ route('villages') }}',
                        data: {
                            id: iddesa
                        },
                        dataType: "json",
                        success: function(response) {
                            console.log('a');
                            if (response) {
                                // console.log(response);
                                $('#desa').empty();
                                $('#desa').append(
                                    '<option>---Pilih Desa---</option>');
                                $.each(response, function(name, id) {
                                    $('#desa').append(
                                        '<option name="village_id" value ="' +
                                        name + '">' + id + '</option>')
                                });
                            }
                        }
                    });
                } else {
                    $('#desa').empty();
                }
            });

            $('#btn-filter').click(function () { 
                var provinsi = $('select[name=province_id]').val();
                var kota = $('select[name=city_id]').val();
                var kec = $('select[name=district_id]').val();
                if (provinsi !='') {
                    $('.tabelmanajemen').DataTable().destroy();
                    load(provinsi)
                }if (provinsi != '' && kota !='') {
                    $('.tabelmanajemen').DataTable().destroy();
                    load(provinsi,kota)
                }if(provinsi != '' && kota !='' && kec !=''){
                    $('.tabelmanajemen').DataTable().destroy();
                    load(provinsi,kota,kec)

                }else{
                    alert('ok');
                }
                // e.preventDefault();
                console.log('oke');
                console.log('kode-provinsi-filter',provinsi);
                console.log('kode-kota-filter',kota);
                console.log('kode-kec-filter',kec);
                
            });
        });
        
        function load (province_id='',district_id='',city_id='') { 
            var provinsi = $('#provinsi').val();
            var kota = $('#kota').val();
            var kec = $('#kecamatan').val();

            var tabelya = $('.tabelmanajemen').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url : "{{ url('/agency/manajemen')}}",
                    data : function(d){
                       d.province_id = provinsi; 
                       d.city_id = kota;
                       d.district_id = kec;
                    //    d.province_id = $('select[name=province_id]').val();
                    //    d.city_id = $('select[name=city_id]').val();
                    //    d.district_id = $('select[name=district_id]').val();
                        // return d;
                    }
                },
                columns:[
                    {data:'id', name:'id'},
                    {data:'name', name:'name'},
                    {data:'address', name:'address'},
                    {data:'person_responsible', name:'person_responsible'},
                    {data:'organizer_phone', name:'organizer_phone'},
                    {data:'status', name:'status'},
                    {
                        data:'aksi',
                        name:'aksi',
                        orderable:true,
                        searchable:true,
                    },
                ]
            });


            
        }

        $(function() {
            var show_modal = "{{ session()->pull('status') }}";

            if (typeof show_modal !== 'undefined' && show_modal) {
                $('#modalIdUpdate').modal('show');
                // This will open up the modal if the variable is present in session as true
                // OR you can simply show an alert message!
                // alert("Data has been submitted");
            }
        });

        $("#btn-reset-filter").click(function(){
            
           
            $("#provinsi").val(0);
            $("#kota").val(0);
            $("#kecamatan").val(0);
            $("#degree").val(0);
            $("#btn-filter").click();
        });
    </script>
@endpush
