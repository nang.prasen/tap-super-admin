@extends('layouts.super-admin')
@section('content')
<div id="page-content-wrapper">
    <!--Content Dashboard-->
    <!-- ........................ -->
    <div class="section-content section-dashboard-bukuterbaru" data-aos="fade-up" data-aos-delay="500">
        <div class="container-fluid">
            <!-- Breandcrumb -->
            <section class="store-breadcrumbs">
                <div class="row">
                    <div class="col-lg-12">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/dashboard.html">Penerbit</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Detail</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </section>
            <!-- Judul -->
            <div class="row justify-content-between">

            </div>
            <!--Aksi Tambah Pengguna-->
            <br>
            <div class="container-fluid table">
                
                <div class="">
                    <h5 class=" content-title data">
                        Tentang Penerbit
                    </h5>
                </div>
                <table class="table table-striped ">
                    <tbody class="body-table">
                        <tr class="d-flex">
                            <th class="col-3">Logo</th>
                            <td class="col-5">
                                <p><img class="profile-picture" src="{{$penerbit->logo}}"
                                    alt=""></p>
                            </td>
                            <td> </td>
                        </tr>
                        <tr class="d-flex">
                            <th class="col-3">Id Penerbit</th>
                            <td class="col-5">
                                <p>{{$penerbit->publisherId}}</p>
                            </td>
                            <td> </td>
                        </tr>
                        <tr class="d-flex">
                            <th class="col-3">Nama Penerbit</th>
                            <td class="col-5">
                                <p>{{$penerbit->name}}</p>
                            </td>
                            <td> </td>
                        </tr>
                        <tr class="d-flex">
                            <th class="col-3">Alamat</th>
                            <td class="col-5">
                                <p>{{$penerbit->address}}</p>
                            </td>
                            <td> </td>
                        </tr>
                        <tr class="d-flex">
                            <th class="col-3">No. Telpon</th>
                            <td class="col-5">
                                {{$penerbit->phone}}
                            </td>
                            <td> </td>

                        </tr>
                        <tr class="d-flex">
                            <th class="col-3">Jumlah Judul Buku</th>
                            <td class="col-5">
                                150
                            </td>
                            <td> </td>
                        </tr>
                        <tr class="d-flex">
                            <th class="col-3">Buku Terdistribusi</th>
                            <td class="col-5">
                                150
                            </td>
                            <td> </td>
                        </tr>
                        <tr class="d-flex">
                            <th class="col-3">Export Buku</th>
                            <td class="col-5">
                                <a class="btn btn-blue"  href="{{route('book.exportdownload',$penerbit->id)}}"><img src="{{asset('super-admin/images/icon/plus_white.svg')}}" aria-hidden="true"> Export Excel</a>
                            </td>
                            <td> </td>
                        </tr>
                    </tbody>
                </table>
                <br>
            </div>
        </div>
    </div>
    <div class="section-content" data-aos="fade-up" data-aos-delay="500">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row justify-content-between">
                        <div class="col-lg-6">
                            <div class="dashboard-heading">
                                <h5 class="dashboard-title">
                                    Daftar Buku
                                </h5>
                                <p class="dashboard-subtitle">
                                   Daftar Buku Penerbit
                                </p>
                            </div>
                        </div>
                        
                    </div>
    
                    <div class="dashboard-heading">
                        <div class="container-fluid table" >
                            <br>
                            <table class="table table-striped " id="table-tambah-pengguna">
                                <thead class="head-table">
                                    <tr>
                                        <th>No</th>
                                        <th>Judul Buku </th>
                                        <th>Download Image</th>
                                        <th>Download PDF</th>
                                        <th>ISBN</th>
                                        <th>Harga</th>
                                        <th>Jumlah Halaman</th>
                                        <th>Total Terdistribusi</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody class="body-table">
                                    @foreach ($penerbit->book as $item)
                                    <tr class="even">
                                        <td class="sorting_1">{{$loop->iteration}}</td>
                                        <td>{{$item->title}}</td>
                                        <td><a href="{{route('book.download')}}">download</a></td>
                                        <td><a href="{{route('baca-buku',$item->id)}}" type="button" class="btn btn-primary" >
                                            Baca
                                          </a>
                                        </td>
                                        <td>{{$item->isbn}}</td>
                                        <td>@currency($item->price)</td>
                                        <td>{{$item->page}}</td>
                                        <td>4</td>
                                        <td>
                                            <!-- Default dropleft button -->
                                            <div class="btn-group dropleft">
                                                <button type="button"
                                                    class="btn btn-secondary fa fa-ellipsis-v"
                                                    data-toggle="dropdown"
                                                    aria-haspopup="true"
                                                    aria-expanded="false">

                                                </button>
                                                <div class="dropdown-menu"
                                                    x-placement="left-start"
                                                    style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-2px, 0px, 0px);">
                                                    <!-- Dropdown menu links -->
                                                    
                                                    <a class="dropdown-item"
                                                        href="#"><img
                                                            src="{{asset('super-admin/images/icon/book-detail.svg')}}"
                                                            alt=""> #</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                    
                                    
                                </tbody>
                            </table>
                            <br>
                        </div>
    
                    </div>
    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="div">
            <iframe src="#" width="100%" height="500px"></iframe>
          </div>
        </div>
        
      </div>
    </div>
  </div>
