@extends('layouts.super-admin')
@section('content')
    <div id="page-content-wrapper">
        <!--Content Dashboard-->

        <div class="section-content aos-init" data-aos="fade-up" data-aos-delay="500">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="dashboard-heading">
                            <h5 class="dashboard-title">Aksi Admin</h5>
                            <p class="dashboard-subtitle">
                                Aksi penting yang bisa admin lakukan
                            </p>
                        </div>
                        <div class="dashboard-heading">
                            <div class="container-fluid table">
                                <br>
                                <table class="table table-striped table-hover dataTable no-footer table-responsive"
                                    id="table-paginate-oke" role="grid">
                                   <thead>
                                       <tr>
                                           <th>No</th>
                                           <th>Nama Instansi</th>
                                           <th>Alamat</th>
                                           <th>Tanggal Pendaftaran</th>
                                           <th>Tanggal Pengiriman Aplikasi</th>
                                           <th>Domain</th>
                                           <th>Nama Aplikasi</th>
                                           <th>Status Aplikasi</th>
                                           <th>Link Aplikasi</th>
                                           <th>Versi Aplikasi</th>
                                           <th>Status</th>
                                           <th>Mitra</th>
                                           <th>Perwakilan</th>
                                           <th>Transaksi</th>

                                       </tr>
                                   </thead>
                                   <tbody>
                                       @foreach ($rekap as $item)
                                       <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $item->name }}</td>
                                            <td>{{ $item->address }}</td>
                                            <td>{{ $item->registration_date }}</td>
                                            <td>{{ $item->delivery_app_date }}</td>
                                            <td>{{ $item->domain }}</td>
                                            <td>{{ $item->nameapp }}</td>
                                            <td>
                                                @if ($item->status_extension_app == 1)
                                                    <p>APK</p>
                                                @elseif($item->status_extension_app == 0)
                                                    <p>Playstore</p>
                                                @endif
                                            </td>
                                            <td>
                                                <p
                                                    style="text-overflow: ellipsis;overflow: hidden;display: block;white-space: nowrap;width: 170px;">
                                                    {{ $item->link_app }}
                                                </p>
                                            </td>
                                            <td>{{ $item->version_app }}</td>
                                            <td>
                                                <div class="text-success">{{ $item->status_progress_app }}</div>
                                            </td>
                                            <td>
                                                @foreach ($item->agentmember as $mitra)
                                                    
                                                {{  $mitra->name  }}
                                                @endforeach
                                            </td>
                                            <td>
                                                @foreach ($item->agentmember as $pwk)
                                                {{ $pwk->agent->name }}
                                                @endforeach
                                            </td>
                                            <td>
                                                <!-- Default dropleft button -->
                                                <div class="btn-group dropleft">
                                                    <button type="button" class="btn btn-secondary fa fa-ellipsis-v"
                                                        data-toggle="dropdown" aria-haspopup="true"
                                                        aria-expanded="false">
                                
                                                    </button>
                                                    <div class="dropdown-menu">
                                                        <a class="dropdown-item "
                                                            href="{{ url('/manajemen/rekapitulasi/data-instansi/detail', $item->id) }}"> <i
                                                                class="fa fa-dollar"></i> Detail
                                                            Transaksi</a>
                                
                                                    </div>
                                                </div>
                                            </td>
                                            
                                            
                                       </tr>
                                       @endforeach
                                   </tbody>


                                    </tbody>
                                </table>
                                <br>
                            </div>
                        </div>
                        <br />
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('after-script')
    <script>
        $(document).ready(function() {
            $('#table-paginate-oke').DataTable();
        } );

        $(function(){
        var show_modal = "{{ session()->pull('status') }}";

        if(typeof show_modal !== 'undefined' && show_modal) {
            $('#modalIdUpdate').modal('show');
            // This will open up the modal if the variable is present in session as true
            // OR you can simply show an alert message!
            // alert("Data has been submitted");
        }
    });
    </script>
@endpush
