@extends('layouts.super-admin')
@section('content')
<div class="section-content" data-aos="fade-up" data-aos-delay="500">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="row justify-content-between">
                    <div class="col-lg-6">
                        <div class="dashboard-heading">
                            <h5 class="dashboard-title">
                                Data User Instansi
                            </h5>
                            <p class="dashboard-subtitle">
                               List instansi dengan jumlah user
                            </p>
                        </div>
                    </div>
                </div>

                <div class="dashboard-heading">
                    <div class="container-fluid table" >
                        <br>
                        <table class="table table-striped " id="table-tambah-pengguna">
                            <thead class="head-table">
                                <tr>
                                    <th>No</th>
                                    <th>Instansi </th>
                                    <th>Jumlah User </th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody class="body-table">
                                @foreach ($instansi as $item)
                                    
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$item->name}}</td>
                                    <td>{{$item->count_user}}</td>
                                    <td><a href="{{route('manajemen.rekapitulasi.data-user.detail',$item->id)}}"><i class="fa fa-eye"></i> Lihat</a></td>
                                </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                        <br>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>
@endsection
@push('after-script')
<script>
$(document).ready(function() {
    var table = $('#table-user-penerbit').DataTable({
        lengthChange: false,
        dom:
        // 'Bfrtip',
            "<'row'<'col-md-3'f><'col-md-6'><'col-md-3'B>>" +
            "<'row'<'col-md-12'tr>>" +
            "<'row'<'col-sm-3'i><'col-sm-3'><'col-sm-3'p>>",
        buttons: [{
            extend: 'csvHtml5',
            text: ' Export   <i class="fa  fa-cloud-download"></i>',
            titleAttr: 'CSV'
        }, ],

        language: {
            searchPlaceholder: "Search",
            search: "",
        },
    });

    table.buttons().container()
        .appendTo('#table_wrapper .col-md-5:eq(0)');
});
</script>
@endpush
