@extends('layouts.super-admin')
@section('content')
    <div class="section-content section-dashboard-home" data-aos="fade-up" data-aos-delay="500">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9">
                    <div class="dashboard-heading">
                        <br>
                        <h5 class="dashboard-title">
                            Portal Home
                        </h5>
                        <p class="dashboard-subtitle">
                            Halaman Sekolah
                        </p>
                        <div class="dashboard-content">
                            <div class="row">
                                <div class=" col-lg-3 col-md-6 col-sm-12 col-12">
                                    <div class="card mb-2">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-3">
                                                    <img src="{{ asset('super-admin/images/dashboard-icon-speacker.png') }}"
                                                        alt="Logo" />
                                                </div>
                                                <div class="col-lg-9 col-md-9 col-sm-9 col-9">
                                                    <div class="dashboard-card-subtitle">
                                                        Status Sekolah
                                                    </div>
                                                    <div style="font-size:24px; font-weight: 600; color: #EB9557;">
                                                        {{ $instansi->status }}
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class=" col-lg-3 col-md-6 col-sm-12 col-12">
                                    <div class="card mb-2">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-3">
                                                    <img src="{{ asset('super-admin/images/dashboard-icon-manageruser.svg') }}"
                                                        alt="Logo" />
                                                </div>
                                                <div class="col-lg-9 col-md-9 col-sm-9 col-9">
                                                    <div class="dashboard-card-subtitle">
                                                        Login Ke Administrasi
                                                    </div>
                                                    <div style="font-size:24px; font-weight: 600; color: #EF7272;">
                                                       <a href="{{ $instansi->domain }}" target="_blank"><span class="badge badge-pill badge-info">Login</span></a>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class=" col-lg-3 col-md-6 col-sm-12 col-12">
                                    <div class="card mb-2">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-3">
                                                    <img src="{{ asset('super-admin/images/dashboard-icon-manageruser.svg') }}"
                                                        alt="Logo" />
                                                </div>
                                                <div class="col-lg-9 col-md-9 col-sm-9 col-9">
                                                    <div class="dashboard-card-subtitle">
                                                        Link Aplikasi
                                                    </div>
                                                    <div style="font-size:24px; font-weight: 600; color: #EF7272;">
                                                       <a href="{{ $instansi->link_app }}" target="_blank"><span class="badge badge-pill badge-info">klik</span></a>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class=" col-lg-3 col-md-6 col-sm-12 col-12">
                                    <div class="card mb-2">
                                        <a href="#" style="text-decoration: none; color:black">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-3">
                                                        <img src="{{ asset('super-admin/images/dashboard-icon-blast.svg') }}"
                                                            alt="Logo" />
                                                    </div>
                                                    <div class="col-lg-9 col-md-9 col-sm-9 col-9">
                                                        <div class="dashboard-card-subtitle">
                                                            Asset
                                                        </div>
                                                        <div style="font-size:24px; font-weight: 600; color: #27BD8F;">
                                                            0
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Box-->

                </div>
                <!--Informasi-->
                <div class="col-lg-3" data-aos="fade-up" data-aos-delay="700">
                    <div class="dashboard-heading">
                        <h5 class="dashboard-title">
                            Informasi
                        </h5>
                        <p class="dashboard-subtitle">
                            Informasi tips & trik untuk instansi
                        </p>

                    </div>
                    <section class="store-carousel-slider ">
                        <p> Informasi dari Troya</p>
                        <section class="store-carousel">
                            <div class="container-slider">
                                <div class="row">
                                    <div class="col-lg-12" data-aos="zoom-in">
                                        <div class="carousel slide" id="storeCarousel" data-ride="carousel">
                                            <ol class="carousel-indicators">
                                                <li class="active" data-target="#storeCarousel" data-slide-to="0">
                                                </li>
                                                <li data-target="#storeCarousel" data-slide-to="1"></li>
                                                <li data-target="#storeCarousel" data-slide-to="2"></li>
                                            </ol>
                                            <div class="carousel-inner">
                                                <div class="carousel-item active">
                                                    <img src="{{ asset('super-admin/images/dashboard-cardinfo.png') }}"
                                                        alt="" class="d-block w-100" />
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="{{ asset('super-admin/images/dashboard-cardinfo.png') }}"
                                                        alt="" class="d-block w-100" />
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="{{ asset('super-admin/images/dashboard-cardinfo.png') }}"
                                                        alt="" class="d-block w-100" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </section>

                    </section>

                </div>
            </div>



        </div>
    </div>
@endsection
