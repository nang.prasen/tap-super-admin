<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />

    <title>TAP - Troya Academic Platform</title>
    <link rel="icon" href="{{ asset('super-admin/images/logo-tap.svg') }}" type="image/x-icon">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet" />
    <link href="style/main.css" rel="stylesheet" />

    <link rel="stylesheet" href="/vendor/datatables/Buttons-1.7.0/css/buttons.bootstrap4.css" />
    <!-- Icon -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <!-- Date Picker -->

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <!-- Datatabeles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css" />

    <!-- Google-Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;300;500;600&display=swap" rel="stylesheet">


</head>

<body>
    <div class="page-dashboard" id="page-content-wrapper" >
        <div class="container my-5" style="background-color: white;" >
            <div class="text-center">
                <img src="{{ asset('super-admin/images/logo-tap.svg') }}" class="mx-auto" />
            </div>
            <br>
            <h4 class="mb-2 text-center" style="font-size: 20; font-weight: 600;">Sekolah {{$instansi->name}}</h4>
            <br>
            <table class="table table-striped">
                    <tr class="d-flex">
                        <th class="col-4" style="font-size: 14px; font-weight: 400;">Nama Instansi</th>
                        <td><span>{{$instansi->name}}</span></td>
                    </tr>
                    <tr class="d-flex">
                        <th class="col-4" style="font-size: 14px; font-weight: 400;">Alamat Instansi</th>
                        <td><span>{{$instansi->address}}</span></td>
                    </tr>
                    <tr class="d-flex">
                        <th class="col-4" style="font-size: 14px; font-weight: 400;">Nama Instansi</th>
                        <td><span>{{$instansi->link_app}}</span></td>
                    </tr>
                    <tr class="d-flex">
                        <th class="col-4" style="font-size: 14px; font-weight: 400;">Nama Instansi</th>
                        <td><span>{{$instansi->domain}}</span></td>
                    </tr>
                    <tr class="d-flex">
                        <th class="col-4" style="font-size: 14px; font-weight: 400;">Nama Instansi</th>
                        <td><span>{{$instansi->status}}</span></td>
                    </tr>

                    
            </table>
            <form action="{{ route('logout')}}" method="POST">
                @csrf
                <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal1" style="background-color: white; border-color: white;">
                    <img src="{{asset('super-admin/images/dashboard-exit.png')}}" alt="" />
                </button>
            </form>
            <div class="text-center mt-5">
                <img src="images/dashboard-text-troya.svg" alt="" />
            </div>
        </div>
        
    </div>
    

</body>


</html>
