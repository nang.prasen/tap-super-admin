@extends('layouts.super-admin')
@section('content')
    <div id="page-content-wrapper">
        <!--Content Dashboard-->

        <div class="section-content aos-init" data-aos="fade-up" data-aos-delay="500">
            <div class="container-fluid">
                <!-- Breandcrumb -->
                <section class="store-breadcrumbs">
                    <div class="row">
                        <div class="col-lg-12">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="/dashboard.html">Rekapitulasi</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Tambah Transaksi</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </section>
                <!-- Judul -->
                <div class="row justify-content-between">
                    <div class="col-lg-6">
                        <div class="dashboard-heading">
                            <h5 class="dashboard-title">
                                Edit Transaksi
                            </h5>
                            <p class="dashboard-subtitle">
                                Lakukan pengisian data dengan teliti
                            </p>
                        </div>
                    </div>
                </div>
                <!--Aksi Tambah Pengguna-->
                <br>

                <div class="container-fluid " style="background-color: white; padding-top: 10px;">
                    <h5 class=" dashboard-title " style="color: #23120B; font-weight: 500; font-size: 20px; ">
                        Data Instansi
                    </h5>
                    <hr>
                    <table class="table table-striped">
                        <tbody>
                            <tr class="d-flex">
                                <th class="col-3">Nama Instansi</th>
                                <td class="col-5">
                                    <input type="text" id="name" name="name"
                                        class="form-control @error('name') is invalid @enderror"
                                        value="{{ old('name') ? old('name') : $edit_trx->agency->name }}"
                                        placeholder="Masukan Nama Instansi" readonly>
                                </td>
                                <td class="col-4"> </td>
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">Alamat</th>
                                <td class="col-5">
                                    <input type="text" id="address" name="address"
                                        class="form-control @error('address') is invalid @enderror" aria-describedby="nama"
                                        value="{{ old('address') ? old('address') : $edit_trx->agency->address }}"
                                        placeholder="Masukan Alamat" readonly>
                                </td>
                                <td> </td>
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">Tanggal Pendaftaran</th>
                                <td class="col-5">
                                    <input type="date" class="form-control @error('registration_date') is invalid @enderror"
                                        id="registration_date" name="registration_date" aria-describedby=""
                                        value="{{ old('registration_date') ? old('registration_date') : $edit_trx->agency->registration_date }}"
                                        readonly>
                                </td>
                                <td> </td>
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">Tanggal Pengiriman Aplikasi</th>
                                <td class="col-5">
                                    <input type="date" id="delivery_app_date" name="delivery_app_date"
                                        class="form-control @error('delivery_app_date') is invalid @enderror" id=""
                                        aria-describedby=""
                                        value="{{ old('delivery_app_date') ? old('delivery_app_date') : $edit_trx->agency->delivery_app_date }}"
                                        readonly>
                                </td>
                                <td> </td>
                            </tr>
                            
                        </tbody>
                    </table>
                    <br>
                </div>
                <br>
                <form action="{{ route('transaction.update',$edit_trx->id) }}" method="POST">
                    @method('put')
                    @csrf
                    <div class="container-fluid " style="background-color: white; padding-top: 10px;">
                        <h5 class=" dashboard-title " style="color: #23120B; font-weight: 500; font-size: 20px; ">
                            Informasi Transaksi
                        </h5>
                        <hr>
                        <table class="table table-striped">
                            <input type="hidden" name="agency_id" id="agency_id" value="{{ $edit_trx->id }}">
                            {{-- <input type="text" name="agency_id" id="agency_id" value="{{ $edit_trx->id }}"> --}}
                            <tbody>
                                <tr class="d-flex">
                                    <th class="col-3">Nilai PO</th>
                                    <td class="col-5">
                                        <input type="numeric" class="form-control" id="preorder" name="preorder"
                                            placeholder="Masukkan Nilai PO" value="{{ $edit_trx->preorder }}">
                                    </td>
                                    <td> </td>
                                </tr>
                                <tr class="d-flex">
                                    <th class="col-3">Tanggal Masuk SPK</th>
                                    <td class="col-5">
                                        <input type="date" id="spk_date" name="spk_date"
                                        class="form-control @error('spk_date') is invalid @enderror" id=""
                                        aria-describedby=""
                                        value="{{ old('spk_date') ? old('spk_date') : $edit_trx->spk_date }}"
                                        >
                                        
                                    </td>
                                    <td> </td>
                                </tr>
                                <tr class="d-flex">
                                    <th class="col-3">Tanggal Masuk RAB</th>
                                    <td class="col-5">
                                        <input type="date" class="form-control" id="rab_date" name="rab_date"
                                            aria-describedby="" value="{{ $edit_trx->rab_date }}">
                                    </td>
                                    <td> </td>
                                </tr>
                                <tr class="d-flex">
                                    <th class="col-3">Status Pembayaran</th>
                                    <td class="col-5">
                                        <select name="status" id="status" class="form-control">
                                            <option value="0">-- Pilih Status --</option>
                                            <option value="Progress" {{ $edit_trx->status == 'Progress' ? 'selected' : '' }}>Progress</option>
                                            <option value="Terkirim" {{ $edit_trx->status == 'Terkirim' ? 'selected' : '' }}>Terkirim</option>
                                            <option value="Terbayar" {{ $edit_trx->status == 'Terbayar' ? 'selected' : '' }}>Terbayar</option>

                                        </select>
                                    </td>
                                </tr>
                                <tr class="d-flex">
                                    <th class="col-3">Tanggal Pengiriman Paket Transaksi</th>
                                    <td class="col-5">
                                        <input type="date" id="delivery_packet_date" name="delivery_packet_date"
                                        class="form-control @error('delivery_packet_date') is invalid @enderror" id=""
                                        aria-describedby=""
                                        value="{{ old('delivery_packet_date') ? old('delivery_packet_date') : $edit_trx->delivery_packet_date }}"
                                        >
                                        
                                    </td>
                                    <td> </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <hr>
                    <div class="container-fluid justify-content-end"
                        style="background-color: white; padding-top: 10px; text-align: right; margin-bottom: 30px;">
                        <a type="button" href="{{ route('rekap.agent') }}" class="btn"
                            style="margin-top: 5px; color: #3685C8; "><img
                                src="{{ asset('super-admin/images/icon/stop_blue.svg') }}" aria-hidden="true">
                            Batal</a>
                        <button class="btn" type="submit"
                            style="margin-top: 5px; background-color: #3685C8; color: white;"><img
                                src="{{ asset('super-admin/images/icon/plus_white.svg') }} " aria-hidden="true"> Update
                            Transaksi</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
{{-- @push('after-script')
    <script>
        var rupiah = document.getElementById('preorder');
		rupiah.addEventListener('keyup', function(e){
			// tambahkan 'Rp.' pada saat form di ketik
			// gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
			rupiah.value = formatRupiah(this.value, 'Rp ');
		});
 
		/* Fungsi formatRupiah */
		function formatRupiah(angka, prefix){
			var number_string = angka.replace(/[^,\d]/g, '').toString(),
			split   		= number_string.split(','),
			sisa     		= split[0].length % 3,
			rupiah     		= split[0].substr(0, sisa),
			ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
 
			// tambahkan titik jika yang di input sudah menjadi angka ribuan
			if(ribuan){
				separator = sisa ? '.' : '';
				rupiah += separator + ribuan.join('.');
			}
 
			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
			return prefix == undefined ? rupiah : (rupiah ?  rupiah : '');
		}
    </script>
@endpush --}}
