@extends('layouts.super-admin')
@section('content')

    <div class="section-content section-dashboard-home" data-aos="fade-up" data-aos-delay="500">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9">
                    {{-- Aksi Admin --}}
                    <div class="dashboard-heading">
                        <h5 class="dashboard-title" style="margin-top: 32px;">
                            Aksi Admin

                        </h5>
                        <p class="dashboard-subtitle">
                            Aksi penting yang bisa admin lakukan
                        </p>

                        <!-- Aksi Admin -->
                        <div class="dashboard-content">
                            <div class="row">
                                <div class="col-md-4 col-sm-12">
                                    <div class=" tile-stats">
                                        <a href="{{ route('article.create') }}">
                                            <div class="row">
                                                <div class="col-4">
                                                    <img src="{{ asset('super-admin/images/dashboard-icon-speacker.png') }}"
                                                        alt="Logo" />
                                                </div>
                                                <div class="col-8">
                                                    <h6> Postingan Artikel</h6>
                                                    <p> Buat Artikel & Berita Harian Anda</p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-12">
                                    <div class=" tile-stats">
                                        <a href="{{ route('publisher-create') }}">
                                            <div class="row">
                                                <div class="col-4">
                                                    <img src="{{ asset('super-admin/images/dashboard-icon-manageruser.svg') }}"
                                                        alt="Logo" />
                                                </div>
                                                <div class="col-8">
                                                    <h6> Tambah Penerbit</h6>
                                                    <p> Tambahkan Penerbit Bekerjasama dengan TAP</p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-12">
                                    <div class=" tile-stats">
                                        <a href="{{ route('book.distribution') }}">
                                            <div class="row">
                                                <div class="col-4">
                                                    <img src="{{ asset('super-admin/images/dashboard-icon-blast.svg') }}"
                                                        alt="Logo" />
                                                </div>
                                                <div class="col-8">
                                                    <h6>Distribusi Buku</h6>
                                                    <p>Bagikan Buku ke Instansi</p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    {{-- Rekapitulasi --}}
                    <div class="dashboard-heading">
                        <h5 class="dashboard-title" style="margin-top: 32px;">
                            Rekapitulasi
                        </h5>
                        <p class="dashboard-subtitle">
                            Rekapitulasi Troya Academic Platform
                        </p>

                        <!-- Rekapitulasi -->
                        <div class="dashboard-content">
                            <div class="row">
                                <div class="col-md-4 col-sm-12">
                                    <div class=" tile-stats">
                                        <a href="">
                                            <div class="row">
                                                <div class="col-4">
                                                    <img src="{{ asset('super-admin/images/dashboard-icon-instansi.svg') }}"
                                                        alt="Logo" />
                                                </div>
                                                <div class="col-8">
                                                    <p> Total Instansi</p>
                                                    <h5> {{ $totinstansi }}</h5>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-12">
                                    <div class=" tile-stats">
                                        <a href="">
                                            <div class="row">
                                                <div class="col-4">
                                                    <img src="{{ asset('super-admin/images/dashboard-icon-totalbook.svg') }}"
                                                        alt="Logo" />
                                                </div>
                                                <div class="col-8">
                                                    <p> Total Buku Terdistribusi
                                                    </p>
                                                    <h5 style="color:#EB9557;">
                                                        {{ $totdistribusi }}</h5>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-12">
                                    <div class=" tile-stats">
                                        <a href="">
                                            <div class="row">
                                                <div class="col-4">
                                                    <img src="{{ asset('super-admin/images/dashboard-icon-book.svg') }}"
                                                        alt="Logo" />
                                                </div>
                                                <div class="col-8">
                                                    <p>Total User </p>
                                                    <h5
                                                        style="color:#EF7272;                                                    ">
                                                        2202</h5>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-sm-12">
                                    <div class=" tile-stats">
                                        <a href="">
                                            <div class="row">
                                                <div class="col-4">
                                                    <img src="{{ asset('super-admin/images/dashboard-icon-instansi.svg') }}"
                                                        alt="Logo" />
                                                </div>
                                                <div class="col-8">
                                                    <p> Total Penerbit</p>
                                                    <h5> {{ $totpublisher }}</h5>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-12">
                                    <div class=" tile-stats">
                                        <a href="">
                                            <div class="row">
                                                <div class="col-4">
                                                    <img src="{{ asset('super-admin/images/dashboard-icon-totalbook.svg') }}"
                                                        alt="Logo" />
                                                </div>
                                                <div class="col-8">
                                                    <p> Total Mitra
                                                    </p>
                                                    <h5 style="color:#EB9557;">
                                                        {{ $totmitra }}</h5>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-12">
                                    <div class=" tile-stats">
                                        <a href="">
                                            <div class="row">
                                                <div class="col-4">
                                                    <img src="{{ asset('super-admin/images/dashboard-icon-book.svg') }}"
                                                        alt="Logo" />
                                                </div>
                                                <div class="col-8">
                                                    <p>Total Aset </p>
                                                    <h5 style="color:#EF7272;">{{$totaset}}</h5>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>

                    <!-- Analisa Buku -->
                    <div class="dashboard-heading">
                        <h5 class="dashboard-title" style="margin-top: 32px;">
                            Analisa Buku
                        </h5>
                        <p class="dashboard-subtitle">
                            Analisa perpustakaan untuk membantu anda
                        </p>
                    </div>
                    <!-- Box-->
                    <div class="dashboard-heading">
                        <div class="container-fluid table">
                            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#dg-seringbaca"
                                        role="tab" aria-controls="pills-home" aria-selected="true">Paling Sering Dibaca</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-profile-tab" data-toggle="pill"
                                        href="#dg-palinglaku" role="tab" aria-controls="pills-profile"
                                        aria-selected="false">Paling Laku</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="pills-tabContent">
                                <div class="tab-pane fade show active" id="dg-seringbaca" role="tabpanel"
                                    aria-labelledby="pills-home-tab">
                                    <table class="table table-striped table-hover" id="table">

                                        <thead class="head-table">
                                            <tr>
                                                <th>No</th>
                                                <th>Judul Buku</th>
                                                <th>Jumlah Peminjam</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody class="body-table">
                                            @foreach ($read as $re)
                                            <tr>
                                                <td>{{$loop->iteration}}</td>
                                                <td>{{$re->title}}</td>
                                                <td>{{$re->total}}</td>
                                                <td><a href="#"> lihat</a> </td>
                                            </tr>  
                                            @endforeach
                                            
                                           

                                        </tbody>
                                    </table>
                                    <hr>
                                    <a href="#">
                                        <p style="text-align: right;"> lihat selengkapnya</p>

                                    </a>

                                </div>
                                <div class="tab-pane fade" id="dg-palinglaku" role="tabpanel"
                                    aria-labelledby="pills-home-tab">
                                    <table class="table table-striped table-hover" id="buku-masuk">
                                        <thead class="head-table">
                                            <tr>
                                                <th>No</th>
                                                <th>Judul Buku</th>
                                                <th>Jumlah Peminjam</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody class="body-table">
                                            @foreach ($popular as $po)
                                            <tr>
                                                <td>{{$loop->iteration}}</td>
                                                <td>{{$po->title}}</td>
                                                <td>{{$po->total}}</td>
                                                <td><a href="#"> lihat</a> </td>
                                            </tr>  
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <hr>
                                    <a href="#">
                                        <p style="text-align: right;"> lihat selengkapnya</p>

                                    </a>
                                </div>

                            </div>

                        </div>
                    </div>
                    <!-- Box-->
                    <div class="dashboard-heading">
                        <div class="container-fluid table">
                            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home"
                                        role="tab" aria-controls="pills-home" aria-selected="true">Paling Sering Dibaca</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-profile-tab" data-toggle="pill"
                                        href="#pills-profile" role="tab" aria-controls="pills-profile"
                                        aria-selected="false">Paling Laku</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="pills-tabContent">
                                <div class="tab-pane fade show active" id="pills-home" role="tabpanel"
                                    aria-labelledby="pills-home-tab">
                                    <p> Isi Chart Paling Sering dibaca</p>
                                    <canvas id="read" width="400" height="400"></canvas>

                                    <hr>
                                    <a href="#">
                                        <p style="text-align: right;"> lihat selengkapnya</p>

                                    </a>

                                </div>
                                <div class="tab-pane fade" id="pills-profile" role="tabpanel"
                                    aria-labelledby="pills-home-tab">
                                    <p> Isi Chart Paling Laku</p>
                                    <canvas id="popular" width="400" height="400"></canvas>

                                    <hr>
                                    <hr>
                                    <a href="#">
                                        <p style="text-align: right;"> lihat selengkapnya</p>

                                    </a>
                                </div>

                            </div>

                        </div>
                    </div>

                </div>

                <!--Informasi-->
                <div class="col-lg-3" data-aos="fade-up" data-aos-delay="200">
                    <br>
                    <div class="dashboard-heading">
                        <h5 class="dashboard-title">
                            Informasi
                        </h5>
                        <p class="dashboard-subtitle">
                            Informasi instansi
                        </p>

                    </div>
                    <section class="news-down">
                        <div class="container">
                            <h5>Riwayat Distribusi Buku</h5>

                            {{-- Card per Eksemplar --}}
                            <div class=" tile-stats">
                                <a href="">
                                    <div class="row">
                                        <div class="col-8">
                                            <h6>SMA Maju Jaya</h6>
                                            <p>
                                                Filosofi Teras <br>
                                                3 Eksemplar <br>
                                                02 April 21 . 14.09

                                            </p>
                                        </div>
                                        <div class="col-4">
                                            <img src="{{ asset('super-admin/images/dashboard-icon-instansi.svg') }}"
                                                alt="Logo" />
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <hr>
                            <div class=" tile-stats">
                                <a href="">
                                    <div class="row">
                                        <div class="col-8">
                                            <h6>SMA Maju Jaya</h6>
                                            <p>
                                                Filosofi Teras <br>
                                                3 Eksemplar <br>
                                                02 April 21 . 14.09

                                            </p>
                                        </div>
                                        <div class="col-4">
                                            <img src="{{ asset('super-admin/images/dashboard-icon-instansi.svg') }}"
                                                alt="Logo" />
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <hr>

                            {{-- Lihat Selengkapnya --}}
                            <a href="" class="align-right">
                                Lihat selengkapnya
                            </a>
                        </div>



                </div>
            </div>



        </div>
    </div>
    @push('after-script')
        <script src="https://cdn.jsdelivr.net/npm/chart.js@3.7.0/dist/chart.min.js"></script>
        <canvas id="myChart" width="300" height="300"></canvas>
        <script>
            function random_rgba() {
                var o = Math.round,
                    r = Math.random,
                    s = 255;
                return 'rgba(' + o(r() * s) + ',' + o(r() * s) + ',' + o(r() * s) + ',' + r().toFixed(1) + ')';
            }
            // untuk popular
            var words = <?php echo json_encode($popular); ?>; // don't use quotes
            let judul = [];
            let total = [];
            let warna = [];
            $.each(words, function(key, value) {
                // console.log(words[key].title);
                judul.push(words[key].title);
                total.push(words[key].total);
                warna.push(random_rgba());
            });




            const ctx = document.getElementById('popular').getContext('2d');
            const myChart = new Chart(ctx, {
                type: 'doughnut',
                data: {
                    labels: judul,
                    datasets: [{
                        label: '# of Votes',
                        data: total,
                        backgroundColor: warna,
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        y: {
                            beginAtZero: true
                        }
                    }
                }
            });


            // untuk read


            var reads = <?php echo json_encode($read); ?>; // don't use quotes
            let judulRead = [];
            let totalRead = [];
            let warnaRead = [];
            $.each(reads, function(key, value) {
                judulRead.push(reads[key].title);
                totalRead.push(reads[key].total);
                warnaRead.push(random_rgba());
            });




            const ctxRead = document.getElementById('read').getContext('2d');
            const myChartRead = new Chart(ctxRead, {
                type: 'doughnut',
                data: {
                    labels: judulRead,
                    datasets: [{
                        label: '# of Votes',
                        data: totalRead,
                        backgroundColor: warnaRead,
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        y: {
                            beginAtZero: true
                        }
                    }
                }
            });
        </script>

    @endpush
@endsection
