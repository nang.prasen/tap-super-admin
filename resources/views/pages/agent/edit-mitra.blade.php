@extends('layouts.super-admin')
@section('content')
    <div class="section-content" data-aos="fade-up" data-aos-delay="500">
        <div class="container-fluid">
            <!-- Breandcrumb -->
            <section class="store-breadcrumbs">
                <div class="row">
                    <div class="col-lg-12">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('agent.index')}}">Perwakilan</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><a href="{{route('agent.show',$editmitra->agent->id)}}">Detail Perwakilan</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Edit Mitra Perwakilan</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </section>
            <div class="row">
                <div class="col-lg-12">
                    <div class="row justify-content-between">
                        <div class="col-lg-6">
                            <div class="dashboard-heading">
                                <h5 class="dashboard-title">
                                    Tambah Perwakilan Baru 
                                </h5>
                                <p class="dashboard-subtitle">
                                    Isi data dengan benar
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <section class="berita">
                <form action="{{ route('agent.update.mitra',$editmitra->id) }}" id="form-addArtikel" method="POST">
                    @method('put')
                    @csrf
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="container-fluid table">
                                <div class="form">
                                    <div class="form-group">
                                        <label class="form-check-label">ID Perwakilan</label>
                                        <input type="text" id="agent_member_id" name="agent_member_id" placeholder="Masukkan Nama Perwakilan"
                                            class="form-control" value="{{ $editmitra->agent_member_id }}" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-check-label">Nama</label>
                                        <input type="text" id="name" name="name" placeholder="Masukkan Nama Perwakilan"
                                            class="form-control" value="{{ $editmitra->name }}">
                                    </div>
                                    @error('name')
                                        <span class="help-block"> {{$message}} </span>
                                    @enderror
                                    <div class="form-group">
                                        <label class="form-check-label">Alamat</label>
                                        <input type="text" id="address" name="address" placeholder="Masukkan Alamat Perwakilan"
                                            class="form-control" value="{{ $editmitra->address }}">
                                    </div>
                                    @error('address')
                                        <span class="help-block"> {{$message}} </span>
                                    @enderror
                                    
                                    <div class="form-group">
                                        <label class="form-check-label">Perwakilan</label>
                                        <select name="agent_id" id="agent_id" class="form-control">
                                            @foreach ($agent as $id => $name)
                                                <option value="{{$id}}" {{$editmitra->agent->id == $id ? 'selected':''}}>{{$name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @error('address')
                                        <span class="help-block"> {{$message}} </span>
                                    @enderror

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid battom-nav">
                        <a  href="{{route('agent.index')}}" class="btn btn-white" id="btnoke-draf"> <i class="fa fa-remove" aria-hidden="true"></i> Batal</a>
                        <button type="submit" value="publish" class="btn btn-blue" id="btnoke-publish"><i class="fa fa-plus" aria-hidden="true"></i> Update</button>
        
                    </div>
                </form>

            </section>
            
        </div>
    </div>

@endsection
