@extends('layouts.super-admin')
@section('content')
    <div id="page-content-wrapper">
        <!--Content Dashboard-->

        <div class="section-content aos-init" data-aos="fade-up" data-aos-delay="500">
            <div class="container-fluid">
                <!-- Breandcrumb -->
                <section class="store-breadcrumbs">
                    <div class="row">
                        <div class="col-lg-12">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Perwakilan</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Detail Perwakilan</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </section>
                <!-- Judul -->
                <div class="row justify-content-between">
                    <div class="col-lg-6">
                        <div class="dashboard-heading">
                            <h5 class="dashboard-title">
                                Detail Perwakilan
                            </h5>
                        </div>
                    </div>
                    @if ($user == null)                        
                        <div class="col-lg-6" style="text-align: right;">
                            <div class="dashboard-heading">
                                <a class="btn btn-blue" href="{{route('agent.create-user', $agentdetail->id)}}" ><img src="{{asset('super-admin/images/icon/plus_white.svg')}}" aria-hidden="true"> Akun Perwakilan</a>
                            </div>
                        </div>   
                    @endif
                </div>
                <!--Aksi Tambah Pengguna-->
                <br>
                <div class="container-fluid " style="background-color: white; padding-top: 10px;">
                    <h5 class=" dashboard-title " style="color: #23120B; font-weight: 500; font-size: 20px; ">
                        Data Perwakilan
                    </h5>
                    <hr>
                    <table class="table table-striped">
                        <tbody>
                            <tr class="d-flex">
                                <th class="col-3">ID Perwakilan</th>
                                <td class="col-5">
                                    <p>
                                        {{$agentdetail->agentId}}
                                    </p>
                                </td>
                                <td class="col-4"> </td>
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">Nama Perwakilan</th>
                                <td class="col-5">
                                    <p>
                                        {{$agentdetail->name}}
                                    </p>
                                </td>
                                <td class="col-4"> </td>
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">Alamat</th>
                                <td class="col-5">
                                    <p>
                                        {{$agentdetail->address}}
                                    </p>
                                </td>
                                <td> </td>
                            </tr>
                            
                        </tbody>
                    </table>
                    <br>

                </div>
                <br>
                <div class="container-fluid " style="background-color: white; padding-top: 10px;">
                    <h5 class=" dashboard-title " style="color: #23120B; font-weight: 500; font-size: 20px; ">
                        Informasi Mitra
                    </h5>
                    <hr>
                    <table class="table table-striped" id="table-mitra">
                        <thead>
                            <tr>
                                <th>ID Mitra</th>
                                <th>Nama Mitra</th>
                                <th>Alamat Mitra</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($agentdetail->member as $item)
                                <tr>
                                    <td>{{isset($item->agent_member_id) ? $item->agent_member_id:'Tidak ditemukan'}}</td>
                                    <td>{{isset($item->name) ? $item->name:'Tidak ditemukan'}}</td>
                                    <td>{{isset($item->address) ? $item->address:'Tidak ditemukan'}}</td>
                                    @if (Auth::user()->role == 'SuperAdmin')
                                        <td><a href="{{route('agent.edit.mitra',$item->id)}}" type="button" class="btn btn-blue">Edit</a></td>
                                        
                                    @else
                                        <td>#</td>
                                        
                                    @endif
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <br>
                </div>
                <hr>
                <div class="container-fluid justify-content-end"
                    style="background-color: white;padding: 5px 10px 5px 0; text-align: right; margin-bottom: 30px;">
                    <a href="{{route('agent.index')}}" class="btn" style="color: #3685C8; ">
                        <i class="fa fa-angle-left"></i> Kembali</a>

                </div>
            </div>
        </div>
    </div>
@endsection
