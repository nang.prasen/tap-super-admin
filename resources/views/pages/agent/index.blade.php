@extends('layouts.super-admin')
@section('content')
<div class="section-content" data-aos="fade-up" data-aos-delay="500">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="row justify-content-between">
                    <div class="col-lg-6">
                        <div class="dashboard-heading">
                            <h5 class="dashboard-title">
                                Daftar Perwakilan
                            </h5>
                            <p class="dashboard-subtitle">
                               Daftar Perwakilan Yang Bekerja
                            </p>
                        </div>
                    </div>
                    @if (Auth::user()->role == 'SuperAdmin')
                        
                    <div class="col-lg-6" style="text-align: right;">
                        <div class="dashboard-heading">
                            <a class="btn btn-blue" href="{{route('agent.create')}}" ><img src="super-admin/images/icon/plus_white.svg" aria-hidden="true"> Tambah Perwakilan</a>
                            
                        </div>
                    </div>
                    @endif
                </div>

                <div class="dashboard-heading">
                    <div class="container-fluid table" >
                        
                        <br>
                        <table class="table table-striped " id="table-tambah-pengguna">
                            <thead class="head-table">
                                <tr>
                                    <th>No</th>
                                    <th>ID</th>
                                    <th>Nama Perwakilan </th>
                                    <th>Alamat</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody class="body-table">
                                @foreach ($agent as $item)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$item->agentId}}</td>
                                    <td>{{$item->name}}</td>
                                    <td>{{$item->address}}</td>
                                    </td>
                                    @if (Auth::user()->role == 'SuperAdmin')
                                        
                                    <td>
                                        <!-- Default dropleft button -->
                                        <div class="btn-group dropleft">
                                            <button type="button" class="btn btn-secondary fa fa-ellipsis-v" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                            <div class="dropdown-menu">
                                                <!-- Dropdown menu links -->
                                                <a class="dropdown-item " href="{{route('agent.show',$item->id)}}"> <img src="super-admin/images/icon/profile.svg" alt=""> Detail Perwakilan</a>
                                                <a class="dropdown-item " href="{{route('agent.mitra',$item->id)}}"> <i class="fa fa-user-plus" aria-hidden="true"></i> Tambah Mitra</a>
                                                <a class="dropdown-item" href="{{route('agent.edit',$item->id)}}"><img src="super-admin/images/icon/edit.svg" alt=""> Edit</a>
                                                <a class="dropdown-item" href="{{route('agent.destroy',$item->id)}}"><img src="super-admin/images/icon/trash.svg" alt=""> Hapus</a>
                                                
                                        </div>
                                    </td>
                                    @else
                                    <td>
                                        <!-- Default dropleft button -->
                                        <div class="btn-group dropleft">
                                            <button type="button" class="btn btn-secondary fa fa-ellipsis-v" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                            <div class="dropdown-menu">
                                                <!-- Dropdown menu links -->
                                                <a class="dropdown-item " href="{{route('agent.show',$item->id)}}"> <img src="super-admin/images/icon/profile.svg" alt=""> Detail Perwakilan</a>
                                                
                                        </div>
                                    </td>
                                    @endif
                                </tr>
                                @endforeach
                                
                                
                            </tbody>
                        </table>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@push('after-script')
    <script>
        $(function(){
        var show_modal = "{{ session()->pull('status') }}";

        if(typeof show_modal !== 'undefined' && show_modal) {
            $('#modalIdUpdate').modal('show');
            // This will open up the modal if the variable is present in session as true
            // OR you can simply show an alert message!
            // alert("Data has been submitted");
        }
    });
    </script>
@endpush
