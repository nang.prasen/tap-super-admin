@extends('layouts.super-admin')
@section('content')
<div class="section-content section-dashboard-bukuterbaru" data-aos="fade-up" data-aos-delay="500">
    <div class="container-fluid">
        <!-- Breandcrumb -->
        <section class="store-breadcrumbs">
            <div class="row">
                <div class="col-lg-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('rekap.bukulaku')}}">Buku Paling Laku</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Detail Buku</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </section>
        <!-- Judul -->
        <div class="row justify-content-between">
        </div>
        <br>
        <div class="container-fluid table">
            <div class="">
                <h5 class=" content-title data">
                    Detail Buku
                </h5>
            </div>
            <hr>
            <table class="table table-striped ">
                <tbody class="body-table">
                    <tr class="d-flex">
                        <th class="col-3">Cover</th>
                        <td class="col-5">
                             <img src="{{url($detail->cover)}}" style="width:10em">
                        </td>
                        <td class="col-4"> </td>
                    </tr>
                    <tr class="d-flex">
                        <th class="col-3">Judul Buku</th>
                        <td class="col-5">
                            <p> {{ $detail->title}}</p>
                        </td>
                    </tr>
                    <tr class="d-flex">
                        <th class="col-3">Pengarang</th>
                        <td class="col-5">
                            {{ $detail->name}}
                        </td>
                    </tr>
                    <tr class="d-flex">
                        <th class="col-3">Deskripsi</th>
                        <td class="col-5">
                            {{ $detail->description}}
                        </td>
                    </tr>
                    <tr class="d-flex">
                        <th class="col-3">Halaman</th>
                        <td class="col-5">
                            {{ $detail->page}}
                        </td>
                        <td></td>
                    </tr>
                    <tr class="d-flex">
                        <th class="col-3">Kategori</th>
                        <td class="col-5">
                            <p> {{ $detail->category->name }}</p>
                        </td>
                    </tr>
                    <tr class="d-flex">
                        <th class="col-3">Bahasa</th>
                        <td class="col-5">
                            <p> {{ $detail->language }}</p>
                        </td>
                    </tr>
                    <tr class="d-flex">
                        <th class="col-3">Penerbit</th>
                        <td class="col-5">
                            {{ $detail->publisher->name }}
                        </td>
                    </tr>
                    <tr class="d-flex">
                        <th class="col-3">Jumlah</th>
                        <td class="col-5">
                            {{ $detail->qty}}
                        </td>
                    </tr>
                    <tr class="d-flex">
                        <th class="col-3">Harga</th>
                        <td class="col-5">
                            @currency($detail->price),-
                        </td>
                    </tr>
                    <tr class="d-flex">
                        <th class="col-3">ISBN</th>
                        <td class="col-5">
                            <p>{{ $detail->isbn}}</p>
                        </td>
                    </tr>
                </tbody>
            </table>
            <br>
        </div>
        <br>
    </div>
</div>
@endsection
