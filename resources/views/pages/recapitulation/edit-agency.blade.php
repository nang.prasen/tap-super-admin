@extends('layouts.super-admin')
@section('content')
    <div id="page-content-wrapper">
        <!--Content Dashboard-->

        <div class="section-content aos-init" data-aos="fade-up" data-aos-delay="500">
            <div class="container-fluid">
                <!-- Breandcrumb -->
                <section class="store-breadcrumbs">
                    <div class="row">
                        <div class="col-lg-12">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="/dashboard.html">Rekapitulasi</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Tambah Transaksi</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </section>
                <!-- Judul -->
                <div class="row justify-content-between">
                    <div class="col-lg-6">
                        <div class="dashboard-heading">
                            <h5 class="dashboard-title">
                                Tambah Transaksi
                            </h5>
                            <p class="dashboard-subtitle">
                                Lakukan pengisian data dengan teliti
                            </p>
                        </div>
                    </div>
                </div>
                <!--Aksi Tambah Pengguna-->
                <br>
                <form action="{{route('rekap.agency.update',$edit->id)}}" method="POST">
                    @method('PUT')
                    @csrf
                    <div class="container-fluid " style="background-color: white; padding-top: 10px;">
                        <h5 class=" dashboard-title " style="color: #23120B; font-weight: 500; font-size: 20px; ">
                            Data Transaksi
                        </h5>
                        <hr>
                        <table class="table table-striped">
                            <tbody>
                                <tr class="d-flex">
                                    <th class="col-3">Nama Instansi</th>
                                    <td class="col-5">
                                        <input type="text" id="name" name="name" class="form-control @error('name') is invalid @enderror"
                                            value="{{old('name') ? old('name') : $edit->name}}" placeholder="Masukan Nama Instansi">
                                    </td>
                                    <td class="col-4"> </td>
                                </tr>
                                <tr class="d-flex">
                                    <th class="col-3">Alamat</th>
                                    <td class="col-5">
                                        <input type="text" id="address" name="address" class="form-control @error('address') is invalid @enderror" aria-describedby="nama"
                                            value="{{old('address') ? old('address') : $edit->address}}" placeholder="Masukan Alamat">
                                    </td>
                                    <td> </td>
                                </tr>
                                <tr class="d-flex">
                                    <th class="col-3">Provinsi</th>
                                    <td class="col-5">
                                        <select class="form-control" name="provinsi" id="provinsi" required>
                                            <option>---Pilih Provinsi---</option>
                                            @foreach ($wilayah as $id => $name)
                                                <option value="{{ $id}}" {{ $edit->province_id == $id ? 'selected':'' }}>{{ $name }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td class="col-4"> </td>
                                </tr>
                                <tr class="d-flex">
                                    <th class="col-3">Kabupaten/Kota</th>
                                    <td class="col-5">
                                        <select class="form-control" name="kota" id="kota" required>
                                            <option>---Pilih Kab/Kota---</option>
                                            @foreach ($city as $id => $name)
                                                <option value="{{ $id}}" {{ $edit->city_id == $id ? 'selected':'' }}>{{ $name }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td class="col-4"> </td>
                                </tr>
                                <tr class="d-flex">
                                    <th class="col-3">Kecamatan</th>
                                    <td class="col-5">
                                        <select class="form-control" name="kecamatan" id="kecamatan" required>
                                            <option>---Pilih Kecamatan---</option>
                                            @foreach ($district as $id => $name)
                                                <option value="{{ $id}}" {{ $edit->district_id == $id ? 'selected':'' }}>{{ $name }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td class="col-4"> </td>
                                </tr>
                                <tr class="d-flex">
                                    <th class="col-3">Desa</th>
                                    <td class="col-5">
                                        <select class="form-control" name="desa" id="desa" required>
                                            <option>---Pilih Desa---</option>
                                            @foreach ($village as $id => $name)
                                                <option value="{{ $id}}" {{ $edit->village_id == $id ? 'selected':'' }}>{{ $name }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td class="col-4"> </td>
                                </tr>
                                <tr class="d-flex">
                                    <th class="col-3">Tanggal Pendaftaran</th>
                                    <td class="col-5">
                                        <input type="date" class="form-control @error('registration_date') is invalid @enderror" id="registration_date" name="registration_date" aria-describedby=""
                                            value="{{old('registration_date') ? old('registration_date') : $edit->registration_date}}">
                                    </td>
                                    <td> </td>
                                </tr>
                                <tr class="d-flex">
                                    <th class="col-3">Tanggal Pengiriman Aplikasi</th>
                                    <td class="col-5">
                                        <input type="date" id="delivery_app_date" name="delivery_app_date" class="form-control @error('delivery_app_date') is invalid @enderror" id="" aria-describedby=""
                                            value="{{old('delivery_app_date') ? old('delivery_app_date') : $edit->delivery_app_date}}">
                                    </td>
                                    <td> </td>
                                </tr>
                                <tr class="d-flex">
                                    <th class="col-3">Domain</th>
                                    <td class="col-5">
                                        <input type="text" id="domain" name="domain" class="form-control @error('domain') is invalid @enderror" 
                                            value="{{old('domain') ? old('domain') : $edit->domain}}" placeholder="Masukan Nama Domain">
                                    </td>
                                    <td class="col-4"> </td>
                                </tr>
                                <tr class="d-flex">
                                    <th class="col-3">Nama Aplikasi</th>
                                    <td class="col-5">
                                        <input type="text" id="nameapp" name="nameapp" class="form-control @error('nameapp') is invalid @enderror" 
                                            placeholder="Masukan Nama Aplikasi" value="{{old('nameapp') ? old('nameapp') : $edit->nameapp}}">
                                    </td>
                                    <td class="col-4"> </td>
                                </tr>

                                <tr class="d-flex">
                                    <th class="col-3">Status Aplikasi</th>
                                    <td class="col-5">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="status_extension_app"
                                                id="status_extension_app" value="1" @if ($edit->status_extension_app == 1) checked @endif>
                                            <label class="form-check-label" for="inlineRadio1">APK</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="status_extension_app"
                                                id="status_extension_app" value="0" @if ($edit->status_extension_app == 0) checked @endif>
                                            <label class="form-check-label" for="inlineRadio2">Play Store</label>
                                        </div>
                                    </td>
                                    <td> </td>
                                </tr>
                                <tr class="d-flex">
                                    <th class="col-3">Link Aplikasi</th>
                                    <td class="col-5">
                                        <input type="text" id="link_app" name="link_app" class="form-control @error('link_app') is invalid @enderror" 
                                            placeholder="Link Aplikasi" value="{{old('link_app') ? old('link_app') : $edit->link_app}}">
                                    </td>
                                    <td class="col-4"> </td>
                                </tr>
                                <tr class="d-flex">
                                    <th class="col-3">Versi Aplikasi</th>
                                    <td class="col-5">
                                        <input type="text" id="version_app" name="version_app" class="form-control @error('version_app') is invalid @enderror"
                                            placeholder="Versi Aplikasi" value="{{old('version_app') ? old('version_app') : $edit->version_app}}">
                                    </td>
                                    <td class="col-4"> </td>
                                </tr>
                                <tr class="d-flex">
                                    <th class="col-3">Status</th>
                                    <td class="col-5">
                                        <select name="status_progress_app" id="status_progress_app" class="form-control">
                                            <option value="Confirm"
                                                {{ $edit->status_progress_app == 'Confirm' ? 'selected' : '' }}>Konfirmasi
                                                Data</option>
                                            <option value="Design"
                                                {{ $edit->status_progress_app == 'Design' ? 'selected' : '' }}>Design</option>
                                            <option value="Web Development"
                                                {{ $edit->status_progress_app == 'Web Development' ? 'selected' : '' }}>Web
                                                Development</option>
                                            <option value="Apk Development"
                                                {{ $edit->status_progress_app == 'Apk Development' ? 'selected' : '' }}>Apk
                                                Development</option>
                                            <option value="Test"
                                                {{ $edit->status_progress_app == 'Test' ? 'selected' : '' }}>Tester</option>
                                            <option value="Approve Production"
                                                {{ $edit->status_progress_app == 'Approve Production' ? 'selected' : '' }}>
                                                Aprove to Production</option>
                                            <option value="Production"
                                                {{ $edit->status_progress_app == 'Production' ? 'selected' : '' }}>Production
                                            </option>
                                            <option value="Done"
                                                {{ $edit->status_progress_app == 'Done' ? 'selected' : '' }}>Done</option>
                                            <option value="Updating"
                                                {{ $edit->status_progress_app == 'Updating' ? 'selected' : '' }}>Updating
                                            </option>
                                        </select>
                                    </td>
                                    <td class="col-4"> </td>
                                </tr>
                                <tr class="d-flex">
                                    <th class="col-3">Perwakilan</th>
                                    <td class="col-5">
                                        <select name="agentID" id="agentID" class="form-control">
                                            <option value="">-- Pilih Perwakilan --</option>
                                            @foreach ($agent as $id => $name)
                                                <option value="{{ $id }}" {{ $edit->agent_member_id == $id ? 'selected':'' }}>{{ $name }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td class="col-4"> </td>
                                </tr>
                                <tr class="d-flex">
                                    <th class="col-3">Mitra</th>
                                    <td class="col-5">
                                        <select class="form-control" name="agentmember" id="agentmember">
                                            <option value="">-- Pilih Mitra --</option>
                                            @foreach ($agentmember as $id => $name)
                                                <option value="{{ $id }}" {{ $edit->agent_member_id == $id ? 'selected':'' }}>{{ $name }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td class="col-4"> </td>
                                </tr>
                            </tbody>
                        </table>
                        <br>
                    </div>
                    <br>
                    <hr>
                    <div class="container-fluid justify-content-end"
                        style="background-color: white; padding-top: 10px; text-align: right; margin-bottom: 30px;">
                        <button class="btn" style="margin-top: 5px; color: #3685C8; "><img
                                src="{{ asset('super-admin/images/icon/stop_blue.svg') }}" aria-hidden="true">
                            Batal</button>
                        <button class="btn" type="submit"
                            style="margin-top: 5px; background-color: #3685C8; color: white;"><img
                                src="{{ asset('super-admin/images/icon/plus_white.svg') }} " aria-hidden="true"> Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('after-script')
    <script>
        $(document).ready(function() {
            $('#agentID').on('change', function() {
                var agentID = $(this).val();
                console.log(agentID);
                if (agentID) {
                    $.ajax({
                        type: "GET",
                        // url: "{{ route('agent.member') }}"+'/'+agentID,
                        // url: '/agent-member/'+agentID,
                        url: "/member/agen?agentID="+agentID,
                        dataType: "json",
                        success: function(res) {
                            console.log('A');
                            if (res) {
                                console.log(res);
                                $('#agentmember').empty();
                                $('#agentmember').append(
                                    '<option>---Pilih Mitra---</option>');
                                $.each(res, function(name, id) {
                                    $('#agentmember').append('<option name="agentmember" value ="'+id+'">'+name+'</option>')
                                });

                            } else {
                                $('#agentmember').empty();
                            }
                        }
                    });
                } else {
                    $('#agentmember').empty();
                }
            });

            $('#provinsi').on('change',function(){
            var idkota = $(this).val();
            console.log(idkota);
            if (idkota) {
                $.ajax({
                    type: "get",
                    url: '{{ route('cities') }}',
                    data: {id:idkota},
                    dataType: "json",
                    success: function (response) {
                        console.log('a');
                        if (response) {
                            // console.log(response);
                            $('#kota').empty();
                            $('#kota').append(
                                    '<option>---Pilih Kab/Kota---</option>');
                            $.each(response, function(name, id) {
                                    $('#kota').append('<option name="kota" value ="'+name+'">'+id+'</option>')
                            });
                        }
                    }
                });
            } else {
                $('#kota').empty();
            }
        });


        $('#kota').on('change',function(){
            var idkec = $(this).val();
            console.log(idkec);
            if (idkec) {
                $.ajax({
                    type: "get",
                    url: '{{ route('districts') }}',
                    data: {id:idkec},
                    dataType: "json",
                    success: function (response) {
                        console.log('a');
                        if (response) {
                            // console.log(response);
                            $('#kecamatan').empty();
                            $('#kecamatan').append(
                                    '<option>---Pilih Kecamatan---</option>');
                            $.each(response, function(name, id) {
                                    $('#kecamatan').append('<option name="kecamatan" value ="'+name+'">'+id+'</option>')
                            });
                        }
                    }
                });
            } else {
                $('#kecamatan').empty();
            }
        });

        $('#kecamatan').on('change',function(){
            var iddesa = $(this).val();
            console.log(iddesa);
            if (iddesa) {
                $.ajax({
                    type: "get",
                    url: '{{ route('villages') }}',
                    data: {id:iddesa},
                    dataType: "json",
                    success: function (response) {
                        console.log('a');
                        if (response) {
                            // console.log(response);
                            $('#desa').empty();
                            $('#desa').append(
                                    '<option>---Pilih Desa---</option>');
                            $.each(response, function(name, id) {
                                    $('#desa').append('<option name="desa" value ="'+name+'">'+id+'</option>')
                            });
                        }
                    }
                });
            } else {
                $('#desa').empty();
            }
        });
        });
    </script>
@endpush
