@extends('layouts.super-admin')
@section('content')
<div id="page-content-wrapper">
    <!--Content Dashboard-->
    <!-- ........................ -->
    <div class="section-content section-dashboard-bukuterbaru" data-aos="fade-up" data-aos-delay="500">
        <div class="container-fluid">
       <!-- Breandcrumb -->
               <section class="store-breadcrumbs">
                   <div class="row">
                       <div class="col-lg-12">
                           <nav aria-label="breadcrumb">
                               <ol class="breadcrumb">
                                   <li class="breadcrumb-item"><a href="/dashboard.html">Daftar Pengguna</a></li>
                                   <li class="breadcrumb-item active" aria-current="page">Detail Pengguna</li>
                               </ol>
                           </nav>
                       </div>
                   </div>
               </section>
           <!-- Judul -->
           <div class="row justify-content-between">
              
           </div>
           <!--Aksi Tambah Pengguna-->
           <br>
           <div class="container-fluid table" >
               <div class="">
                   <h5 class=" content-title data" >
                   Data Pengguna
                   </h5> 
                  
               </div>
                      
               <hr>
               <table class="table table-striped ">
                   <tbody class="body-table">
                       <tr class="d-flex">
                           <th class="col-3">Avatar</th>
                           <td class="col-5"> 
                             <input type="file" name="thumnail">
                           </td>
                           <td class="col-4"> </td>
                       </tr>
                       <tr class="d-flex">
                           <th  class="col-3">Nama Lengkap</th>
                           <td class="col-5">
                                 <input type="text" class="form-control" id="NamaLengkap" aria-describedby="nama" placeholder="Masukan nama lengkap pengguna">
                           </td>
                           <td> </td>
                       </tr>
                       <tr class="d-flex">
                           <th class="col-3">No Identitas</th>
                           <td class="col-5">
                               <input type="text" class="form-control" id="NoKaryawan" aria-describedby="nokaryawan" placeholder="NISN/No Karyawan">
                           </td>
                           <td> </td>
                       </tr>
                       <tr class="d-flex">
                           <th class="col-3">Tanggal Lahir</th>
                           <td class="col-5"> 
                               <input type="date" class="form-control" id="TanggalLahir" aria-describedby="ttl">
                           </td>
                           <td> </td>
                           
                       </tr>
                       <tr class="d-flex">
                           <th class="col-3">Jenis Kelamin</th>
                           <td class="col-5">
                               <div class="form-check form-check-inline">
                                   <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                                   <label class="form-check-label" for="inlineRadio1">Laki - Laki</label>
                                 </div>
                                 <div class="form-check form-check-inline">
                                   <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                                   <label class="form-check-label" for="inlineRadio2">Perempuan</label>
                                 </div>
                           </td>
                           <td> </td>
                       </tr>
                       <tr class="d-flex">
                           <th class="col-3">Alamat Lengkap</th>
                           <td class="col-5">
                               <input type="text" class="form-control" id="inputAddress" placeholder="Masukan alamat pengguna">
                           </td>
                           <td></td>
                       </tr>
                       <tr  class="d-flex">
                           <th class="col-3">No Handphone</th>
                           <td  class="col-5">
                               <input type="number" class="form-control" id="inputPhone" placeholder="0819000XX">
                           </td>
                           <td> </td>
                       </tr>
               </table>
               <br>
           </div>
           <br>

           <div class="container-fluid table" >
               <h5 class=" content-title ">
                   Informasi Akun
               </h5> 
               <hr>
               <table class="table table-striped">
                   <tbody class="body-table">
                       <tr class="d-flex">
                           <th class="col-3">E-mail</th>
                           <td class="col-5">
                                 <input type="email" class="form-control" id="InputEmail" aria-describedby="email" placeholder="Masukan email pengguna">
                           </td>
                           <td> </td>
                       </tr>
                       <tr class="d-flex">
                           <th class="col-3">Password</th>
                           <td class="col-5">
                               <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                           </td>
                           <td> </td>
                       </tr>
                       <tr class="d-flex">
                           <th class="col-3">Re-Type Password</th>
                           <td class="col-5">
                               <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                           </td>
                           <td> </td>
                       </tr>
                       <tr class="d-flex">
                           <th class="col-3">Departemen</th>
                           <td class="col-5">
                               <div class="dropdown show">
                                   <a class="btn " href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                       Tata Usaha <i class="fa fa-chevron-down" aria-hidden="true"></i>
                                   </a>
                                 
                                   <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                     <a class="dropdown-item" href="#">Tata Usaha</a>
                                     <a class="dropdown-item" href="#">Pengajar</a>
                                     <a class="dropdown-item" href="#">Admin</a>
                                     <a class="dropdown-item" href="#">Staf</a>
                                   </div>
                                 </div>
                           </td>
                           <td> 
                               <button class="btn btn-white" data-toggle="modal" data-target="#exampleModal2">
                                   <img src="super-admin/images/icon/plus_blue.svg" aria-hidden="true"> 
                                       Tambah Departemen
                               </button>
                     
                           </td>
                       </tr>
                      
                   </tbody>
               </table>
               <br>
           </div>
           <hr>
       <div class="container-fluid battom-nav">
                   <button class="btn btn-white" ><img src="super-admin/images/icon/stop_blue.svg" aria-hidden="true"> Batal</button>
                   <button class="btn btn-blue" ><i class="fa fa-save" aria-hidden="true"></i> Simpan Perubahan</button>
            
           </div>
           
                                                       
       </div>
   </div>

</div>
@endsection
