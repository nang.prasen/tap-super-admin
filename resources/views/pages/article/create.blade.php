@extends('layouts.super-admin')
@section('content')
    <div class="section-content" data-aos="fade-up" data-aos-delay="500">
        <div class="container-fluid">
            <!-- Breandcrumb -->
            <section class="store-breadcrumbs">
                <div class="row">
                    <div class="col-lg-12">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/dashboard.html">Berita</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Buat Postingan</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </section>
            <div class="row">
                <div class="col-lg-12">
                    <div class="row justify-content-between">
                        <div class="col-lg-6">
                            <div class="dashboard-heading">
                                <h5 class="dashboard-title">
                                    Buat Berita Umum
                                </h5>
                                <p class="dashboard-subtitle">
                                    Isi berita dengan benar
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <section class="berita">
                <form action="{{ route('article.store') }}" id="form-addArtikel" method="POST"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="container-fluid table">
                                <div class="form">
                                    <div class="form-group">
                                        <label class="form-check-label">Judul Berita</label>
                                        <input type="text" id="title" name="title" placeholder="Masukkan Judul Anda"
                                            class="form-control" value="{{ old('title') }}">
                                    </div>

                                    <div class="form-group">
                                        <div class="container-fluid upload-template justify-content-center"><br>
                                            <div class="custom-file">
                                                <label for="customFile"><img
                                                        src="{{ asset('super-admin/images/icon/img-input.svg') }}" alt="">
                                                </label>
                                            </div>
                                            <p>Click your image file here or
                                                <a href=""> <label for="customFile"> <b> browse</b></label></a>
                                            </p>
                                            <p><b> Max. File Size: 2MB</b></p>
                                        </div>
                                        <br>
                                        <input type="file" class="form-control-files" id="customFile" name="thumbnail">
                                    </div>

                                </div>
                                <div>
                                    <textarea class="form-control" value="{{old('content')}}" rows="10"
                                        placeholder="Masukkan isi berita" id="content" name="content"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="container-fluid table">
                                <div class="titile-content">
                                    Opsi
                                </div>
                                <div class="subtitle-content">
                                    Kategori Berita
                                </div>
                                <br>
                                <div class="form">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="category"
                                            id="exampleRadios1" value="Internal">
                                        <label class="form-check-label" for="exampleRadios1">
                                            Internal
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="category"
                                            id="exampleRadios2" value="Umum" checked>
                                        <label class="form-check-label" for="exampleRadios2">
                                            Umum
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="container-fluid battom-nav">
                            <a href="{{route('article.index')}}" name="status" value="draft" class="btn btn-blue" id="btnoke-draf"> 
                                <i class="fa fa-arrow-left" aria-hidden="true"></i> Kembali</a>
                            <button type="button" name="status" value="draft" class="btn btn-white" id="btnoke-draf"> <i
                                    class="fa fa-bookmark" aria-hidden="true"></i> Draft</button>
                            <button type="button" name="status" value="publish" class="btn btn-blue" id="btnoke-publish"><i
                                    class="fa fa-send" aria-hidden="true"></i> Publish</button>
                        </div>
                    </div>
                </form>
                <br>
            </section>
            
        </div>
    </div>

@endsection
@push('after-scripts')
    <script>
        $(document).ready(function() {

            var title = $("#title").val();
            var content = $("#content").val();
            var category = $("#category").val();
            var thumbnail = $("#thumbnail").val();
            var status = $("#status").val();

            $("#btnoke-draf").hide();
            $("#btnoke-publish").hide();

            // Hide submit button if either field is empty
            $('#form-addArtikel').keyup(function() {
                if (title == "" || content == "" || category == "" || status == "" || thumbnail == "") {
                    $('#btnoke-draf').show();
                    $('#btnoke-publish').show();
                    $("#btndis-draf").css("display", "none");
                    $("#btndis-publish").css("display", "none");
                } else {
                    $('#btndis-draf').hide();
                    $('#btndis-publish').hide();
                    $("#btndis-draf").css("display", "none");
                    $("#btndis-publish").css("display", "none");
                }
            });
            // Don't submit form if either field is empty
            $('#form-addArtikel').submit(function() {
                if (title == "" || content == "" || category == "" || status == "" || thumbnail == "") {
                    return true;
                }
            });
        });
    </script>
@endpush
