@extends('layouts.super-admin')
@section('content')
    <div class="section-content" data-aos="fade-up" data-aos-delay="500">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row justify-content-between">
                        <div class="col-lg-6">
                            <div class="dashboard-heading">
                                <h5 class="dashboard-title">
                                    Daftar Artikel
                                </h5>
                                <p class="dashboard-subtitle">
                                    Riwayat Artikel
                                </p>
                            </div>
                        </div>
                        <div class="col-lg-6" style="text-align: right;">
                            <div class="dashboard-heading ">
                                <a href="{{ route('article.create') }}">
                                    <button class="btn btn-blue"><i class="fa fa-plus-circle" aria-hidden="true"></i> Posting Artikel</button>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="dashboard-heading">
                        <div class="container-fluid table">
                            <br>
                            <table class="table table-striped " id="table-tambah-pengguna">
                                <thead class="head-table">
                                    <tr>
                                        <th>No</th>
                                        <th>Judul Berita </th>
                                        <th>Tgl Pembuatan</th>
                                        <th>Kategori</th>
                                        <th>Status</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody class="body-table">
                                    @foreach ($article as $item)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $item->title }}</td>
                                            <td>{{ $item->created_at }}</td>
                                            <td>{{ $item->category }}</td>
                                            <td>{{ $item->status }}</td>
                                            <td>
                                                <!-- Default dropleft button -->
                                                <div class="btn-group dropleft">
                                                    <button type="button" class="btn btn-secondary fa fa-ellipsis-v"
                                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                                    </button>
                                                    <div class="dropdown-menu">
                                                        <!-- Dropdown menu links -->
                                                        <a class="dropdown-item " href="{{route('article.show',$item->id)}}">
                                                            <img src="super-admin/images/icon/profile.svg" alt=""> Lihat</a>
                                                        <a class="dropdown-item" href="{{route('article.edit',$item->id)}}"><img
                                                                src="super-admin/images/icon/edit.svg" alt=""> Edit</a>
                                                        <a class="dropdown-item red-button" href="#"><img
                                                                src="super-admin/images/icon/love.svg" alt=""> Publish</a>
                                                        <a class="dropdown-item" href="#"><img
                                                                src="super-admin/images/icon/trash.svg" alt=""> Hapus</a>

                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach


                                </tbody>
                            </table>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>






@endsection
