@extends('layouts.super-admin')
@section('content')
    <div class="section-content" data-aos="fade-up" data-aos-delay="500">
        <div class="container-fluid">
            <!-- Breandcrumb -->
            <section class="store-breadcrumbs">
                <div class="row">
                    <div class="col-lg-12">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/dashboard.html">Berita</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Buat Postingan</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </section>
            <div class="row">
                <div class="col-lg-12">
                    <div class="row justify-content-between">
                        <div class="col-lg-6">
                            <div class="dashboard-heading">
                                <h5 class="dashboard-title">
                                    Edit Berita Umum
                                </h5>
                                <p class="dashboard-subtitle">
                                    Isi berita dengan benar
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <section class="berita">
                <form action="{{ route('article.update',$article_edit->id) }}" id="form-addArtikel" method="POST"
                    enctype="multipart/form-data">
                    @method('PUT')
                    @csrf
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="container-fluid table">
                                <div class="form">
                                    <div class="form-group">
                                        <label class="form-check-label">Judul Berita</label>
                                        <input type="text" id="title" name="title" placeholder="Masukkan Judul Anda"
                                            class="form-control" value="{{ $article_edit->title }}">
                                    </div>

                                    <div class="form-group">
                                        <div class="container-fluid upload-template justify-content-center"><br>
                                            <div class="custom-file">
                                                <label for="customFile" >
                                                    <img style="height: 150px; width:150px;" src="{{$article_edit->thumbnail}}" alt="">
                                                </label>
                                            </div>
                                            
                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <p>Click your image file here or
                                            <a href=""> <label for="customFile"> <b> browse</b></label></a>
                                            <p><b> Max. File Size: 2MB</b></p>
                                        </p>
                                        <input type="file" class="form-control-files" id="customFile" name="thumbnail" value="{{$article_edit->thumbnail}}">
                                    </div>

                                </div>
                                <div>
                                    <textarea class="form-control" value="{{old('content')}}" rows="10"
                                        placeholder="Masukkan isi berita" id="content" name="content">{{$article_edit->content}}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="container-fluid table">
                                <div class="titile-content">
                                    Opsi
                                </div>
                                <div class="subtitle-content">
                                    Kategori Berita
                                </div>
                                <br>
                                <div class="form">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="category"
                                            id="exampleRadios1" value="Internal" {{ $article_edit->category == 'Internal' ? 'checked':'' }}>
                                        <label class="form-check-label" for="exampleRadios1">
                                            Internal
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="category"
                                            id="exampleRadios2" value="Umum" {{ $article_edit->category == 'Umum' ? 'checked':'' }}>
                                        <label class="form-check-label" for="exampleRadios2">
                                            Umum
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="container-fluid table">
                                <div class="titile-content">
                                    Status
                                </div>
                                <div class="subtitle-content">
                                    Status Berita
                                </div>
                                <br>
                                <div class="form">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="status"
                                            id="exampleRadios1" value="publish" {{ $article_edit->status == 'publish' ? 'checked':'' }}>
                                        <label class="form-check-label" for="exampleRadios1">
                                            Publish
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="status"
                                            id="exampleRadios2" value="draft" {{ $article_edit->status == 'draft' ? 'checked':'' }}>
                                        <label class="form-check-label" for="exampleRadios2">
                                            Draft
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="container-fluid battom-nav">
                            <a href="{{route('article.index')}}" name="status" value="draft" class="btn btn-blue"> 
                                <i class="fa fa-arrow-left" aria-hidden="true"></i> Kembali</a>
                            <button type="submit" class="btn btn-blue" id="btndis-publish"><i
                                    class="fa fa-send" aria-hidden="true"></i> Update</button>
                        </div>
                    </div>
                </form>
                <br>
            </section>
            
        </div>
    </div>

@endsection
