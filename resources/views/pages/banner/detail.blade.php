@extends('layouts.super-admin')
@section('content')
<div id="page-content-wrapper">
    <!--Content Dashboard-->
    <!-- ........................ -->
    <div class="section-content section-dashboard-bukuterbaru" data-aos="fade-up" data-aos-delay="500">
        <div class="container-fluid">
            <!-- Breandcrumb -->
            <section class="store-breadcrumbs">
                <div class="row">
                    <div class="col-lg-12">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/dashboard.html">Instansi</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Detail</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </section>
            <!-- Judul -->
            <div class="row justify-content-between">

            </div>
            <!--Aksi Tambah Pengguna-->
            <br>
            <div class="container-fluid table">
                <div class="">
                    <h5 class=" content-title data">
                        Detail Instansi
                    </h5>
                </div>
                <table class="table table-striped ">
                    <tbody class="body-table">
                        <tr class="d-flex">
                            <th class="col-3">Logo</th>
                            <td class="col-5">
                                <img class="rounded-circle mr-2 profile-picture" src="super-admin/images/icon-user.png"
                                    alt="">
                            </td>
                            <td class="col-4"> </td>
                        </tr>
                        <tr class="d-flex">
                            <th class="col-3">Id Instansi</th>
                            <td class="col-5">
                                <p>126</p>
                            </td>
                            <td> </td>
                        </tr>
                        <tr class="d-flex">
                            <th class="col-3">Nama Instansi</th>
                            <td class="col-5">
                                <p>SMA N 1 Yogyakarta</p>
                            </td>
                            <td> </td>
                        </tr>
                        <tr class="d-flex">
                            <th class="col-3">Jenis Instansi</th>
                            <td class="col-5">
                                <p>SMA</p>
                            </td>
                            <td> </td>
                        </tr>
                        <tr class="d-flex">
                            <th class="col-3">No. Telpon</th>
                            <td class="col-5">
                                082220093198
                            </td>
                            <td> </td>

                        </tr>
                        <tr class="d-flex">
                            <th class="col-3">Alamat</th>
                            <td class="col-5">
                                <p>Yogyakarta</p>
                            </td>
                            <td> </td>
                        </tr>
                        <tr class="d-flex">
                            <th class="col-3">Jumlah User</th>
                            <td class="col-5">
                                200
                            </td>
                            <td></td>
                        </tr>
                        <tr class="d-flex">
                            <th class="col-3">Jumlah Buku</th>
                            <td class="col-5">
                                150
                            </td>
                            <td> </td>
                        </tr>

                        <tr class="d-flex">
                            <th class="col-3">Visi</th>
                            <td class="col-5">
                                Menjadi Sekolah Teladan
                            </td>
                            <td> </td>
                        </tr>
                        <tr class="d-flex">
                            <th class="col-3">Misi</th>
                            <td class="col-5">
                                Menjadi Sekolah Unggulan & Teladan
                            </td>
                            <td> </td>
                        </tr>
                    </tbody>
                </table>
                <br>
            </div>
        </div>
    </div>
</div>
@endsection
