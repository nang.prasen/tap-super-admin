@extends('layouts.super-admin')
@section('content')
    <div class="section-content" data-aos="fade-up" data-aos-delay="500">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row justify-content-between">
                        <div class="col-lg-6">
                            <div class="dashboard-heading">
                                <h5 class="dashboard-title">
                                    Daftar Banner
                                </h5>
                                <p class="dashboard-subtitle">
                                    Banner untuk beberapa kategori
                                </p>
                            </div>
                        </div>
                        
                    </div>

                    
                </div>
            </div>
            <div class="dashboard-heading">
                <h5 class="dashboard-title">
                    Banner Dashboard Instansi
                </h5>
            </div>
            <!-- Banner Utama Dashboard-->
            <section class="content-dashoboard-admin">
                <div class="container-fluid table">
                    <div class="row">
                        @foreach ($banner1 as $item)
                        <div class="col-sm-3">
                            <div class="card">
                                <div class="card-header text-right">
                                    <div class="dropdown">
                                        <button class="btn btn-secondary " type="button" id="dropdownMenuButton"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            {{-- <a class="dropdown-item" onclick="loadDeleteModal({{ $item->id }}, `{{ $item->name }}`)">Hapus</a> --}}
                                            <a class="dropdown-item" onclick="return confirm('Yakin ingin menghapus banner ini?')" href="{{route('banner.destroy', $item->id)}}">Hapus</a>
                                        </div>
                                    </div>
                                </div>
                                @if ($item->type == "dashboard_instansi")
                                    <img class="card-img-top dashboard" src="{{$item->image}}">
                                @endif
                                <!-- <div class="card-body">
                                </div> -->
                            </div>
                        </div>
                        @endforeach
                        
                        
                    </div>


                </div>
                <button type="button" data-toggle="modal" id="addBannerDashboard" data-target="#addBannerDashboard" class="btn btn-white"><i class="fa fa-plus-circle" aria-hidden="true"></i> Posting
                    Banner</button>
  
                <br><br>
                

                <!-- Banner Katalog -->
                <div class="dashboard-heading">
                    <h5 class="dashboard-title">
                        Banner Katalog Instansi
                    </h5>
                </div>
                <div class="container-fluid table">
                    <div class="row">
                        @foreach ($banner2 as $item)
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header text-right">
                                    <div class="dropdown">
                                        <button class="btn btn-secondary " type="button" id="dropdownMenuButton"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item" onclick="return confirm('Yakin ingin menghapus banner ini?')" href="{{route('banner.destroy', $item->id)}}">Hapus</a>
                                        </div>
                                    </div>
                                </div>
                                @if ($item->type == "katalog_instansi")
                                    
                                <img class="card-img-top dashboard" src="{{$item->image}}"
                                    alt="Card image cap">
                                @endif
                                <!-- <div class="card-body">
                                </div> -->
                            </div>
                        </div>
                        @endforeach
                    </div>


                </div>
                <button type="button" data-toggle="modal" id="addBannerKatalog" data-target="#addBannerKatalog" class="btn btn-white"><i class="fa fa-plus-circle" aria-hidden="true"></i> Posting
                    Banner</button>
                <br><br>


                <!-- Banner Pengguna -->
                <div class="dashboard-heading">
                    <h5 class="dashboard-title">
                        Banner Pengguna
                    </h5>
                </div>
                <div class="container-fluid table">
                    <div class="row">
                        @foreach ($banner3 as $item)
                        <div class="col-sm-4">
                            <div class="card">
                                <div class="card-header text-right">
                                    <div class="dropdown">
                                        <button class="btn btn-secondary " type="button" id="dropdownMenuButton"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item" onclick="return confirm('Yakin ingin menghapus banner ini?')" href="{{route('banner.destroy', $item->id)}}">Hapus</a>
                                        </div>
                                    </div>
                                </div>
                                @if ($item->type == "user_instansi")
                                    
                                <img class="card-img-top dashboard" src="{{$item->image}}"
                                    alt="Card image cap">
                                @endif
                                <!-- <div class="card-body">
                                </div> -->
                            </div>
                        </div>
                        @endforeach
                    </div>


                </div>
                <button type="button" data-toggle="modal" id="addBannerPengguna" data-target="#addBannerPengguna" class="btn btn-white"><i class="fa fa-plus-circle" aria-hidden="true"></i> Posting
                    Banner</button>
                <br><br>
            </section>
        </div>
    </div>

    




@endsection
<!-- Modal Hapus-->
<div class="modal fade" id="deleteBanner" data-backdrop="static" tabindex="-1" role="dialog"
             aria-labelledby="deleteBanner" aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">This action is not reversible.</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Are you sure you want to delete <span id="modal-banner_name"></span>?
                        <input type="text" id="id" name="id">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn bg-white" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-danger" id="modal-confirm_delete">Delete</button>
                    </div>
                </div>
            </div>
        </div>
<!-- Add Banner Dashboard-->
<div class="modal fade" id="addBannerDashboard" tabindex="-1" role="dialog" aria-labelledby="addBannerDashboard" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="addBannerDashboard">Tambah Banner Dashboard Instansi</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="{{route('banner.store')}}" method="POST" enctype="multipart/form-data">
            @csrf  
            <div class="form-group">
                <label for="nama">Judul Banner</label>
                <input type="text" id="name" name="name"class="form-control" placeholder="judul banner">
                <label for="link">Link Eksternal</label>
                <input type="text" id="link" name="link"class="form-control" placeholder="link banner">
                  <input type="hidden" name="type" value="dashboard_instansi">
                <label for="image">Gambar Banner</label>
                  <input type="file" id="image" accept="image/*" name="image"class="form-control" onchange="previewFile(this)">
                  <div class="col-md-12">
                      <div class="mt-1 text-center">
                          <img id="img" class="images-preview-div">
                          <img id="previewImg" style="max-width:100px">
                      </div>
                  </div>
            </div>
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Upload</button>
        </div>
        </form>
      </div>
    </div>
  </div>

<!-- Add Banner Katalog-->
<div class="modal fade" id="addBannerKatalog" tabindex="-1" role="dialog" aria-labelledby="addBannerKatalog" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="addBannerKatalog">Tambah Banner Katalog Instansi</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="{{route('banner.store')}}" method="POST" enctype="multipart/form-data">
            @csrf  
            <div class="form-group">
                <label for="nama">Judul Banner</label>
                <input type="text" id="name" name="name"class="form-control" placeholder="judul banner">
                <label for="link">Link Eksternal</label>
                <input type="text" id="link" name="link"class="form-control" placeholder="link banner">
                  <input type="hidden" name="type" value="katalog_instansi">
                <label for="image">Gambar Banner</label>
                  <input type="file" id="image" accept="image/*" name="image"class="form-control" onchange="previewFile(this)">
                  <div class="col-md-12">
                      <div class="mt-1 text-center">
                          <img id="img" class="images-preview-div">
                          <img id="previewImg" style="max-width:100px">
                      </div>
                  </div>
            </div>
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Upload</button>
        </div>
        </form>
      </div>
    </div>
  </div>
  
<!-- Add Banner User-->
<div class="modal fade" id="addBannerPengguna" tabindex="-1" role="dialog" aria-labelledby="addBannerPengguna" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="addBannerPengguna">Tambah Banner Pengguna/User Instansi</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="{{route('banner.store')}}" method="POST" enctype="multipart/form-data">
            @csrf  
            <div class="form-group">
                <label for="nama">Judul Banner</label>
                <input type="text" id="name" name="name"class="form-control" placeholder="judul banner">
                <label for="link">Link Eksternal</label>
                <input type="text" id="link" name="link"class="form-control" placeholder="link banner">
                  <input type="hidden" name="type" value="user_instansi">
                <label for="image">Gambar Banner</label>
                  <input type="file" id="image" accept="image/*" name="image"class="form-control" onchange="previewFile(this)">
                  <div class="col-md-12">
                      <div class="mt-1 text-center">
                          <img id="img" class="images-preview-div">
                          <img id="previewImg" style="max-width:100px">
                      </div>
                  </div>
            </div>
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Upload</button>
        </div>
        </form>
      </div>
    </div>
  </div>
@push('push-script')

@endpush
