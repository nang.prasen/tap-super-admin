@extends('layouts.super-admin')
@section('content')
    <div id="page-content-wrapper">
        <!--Content Dashboard-->
        <!-- ........................ -->
        <div class="section-content section-dashboard-bukuterbaru" data-aos="fade-up" data-aos-delay="500">
            <div class="container-fluid">
                <!-- Breandcrumb -->
                <section class="store-breadcrumbs">
                    <div class="row">
                        <div class="col-lg-12">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Penerbit</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Tambah Penerbit</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </section>
                <!-- Judul -->
                <div class="row justify-content-between">

                </div>
                <!--Aksi Tambah Pengguna-->
                <br>
                <form action="{{ route('publisher.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="container-fluid table">
                        <div class="">
                        <h5 class=" content-title data">
                            Data Penerbit
                            </h5>
                        </div>
                        <hr>
                        <table class="table table-striped ">
                            <tbody class="body-table">
                                <tr class="d-flex">
                                    <th class="col-3">Logo</th>
                                    <td class="col-5">
                                        <input type="file" class="" name="logo" accept="image/jpeg,image/png">
                                        <small id="passwordHelpBlock" class="form-text text-muted">
                                            Format gambar .jpeg atau .png
                                        </small>
                                    </td>
                                    <td class=" col-4">
                                        @error('logo')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </td>
                                </tr>
                                <tr class="d-flex">
                                    <th class="col-3">Nama Perusahaan</th>
                                    <td class="col-5">
                                        <input type="text" name="name"
                                            class="form-control @error('name') is-invalid @enderror"
                                            value="{{ old('name') }}" id="NamaLengkap" aria-describedby="nama"
                                            placeholder="Masukkan Nama Perusahaan Penerbit">
                                    </td>
                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror

                                </tr>
                                <tr class="d-flex">
                                    <th class="col-3">ID Penerbit</th>
                                    <td class="col-5">
                                        <input type="number" name="publisherId" class="form-control @error('publisherId') is-invalid @enderror"
                                        value="{{ old('publisherId') }}" id="NamaLengkap"
                                        aria-describedby="nama" placeholder="Masukkan Id Penerbit">
                                    </td>
                                    @error('publisherId')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror

                                </tr>
                                <tr class="d-flex">
                                    <th class="col-3">Alamat Lengkap</th>
                                    <td class="col-5">
                                        <textarea name="address" class="form-control @error('address') is-invalid @enderror"
                                        value="{{ old('address') }}" id="address" rows="3"
                                            placeholder="Masukkan Alamat Lengkap"></textarea>
                                    </td>
                                    @error('address')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror

                                </tr>

                                <tr class="d-flex">
                                    <th class="col-3">No. Telpon</th>
                                    <td class="col-5">
                                        <input type="number" name="phone" class="form-control @error('phone') is-invalid @enderror"
                                        value="{{ old('phone') }}" id="NamaLengkap" aria-describedby="nama"
                                            placeholder="Masukkan Nomor Telepon Penerbit">
                                    </td>
                                    @error('phone')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror

                                </tr>
                                <tr class="d-flex">
                                    <th class="col-3">Email</th>
                                    <td class="col-5">
                                        <input type="email" name="email" class="form-control @error('email') is-invalid @enderror"
                                        value="{{ old('email') }}" id="NamaLengkap" aria-describedby="nama"
                                            placeholder="Masukkan Email Penerbit">
                                    </td>
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </tr>
                        </table>
                    </div>
                    <div class="container-fluid battom-nav">
                        <a href="{{ url('publisher') }}" class="btn btn-white"><img
                                src="super-admin/images/icon/stop_blue.svg" aria-hidden="true">
                            Batal
                        </a>
                        <button type="submit" class="btn btn-blue"><i class="fa fa-save" aria-hidden="true"></i> Tambah Penerbit
                        </button>
                    </div>
                </form>

            </div>
        </div>
    </div>
@endsection
