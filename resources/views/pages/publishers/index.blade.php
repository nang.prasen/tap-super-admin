@extends('layouts.super-admin')
@section('content')
<div class="section-content" data-aos="fade-up" data-aos-delay="500">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="row justify-content-between">
                    <div class="col-lg-6">
                        <div class="dashboard-heading">
                            <h5 class="dashboard-title">
                                Daftar Penerbit
                            </h5>
                            <p class="dashboard-subtitle">
                               Daftar Penerbit Yang Bekerja
                            </p>
                        </div>
                    </div>
                    @if (Auth::user()->role == 'SuperAdmin')
                        
                    <div class="col-lg-6" style="text-align: right;">
                        <div class="dashboard-heading">
                            <a class="btn btn-blue" href="{{url('create-publisher')}}" ><img src="super-admin/images/icon/plus_white.svg" aria-hidden="true"> Tambah Penerbit</a>
                            
                        </div>
                    </div>
                    @endif
                </div>

                <div class="dashboard-heading">
                    <div class="container-fluid table" >
                        <br>
                        <table class="table table-striped " id="table-tambah-pengguna">
                            <thead class="head-table">
                                <tr>
                                    <th>No</th>
                                    <th>Nama Penerbit </th>
                                    <th>ID Penerbit </th>
                                    <th>Jumlah Buku</th>
                                    <th>Alamat</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody class="body-table">
                                @foreach ($penerbit as $item)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$item->name}}</td>
                                    <td>{{$item->publisherId}}</td>
                                    <td>{{$item->jumlah_buku}}</td>
                                    <td>{{$item->address}}</td>
                                    <td>
                                        <a href="{{route('publisher.detail',$item->id)}}" style="text-decoration: none;"> <img src="super-admin/images/icon/eye.svg" alt=""> Lihat</a>
                                    </td>
                                </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                        <br>
                    </div>

                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="row justify-content-between">
                    <div class="col-lg-6">
                        <div class="dashboard-heading">
                            <h5 class="dashboard-title">
                                Daftar User Penerbit
                            </h5>
                            <p class="dashboard-subtitle">
                               Daftar User Penerbit Yang Bekerja
                            </p>
                        </div>
                    </div>
                    @if (Auth::user()->role == 'SuperAdmin')
                        
                    <div class="col-lg-6" style="text-align: right;">
                        <div class="dashboard-heading">
                            
                            <a class="btn btn-blue" href="{{url('publisher/register-user')}}" ><img src="super-admin/images/icon/user-active.svg" aria-hidden="true"> + User</a>
                        </div>
                    </div>
                    @endif
                </div>

                <div class="dashboard-heading">
                    <div class="container-fluid table" >
                        <br>
                        <table class="table table-striped " id="table-user-penerbit">
                            <thead class="head-table">
                                <tr>
                                    <th>No</th>
                                    <th>Nama Penerbit </th>
                                    <th>Email Penerbit </th>
                                    <th>Role</th>
                                </tr>
                            </thead>
                            <tbody class="body-table">
                                @foreach ($userPublisher as $item)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$item->name}}</td>
                                    <td>{{$item->email}}</td>
                                    <td>
                                        <div class="btn-status success">
                                            {{$item->role}}
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                        <br>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>
@endsection
@push('after-script')
<script>
$(document).ready(function() {
    var table = $('#table-user-penerbit').DataTable({
        lengthChange: false,
        dom:
        // 'Bfrtip',
            "<'row'<'col-md-3'f><'col-md-6'><'col-md-3'B>>" +
            "<'row'<'col-md-12'tr>>" +
            "<'row'<'col-sm-3'i><'col-sm-3'><'col-sm-3'p>>",
        buttons: [{
            extend: 'csvHtml5',
            text: ' Export   <i class="fa  fa-cloud-download"></i>',
            titleAttr: 'CSV'
        }, ],

        language: {
            searchPlaceholder: "Search",
            search: "",
        },
    });

    table.buttons().container()
        .appendTo('#table_wrapper .col-md-5:eq(0)');
});
</script>
@endpush
