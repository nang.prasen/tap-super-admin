@extends('layouts.super-admin')
@section('content')
<div id="page-content-wrapper">
    <!--Content Dashboard-->
    <div class="section-content aos-init" data-aos="fade-up" data-aos-delay="500">
        <div class="container-fluid">
            <!-- Breandcrumb -->
            <section class="store-breadcrumbs">
                <div class="row">
                    <div class="col-lg-12">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="data-penerbit.html">Data Penerbit</a>
                                </li>
                                {{-- <li class="breadcrumb-item"><a
                                        href="#">Gramedia Pustaka</a></li> --}}
                                <li class="breadcrumb-item active" aria-current="page">Rekapitulasi</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </section>

            <!--Content Detail Buku-->
            <section class="section-inventaris">
                <div class="row">
                    <div class="col-lg-3" style="">
                        <div class="book-rekapitulasi">
                            <img src="{{$rekap->cover}}" alt="" class="img-detail-book" style="">
                        </div>
                        <br />
                    </div>
                    <div class="col-lg-9" style="">
                        <div class="container">
                            <div class="book-rekapitulasi-detail">
                                <div class="dashboard-heading">
                                    <h6 class="dashboard-title">
                                        {{$rekap->title}}
                                    </h6>
                                    <p class="dashboard-subtitle">
                                        {{$rekap->author}}
                                    </p>
                                    <div class="container border">
                                        <h5 class=" sinopsis-title ">Sinopsis Buku</h5>
                                        <p>{{$rekap->description}}
                                            <a href=" ">Lihat Detail Buku </a></p>
                                    </div>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
            </section>

            <!-- Judil Detail Buku -->
            <div class=" row">
                <div class="col-lg-9">
                    <h5 class="dashboard-title">
                        List Distribusi Buku
                    </h5>
                    <p class="dashboard-subtitle">
                        Daftar peminjaman buku selama periode tertentu
                    </p>
                </div>
            </div>

            <div class="dashboard-heading">
                <div class="container-fluid table">
                    <br>
                    <div id="table-tambah-pengguna_wrapper"
                        class="dataTables_wrapper dt-bootstrap4 no-footer">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-striped dataTable no-footer"
                                    id="table-tambah-pengguna" role="grid"
                                    aria-describedby="table-tambah-pengguna_info">
                                    <thead class="head-table">
                                        <tr role="row">
                                            <th class="sorting sorting_asc" tabindex="0"
                                                aria-controls="table-tambah-pengguna" rowspan="1"
                                                colspan="1" aria-sort="ascending"
                                                aria-label="No: activate to sort column descending"
                                                style="">No</th>
                                            <th class="sorting" tabindex="0"
                                                aria-controls="table-tambah-pengguna" rowspan="1"
                                                colspan="1"
                                                aria-label="Nama Instansi : activate to sort column ascending"
                                                style="">Nama Instansi
                                            </th>
                                            <th class="sorting" tabindex="0"
                                                aria-controls="table-tambah-pengguna" rowspan="1"
                                                colspan="1"
                                                aria-label="Jenis Instansi: activate to sort column ascending"
                                                style="">Jenis Instansi</th>
                                            <th class="sorting" tabindex="0"
                                                aria-controls="table-tambah-pengguna" rowspan="1"
                                                colspan="1"
                                                aria-label="Jumlah Eksemplar: activate to sort column ascending"
                                                style="">Jumlah Eksemplar</th>
                                            <th class="sorting" tabindex="0"
                                                aria-controls="table-tambah-pengguna" rowspan="1"
                                                colspan="1"
                                                aria-label="Tangga; Distribusi: activate to sort column ascending"
                                                style="">Tanggal Distribusi</th>
                                            <th class="sorting" tabindex="0"
                                                aria-controls="table-tambah-pengguna" rowspan="1"
                                                colspan="1"
                                                aria-label="Status: activate to sort column ascending"
                                                style="">Status
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody class="body-table">
                                        @foreach ($pitulasi as $item)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{isset($item->agencies->name) ? $item->agencies->name:'Tidak ditemukan'}}</td>
                                            <td>SMA</td>
                                            <td> {{isset($item->qty) ? $item->qty:'Tidak ditemukan'}}</td>
                                            <td>{{isset($item->created_at) ? $item->created_at:'Tidak ditemukan'}}</td>
                                            <td>
                                                <div class="text-success">
                                                    Aktif
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                        
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <br>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
