@extends('layouts.super-admin')
@section('content')
    <div id="page-content-wrapper">
        <!--Content Dashboard-->
        <!-- ........................ -->
        <div class="section-content section-dashboard-bukuterbaru" data-aos="fade-up" data-aos-delay="500">
            <div class="container-fluid">
                <!-- Breandcrumb -->
                <section class="store-breadcrumbs">
                    <div class="row">
                        <div class="col-lg-12">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Penerbit</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Tambah User Penerbit</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </section>
                <!-- Judul -->
                <div class="row justify-content-between">

                </div>
                <!--Aksi Tambah Pengguna-->
                <br>
                <form action="{{ route('publisher.user.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="container-fluid table">
                        <div class="">
                        <h5 class=" content-title data">
                            Data User Penerbit
                            </h5>
                        </div>
                        <hr>
                        <table class="table table-striped ">
                            <tbody class="body-table">
                                <tr class="d-flex">
                                    <th class="col-3">ID Penerbit</th>
                                    <td class="col-5">
                                        <input type="number" name="publisherId" class="form-control @error('publisherId') is-invalid @enderror"
                                        value="{{ old('publisherId') }}" id="NamaLengkap"
                                        aria-describedby="nama" placeholder="Masukkan Id Penerbit" required>
                                    </td>
                                    @error('publisherId')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror

                                {{-- </tr>
                                <tr class="d-flex">
                                    <th class="col-3">Photo</th>
                                    <td class="col-5">
                                        <input type="file" class="" name="logo" accept="image/jpeg,image/png">
                                        <small id="passwordHelpBlock" class="form-text text-muted">
                                            Format gambar .jpeg atau .png
                                        </small>
                                    </td>
                                    <td class=" col-4">
                                        @error('logo')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </td>
                                </tr> --}}
                                {{-- <tr class="d-flex">
                                    <th class="col-3">Nama</th>
                                    <td class="col-5">
                                        <input type="text" name="name"
                                            class="form-control @error('name') is-invalid @enderror"
                                            value="{{ old('name') }}" id="NamaLengkap" aria-describedby="nama"
                                            placeholder="Masukkan Nama User Penerbit" required>
                                    </td>
                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror

                                </tr> --}}
                                
                                <tr class="d-flex">
                                    <th class="col-3">Email</th>
                                    <td class="col-5">
                                        <input type="email" name="email" class="form-control @error('email') is-invalid @enderror"
                                        value="{{ old('email') }}" id="NamaLengkap" aria-describedby="nama"
                                            placeholder="Masukkan Email Penerbit" required>
                                    </td>
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </tr>
                                <tr class="d-flex">
                                    <th class="col-3">Password</th>
                                    <td class="col-5">
                                        <input type="password" name="password" class="form-control @error('password') is-invalid @enderror"
                                        value="{{ old('password') }}" id="NamaLengkap" aria-describedby="password"
                                            placeholder="Masukkan password" required>
                                    </td>
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </tr>
                                <tr class="d-flex">
                                    <th class="col-3">Password</th>
                                    <td class="col-5">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                    </td>
                                </tr>
                        </table>
                    </div>
                    <div class="container-fluid battom-nav">
                        <a href="{{ url('publisher') }}" class="btn btn-white"><img
                                src="super-admin/images/icon/stop_blue.svg" aria-hidden="true">
                            Batal
                        </a>
                        <button type="submit" class="btn btn-blue"><i class="fa fa-save" aria-hidden="true"></i> Tambah Penerbit
                        </button>
                    </div>
                </form>

            </div>
        </div>
    </div>
@endsection
