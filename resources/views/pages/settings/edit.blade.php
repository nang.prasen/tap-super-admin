@extends('layouts.super-admin')
@section('content')
<div id="page-content-wrapper">
    <!--Content Dashboard-->
    <!-- ........................ -->
    <div class="section-content section-dashboard-bukuterbaru" data-aos="fade-up" data-aos-delay="500">
        <div class="container-fluid">
            <!-- Breandcrumb -->
            <section class="store-breadcrumbs">
                <div class="row">
                    <div class="col-lg-12">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/dashboard.html">Pengaturan</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Data Instansi</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </section>
            <!-- Judul -->
            <div class="row justify-content-between">

            </div>
            <!--Aksi Tambah Pengguna-->
            <br>
            <div class="container-fluid table">
                <div class="">
                    <h5 class=" content-title data">
                        Data Instansi
                    </h5>

                </div>
                <table class="table table-striped ">
                    <tbody class="body-table">
                        <tr class="d-flex">
                            <th class="col-3">Logo perusahaan</th>
                            <td class="col-5">
                                <input type="file" nama="thumnile">
                            </td>
                        </tr>
                        <tr class="d-flex">
                            <th class="col-3">Nama Instansi</th>
                            <td class="col-5">
                                <input type="text" class="form-control" id="NamaLengkap" aria-describedby="nama"
                                    placeholder="Masukkan Nama Instansi">
                            </td>
                        </tr>
                        <tr class="d-flex">
                            <th class="col-3">Visi Instansi</th>
                            <td class="col-5">
                                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                            </td>
                        </tr>
                        <tr class="d-flex">
                            <th class="col-3">Misi Instansi</th>
                            <td class="col-5">
                                <textarea class="form-control" id="exampleFormControlTextarea2" rows="8"></textarea>
                            </td>
                        </tr>
                </table>
            </div>
            <div class="container-fluid battom-nav">
                <button class="btn btn-white"><img src="super-admin/images/icon/stop_blue.svg" aria-hidden="true">
                    Batal</button>
                <button class="btn btn-blue"><i class="fa fa-save" aria-hidden="true"></i> Simpan Perubahan</button>
            </div>
        </div>
    </div>
</div>
@endsection
