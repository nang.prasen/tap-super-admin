@extends('layouts.super-admin')
@section('content')
<div id="page-content-wrapper">
    <!--Content Dashboard-->
    <!-- ........................ -->
    <div class="section-content section-dashboard-detail" data-aos="fade-up" data-aos-delay="500">
        <div class="container-fluid">
            <!-- Breandcrumb -->
            <section class="store-breadcrumbs">
                <div class="row">
                    <div class="col-lg-12">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/dashboard.html">Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Detail Buku</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </section>
            <!-- Judil Detail Buku -->
            <div class="row">
                <div class="col-lg-9">
                    <h5 class="dashboard-title">
                        Detail Buku
                    </h5>
                    <p class="dashboard-subtitle">
                        Sagittis, gravida hac sagittis adipiscing feugiat bibendum adipiscing
                    </p>
                </div>
            </div>
            <!--Content Detail Buku-->
            <section class="section-inventaris">
                <div class="row">
                <div class="col-lg-3">
                    <div class="book-rekapitulasi">
                        <img src="super-admin/images/book/book1.png" alt="" class="img-detail-book" >
                    </div>
                    <br/>
                    <div class="container">
                        <p>
                            Harga Buku
                        </p>
                        <h6>
                            Rp. 101.000,-
                        </h6>
                        <button class="btn "><img src="super-admin/images/icon/add.svg" alt=""> <a href="inventaris-detail-buku-tersedia.html">Rencana Pekngadaan</a>
                        </button>
                        <hr>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="container">
                        <div class="book-rekapitulasi-detail">
                            <div class="dashboard-heading">
                                <h6 class="dashboard-title">
                                    Sebuah Seni Untuk Bersikap Bodo Amat
                                </h6>
                                <p class="dashboard-subtitle" >
                                    Mark Manson
                                </p>
                                <div class="container border">
                                    <h5 class=" sinopsis-title "">
                                        Sinopsis Buku
                                    </h5>
                                    <p>
                                        Selama beberapa tahun belakangan, Mark Manson—melalui blognya yang sangat populer—telah membantu mengoreksi harapan-harapan delusional kita, baik mengenai diri kita sendiri
                                        <a href=" ">Lihat Detail Buku </a>
    
                                    </p>
                                </div>
                                <br>
                                <div class="container border">
                                    <h5 class=" sinopsis-title " >
                                        Tentang Buku
                                    </h5>
                                    <table class="table table-striped">
                                        <tbody class="body-table">
                                            <tr >
                                                <th scope="row">ID BUKU</th>
                                                <td>3224rt</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">ISBN</th>
                                                <td>9786024526986</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Tanggal Terbit</th>
                                                <td>20 Februari 2005</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Penerbit</th>
                                                <td>Gramedia Widiasarana Indonesia</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Halaman</th>
                                                <td>256 Halaman</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Qty</th>
                                                <td>134 Buku</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Bahasa</th>
                                                <td>Indonesia</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Kategori</th>
                                                <td >
                                                    <div class="icon-kategori" >
                                                        <p> <img src="super-admin/images/icon/masjid.svg" alt=""> Agama
                                                        </p>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <br/>
                            </div>
    
                        </div>
                    </div>
                </div>
    
            </div>
            </section>
            
        </div>
    </div>

</div>
@endsection
