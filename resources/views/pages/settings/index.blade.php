@extends('layouts.super-admin')
@section('content')
<div id="page-content-wrapper">
    <!--Content Dashboard-->
    <!-- ........................ -->
    <div class="section-content section-dashboard-bukuterbaru" data-aos="fade-up" data-aos-delay="500">
        <div class="container-fluid">
       <!-- Breandcrumb -->
               <section class="store-breadcrumbs">
                   <div class="row">
                       <div class="col-lg-12">
                           <nav aria-label="breadcrumb">
                               <ol class="breadcrumb">
                                   <li class="breadcrumb-item"><a href="/dashboard.html">Pengaturan</a></li>
                                   <li class="breadcrumb-item active" aria-current="page">Data Instansi</li>
                               </ol>
                           </nav>
                       </div>
                   </div>
               </section>
           <!-- Judul -->
           <div class="row justify-content-between">
              
           </div>
           <!--Aksi Tambah Pengguna-->
           <br>
           <div class="container-fluid table" >
               <div class="">
                   <h5 class=" content-title data" >
                    Data Instansi
                   <a href="pengaturan-edit.html"><button class="btn btn-white data" ><img src="super-admin/images/icon/pen_blue.svg" aria-hidden="true"> Edit</button></a>
                   
                    </h5> 
                  
               </div>
                      
               <hr>
               <table class="table table-striped ">
                   <tbody class="body-table">
                       <tr class="d-flex">
                           <th class="col-3">Logo Instansi</th>
                           <td class="col-5"> 
                              <img class="rounded-circle mr-2 profile-picture" src="super-admin/images/icon-user.png" alt="">
                           </td>
                           <td class="col-4"> </td>
                       </tr>
                       <tr class="d-flex">
                           <th  class="col-3">Nama Instansi</th>
                           <td class="col-5">
                               <p> Troya High School</p>      
                           </td>
                           <td> </td>
                       </tr>
                       <tr class="d-flex">
                           <th class="col-3">Misi Instansi</th>
                           <td class="col-5">
                               <p> 1. Lorem ipsum dolor sit amet, consectetur adipiscing elit.    Viverra mattis egestas justo, cras sed. Turpis nulla dictum sit
                               <p>2. Nibh mollis. Natoque eget rhoncus, mattis quis urna tellus, nibh gravida. Justo, tristique viverra donec ut sed urna tortor.                                     </p> 
                                 <p> 3.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Viverra mattis egestas justo, cras sed. Turpis nulla dictum sit
                                </p> 
                                <p> 4.Nibh mollis. Natoque eget rhoncus, mattis quis urna tellus, nibh gravida. Justo, tristique viverra donec ut sed urna tortor.
                                </p>
                                
                                </p>
                           </td>
                           <td> </td>
                       </tr>
                   </tbody>
               </table>
               <br>
           </div>
           <hr>
       <!-- <div class="container-fluid battom-nav">
           <button class="btn btn-white" ><img src="super-admin/images/icon/stop_blue.svg" aria-hidden="true"> Batal</button>
           <button class="btn btn-disabled" ><img src="super-admin/images/icon/plus_white.svg" aria-hidden="true"> Tambah  Pengguna</button>
            -->
           <!-- jika aktif -->
           <!-- <button class="btn btn-blue" ><img src="super-admin/images/icon/plus_white.svg" aria-hidden="true"> Tambah  Pengguna</button>
                                                       
       </div> -->
   </div>
   </div>

</div>
@endsection
