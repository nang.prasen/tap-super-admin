@extends('layouts.super-admin')
@section('content')

<div id="page-content-wrapper">
    <!--Content Dashboard-->
    <!-- ........................ -->
    <div class="section-content section-inventaris-buku" data-aos="fade-up" data-aos-delay="500">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-6">
                            <div class="dashboard-heading">
                                <h5 class="dashboard-title" style="margin-top: 32px;">
                                    Inventaris Buku
                                </h5>
                                <p class="dashboard-subtitle">
                                    Daftar buku yang ada di perpustakaan
                                </p>
                            </div>
                        </div>
                        <div class="col-6 button-right">
                            <div class="dashboard-heading" style="align-items: baseline;">
                                <button class="btn" style="margin-top: 32px; color: #3685C8;"><img
                                        src="super-admin/images/icon/love.svg" alt=""> <a href="inventaris-permintaan.html">
                                        Permintaan Buku</a>
                                    </button>
                                <button class="btn" style="margin-top: 32px; color: #3685C8"><img
                                        src="super-admin/images/icon/add.svg" alt=""> <a href="inventaris-pengadaan.html">Rencana
                                        Pengadaan</a> 
                                    </button>

                            </div>
                        </div>
                    </div>

                    <div class="dashboard-heading">
                        <div class="container-fluid table">
                            <table class="table table-striped table-hover" id="table-inventaris">
                                <thead class="head-table">
                                    <tr>
                                        <th>No</th>
                                        <th>Judul Buku</th>
                                        <th>ISBN</th>
                                        <th>Total Buku</th>
                                        <th>Buku dipinjam</th>
                                        <th>Buku Tersedia</th>
                                        <th>Penerbit</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody class="body-table">
                                    <tr>
                                        <td>1</td>
                                        <td>Sebuah Seni Untuk bersikap...</td>
                                        <td>123123123</td>
                                        <td> 123325</td>
                                        <td>123</td>
                                        <td>15</td>
                                        <td>Gramedia Pustaka</td>
                                        <td>
                                            <!-- Default dropleft button -->
                                            <div class="btn-group dropleft">
                                                <button type="button" class="btn btn-secondary fa fa-ellipsis-v"
                                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                                </button>
                                                <div class="dropdown-menu">
                                                    <!-- Dropdown menu links -->
                                                    <a class="dropdown-item" href="{{url('inventory-recapitulation')}}">
                                                        <img src="super-admin/images/icon/inv-03.svg" alt=""> Rekapitulasi</a>
                                                    <a class="dropdown-item" href="#"><img src="super-admin/images/icon/inv-02.svg"
                                                            alt=""> Hapus Pengadaan</a>
                                                    <a class="dropdown-item" href="{{url('detail-book')}}"><img src="super-admin/images/icon/inv-01.svg"
                                                            alt=""> Detail Buku</a>

                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>Sebuah Seni Untuk bersikap...</td>
                                        <td>123123123</td>
                                        <td> 123325</td>
                                        <td>123</td>
                                        <td>15</td>
                                        <td>Gramedia Pustaka</td>
                                        <td>
                                            <!-- Default dropleft button -->
                                            <div class="btn-group dropleft">
                                                <button type="button" class="btn btn-secondary fa fa-ellipsis-v"
                                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                                </button>
                                                <div class="dropdown-menu">
                                                    <!-- Dropdown menu links -->
                                                    <a class="dropdown-item" href="{{url('inventory-recapitulation')}}">
                                                        <img src="super-admin/images/icon/inv-03.svg" alt=""> Rekapitulasi</a>
                                                    <a class="dropdown-item" href="#"><img src="super-admin/images/icon/inv-02.svg"
                                                            alt=""> Hapus Pengadaan</a>
                                                    <a class="dropdown-item" href="{{url('detail-book')}}"><img src="super-admin/images/icon/inv-01.svg"
                                                            alt=""> Detail Buku</a>

                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>Sebuah Seni Untuk bersikap...</td>
                                        <td>123123123</td>
                                        <td> 123325</td>
                                        <td>123</td>
                                        <td>15</td>
                                        <td>Gramedia Pustaka</td>
                                        <td>
                                            <!-- Default dropleft button -->
                                            <div class="btn-group dropleft">
                                                <button type="button" class="btn btn-secondary fa fa-ellipsis-v"
                                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                                </button>
                                                <div class="dropdown-menu">
                                                    <!-- Dropdown menu links -->
                                                    <a class="dropdown-item" href="{{url('inventory-recapitulation')}}">
                                                        <img src="super-admin/images/icon/inv-03.svg" alt=""> Rekapitulasi</a>
                                                    <a class="dropdown-item" href="#"><img src="super-admin/images/icon/inv-02.svg"
                                                            alt=""> Hapus Pengadaan</a>
                                                    <a class="dropdown-item" href="{{url('detail-book')}}"><img src="super-admin/images/icon/inv-01.svg"
                                                            alt=""> Detail Buku</a>

                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>Sebuah Seni Untuk bersikap...</td>
                                        <td>123123123</td>
                                        <td> 123325</td>
                                        <td>123</td>
                                        <td>15</td>
                                        <td>Gramedia Pustaka</td>
                                        <td>
                                            <!-- Default dropleft button -->
                                            <div class="btn-group dropleft">
                                                <button type="button" class="btn btn-secondary fa fa-ellipsis-v"
                                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                                </button>
                                                <div class="dropdown-menu">
                                                    <!-- Dropdown menu links -->
                                                    <a class="dropdown-item" href="{{url('inventory-recapitulation')}}">
                                                        <img src="super-admin/images/icon/inv-03.svg" alt=""> Rekapitulasi</a>
                                                    <a class="dropdown-item" href="#"><img src="super-admin/images/icon/inv-02.svg"
                                                            alt=""> Hapus Pengadaan</a>
                                                    <a class="dropdown-item" href="{{url('detail-book')}}"><img src="super-admin/images/icon/inv-01.svg"
                                                            alt=""> Detail Buku</a>

                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>Sebuah Seni Untuk bersikap...</td>
                                        <td>123123123</td>
                                        <td> 123325</td>
                                        <td>123</td>
                                        <td>15</td>
                                        <td>Gramedia Pustaka</td>
                                        <td>
                                            <!-- Default dropleft button -->
                                            <div class="btn-group dropleft">
                                                <button type="button" class="btn btn-secondary fa fa-ellipsis-v"
                                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                                </button>
                                                <div class="dropdown-menu">
                                                    <!-- Dropdown menu links -->
                                                    <a class="dropdown-item" href="{{url('inventory-recapitulation')}}">
                                                        <img src="super-admin/images/icon/inv-03.svg" alt=""> Rekapitulasi</a>
                                                    <a class="dropdown-item" href="#"><img src="super-admin/images/icon/inv-02.svg"
                                                            alt=""> Hapus Pengadaan</a>
                                                    <a class="dropdown-item" href="{{url('detail-book')}}"><img src="super-admin/images/icon/inv-01.svg"
                                                            alt=""> Detail Buku</a>

                                                </div>
                                            </div>
                                        </td>
                                    </tr>




                                </tbody>
                            </table>
                            <br>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

</div>
@endsection
