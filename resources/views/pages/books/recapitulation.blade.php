@extends('layouts.super-admin')
@section('content')
<div id="page-content-wrapper">
    <!--Content Dashboard-->
    <!-- ........................ -->
    <div class="section-content section-inventaris-buku" data-aos="fade-up" data-aos-delay="500">
        <div class="container-fluid">
            <!-- Breandcrumb -->
            <section class="store-breadcrumbs">
                <div class="row">
                    <div class="col-lg-12">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/dashboard.html">Inventaris Buku</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Rekapitulasi Buku</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </section>
            <!--Rekapitulasi Buku-->
            <div class="row">
                <div class="col-lg-2">
                    <div class="book-rekapitulasi">
                        <img src="super-admin/images/book/book1.png" alt="">
                    </div>
                </div>
                <div class="col-lg-10">
                    <div class="container">
                        <div class="book-rekapitulasi-detail">
                            <div class="dashboard-heading">
                                <h6 class="dashboard-title">
                                    Sebuah Seni Untuk Bersikap Bodo Amat
                                </h6>
                                <p class="dashboard-subtitle">
                                    Mark Manson
                                </p>
                                <div class="container border">
                                    <h5 class="dashboard-title">
                                        Sinopsis Buku
                                    </h5>
                                    <p>
                                        Selama beberapa tahun belakangan, Mark Manson—melalui blognya yang sangat
                                        populer—telah membantu mengoreksi harapan-harapan delusional kita, baik mengenai
                                        diri kita sendiri maupun dunia. Ia kini menuangkan buah pikirnya yang keren itu
                                        di dalam buku hebat
                                        ini....
                                        <a href="">Lihat Detail Buku </a>

                                    </p>
                                    <br />
                                </div>
                                <br>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- Atasan -->
            <br />
            <div class="dashboard-content">
                <div class="row">
                    <div class=" col-lg-4 col-md-6 col-sm-12 col-6">
                        <div class="card mb-2">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-3">
                                        <img src="super-admin/images/dashboard-icon-openbook.svg" alt="Logo" />
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-9">
                                        <div class="rekapitulasi-title">
                                            Buku Dipinjam
                                        </div>
                                        <div class="rekapitulasi-subtitle">
                                            1204
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class=" col-lg-4 col-md-6 col-sm-12 col-12">
                        <div class="card mb-2">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-3">
                                        <img src="super-admin/images/dashboard-icon-totalbook.svg" alt="Logo" />
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-9">
                                        <div class="rekapitulasi-title">
                                            Buku Dipinjam
                                        </div>
                                        <div class="rekapitulasi-subtitle orange">
                                            1204
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class=" col-lg-4 col-md-6 col-sm-12 col-12">
                        <div class="card mb-2">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-3">
                                        <img src="super-admin/images/dashboard-icon-book.svg" alt="Logo" />
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-9">
                                        <div class="rekapitulasi-title">
                                            Buku Dipinjam
                                        </div>
                                        <div class="rekapitulasi-subtitle red">
                                            1204
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br />

            <!-- List Peminjaman Buku -->
            <section class="list-penjualan-buku">
                <div class="row">
                    <div class="col-6">
                        <div class="dashboard-heading">
                            <h5 class="dashboard-title" style="margin-top: 32px;">
                                List Peminjaman Buku
                            </h5>
                            <p class="dashboard-subtitle">
                                Daftar peminjaman buku selama periode tertentu
                            </p>
                        </div>
                    </div>
                    <div class="col-6 button-right">
                        <div class="dashboard-heading" style="align-items: baseline;">
                            <button class="btn" style="margin-top: 32px; color: #3685C8"><img
                                    src="super-admin/images/icon/katalog-blue.svg" alt=""> <a href="katalog.html">Lihat Katalog</a>
                            </button>

                        </div>
                    </div>
                </div>
                <div class="dashboard-heading">
                    <div class="container-fluid table">
                        <table class="table table-striped table-hover" id="rekapitulasi-buku">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Kelas dan Jurusan</th>
                                    <th>Tanggal Pinjam</th>
                                    <th>Tanggal Kembali</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Sebuah Seni Untuk bersikap...</td>
                                    <td>XII</td>
                                    <td> 06 April 2021</td>
                                    <td> 12 April 2021</td>
                                    <td>
                                        <!-- Default dropleft button -->
                                        <div class="btn-group dropleft">
                                            <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            </button>
                                            <div class="dropdown-menu">
                                                <!-- Dropdown menu links -->
                                                <a class="dropdown-item" href="#"> <img src="super-admin/images/icon/uaer.svg"
                                                        alt=""> Detail Pengguna
                                                </a>
                                                <a class="dropdown-item" href="#"><img src="super-admin/images/icon/book.svg"
                                                        alt=""> Tarik Buku
                                                </a>

                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
@endsection
