@extends('layouts.super-admin')
@section('content')
    <div id="page-content-wrapper">
        <form>
            @csrf
            <div class="section-content section-dashboard-detail" data-aos="fade-up" data-aos-delay="500">
                <div class="container-fluid">
                    <!-- Judil Detail Buku -->
                    <div class="row">
                        <div class="col-lg-9">
                            <h5 class="dashboard-title">
                                Distribusi Buku
                            </h5>
                            <p class="dashboard-subtitle">
                                Pilih buku untuk di bagikan kepada instansi
                            </p>
                        </div>
                    </div>
                    <br>
                    <section>
                        <div class="container">
                            <h5 class="dashboard-title">
                                Pilih Buku
                            </h5>
                            <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                    <form action="" method="GET">
                                        <div class="input-group rounded">
                                            <input value="{{ request()->get('search') }}" type="search" name="search"
                                                class="form-control rounded" placeholder="Pencarian" />
                                            <button type="submit" class="input-group-text border-0" id="search-addon">
                                                <i class="fa fa-search"></i>
                                                </span>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <br>
                            <!-- Table Bootstrap -->
                            <table class="table table-striped" id="distribusi-buku">

                                <tr>
                                    <th class="active">
                                        <input id="check-all" type="checkbox" class="select-all checkbox"
                                            value="select-all" />
                                    </th>
                                    <th class="success">Judul Buku</th>
                                    <th class="warning">ISBN</th>
                                    <th class="warning">Total Buku</th>
                                    <th class="warning">Harga</th>
                                    <th class="danger">Pengarang</th>
                                    <th class="danger">Penerbit</th>
                                </tr>
                                @foreach ($books as $item)
                                    <tr>
                                        <td class="active">
                                            <input name="book_id[]" class="select-item checkbox book-id" type="checkbox"
                                                value="{{ $item->id }}">
                                            <input type="hidden" name="publisher_id[]" class="publisher"
                                                value="{{ $item->publisher_id }}">

                                        </td>
                                        <td>{{ $item->title }}</td>
                                        <td>{{ $item->isbn }}</td>
                                        <td>
                                            <input type="number" name="qty[]" class="form-control book-qty">
                                        </td>
                                        <td>{{ $item->price }}</td>
                                        <td>{{ $item->author }}</td>
                                        <td>{{ $item->publisher->name ?? "-" }}

                                        </td>
                                    </tr>
                                @endforeach

                            </table>
                            <input type="text" name="publisherid[]" id="showValue">
                            {{-- {{$books->links()}} --}}
                        </div>
                    </section>
                    <section form-control>
                        <div class="container">
                            <h5 class="dashboard-title">
                                Pilih Instansi Pengguna
                            </h5>
                            <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="input-group rounded">
                                        <input type="search" class="form-control rounded" placeholder="Search"
                                            aria-label="Search" aria-describedby="search-addon" />
                                        <span class="input-group-text border-0" id="search-addon">
                                            <i class="fa fa-search"></i>
                                        </span>
                                    </div>
                                </div>

                            </div>
                            <br>
                            <!-- Table Bootstrap -->
                            <table class="table table-striped" id="data-instansi">

                                <tr>
                                    <th class="active">
                                        <input type="checkbox" class="select-all checkbox" name="select-all" />
                                    </th>
                                    <th class="success">Nama Instansi</th>
                                    <th class="success">Alamat</th>
                                </tr>
                                @foreach ($instansi as $item)
                                    <tr>
                                        <td class="active">
                                            <input type="checkbox" class="select-item checkbox agency-id"
                                                name="agencies_id[]" value="{{ $item->id }}" />
                                        </td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->address }}</td>
                                    </tr>
                                @endforeach

                            </table>
                            <button id="select-all" class="btn btn-blue">SelectAll/Cancel</button>
                            <button id="select-invert" class="btn btn-white">Invert</button>
                            <button id="selected" class="btn btn-red">GetSelected</button>
                            <br>
                            <br>
                        </div>
                        <div class="container-fluid battom-nav">
                            <button class="btn btn-white"><img
                                    src="{{ asset('super-admin/images/icon/plus_blue.svg') }}" alt=""> Batal </button>
                            <button class="btn btn-blue btn_save" type="submit"><img
                                    src="{{ asset('super-admin/images/icon/katalog-active.svg') }}" alt=""> Distribusi
                                Buku</button>
                        </div>
                    </section>
                </div>
            </div>
        </form>

    </div>
@endsection
@push('after-script')
    <script>
        $(document).ready(function() {
            // alert(1);

            

            $('.book-id').on("change", function() {
                var id = $(":checkbox:checked").map(function() {
                    return $(this).next().val();
                }).get();
                
                var ss = $('#showValue').val(id);
                console.log(id);
            });
             

            $('.btn_save').click(function(e) {
                // alert(1);


                e.preventDefault();

                const book_id = [];
                const qty = [];
                const publisher_id = [];
                const agencies_id = [];

                let string = id;
                let newArray = string.split('');
                
                $('.book-id').each(function() {
                    var ischecked = $(this).is(":checked")
                    if (ischecked) {
                        book_id.push($(this).val());

                    }
                });


                $('input[name^="qty"]').each(function() {
                    qty.push($(this).val());
                });




                $('input[name^="publisherid"]').each(function() {
                    publisher_id.push($(this).val());
                });


                $('.agency-id').each(function() {
                    if ($(this).is(":checked")) {
                        agencies_id.push($(this).val());
                    }
                });


                $.ajax({
                    url: '{{ route('book.distribution.store') }}',
                    type: 'POST',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        book_id: book_id,
                        qty: qty,
                        publisher_id: publisher_id,
                        agencies_id: agencies_id
                    },
                    success: function(response) {
                        if (response.success) {
                            alert('berhasil distribusi buku');
                            console.log(response.success);
                            // $('input=[type="checkbox"]').prop('checked', false);
                            // $('[name="qty[]"]').val('');
                        } else {
                            console.log(response.error);
                        }
                    },
                    error: function(response) {
                        console.log('error');
                    }
                });
            });
        });
    </script>
@endpush
