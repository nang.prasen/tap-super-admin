@extends('layouts.super-admin')
@section('content')
<div id="page-content-wrapper">
    <!--Content Dashboard-->
    <!-- ........................ -->
    <div class="section-content section-dashboard-bukuterbaru" data-aos="fade-up" data-aos-delay="500">
        <div class="container-fluid">
            <!-- Breandcrumb -->
            <section class="store-breadcrumbs">
                <div class="row">
                    <div class="col-lg-12">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/dashboard.html">Instansi</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Detail</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </section>
            <!-- Judul -->
            <div class="row justify-content-between">

            </div>
            <!--Aksi Tambah Pengguna-->
            <br>
            <div class="container-fluid table">
                <div class="">
                    <h5 class=" content-title data">
                        Detail Instansi
                    </h5>
                </div>
                <table class="table table-striped ">
                    <tbody class="body-table">
                        <tr class="d-flex">
                            <th class="col-3">Logo</th>
                            <td class="col-5">
                                <img class="rounded-circle mr-2 profile-picture" src="{{url($agency->logo)}}"
                                    alt="">
                                <a href="{{$agency->logo}}" target="_blank"> Download</a>
                            </td>
                            <td class="col-4"> </td>
                        </tr>
                        <tr class="d-flex">
                            <th class="col-3">Id Instansi</th>
                            <td class="col-5">
                                <p>{{$agency->id}}</p>
                            </td>
                            
                        </tr>
                        <tr class="d-flex">
                            <th class="col-3">Nama Instansi</th>
                            <td class="col-5">
                                <p>{{$agency->name}}</p>
                            </td>
                            
                        </tr>
                        <tr class="d-flex">
                            <th class="col-3">Jumlah Siswa</th>
                            <td class="col-5">
                                <p>{{$agency->students}}</p>
                            </td>
                            
                        </tr>
                        <tr class="d-flex">
                            <th class="col-3">Nama Aplikasi Instansi</th>
                            <td class="col-5">
                                <p>{{$agency->nameapp}}</p>
                            </td>
                            
                        </tr>
                        <tr class="d-flex">
                            <th class="col-3">Alamat</th>
                            <td class="col-5">
                                <p>{{$agency->address}}</p>
                            </td>
                        </tr>
                        <tr class="d-flex">
                            <th class="col-3">No. Telpon</th>
                            <td class="col-5">
                                {{$agency->phone}}
                            </td>
                        </tr>
                        <tr class="d-flex">
                            <th class="col-3">Email</th>
                            <td class="col-5">
                                {{$agency->email}}
                            </td>
                        </tr>
                        <tr class="d-flex">
                            <th class="col-3">NPWP</th>
                            <td class="col-5">
                                {{$agency->npwp}}
                            </td>
                        </tr>
                        <tr class="d-flex">
                            <th class="col-3">Penanggung Jawab</th>
                            <td class="col-5">
                                {{$agency->person_responsible}}
                            </td>
                        </tr>
                        <tr class="d-flex">
                            <th class="col-3">Jabatan Penanggung Jawab</th>
                            <td class="col-5">
                                {{$agency->departement_responsible}}
                            </td>
                        </tr>
                        <tr class="d-flex">
                            <th class="col-3">Pengelola Aplikasi</th>
                            <td class="col-5">
                                {{$agency->organizer_app}}
                            </td>
                        </tr>
                        <tr class="d-flex">
                            <th class="col-3">Nomer Pengelola Aplikasi</th>
                            <td class="col-5">
                                {{$agency->organizer_phone}}
                            </td>
                        </tr>
                        <tr class="d-flex">
                            <th class="col-3">Pendaftaran</th>
                            <td class="col-5">
                                @if ($agency->registration == 1)
                                    Online
                                @else
                                    Offline
                                @endif
                            </td>
                        </tr>
                        <tr class="d-flex">
                            <th class="col-3">Deskripsi</th>
                            <td class="col-5">
                                {{$agency->description}}
                            </td>
                        </tr>
                        <tr class="d-flex">
                            <th class="col-3">Surat Perjanjian</th>
                            <td class="col-5">
                                <iframe src=" {{ asset('storage/pdf/' . $agency->statement_letter_work) }}"
                                    frameborder="0" width="120%" height="500px">
                                </iframe>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <br>
            </div>
        </div>
    </div>
</div>
@endsection
