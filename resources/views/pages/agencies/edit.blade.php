@extends('layouts.super-admin')
@section('content')
<div id="page-content-wrapper">
    <!--Content Dashboard-->
    <!-- ........................ -->
    <div class="section-content section-dashboard-bukuterbaru" data-aos="fade-up" data-aos-delay="500">
        <div class="container-fluid">
            <!-- Breandcrumb -->
            <section class="store-breadcrumbs">
                <div class="row">
                    <div class="col-lg-12">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/dashboard.html">Instansi</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Edit</li>
                            </ol>
                        </nav>
                        @if(session('status'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Sukses!</strong> {{ session('status') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
        @endif
                    </div>
                </div>
            </section>
            <!-- Judul -->
            <div class="row justify-content-between">

            </div>
            <!--Aksi Tambah Pengguna-->
            {{-- Alert berhasil --}}
            
            <br>
            <div class="container-fluid table">
                <div class="">
                    <h5 class=" content-title data">
                        Edit Instansi
                    </h5>
                </div>
                <form action="{{route('agency.update',$agency->id)}}" method="POST">
                    @method('PUT')
                    @csrf
                    <table class="table table-striped ">
                        <tbody class="body-table">
                            <tr class="d-flex">
                                <th class="col-3">Logo</th>
                                <td class="col-5">
                                    <img class="rounded-circle mr-2 profile-picture" src="{{url($agency->logo)}}"
                                        alt="">
                                </td>
                                <td class="col-4"> </td>
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">Id Instansi</th>
                                <td class="col-5">
                                    
                                    <p>{{$agency->id}}</p>
                                </td>
                                
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">Nama Instansi</th>
                                <td class="col-5">
                                    <input type="text" id="name" name="name" placeholder="Banner Name"
                                            class="form-control @error('name') is invalid @enderror"
                                            value="{{old('name') ? old('name') : $agency->name}}">
                                    @error('name')
                                        <span class="help-block"> {{$message}} </span>
                                    @enderror
                                </td>
                                
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">Nama Aplikasi Instansi</th>
                                <td class="col-5">
                                    <input type="text" id="nameapp" name="nameapp" placeholder="Banner Name"
                                            class="form-control @error('name') is invalid @enderror"
                                            value="{{old('nameapp') ? old('nameapp') : $agency->nameapp}}">
                                    @error('nameapp')
                                        <span class="help-block"> {{$message}} </span>
                                    @enderror
                                </td>
                                
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">Alamat</th>
                                <td class="col-5">
                                    <input type="text" id="address" name="address" placeholder="Banner Name"
                                            class="form-control @error('name') is invalid @enderror"
                                            value="{{old('address') ? old('address') : $agency->address}}">
                                    @error('address')
                                        <span class="help-block"> {{$message}} </span>
                                    @enderror
                                </td>
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">Provinsi</th>
                                <td class="col-5">
                                    <select class="form-control" name="province_id" id="provinsi" required>
                                        <option>---Pilih Provinsi---</option>
                                        @foreach ($wilayah as $id => $name)
                                            <option value="{{ $id}}" {{ $agency->province_id == $id ? 'selected':'' }}>{{ $name }}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td class="col-4"> </td>
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">Kabupaten/Kota</th>
                                <td class="col-5">
                                    <select class="form-control" name="city_id" id="kota" required>
                                        <option>---Pilih Kab/Kota---</option>
                                        @foreach ($city as $id => $name)
                                                <option value="{{ $id}}" {{ $agency->city_id == $id ? 'selected':'' }}>{{ $name }}</option>
                                            @endforeach
                                    </select>
                                </td>
                                <td class="col-4"> </td>
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">Kecamatan</th>
                                <td class="col-5">
                                    <select class="form-control" name="district_id" id="kecamatan" required>
                                        <option>---Pilih Kecamatan---</option>
                                        @foreach ($district as $id => $name)
                                            <option value="{{ $id}}" {{ $agency->district_id == $id ? 'selected':'' }}>{{ $name }}</option>
                                         @endforeach
                                    </select>
                                </td>
                                <td class="col-4"> </td>
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">Desa</th>
                                <td class="col-5">
                                    <select class="form-control" name="village_id" id="desa" required>
                                        <option>---Pilih Desa---</option>
                                        @foreach ($village as $id => $name)
                                            <option value="{{ $id}}" {{ $agency->village_id == $id ? 'selected':'' }}>{{ $name }}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td class="col-4"> </td>
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">No. Telpon</th>
                                <td class="col-5">
                                    <input type="text" id="phone" name="phone" placeholder="Banner Name"
                                            class="form-control @error('name') is invalid @enderror"
                                            value="{{old('phone') ? old('phone') : $agency->phone}}">
                                    @error('phone')
                                        <span class="help-block"> {{$message}} </span>
                                    @enderror
                                </td>
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">Status</th>
                                <td class="col-5">
                                    <select name="degree" id="degree" class="form-control">
                                        <option>---Pilih Jenjang---</option>
                                        <option value="1" {{ $agency->degree == '1' ? 'selected' : '' }} >SD</option>
                                        <option value="2" {{ $agency->degree == '2' ? 'selected' : '' }} >SMP</option>
                                        <option value="3" {{ $agency->degree == '3' ? 'selected' : '' }} >SMA</option>
                                        <option value="4" {{ $agency->degree == '4' ? 'selected' : '' }} >SMK</option>
                                        <option value="5" {{ $agency->degree == '5' ? 'selected' : '' }} >PERGURUAN TINGGI</option>
                                        <option value="6" {{ $agency->degree == '6' ? 'selected' : '' }} >SLB</option>
                                    </select>
                                </td>
                                <td class="col-4"> </td>
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">NPWP</th>
                                <td class="col-5">
                                    <input type="text" id="npwp" name="npwp" placeholder="Banner Name"
                                            class="form-control @error('name') is invalid @enderror"
                                            value="{{old('npwp') ? old('npwp') : $agency->npwp}}">
                                    @error('npwp')
                                        <span class="help-block"> {{$message}} </span>
                                    @enderror
                                </td>
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">Penanggung Jawab</th>
                                <td class="col-5">
                                    <input type="text" id="person_responsible" name="person_responsible" placeholder="Banner Name"
                                            class="form-control @error('name') is invalid @enderror"
                                            value="{{old('person_responsible') ? old('person_responsible') : $agency->person_responsible}}">
                                    @error('person_responsible')
                                        <span class="help-block"> {{$message}} </span>
                                    @enderror
                                </td>
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">Jabatan Penanggung Jawab</th>
                                <td class="col-5">
                                    <input type="text" id="departement_responsible" name="departement_responsible" placeholder="Banner Name"
                                            class="form-control @error('name') is invalid @enderror"
                                            value="{{old('departement_responsible') ? old('departement_responsible') : $agency->departement_responsible}}">
                                    @error('departement_responsible')
                                        <span class="help-block"> {{$message}} </span>
                                    @enderror
                                </td>
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">Pengelola Aplikasi</th>
                                <td class="col-5">
                                    <input type="text" id="organizer_app" name="organizer_app" placeholder="Banner Name"
                                            class="form-control @error('name') is invalid @enderror"
                                            value="{{old('organizer_app') ? old('organizer_app') : $agency->organizer_app}}">
                                    @error('organizer_app')
                                        <span class="help-block"> {{$message}} </span>
                                    @enderror
                                </td>
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">Nomer Pengelola Aplikasi</th>
                                <td class="col-5">
                                    <input type="text" id="organizer_phone" name="organizer_phone" placeholder="Banner Name"
                                            class="form-control @error('name') is invalid @enderror"
                                            value="{{old('organizer_phone') ? old('organizer_phone') : $agency->organizer_phone}}">
                                    @error('organizer_phone')
                                        <span class="help-block"> {{$message}} </span>
                                    @enderror
                                </td>
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">Pendaftaran</th>
                                <td class="col-5">
                                    @if ($agency->registration == 1)
                                        Online
                                    @else
                                        Offline
                                    @endif
                                </td>
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">Deskripsi</th>
                                <td class="col-5">
                                    <textarea class="form-control @error('description') is invalid @enderror"
                                        value="{{old('description')}}" placeholder="Masukkan Deskripsi" id="content"
                                        name="description" rows="4">{{$agency->description}}</textarea>
                                    @error('description')
                                    <span class="help-block"> {{$message}} </span>
                                    @enderror
                                </td>
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">Surat Perjanjian</th>
                                <td class="col-5">
                                    <iframe src=" {{ asset('storage/pdf/' . $agency->statement_letter_work) }}"
                                        frameborder="0" width="120%" height="500px">
                                    </iframe>
                                </td>
                            </tr>
                        </tbody>
                        {{-- <div class="clearfix"></div --}}
                    </table>
                    <button type="submit" class="btn btn-success btn-icon left-icon mr-10 "> <i class="fa fa-check"></i> <span>save</span></button>
                    <a href="{{ route('agency')}}" class="btn btn-warning">Cancel</a>
                    <br>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@push('after-script')
    <script>
        $(document).ready(function() {
            $('#provinsi').on('change',function(){
            var idkota = $(this).val();
            console.log(idkota);
            if (idkota) {
                $.ajax({
                    type: "get",
                    url: '{{ route('cities') }}',
                    data: {id:idkota},
                    dataType: "json",
                    success: function (response) {
                        console.log('a');
                        if (response) {
                            // console.log(response);
                            $('#kota').empty();
                            $('#kota').append(
                                    '<option>---Pilih Kab/Kota---</option>');
                            $.each(response, function(name, id) {
                                    $('#kota').append('<option name="city_id" value ="'+name+'">'+id+'</option>')
                            });
                        }
                    }
                });
            } else {
                $('#kota').empty();
            }
        });


        $('#kota').on('change',function(){
            var idkec = $(this).val();
            console.log(idkec);
            if (idkec) {
                $.ajax({
                    type: "get",
                    url: '{{ route('districts') }}',
                    data: {id:idkec},
                    dataType: "json",
                    success: function (response) {
                        console.log('a');
                        if (response) {
                            // console.log(response);
                            $('#kecamatan').empty();
                            $('#kecamatan').append(
                                    '<option>---Pilih Kecamatan---</option>');
                            $.each(response, function(name, id) {
                                    $('#kecamatan').append('<option name="district_id" value ="'+name+'">'+id+'</option>')
                            });
                        }
                    }
                });
            } else {
                $('#kecamatan').empty();
            }
        });

        $('#kecamatan').on('change',function(){
            var iddesa = $(this).val();
            console.log(iddesa);
            if (iddesa) {
                $.ajax({
                    type: "get",
                    url: '{{ route('villages') }}',
                    data: {id:iddesa},
                    dataType: "json",
                    success: function (response) {
                        console.log('a');
                        if (response) {
                            // console.log(response);
                            $('#desa').empty();
                            $('#desa').append(
                                    '<option>---Pilih Desa---</option>');
                            $.each(response, function(name, id) {
                                    $('#desa').append('<option name="village_id" value ="'+name+'">'+id+'</option>')
                            });
                        }
                    }
                });
            } else {
                $('#desa').empty();
            }
        });
        });
    </script>
@endpush
