@extends('layouts.super-admin')
@section('content')
    {{-- @include('sweetalert::alert') --}}
    {{-- <form action="#">
        @csrf --}}
    <div class="section-content section-dashboard-detail" data-aos="fade-up" data-aos-delay="500">
        <div class="container-fluid">
            <!-- Breandcrumb -->
            <section class="store-breadcrumbs">
                <div class="row">
                    <div class="col-lg-12">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('instansi/dashboard') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page"><a href="#">Master RAB Buku</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Buat RAB Buku</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </section>
            <!-- Judil Detail Buku -->
            <div class="row">
                <div class="col-lg-9">
                    <h5 class="dashboard-title">
                        Edit RAB Buku
                    </h5>
                    <p class="dashboard-subtitle">
                        Edit beberapa buku untuk di buat di RAB kepada instansi
                    </p>
                </div>
            </div>
            <!--Content Detail Pengaturan Buku Detail-->
            @if( Session::has("status") )
            <div class="message">
                <div class="alert alert-success alert-block" role="alert">
                    <button class="close" data-dismiss="alert"></button>
                    {{ Session::get("status") }}
                </div>
            </div>
            @endif

            <br>
            <section>
                <section>
                    <div class="container">
                        <h5 class="dashboard-title">
                            Nama Bundle RAB
                        </h5>
                        <input type="text" value="{{$editrab->name}}" readonly class="form-control" placeholder="Nama Bundle RAB : RAB Sekolah Pratama">
                        <input type="hidden" value="{{$editrab->id}}" id="budgetplan_id" name="budgetplan_id" readonly class="form-control" placeholder="Nama Bundle RAB : RAB Sekolah Pratama">
                        <br>
                    </div>
                </section>
                <div class="container">
                    <!-- Table Bootstrap -->
                    <table class="table table-striped" id="table-tambah-pengguna">
                        <thead>

                            <tr>
                                <th class="active">
                                    No
                                </th>
                                <th class="success">Judul Buku</th>
                                <th class="warning">ISBN</th>
                                <th class="warning">Qty</th>
                                <th class="danger">Harga</th>
                                <th class="danger">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($editrab->detail as $item)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$item->book->title}}</td>
                                <td>{{$item->book->isbn}}</td>
                                <td class="qty">
                                    <input type="hidden" class="form-control idbook" name="iddetail" id="iddetail" value="{{$item->id}}">
                                    <input type="text" class="form-control detail_id" name="qty" value="{{$item->qty}}">
                                    <input type="hidden" value="{{$item->id}}" id="budgetplan_id" name="budgetplan_id" readonly class="form-control" placeholder="Nama Bundle RAB : RAB Sekolah Pratama">
                                </td>
                                <td>@currency($item->book->price) </td>
                                <td><button class="btn btn-warning" id="btnSingle" type="button" >Update</button></td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
            </section>
            <div class="container">
                <div class="row">

                    <div class="col-md-12">
    
                        <button type="button" id="btnAddKeranjang"  disabled onclick="addKeranjang()" class="btn btn-danger"
                            style="margin-bottom: 1rem;">Update</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- </form> --}}
    <meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection
{{-- sukses --}}
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Informasi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {{ session('status') }}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Oke</button>
            </div>
        </div>
    </div>
</div>

@push('after-script')
    <script>
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $(document).ready(function () {
            $("#table-tambah-pengguna tbody").on('click','#btnSingle', function () {
                var currentrow = $(this).closest('tr');
                var budgetplan_id = $("input[type=hidden][name=budgetplan_id]").val();
                var iddetail = currentrow.find('input[type=hidden][name=iddetail]').val();
                var qty = currentrow.find('input[type=text][name=qty]').val();
                var urli = '{{ route("master-rab.update", ":id") }}';
                url = urli.replace(':id', iddetail);
                // console.log(iddetail);
                console.log("qty:",qty);
                console.log(budgetplan_id);
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {
                        _token: CSRF_TOKEN,
                        _method: "PUT", 
                        id:iddetail,
                        qty:qty,
                    },
                    dataType: "json",
                    success: function (response) {
                        console.log(response);
                        location.reload();
                        // $("div.message").html(status);
                    },
                    error: function(jqXHR, textStatus, errorThrown, response) {
                        console.log(textStatus, errorThrown, response);
                    }
                });
            });
        });
        
    </script>
@endpush

