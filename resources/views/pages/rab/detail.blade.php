@extends('layouts.super-admin')
@section('content')

<div id="page-content-wrapper">
    <!--Content Dashboard-->
    <!-- ........................ -->
    <div class="section-content section-inventaris-buku" data-aos="fade-up" data-aos-delay="500">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-6">
                            <div class="dashboard-heading">
                                <h5 class="dashboard-title" style="margin-top: 32px;">
                                    Detail RAB {{$rabmitradetail->name}}
                                </h5>
                                <p class="dashboard-subtitle">
                                    Daftar buku dari Rencana Anggaran Belanja yang Anda Buat
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="dashboard-heading">
                        <div class="container">
                            <h5 class="dashboard-title">
                                Pilih Instansi Pengguna
                            </h5>
                            <hr>
                            <br>
                            <!-- Table Bootstrap -->
                            <table class="table table-striped" id="blast-table-siswa-kelas">
                                <select name="agencies_id" id="instansi_id" class="form-control instansi_id @error('agencies_id') is invalid @enderror"
                                    tabindex="1">
                                    
                                </select>
                                <br>
                                <br>
                            </table>
        
                        </div>
                        <div class="container-fluid table">
                            <table class="table table-striped table-hover" id="table-inventaris">
                                <thead class="head-table">
                                    <tr>
                                        <th>No</th>
                                        <th>##</th>
                                        <th>Judul Buku</th>
                                        <th>ISBN</th>
                                        <th>Qty</th>
                                        <th>Harga</th>
                                    </tr>
                                </thead>
                                <tbody class="body-table" id="table-detail">
                                    @foreach ($rabmitradetail->detail as $item)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td class="book_id">{{ isset ($item->book->id) ? $item->book->id : ''}}</td>
                                            <td>{{ isset ($item->book->title) ? $item->book->title : ''}} </td>
                                            <td>{{ isset ($item->book->isbn) ? $item->book->isbn : ''}} </td>
                                            <td class="qty"> {{ isset ($item->qty) ? $item->qty : ''}}</td>
                                            @isset($item->book->price)
                                            <td>@currency($item->book->price)</td>
                                            @endisset
                                        </tr>
                                    @endforeach
                                    
                                    
                                </tbody>
                            </table>
                            <br>
                        </div>

                    </div>
                    <div class="container-fluid battom-nav">
                        <a  href="{{route('master-rab.index')}}" class="btn btn-white" id="btnoke-draf"> <i class="fa fa-remove" aria-hidden="true"></i> Batal</a>
                        <button type="submit" class="btn btn-blue" id="btn-simpan-distribusi" onclick="simpanDistribute()"><i class="fa fa-plus" aria-hidden="true"></i> Simpan</button>
                        <button type="submit" class="btn btn-blue" id="btn-distribution"><i class="fa fa-plus" aria-hidden="true"></i> Bagikan Buku</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection
@push('after-script')
    <script>
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $(document).ready(function(){
            $('#instansi_id').select2({
                placeholder: 'Pilih Intansi',
                ajax:{
                    url:'/distribusi/instansi/get',
                    type: 'post',
                    dataType:'json',
                    delay:250,
                    data: function(params){
                        return{
                            _token: CSRF_TOKEN,
                            search: params.term
                        };
                    },
                    processResults:function(response){
                        return{
                            results: response
                        };
                    },
                    cache:true
                }
            });
        });

        function simpanDistribute(){
            var data = [];
            let instansi = $("#instansi_id").val();
            var book_id, qty;
            $("table tbody tr").each(function(index){
                book_id = $(this).find('.book_id').text();
                qty = $(this).find('.qty').text();
                data.push({
                    book_id:book_id,
                    qty:qty
                });
            });

            $.ajax({
                /* the route pointing to the post function */
                url: `{{route('master-rab.move.detail')}}`,
                type: 'POST',
                /* send the csrf-token and the input to the controller */
                data: {_token: CSRF_TOKEN, data:data, instansi:instansi},
                dataType: 'JSON',
                /* remind that 'data' is the response of the AjaxController */
                success: function (data) { 
                    // createTableFromAjax()
                    alert(data.status)
                    
                    
                }
            }); 

        }

        $("#btn-distribution").click(function() {
            var agency = $("#instansi_id").val()
            var isGood=confirm('apakah anda yakin akan membagikan buku? data akan hilang setelah anda melakukan pembagian buku');
            if (isGood) {
                $.ajax({
                    url: `{{route('post-destribute')}}`,
                    type: 'POST',
                    data: {_token: CSRF_TOKEN, id:agency},
                    dataType: 'JSON',
                    success: function (data) { 
                        alert(data.status)
                        createTableFromAjax()
                    },
                    error:function(XMLHttpRequest, textStatus, errorThrown){
                       alert(XMLHttpRequest.responseJSON.status)
                      
                    }
                }); 
            } 
        });
    </script>
@endpush
