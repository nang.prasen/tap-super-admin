@extends('layouts.super-admin')
@section('content')
    <div class="section-content" data-aos="fade-up" data-aos-delay="500">
        <div class="container-fluid">
            <!-- Breandcrumb -->
            <section class="store-breadcrumbs">
                <div class="row">
                    <div class="col-lg-12">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('master-rab.index')}}">Master RAB</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Tambah Master RAB</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </section>
            <div class="row">
                <div class="col-lg-12">
                    <div class="row justify-content-between">
                        <div class="col-lg-6">
                            <div class="dashboard-heading">
                                <h5 class="dashboard-title">
                                    Tambah Master RAB Baru 
                                </h5>
                                <p class="dashboard-subtitle">
                                    Isi data dengan benar
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <section class="berita">
                <form action="{{ route('master-rab.insert') }}" id="form-addArtikel" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="container-fluid table">
                                <div class="form">
                                    <div class="form-group">
                                        <label class="form-check-label">Nama</label>
                                        <input type="text" id="name" name="name" placeholder="Masukkan Nama RAB"
                                            class="form-control" value="{{ old('name') }}">
                                    </div>
                                    @error('name')
                                        <span class="help-block"> {{$message}} </span>
                                    @enderror

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid battom-nav">
                        <a  href="{{route('master-rab.index')}}" class="btn btn-white" id="btnoke-draf"> <i class="fa fa-remove" aria-hidden="true"></i> Batal</a>
                        <button type="submit" class="btn btn-blue" id="btnoke-publish"><i class="fa fa-plus" aria-hidden="true"></i> Simpan</button>
        
                    </div>
                </form>

            </section>
            
        </div>
    </div>

@endsection
