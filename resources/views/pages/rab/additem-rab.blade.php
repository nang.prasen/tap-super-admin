@extends('layouts.super-admin')
@section('content')
    {{-- @include('sweetalert::alert') --}}
    {{-- <form action="#">
        @csrf --}}
    <div class="section-content section-dashboard-detail" data-aos="fade-up" data-aos-delay="500">
        <div class="container-fluid">
            <!-- Breandcrumb -->
            <section class="store-breadcrumbs">
                <div class="row">
                    <div class="col-lg-12">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('instansi/dashboard') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page"><a href="#">Master RAB Buku</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Buat RAB Buku</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </section>
            <!-- Judil Detail Buku -->
            <div class="row">
                <div class="col-lg-9">
                    <h5 class="dashboard-title">
                        Buat RAB Buku
                    </h5>
                    <p class="dashboard-subtitle">
                        Pilih beberapa buku untuk di buat di RAB kepada instansi
                    </p>
                </div>
            </div>
            <!--Content Detail Pengaturan Buku Detail-->

            <br>
            <section>
                <section>
                    <div class="container-fluid bg-white">
                        <h5 class="dashboard-title">
                            Nama Bundle RAB
                        </h5>
                        <input type="text" value="{{$budget_plan_item->name}}" readonly class="form-control" placeholder="Nama Bundle RAB : RAB Sekolah Pratama">
                        <input type="hidden" value="{{$budget_plan_item->id}}" id="budgetplan_id" name="budgetplan_id" readonly class="form-control" placeholder="Nama Bundle RAB : RAB Sekolah Pratama">
                        <br>
                    </div>
                </section>
                <div class="container-fluid bg-white">
                    <h5 class="dashboard-title">
                        Pilih Buku Dari Penerbit
                    </h5>
                    <hr>
                    <div class="row">
                        {{-- <div class="col-sm-3">
                                <form action="" method="GET">
                                    <div class="input-group rounded">
                                        <input value="{{ request()->get('search') }}" type="search" name="search"
                                            class="form-control rounded" placeholder="Pencarian" />
                                        <button type="submit" class="input-group-text border-0" id="search-addon">
                                            <i class="fa fa-search"></i>
                                            </span>
                                    </div>
                                </form>
                            </div> --}}
                        <div class="col-sm-3">
                            <div class="dropdown">
                                <select name="filter_publisher" id="filter_publisher"
                                    class="form-control @error('publisher_id') is invalid @enderror" tabindex="1">
                                    <option value="#">--pilih penerbit--</option>
                                    @foreach ($publisher as $id => $name)
                                        <option value="{{ $id }}">{{ $name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <br>
                    <!-- Table Bootstrap -->
                    <div class="table table-responsive">
                        <table class="table table-striped" id="distribution">
                            <thead>
    
                                <tr>
                                    <th class="active">
                                        <input id="check-all" type="checkbox" class="select-all checkbox" value="select-all" />
                                    </th>
                                    <th class="success">Judul Buku</th>
                                    <th class="warning">ISBN</th>
                                    <th class="warning">Qty</th>
                                    <th class="danger">Penerbit</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
    
                        </table>
                    </div>
                </div>
            </section>
            <div class="container-fluid bg-white">
                <div class="row">

                    <div class="col-md-12">
    
                        <button type="button" id="btnAddKeranjang"  disabled onclick="addKeranjang()" class="btn btn-danger"
                            style="margin-bottom: 1rem;">Simpan</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- </form> --}}
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <style media="all" type="text/css">
        .ellipsis-500 { 
            display: block;
            text-overflow: ellipsis;
            overflow: hidden;
            max-width: 500px;
        }
    </style>
@endsection
@push('after-script')
    <script>
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var agency = $("#instansi_id").val()
        console.log(agency)
        $(document).ready(function() {
            createTableFromAjax();
            fetch_data();
            let publisher = $("#filter_publisher").val()
    
            function fetch_data(filter_publisher = '') {
                var dataTable = $('#distribution').DataTable({
                    responsive: true,
                    autoWidth: false,
                    processing: true,
                    serverSide: true,
                    order: [
                        [2, "desc"]
                    ],
                    // pageLength: 100,
                    // lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'semua']],
                    ajax: {
                        url: "{{ route('distribution-book') }}",
                        data: function(d) {
                            // d.publisher = publisher;
                            d.filter_publisher = filter_publisher;
                            return d;
                        }
                    },
                    columnDefs: [{
                            "targets": 0,
                            "class": "text-nowrap",
                            "sortable": false,
                            "render": function(data, type, row, meta) {
                                return `<input type="checkbox" id="bookid" name="bookid[]" class="select-all checkbox" value="${row.bookId}">`;
                            }
                        },
                        {
                            "targets": 1,
                            "class": "text-nowrap title ellipsis-500",
                            "render": function(data, type, row, meta) {
                                return row.title
                            }
                        },
                        {
                            "targets": 2,
                            "class": "text-nowrap isbn",
                            "render": function(data, type, row, meta) {
                                return row.isbn
                            }
                        },
                        {
                            "targets": 3,
                            "class": "text-nowrap",
                            "sortable": false,
                            "render": function(data, type, row, meta) {
                                return `<input type="text" name="qty[]" class="form-control qty form-qty_${row.bookId}" style="width: 150px;"  data-id="" />`;
                            }
                        },
                        {
                            "targets": 4,
                            "class": "text-nowrap pbl",
                            "render": function(data, type, row, meta) {
                                return row.name
                            }
                        },
                        
                    ]
                });
            }

            // checkbox all
            $("#check-all").on('click', function() {
                var isChecked = $("#check-all").prop('checked')
                $(".checkbox").prop('checked', isChecked)
                $("#btnAddKeranjang").prop('disabled', !isChecked)
            })

            // single/uncheck checkbox
            $("#distribution tbody").on('click', '.checkbox', function() {
                if ($(this).prop('checked') != true) {
                    $("#check-all").prop('checked', false)
                }

                let semua_checkbox = $("#distribution tbody .checkbox:checked")
                let button_add_keranjang = (semua_checkbox.length > 0)
                let button_terpilih = button_add_keranjang;
                $("#btnAddKeranjang").prop('disabled', !button_add_keranjang)
            })

            // filter publisher
            $("#filter_publisher").on('change', function() {
                publisher = $("#filter_publisher").val()
                // dataTable.ajax.reload(null,false);
                $('#distribution').DataTable().destroy();
                fetch_data(publisher);
            })

            // reset
            $('#reset').click(function() {
                $('#filter_publisher').val('');
                $('#distribution').DataTable().destroy();
                fetch_data();
            });

            

        });

        const uncheckall=()=>{
            $(".checkbox").each(function() {
                $(this).prop('checked', false);
            });
            let qtyItem = $("#distribution tbody .qty").val('');
            
        };

        //add button keranjang buku
        function addKeranjang() {
            // var array = [];

            let checkbox_terpilih = $("#distribution tbody .checkbox:checked")
        
            let semua_id = [];
            $.each(checkbox_terpilih, function(index, elm) {
                semua_id.push(elm.value)
                // console.log(elm.value)
            })
            // console.log(semua_id)
            // cari inputnya
            let result=[];
            $.each(semua_id, function( index, value ) {
                //  alert( value );
                    let isi = $(`.form-qty_${value}`).val();
                    let arr = {"book_api_id":value, "value":isi}
                    result.push(arr)
                //  console.log(arr)
                //  let temp=["book_id"=>value,"value"=>isi]
                //  console.log(temp)
            });
            // cons
            console.log(result)
            

            $("#btnAddKeranjang").prop('disabled', true)
            // ambil instansi yg tterpilih
            let budgetplan_id = $("#budgetplan_id").val();
            
            $.ajax({
                /* the route pointing to the post function */
                url: `{{route('master-rab.store')}}`,
                type: 'POST',
                /* send the csrf-token and the input to the controller */
                data: {_token: CSRF_TOKEN, data:result, budgetplan_id:budgetplan_id},
                dataType: 'JSON',
                /* remind that 'data' is the response of the AjaxController */
                success: function (data) {
                    uncheckall();
                    alert(data.status);
                    
                    
                }
            }); 
            $("#btnAddKeranjang").prop('disabled', false)
           
    
        }
        $("#instansi_id").on("change",function(){
            createTableFromAjax()
            var id = $(this).val()
            console.log(id)
            $("#btn-distribution").attr("data-id","berubah");
        });
        
        function createTableFromAjax(){
            $(document).find(".template-sementara").remove();
            let url = `{{route('master-rab.list.item')}}`
            let idbudget = $("#budgetplan_id").val()
            // let url = `{{route('list-destribute')}}``
            $.ajax({
                /* the route pointing to the post function */
                url: `{{route('master-rab.list.item')}}`,
                type: 'POST',
                /* send the csrf-token and the input to the controller */
                data: {_token: CSRF_TOKEN,id:idbudget},
                dataType: 'JSON',
                /* remind that 'data' is the response of the AjaxController */
                success: function (data) { 
                    let length = data.data.length;
                    
                    if(length==0){
                        var template=`<tr class="template-sementara">
                                        <td colspan="6">Data Tidak ada</td>
                                    </tr>`
                        $( ".table-keranjang" ).append(template);
                    }else{
                        $.each(data.data, function(index, value){
                        
                        var template=`<tr class="template-sementara">
                                        <td>${value.book_id}</td>
                                        <td>${value.title}</td>
                                        <td>${value.isbn}</td>
                                        <td>${value.qty}</td>
                                        <td>${value.penerbit}</td>
                                        <td>Aksi</td>
                                    </tr>`
                            $( ".table-keranjang" ).append(template);
                        });
                    }
                  
                   

                }
            }); 
        }
            

        //  btn distribusi
        $("#btn-distribution").click(function() {
            var agency = $("#instansi_id").val()
            var isGood=confirm('apakah anda yakin akan membagikan buku? data akan hilang setelah anda melakukan pembagian buku');
            if (isGood) {
                $.ajax({
                    url: `{{route('post-destribute')}}`,
                    type: 'POST',
                    data: {_token: CSRF_TOKEN, id:agency},
                    dataType: 'JSON',
                    success: function (data) { 
                        alert(data.status)
                        createTableFromAjax()
                    },
                    error:function(XMLHttpRequest, textStatus, errorThrown){
                       alert(XMLHttpRequest.responseJSON.status)
                      
                    }
                }); 
            } 
        });
        
    </script>
@endpush

