@extends('layouts.super-admin')
@section('content')
    <div class="section-content" data-aos="fade-up" data-aos-delay="500">
        <div class="container-fluid">
            <!-- Breandcrumb -->
            <section class="store-breadcrumbs">
                <div class="row">
                    <div class="col-lg-12">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('agent.index')}}">Pengaturan</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Tambah User</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </section>
            <div class="row">
                <div class="col-lg-12">
                    <div class="row justify-content-between">
                        <div class="col-lg-6">
                            <div class="dashboard-heading">
                                <h5 class="dashboard-title">
                                    Tambah User Baru 
                                </h5>
                                <p class="dashboard-subtitle">
                                    Isi data dengan benar
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <section class="berita">
                <form action="{{route('user-store')}}" id="form-addArtikel" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="container-fluid table">
                                <div class="form">
                                    <div class="form-group">
                                        <label class="form-check-label">Nama</label>
                                        <input type="text" id="name" name="name" placeholder="Masukkan Nama User"
                                            class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-check-label">Email</label>
                                        <input type="email" name="email" class="form-control @error('email') is-invalid @enderror"
                                        value="{{ old('email') }}" id="NamaLengkap" aria-describedby="nama"
                                            placeholder="Masukkan Email " required>
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label class="form-check-label">Role</label>
                                        <div class="dropdown">
                                            <select name="role" id="role"
                                                class="form-control @error('role') is invalid @enderror" tabindex="1">
                                                <option value="#">--pilih role--</option>
                                                <option value="Manajemen">Manajemen</option>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="form-check-label">Password</label>
                                        <input type="password" name="password" class="form-control @error('password') is-invalid @enderror"
                                        value="{{ old('password') }}" id="NamaLengkap" aria-describedby="password"
                                            placeholder="Masukkan password" required>
                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label class="form-check-label">Ulangi Password</label>
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Ulangi password" required autocomplete="new-password">
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="container-fluid battom-nav">
                        <a  href="#" class="btn btn-white" id="btnoke-draf"> <i class="fa fa-remove" aria-hidden="true"></i> Batal</a>
                        <button type="submit" value="publish" class="btn btn-blue" id="btnoke-publish"><i class="fa fa-plus" aria-hidden="true"></i> Simpan</button>
        
                    </div>
                </form>

            </section>
            
        </div>
    </div>

@endsection
