@extends('layouts.super-admin')
@section('content')
<div id="page-content-wrapper">
    <!--Content Dashboard-->
    <!-- ........................ -->
    <div class="section-content" data-aos="fade-up" data-aos-delay="500">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row justify-content-between">
                        <div class="col-lg-6">
                            <div class="dashboard-heading">
                                <h5 class="dashboard-title">
                                    Daftar Pengguna
                                </h5>
                                <p class="dashboard-subtitle">
                                    Sagittis, gravida hac sagittis adipiscing feugiat bibendum adipiscing
                                </p>
                            </div>
                        </div>
                        <div class="col-lg-6" style="text-align: right;">
                            <div class="dashboard-heading " >
                                <a href="{{route('user-create')}}" class="btn btn-blue" ><img src="super-admin/images/icon/plus_white.svg" aria-hidden="true"> Tambah Pengguna</a>
                              
                            </div>
                        </div>
                    </div>

                    <div class="dashboard-heading">
                        <div class="container-fluid table" >
                            <br>
                            <table class="table table-striped " id="table-tambah-pengguna">
                                <thead class="head-table">
                                    <tr>
                                        <th>No</th>
                                        <th>Nama </th>
                                        <th>Email</th>
                                        <th>Role</th>
                                        <th>Id Card</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody class="body-table">
                                    @foreach ($user as $item)
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{$item->name}}</td>
                                        <td>{{$item->email}}</td>
                                        <td>{{$item->role}}</td>
                                        <td>{{$item->idcard}}</td>
                                        <td>
                                            <!-- Default dropleft button -->
                                            <div class="btn-group dropleft">
                                                <button type="button" class="btn btn-secondary fa fa-ellipsis-v" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                           
                                                                            </button>
                                                <div class="dropdown-menu">
                                                    <!-- Dropdown menu links -->
                                                    <a class="dropdown-item " href="{{url('user-detail')}}"> <img src="super-admin/images/icon/profile.svg" alt=""> Detail Pengguna</a>
                                                    <a class="dropdown-item" href="{{url('user-edit')}}"><img src="super-admin/images/icon/edit.svg" alt=""> Edit</a>
                                                    <a class="dropdown-item red-button" href="#"><img src="super-admin/images/icon/banned-active.svg" alt=""> Banned</a>
                                                    <a class="dropdown-item" href="#"><img src="super-admin/images/icon/trash.svg" alt=""> Hapus</a>

                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <br>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
