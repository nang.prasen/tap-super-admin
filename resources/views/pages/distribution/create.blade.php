@extends('layouts.super-admin')
@section('content')
{{-- @include('sweetalert::alert') --}}
<form action="{{route('distribution.store')}}" method="post">
    @csrf
    <div class="section-content section-dashboard-detail" data-aos="fade-up" data-aos-delay="500">
        <div class="container-fluid">
            <!-- Breandcrumb -->
            <section class="store-breadcrumbs">
                <div class="row">
                    <div class="col-lg-12">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('instansi/dashboard')}}">Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><a href="#">Pengaturan Distribusi Buku</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Bagikan Buku</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </section>
            <!-- Judil Detail Buku -->
            <div class="row">
                <div class="col-lg-9">
                    <h5 class="dashboard-title">
                        Buat Distribusi Buku
                    </h5>
                    <p class="dashboard-subtitle">
                        Pilih beberapa buku untuk di bagikan kepada instansi
                    </p>
                </div>
            </div>
            <!--Content Detail Pengaturan Buku Detail-->
            
            <br>
            <section>
                <div class="container">
                    <h5 class="dashboard-title">
                        Pilih Buku Dari Penerbit
                    </h5>
                    <hr>
                    <div class="row">
                        <div class="col-sm-3">
                            <form action="" method="GET">
                                <div class="input-group rounded">
                                    <input value="{{ request()->get('search') }}" type="search" name="search" class="form-control rounded" placeholder="Pencarian"/>
                                    <button type="submit" class="input-group-text border-0" id="search-addon">
                                        <i class="fa fa-search"></i>
                                    </span>
                                </div>
                            </form> 
                        </div>
                        <div class="col-sm-3">
                            <div class="dropdown">
                                <select name="publisher_id" class="form-control @error('publisher_id') is invalid @enderror" tabindex="1">
                                    @foreach ($publisher as $id => $name)
                                        <option value="{{$id}}">{{$name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <br>
                    <!-- Table Bootstrap -->
                    <table class="table table-striped" id="distribution-agencies">
                        
                        <tr>
                        <th class="active">
                            <input  id="check-all" type="checkbox" class="select-all checkbox" value="select-all" />
                        </th>
                        <th class="success">Judul Buku</th>
                        <th class="warning">ISBN</th>
                        <th class="warning">Qty</th>
                        <th class="danger">Penerbit</th>
                    </tr>
                    @foreach ($bookPublisher as $book)
                    <tr>
                        <td class="active">
                            <input name="bookid[]" class="select-item checkbox"  type="checkbox" value="{{ $book->id }}">
                        </td>
                        <td>{{ $book->title }}</td>
                        <td>{{ $book->isbn }}</td>
                        <td><input class="form-control" name="qty[]" type="text"/></td>
                        <td>{{ isset ($book->publisher->name) ? $book->publisher->name : '' }}</td>
                    </tr>
                    @endforeach
    
                    </table>
                    {{-- {{$books->links()}} --}}
                </div>
            </section>
            <section form-control>
                <div class="container">
                    <h5 class="dashboard-title">
                        Pilih Instansi Pengguna
                    </h5>
                    <hr>
                    {{-- <div class="row">
                        <div class="col-sm-3">
                            <div class="input-group rounded">
                                <input type="search" class="form-control rounded" placeholder="Search" aria-label="Search" aria-describedby="search-addon" />
                                <span class="input-group-text border-0" id="search-addon">
                                  <i class="fa fa-search"></i>
                                </span>
                            </div>
                        </div>
                    </div> --}}
                    <br>
                    <!-- Table Bootstrap -->
                    <table class="table table-striped" id="blast-table-siswa-kelas">
                        <select name="agencies_id" class="form-control @error('agencies_id') is invalid @enderror" tabindex="1">
                            @foreach ($agencies as $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
    
                    </table>
                    <button id="select-all" class="btn btn-blue">SelectAll/Cancel</button>
                    <button id="select-invert" class="btn btn-white">Invert</button>
                    <button id="selected" class="btn btn-red">GetSelected</button>
                    <br>
                    <br>
                </div>
                <div class="container-fluid battom-nav">
                    <button class="btn btn-white" ><img src="{{ asset('instansi/images/icon/plus_blue.svg')}}" alt=""> Batal </button>
                    <button class="btn btn-blue " ><img src="{{ asset ('instansi/images/icon/katalog-active.svg')}}" alt=""> Bagikan Paket Buku</button>
                </div>
            </section>
        </div>
    </div>
</form>
@endsection
@push('after-scripts')

@endpush
