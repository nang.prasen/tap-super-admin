@extends('layouts.super-admin')
@section('content')
    {{-- @include('sweetalert::alert') --}}
    <form action="{{ route('distribution.store') }}" method="post">
        @csrf
        <div class="section-content section-dashboard-detail" data-aos="fade-up" data-aos-delay="500">
            <div class="container-fluid">
                <!-- Breandcrumb -->
                <section class="store-breadcrumbs">
                    <div class="row">
                        <div class="col-lg-12">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ url('instansi/dashboard') }}">Dashboard</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page"><a href="#">Pengaturan Distribusi
                                            Buku</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Bagikan Buku</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </section>
                <!-- Judil Detail Buku -->
                <div class="row">
                    <div class="col-lg-12">
                        <h5 class="dashboard-title">
                            Buat Distribusi Buku
                        </h5>
                        <p class="dashboard-subtitle">
                            Pilih beberapa buku untuk di bagikan kepada instansi
                        </p>
                    </div>
                </div>
                <!--Content Detail Pengaturan Buku Detail-->

                <br>
                <section>
                    <div class="container-table container-fluid">
                        <h5 class="dashboard-title">
                            Pilih Buku Dari Penerbit
                        </h5>
                        <hr>
                        <div class="row">
                            {{-- <div class="col-sm-3">
                                <form action="" method="GET">
                                    <div class="input-group rounded">
                                        <input value="{{ request()->get('search') }}" type="search" name="search"
                                            class="form-control rounded" placeholder="Pencarian" />
                                        <button type="submit" class="input-group-text border-0" id="search-addon">
                                            <i class="fa fa-search"></i>
                                            </span>
                                    </div>
                                </form>
                            </div> --}}
                            <div class="col-sm-3">
                                <div class="dropdown">
                                    <select name="filter_publisher" id="filter_publisher"
                                        class="form-control @error('publisher_id') is invalid @enderror" tabindex="1">
                                        <option value="#">--pilih penerbit--</option>
                                        @foreach ($publisher as $id => $name)
                                            <option value="{{ $id }}">{{ $name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <br>
                        <!-- Table Bootstrap -->
                        <table class="table table-striped" id="distribution">
                            <thead>

                                <tr>
                                    <th class="active">
                                        <input id="check-all" type="checkbox" class="select-all checkbox"
                                            value="select-all" />
                                    </th>
                                    <th class="success">Judul Buku</th>
                                    <th class="warning">ISBN</th>
                                    <th class="warning">Qty</th>
                                    <th class="danger">Penerbit</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                            {{-- @foreach ($bookPublisher as $book)
                                <tr>
                                    <td class="active">
                                        <input name="bookid[]" class="select-item checkbox" type="checkbox"
                                            value="{{ $book->id }}">
                                    </td>
                                    <td>{{ $book->title }}</td>
                                    <td>{{ $book->isbn }}</td>
                                    <td><input class="form-control" name="qty[]" type="text" /></td>
                                    <td>{{ isset($book->publisher->name) ? $book->publisher->name : '' }}</td>
                                </tr>
                            @endforeach --}}

                        </table>
                        {{-- {{$books->links()}} --}}
                    </div>
                </section>
                <section form-control>
                    <div class="container-table">
                        <h5 class="dashboard-title">
                            Pilih Instansi Pengguna
                        </h5>
                        <hr>
                        {{-- <div class="row">
                        <div class="col-sm-3">
                            <div class="input-group rounded">
                                <input type="search" class="form-control rounded" placeholder="Search" aria-label="Search" aria-describedby="search-addon" />
                                <span class="input-group-text border-0" id="search-addon">
                                  <i class="fa fa-search"></i>
                                </span>
                            </div>
                        </div>
                    </div> --}}
                        <br>
                        <!-- Table Bootstrap -->
                        <table class="table table-striped" id="blast-table-siswa-kelas">
                            <select name="agencies_id" id="getinstansi"
                                class="form-control instansi-get @error('agencies_id') is invalid @enderror" tabindex="1">
                                {{-- @foreach ($agencies as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach --}}
                            </select>
                            <br>
                            <br>
                        </table>

                    </div>
                    <div class="container-fluid battom-nav">
                        <button class="btn btn-white"><img src="{{ asset('instansi/images/icon/plus_blue.svg') }}"
                                alt="">
                            Batal </button>
                        <button class="btn btn-blue" id="btn-distribution"><img
                                src="{{ asset('instansi/images/icon/katalog-active.svg') }}" alt=""> Bagikan Buku</button>
                    </div>
                </section>
            </div>
        </div>
    </form>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection
@push('after-script')
    <script>
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $(document).ready(function() {
            fetch_data();
            let publisher = $("#filter_publisher").val()

            function fetch_data(filter_publisher = '') {
                var dataTable = $('#distribution').DataTable({
                    responsive: true,
                    autoWidth: false,
                    processing: true,
                    serverSide: true,
                    scrollX: true,
                    order: [
                        [2, "desc"]
                    ],
                    pageLength: 100,
                    lengthMenu: [
                        [10, 25, 50, 100, -1],
                        [10, 25, 50, 100, 'semua']
                    ],
                    ajax: {
                        url: "{{ route('distribution-book') }}",
                        data: function(d) {
                            // d.publisher = publisher;
                            d.filter_publisher = filter_publisher;
                            return d;
                        }
                    },
                    columnDefs: [{
                            "targets": 0,
                            "class": "text-nowrap",
                            "sortable": false,
                            "render": function(data, type, row, meta) {
                                return `<input type="checkbox" name="bookid[]" class="select-all checkbox" value="${row.bookId}">`;
                            }
                        },
                        {
                            "targets": 1,
                            "class": "text-nowrap limit-text-table",
                            "render": function(data, type, row, meta) {
                                return row.title
                            }
                        },
                        {
                            "targets": 2,
                            "class": "text-nowrap",
                            "render": function(data, type, row, meta) {
                                return row.isbn
                            }
                        },
                        {
                            "targets": 3,
                            "class": "text-nowrap",
                            "sortable": false,
                            "render": function(data, type, row, meta) {
                                return `<input type="text" name="qty[]" class="form-control qty" style="width: 40%;" />`;
                                
                            }
                        },
                        {
                            "targets": 4,
                            "class": "text-nowrap",
                            "render": function(data, type, row, meta) {
                                return row.name
                            }
                        },
                        {
                            "targets": 5,
                            "visible": true,
                            "class": "text-nowrap",
                            "render": function(data, type, row, meta) {
                                return `<input type="hidden" name="publisher_id[]" class="form-control publisher_id" value="${row.publisherId}" style="width: 30%;" />`;
                            }
                        },
                    ]
                });
            }
            // checkbox all
            $("#check-all").on('click', function() {
                var isChecked = $("#check-all").prop('checked')
                $(".checkbox").prop('checked', isChecked)
                // console.log("OK");
            })
            // single/uncheck checkbox
            $("#table tbody").on('click', '.checkbox', function() {
                if ($(this).prop('checked') != true) {
                    $("#check-all").prop('checked', false)
                }
            })
            // filter publisher
            $("#filter_publisher").on('change', function() {
                publisher = $("#filter_publisher").val()
                // dataTable.ajax.reload(null,false);
                $('#distribution').DataTable().destroy();
                fetch_data(publisher);
            })
            // button blast
            // $("#btn-distribution").on('click',function () {
            //     let idBook = $(".checkbox").val()
            //     let qty = $(".qty").val()
            //     let publisherid = $(".publisher_id").val()
            //     console.log(idBook,qty,publisherid);
            // })
            // reset
            $('#reset').click(function() {
                $('#filter_publisher').val('');
                $('#distribution').DataTable().destroy();
                fetch_data();
            });

            $('#getinstansi').select2({
                placeholder: 'Pilih Intansi',
                ajax:{
                    url:'/distribusi/instansi/get',
                    type: 'post',
                    dataType:'json',
                    delay:250,
                    data: function(params){
                        return{
                            _token: CSRF_TOKEN,
                            search: params.term
                        };
                    },
                    processResults:function(response){
                        return{
                            results: response
                        };
                    },
                    cache:true
                }
            });
        });
    </script>
@endpush
