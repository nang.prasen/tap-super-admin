@extends('layouts.super-admin')
@section('content')
    {{-- @include('sweetalert::alert') --}}
    {{-- <form action="#">
        @csrf --}}
    <div class="section-content section-dashboard-detail" data-aos="fade-up" data-aos-delay="500">
        <div class="container-fluid">
            <!-- Breandcrumb -->
            <section class="store-breadcrumbs">
                <div class="row">
                    <div class="col-lg-12">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('instansi/dashboard') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page"><a href="#">Pengaturan Distribusi
                                        Buku</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Bagikan Buku</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </section>
            <!-- Judil Detail Buku -->
            <div class="row">
                <div class="col-lg-9">
                    <h5 class="dashboard-title">
                        Buat Distribusi Buku
                    </h5>
                    <p class="dashboard-subtitle">
                        Pilih beberapa buku untuk di bagikan kepada instansi
                    </p>
                </div>
            </div>
            <!--Content Detail Pengaturan Buku Detail-->

            <br>
            <section>
                <div class="container-fluid bg-white">
                    <h5 class="dashboard-title">
                        Pilih Buku Dari Penerbit
                    </h5>
                    <hr>
                    <div class="row">
                        {{-- <div class="col-sm-3">
                                <form action="" method="GET">
                                    <div class="input-group rounded">
                                        <input value="{{ request()->get('search') }}" type="search" name="search"
                                            class="form-control rounded" placeholder="Pencarian" />
                                        <button type="submit" class="input-group-text border-0" id="search-addon">
                                            <i class="fa fa-search"></i>
                                            </span>
                                    </div>
                                </form>
                            </div> --}}
                        <div class="col-sm-3">
                            <div class="dropdown">
                                <select name="filter_publisher" id="filter_publisher"
                                    class="form-control @error('publisher_id') is invalid @enderror" tabindex="1">
                                    <option value="#">--pilih penerbit--</option>
                                    @foreach ($publisher as $id => $name)
                                        <option value="{{ $id }}">{{ $name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <br>
                    <!-- Table Bootstrap -->
                    <div class="table table-responsive">
                        <table class="table table-striped" id="distribution">
                            <thead>
    
                                <tr>
                                    <th class="active">
                                        <input id="check-all" type="checkbox" class="select-all checkbox" value="select-all" />
                                    </th>
                                    <th class="success">Judul Buku</th>
                                    <th class="warning">ISBN</th>
                                    <th class="warning">Qty</th>
                                    <th class="danger">Penerbit</th>
                                    <th>##</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                            {{-- @foreach ($bookPublisher as $book)
                                    <tr>
                                        <td class="active">
                                            <input name="bookid[]" class="select-item checkbox" type="checkbox"
                                                value="{{ $book->id }}">
                                        </td>
                                        <td>{{ $book->title }}</td>
                                        <td>{{ $book->isbn }}</td>
                                        <td><input class="form-control" name="qty[]" type="text" /></td>
                                        <td>{{ isset($book->publisher->name) ? $book->publisher->name : '' }}</td>
                                    </tr>
                                @endforeach --}}
    
                        </table>
                    </div>
                    {{-- {{$books->links()}} --}}
                </div>
            </section>
            <button type="button" id="btnAddKeranjang"  disabled onclick="addKeranjang()" class="btn btn-danger"
                style="margin-bottom: 1rem;">Add Keranjang</button>
            <section form-control>
                <div class="container-fluid bg-white">
                    <h5 class="dashboard-title">
                        Pilih Instansi Pengguna
                    </h5>
                    <hr>
                    <br>
                    <!-- Table Bootstrap -->
                    <table class="table table-striped" id="blast-table-siswa-kelas">
                        <select name="agencies_id" id="instansi_id" class="form-control @error('agencies_id') is invalid @enderror"
                            tabindex="1">
                            
                        </select>
                        <br>
                        <br>
                    </table>

                </div>

                {{-- Keranjang --}}
                <div class="container-fluid bg-white">
                    <h5 class="dashboard-title">
                        Pilihan Buku
                    </h5>
                    <hr>
                    <br>
                    <!-- Table Bootstrap -->
                    <div class="table table-responsive">
                        <table class="table table-striped" id="keranjang-buku">
                            <thead>
    
                                <tr>
                                    <th class="active">ID Buku</th>
                                    <th class="success">Judul Buku</th>
                                    <th class="warning">ISBN</th>
                                    <th class="warning">Qty</th>
                                    <th class="danger">Penerbit</th>
                                    <th>##</th>
                                </tr>
                            </thead>
                            <tbody class="table-keranjang">
                               
                            </tbody>
                        </table>
                    </div>

                </div>
                <div class="container-fluid battom-nav">
                    <button class="btn btn-white"><img src="{{ asset('instansi/images/icon/plus_blue.svg') }}" alt="">
                        Batal </button>
                    <button class="btn btn-blue" data-id="0" type="button" id="btn-distribution" ><img
                            src="{{ asset('instansi/images/icon/katalog-active.svg') }}" alt=""> Bagikan Buku</button>
                </div>
            </section>
        </div>
    </div>
    {{-- </form> --}}
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <style media="all" type="text/css">
        .ellipsis-500 { 
            display: block;
            text-overflow: ellipsis;
            overflow: hidden;
            max-width: 500px;
        }
    </style>
@endsection
@push('after-script')
    <script>
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var agency = $("#instansi_id").val()
        console.log(agency)
        $(document).ready(function() {
            createTableFromAjax();
            fetch_data();
            let publisher = $("#filter_publisher").val()
    
            function fetch_data(filter_publisher = '') {
                var dataTable = $('#distribution').DataTable({
                    responsive: true,
                    autoWidth: false,
                    processing: true,
                    serverSide: true,
                    order: [
                        [2, "desc"]
                    ],
                    // pageLength: 100,
                    // lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'semua']],
                    ajax: {
                        url: "{{ route('distribution-book') }}",
                        data: function(d) {
                            // d.publisher = publisher;
                            d.filter_publisher = filter_publisher;
                            return d;
                        }
                    },
                    columnDefs: [{
                            "targets": 0,
                            "class": "text-nowrap",
                            "sortable": false,
                            "render": function(data, type, row, meta) {
                                return `<input type="checkbox" id="bookid" name="bookid[]" class="select-all checkbox" value="${row.bookId}">`;
                            }
                        },
                        {
                            "targets": 1,
                            "class": "text-nowrap title ellipsis-500",
                            "render": function(data, type, row, meta) {
                                return row.title
                            }
                        },
                        {
                            "targets": 2,
                            "class": "text-nowrap isbn",
                            "render": function(data, type, row, meta) {
                                return row.isbn
                            }
                        },
                        {
                            "targets": 3,
                            "class": "text-nowrap",
                            "sortable": false,
                            "render": function(data, type, row, meta) {
                                return `<input type="text" name="qty[]" class="form-control qty form-qty_${row.bookId}" style="width: 150px;"  data-id="" />`;
                            }
                        },
                        {
                            "targets": 4,
                            "class": "text-nowrap pbl",
                            "render": function(data, type, row, meta) {
                                return row.name
                            }
                        },
                        {
                            "targets": 5,
                            "visible": true,
                            "class": "text-nowrap",
                            "render": function(data, type, row, meta) {
                                return `<input type="hidden" name="publisher_id[]" class="form-control publisher_id" value="${row.publisherId}" style="width: 30%;" />
                                    <button type="button" class='btn btn-blue btnAdd'> + </button>
                            `;
                            }
                        },
                    ]
                });
            }

            // checkbox all
            $("#check-all").on('click', function() {
                var isChecked = $("#check-all").prop('checked')
                $(".checkbox").prop('checked', isChecked)
                $("#btnAddKeranjang").prop('disabled', !isChecked)
            })

            // single/uncheck checkbox
            $("#distribution tbody").on('click', '.checkbox', function() {
                if ($(this).prop('checked') != true) {
                    $("#check-all").prop('checked', false)
                }

                let semua_checkbox = $("#distribution tbody .checkbox:checked")
                let button_add_keranjang = (semua_checkbox.length > 0)
                let button_terpilih = button_add_keranjang;
                $("#btnAddKeranjang").prop('disabled', !button_add_keranjang)
            })

            // filter publisher
            $("#filter_publisher").on('change', function() {
                publisher = $("#filter_publisher").val()
                // dataTable.ajax.reload(null,false);
                $('#distribution').DataTable().destroy();
                fetch_data(publisher);
            })

            // button blast
            // $("#btn-distribution").on('click',function () {
            //     let idBook = $(".checkbox").val()
            //     let qty = $(".qty").val()
            //     let publisherid = $(".publisher_id").val()
            //     console.log(idBook,qty,publisherid);
            // })

            // reset
            $('#reset').click(function() {
                $('#filter_publisher').val('');
                $('#distribution').DataTable().destroy();
                fetch_data();
            });

            $('#instansi_id').select2({
                placeholder: 'Pilih Intansi',
                ajax:{
                    url:'/distribusi/instansi/get',
                    type: 'post',
                    dataType:'json',
                    delay:250,
                    data: function(params){
                        return{
                            _token: CSRF_TOKEN,
                            search: params.term
                        };
                    },
                    processResults:function(response){
                        return{
                            results: response
                        };
                    },
                    cache:true
                }
            });

        });

        //add button keranjang buku
        function addKeranjang() {
            // var array = [];

            let checkbox_terpilih = $("#distribution tbody .checkbox:checked")
        
            let semua_id = [];
            $.each(checkbox_terpilih, function(index, elm) {
                semua_id.push(elm.value)
                // console.log(elm.value)
            })
            // console.log(semua_id)
            // cari inputnya
            let result=[];
            $.each(semua_id, function( index, value ) {
                //  alert( value );
                    let isi = $(`.form-qty_${value}`).val();
                    let arr = {"book_api_id":value, "value":isi}
                    result.push(arr)
                //  console.log(arr)
                //  let temp=["book_id"=>value,"value"=>isi]
                //  console.log(temp)
            });
            // cons
            // console.log(result)
            

            $("#btnAddKeranjang").prop('disabled', true)
            // ambil instansi yg tterpilih
            let instansi = $("#instansi_id").val();
            
            $.ajax({
                /* the route pointing to the post function */
                url: `{{route('distribution-book.post')}}`,
                type: 'POST',
                /* send the csrf-token and the input to the controller */
                data: {_token: CSRF_TOKEN, data:result, instansi:instansi},
                dataType: 'JSON',
                /* remind that 'data' is the response of the AjaxController */
                success: function (data) { 
                    createTableFromAjax()
                    dataTable.ajax.url("{{ route('distribution-book') }}").load();
                    
                    
                }
            }); 
            $("#btnAddKeranjang").prop('disabled', false)
           
    
        }
        $("#instansi_id").on("change",function(){
            createTableFromAjax()
            var id = $(this).val()
            console.log(id)
            $("#btn-distribution").attr("data-id","berubah");
        });
        
        function createTableFromAjax(){
            $(document).find(".template-sementara").remove();
            let url = `{{route('list-destribute')}}`
            let instansiId = $("#instansi_id").val()
            // let url = `{{route('list-destribute')}}``
            $.ajax({
                /* the route pointing to the post function */
                url: `{{route('list-destribute')}}`,
                type: 'POST',
                /* send the csrf-token and the input to the controller */
                data: {_token: CSRF_TOKEN, id:instansiId},
                dataType: 'JSON',
                /* remind that 'data' is the response of the AjaxController */
                success: function (data) { 
                    let length = data.data.length;
                    
                    if(length==0){
                        var template=`<tr class="template-sementara">
                                        <td colspan="6">Data Tidak ada</td>
                                    </tr>`
                        $( ".table-keranjang" ).append(template);
                    }else{
                        $.each(data.data, function(index, value){
                        
                        var template=`<tr class="template-sementara">
                                        <td>${value.book_id}</td>
                                        <td>${value.title}</td>
                                        <td>${value.isbn}</td>
                                        <td>${value.qty}</td>
                                        <td>${value.penerbit}</td>
                                        <td>Aksi</td>
                                    </tr>`
                            $( ".table-keranjang" ).append(template);
                        });
                    }
                  
                   

                }
            }); 
        }
            

        //  btn distribusi
        $("#btn-distribution").click(function() {
            var agency = $("#instansi_id").val()
            var isGood=confirm('apakah anda yakin akan membagikan buku? data akan hilang setelah anda melakukan pembagian buku');
            if (isGood) {
                $.ajax({
                    url: `{{route('post-destribute')}}`,
                    type: 'POST',
                    data: {_token: CSRF_TOKEN, id:agency},
                    dataType: 'JSON',
                    success: function (data) { 
                        alert(data.status)
                        createTableFromAjax()
                    },
                    error:function(XMLHttpRequest, textStatus, errorThrown){
                       alert(XMLHttpRequest.responseJSON.status)
                      
                    }
                }); 
            } 
        });
        
    </script>
@endpush

