@extends('layouts.super-admin')
@section('content')
<div class="section-content" data-aos="fade-up" data-aos-delay="500">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="row justify-content-between">
                    <div class="col-lg-6">
                        <div class="dashboard-heading">
                            <h5 class="dashboard-title">
                                RAB Distribusi Buku
                            </h5>
                            <p class="dashboard-subtitle">
                               List distribusi buku terakhir dilakukan
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-6" style="text-align: right;">
                        <div class="dashboard-heading">
                            <a class="btn btn-blue" href="{{route('master-rab.index')}}" ><img src="super-admin/images/icon/plus_white.svg" aria-hidden="true"> Master RAB</a>
                            <a class="btn btn-blue" href="{{route('distribution.create')}}" ><img src="super-admin/images/icon/plus_white.svg" aria-hidden="true"> Tambah RAB</a>
                        </div>
                    </div>
                </div>

                <div class="dashboard-heading">
                    <div class="container-fluid table" >
                        <br>
                        @if($errors->any())
                        <div class="alert alert-danger" role="alert">
                            {{$errors->first()}}
                          </div>
                        @endif
                        @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif

                        <table class="table table-striped " id="table-tambah-pengguna">
                            <thead class="head-table">
                                <tr>
                                    <th>No</th>
                                    <th>INV </th>
                                    <th>Instansi </th>
                                    <th>Tanggal Pembuatan</th>
                                    <th>Jumlah Buku</th>
                                    <th>Status</th>
                                    <th>Status Pembagian</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody class="body-table">
                                @foreach ($distribution as $item)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$item->kode}}</td>
                                    <td>
                                        {{isset($item->name) ? $item->name:'Belum ada'}}
                                    </td>
                                    <td>{{$item->created_at}}</td>
                                    <td>{{$item->total}} eksemplar</td>
                                    <td>{{$item->flag_bagi}}</td>
                                    <td>{{$item->status}}</td>
                                    <td>
                                        <div class="btn-group dropleft">
                                            <button type="button" class="btn btn-secondary fa fa-ellipsis-v"
                                                data-toggle="dropdown" aria-haspopup="true"
                                                aria-expanded="false">

                                            </button>
                                            <div class="dropdown-menu">
                                                <!-- Dropdown menu links -->
                                                @if ($item->status=='bayar')
                                                <a class="dropdown-item" 
                                                href="{{$item->bukti_pembayaran}}" target="_blank"
                                                ><i class="fa fa-eye"></i> Lihat Bukti Pembayaran</a>
                                                @endif


                                                <a class="dropdown-item" href="{{route('distribution.detail',$item->temp_distribute_plans_id)}}"><i class="fa fa-eye"></i> Lihat</a>
                                                <a class="dropdown-item" href="{{route('distribution.banned',$item->temp_distribute_plans_id)}}"> <i class="fa fa-ban"></i> Banned</a>
                                                <a class="dropdown-item" href="{{route('distribution.publish',$item->temp_distribute_plans_id)}}"><i class="fa fa-upload"></i> Publish</a>
                                                <a class="dropdown-item" href="#"> <i class="fa fa-download"></i> Cetak Excel</a>

                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                        <br>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>
@endsection
@push('after-script')
<script>
$(document).ready(function() {
    var table = $('#table-user-penerbit').DataTable({
        lengthChange: false,
        dom:
        // 'Bfrtip',
            "<'row'<'col-md-3'f><'col-md-6'><'col-md-3'B>>" +
            "<'row'<'col-md-12'tr>>" +
            "<'row'<'col-sm-3'i><'col-sm-3'><'col-sm-3'p>>",
        buttons: [{
            extend: 'csvHtml5',
            text: ' Export   <i class="fa  fa-cloud-download"></i>',
            titleAttr: 'CSV'
        }, ],

        language: {
            searchPlaceholder: "Search",
            search: "",
        },
    });

    table.buttons().container()
        .appendTo('#table_wrapper .col-md-5:eq(0)');
});
</script>
@endpush
