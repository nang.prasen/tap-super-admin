<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />

    <title>TAP - Troya Academic Platform</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('components.style')

</head>

<body>
    {{-- preloader --}}
    {{-- <div class="preloader" id="preloader">
    <div class="content ">
        <div class="tap ">
            <div class="__tap__wrapper loading">
                <div class=" __ttap__1">
                    <div class=" __ttap__2"></div>
                </div>

                <div class=" __atap__1">
                    <div class="__atap__circle"></div>
                </div>
                <div class=" __atap__2"></div>

                <div class=" __ptap__1">
                </div>
                <div class="__ptap__2">
                    <div class="__ptap__circle"></div>
                </div>
                <div class=" __pattern">
                    <div class="__pattern__wrapper">
                        <div class="row">

                            <div class="__pattern__1"></div>
                            <div class="__pattern__2"></div>
                        </div>
                    </div>
                    <div class="__pattern__wrapper">
                        <div class="row">

                            <div class="__pattern__2"></div>
                            <div class="__pattern__1"></div>
                        </div>
                    </div>
                    <div class="__pattern__wrapper">
                        <div class="row">

                            <div class="__pattern__1"></div>
                            <div class="__pattern__2"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div> --}}
    <!-- Navbar-->
    <x-navbar></x-navbar>
    <div class="page-dashboard">
        <div class="d-flex" id="wrapper" data-aos="fade-right" data-aos-delay="300">

            <!-- Side Bar-->
            {{-- <x-sidebar></x-sidebar> --}}
            @include('components.sidebar')

            <!-- Page Content -->
            <div id="page-content-wrapper">
                <!--Content Dashboard-->
                <div class="section-content section-dashboard-home aos-init aos-animate" data-aos="fade-up"
                    data-aos-delay="500">
                    <div class="container-fluid">
                        @yield('content')
                    </div>
                </div>


            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Informasi</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Apakah anda ingin keluar dari aplikasi?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
                        <a type="button" class="btn btn-primary" data-dismiss="modal" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                            <span>Ya</span>
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        ...
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade bd-example-modal-sm" id="modalIdUpdate" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Peringatan</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Data berhasil diperbaharui
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    @include('components.script')
    @stack('after-script')
</body>

</html>
