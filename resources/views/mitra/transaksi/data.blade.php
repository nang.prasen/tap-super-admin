@extends('layouts.super-admin')
@section('content')
<div class="section-content" data-aos="fade-up" data-aos-delay="500">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="row justify-content-between">
                    <div class="col-lg-6">
                        <div class="dashboard-heading">
                            <h5 class="dashboard-title">
                                Transaksi Buku
                            </h5>
                            <p class="dashboard-subtitle">
                               List transaksi buku terakhir dilakukan
                            </p>
                        </div>
                    </div>
                    
                </div>

                <div class="dashboard-heading">
                    <div class="container-fluid table" >
                        <br>
                        @if($errors->any())
                        <div class="alert alert-danger" role="alert">
                            {{$errors->first()}}
                          </div>
                        @endif
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        <table class="table table-striped " id="table-tambah-pengguna">
                            <thead class="head-table">
                                <tr>
                                    <th>No</th>
                                    <th>INV </th>
                                    <th>Instansi </th>
                                    <th>Tanggal Pembuatan</th>
                                    <th>Jumlah Buku</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody class="body-table">
                                @foreach ($distribution as $item)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$item->kode}}</td>
                                    <td>
                                        {{isset($item->name) ? $item->name:'Belum ada'}}
                                    </td>
                                    <td>{{$item->created_at}}</td>
                                    <td>{{$item->total}} eksemplar</td>
                                    @if ($item->flag_bagi == 1)
                                        <td>
                                            <p>Terbagi</p>
                                        </td>
                                    @endif
                                    <td><a class="btn btn-blue" type="button" href="{{route('mitra.show',$item->id)}}"> <i class="fa fa-dollar"></i> Detail</a></td>
                                    
                                </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                        <br>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>
@endsection
@push('after-script')
<script>
$(document).ready(function() {
    var table = $('#table-user-penerbit').DataTable({
        lengthChange: false,
        dom:
        // 'Bfrtip',
            "<'row'<'col-md-3'f><'col-md-6'><'col-md-3'B>>" +
            "<'row'<'col-md-12'tr>>" +
            "<'row'<'col-sm-3'i><'col-sm-3'><'col-sm-3'p>>",
        buttons: [{
            extend: 'csvHtml5',
            text: ' Export   <i class="fa  fa-cloud-download"></i>',
            titleAttr: 'CSV'
        }, ],

        language: {
            searchPlaceholder: "Search",
            search: "",
        },
    });

    table.buttons().container()
        .appendTo('#table_wrapper .col-md-5:eq(0)');
});
</script>
@endpush
