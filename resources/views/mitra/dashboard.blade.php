@extends('layouts.super-admin')
@section('content')

    <div class="section-content section-dashboard-home" data-aos="fade-up" data-aos-delay="500">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9">
                    <div class="dashboard-heading">
                    <br>
                        <h5 class="dashboard-title">
                            Rekapitulasi Mitra
                        </h5>
                        <p class="dashboard-subtitle">
                           Rekapitulasi Mitra
                        </p>
                        <div class="dashboard-content">
                            <div class="row">
                                <div class=" col-lg-4 col-md-6 col-sm-12 col-12">
                                    <div class="card mb-2">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-3">
                                                    <img src="{{ asset('super-admin/images/dashboard-icon-speacker.png') }}"
                                                        alt="Logo" />
                                                </div>
                                                <div class="col-lg-9 col-md-9 col-sm-9 col-9">
                                                    <div class="dashboard-card-subtitle">
                                                        Total Instansi
                                                    </div>
                                                    <div style="font-size:24px; font-weight: 600; color: #EB9557;">
                                                        {{$countinstansi}}
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class=" col-lg-4 col-md-6 col-sm-12 col-12">
                                    <div class="card mb-2">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-3">
                                                    <img src="{{ asset('super-admin/images/dashboard-icon-manageruser.svg') }}"
                                                        alt="Logo" />
                                                </div>
                                                <div class="col-lg-9 col-md-9 col-sm-9 col-9">
                                                    <div class="dashboard-card-subtitle">
                                                        Total PO
                                                    </div>
                                                    <div style="font-size:24px; font-weight: 600; color: #EF7272;">
                                                        Rp {{$totpo}}
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <div class="dashboard-heading">
                        <h5 class="dashboard-title" style="margin-top: 32px;">
                            Buku terlaris anda
                        </h5>
                        <p class="dashboard-subtitle">
                            <br>
                        </p>

                        <!-- Rekapitulasi -->
                        <div class="dashboard-content">
                            <div class="container">
                                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home"
                                            role="tab" aria-controls="pills-home" aria-selected="true">Sering Dibaca</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill"
                                            href="#pills-profile" role="tab" aria-controls="pills-profile"
                                            aria-selected="false">Paling Laku</a>
                                    </li>
                                </ul>
                                <div class="tab-content" id="pills-tabContent">
                                    <div class="tab-pane fade show active" id="pills-home" role="tabpanel"
                                        aria-labelledby="pills-home-tab">
                                        {{-- <table class="table table-striped table-hover" id="table">

                                            <thead class="head-table">
                                                <tr>
                                                    <th>No</th>
                                                    <th>Judul Buku</th>
                                                    <th>Nama Peminjam</th>
                                                    <th>Waktu Peminjaman</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody class="body-table">
                                                <tr>
                                                    <td>1</td>
                                                    <td>haloooooo</td>
                                                    <td>Scarllet Fatimah</td>
                                                    <td>10:05:30 AM • 20 Mar 2021</td>
                                                    <td><a href="#"> lihat</a> </td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td>Sebuah mandiri</td>
                                                    <td>Scarllet Fatimah</td>
                                                    <td>10:05:30 AM • 20 Mar 2021</td>
                                                    <td><a href="#"> lihat</a> </td>
                                                </tr>
                                                <tr>
                                                    <td>3</td>
                                                    <td>Sebuah Seni Untuk bersikap...</td>
                                                    <td>Scarllet Fatimah</td>
                                                    <td>10:05:30 AM • 20 Mar 2021</td>
                                                    <td><a href="#"> lihat</a> </td>
                                                </tr>
                                                <tr>
                                                    <td>4</td>
                                                    <td>Sebuah Seni Untuk bersikap...</td>
                                                    <td>Scarllet Fatimah</td>
                                                    <td>10:05:30 AM • 20 Mar 2021</td>
                                                    <td><a href="#"> lihat</a> </td>
                                                </tr>
                                                <tr>
                                                    <td>5</td>
                                                    <td>Sebuah Seni Untuk bersikap...</td>
                                                    <td>Scarllet Fatimah</td>
                                                    <td>10:05:30 AM • 20 Mar 2021</td>
                                                    <td><a href="#"> lihat</a> </td>
                                                </tr>

                                            </tbody>
                                        </table>
                                        <h2 style="text-align: right;"> lihat selengkapnya</h2> --}}
                                        <table>
                                            <img src="{{asset('images/404.png')}}" alt="" style="width: 40%; height:40%; margin-left:30%; margin-bottom:20%; margin-top:10%; text-align:center;">
                                        </table>

                                    </div>
                                    <div class="tab-pane fade" id="pills-profile" role="tabpanel"
                                        aria-labelledby="pills-profile-tab">
                                        <table>
                                        
                                            <img src="{{asset('images/404.png')}}" alt="" style="width: 40%; height:40%; margin-left:30%; margin-bottom:20%; margin-top:10%; text-align:center;">
                                        </table>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                    <br>
                    <!-- Box-->

                </div>
                <!--Informasi-->
                <div class="col-lg-3" data-aos="fade-up" data-aos-delay="700">
                    <div class="dashboard-heading">
                        <h5 class="dashboard-title">
                            Informasi
                        </h5>
                        <p class="dashboard-subtitle">
                            Informasi tips & trik untuk instansi
                        </p>

                    </div>
                    <section class="store-carousel-slider ">
                        <p> Informasi dari Troya</p>
                        <section class="store-carousel">
                            <div class="container-slider">
                                <div class="row">
                                    <div class="col-lg-12" data-aos="zoom-in">
                                        <div class="carousel slide" id="storeCarousel" data-ride="carousel">
                                            <ol class="carousel-indicators">
                                                <li class="active" data-target="#storeCarousel" data-slide-to="0">
                                                </li>
                                                <li data-target="#storeCarousel" data-slide-to="1"></li>
                                                <li data-target="#storeCarousel" data-slide-to="2"></li>
                                            </ol>
                                            <div class="carousel-inner">
                                                <div class="carousel-item active">
                                                    <img src="{{asset('super-admin/images/dashboard-cardinfo.png')}}" alt=""
                                                        class="d-block w-100" />
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="{{asset('super-admin/images/dashboard-cardinfo.png')}}" alt=""
                                                        class="d-block w-100" />
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="{{asset('super-admin/images/dashboard-cardinfo.png')}}" alt=""
                                                        class="d-block w-100" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </section>

                    </section>
                    <section class="news-down">
                        <div class="container">
                            <p> Riwayat Distribusi Buku</p>
                            <div class="dashboard-content">
                                <div class="row">
                                    <div class=" col-lg-12 col-md-6 col-sm-12 col-12">
                                        <div class="card mb-2">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-3">
                                                        <img src="{{asset('super-admin/images/dashboard-icon-openbook.svg')}}"
                                                            alt="Logo" />
                                                    </div>
                                                    <div class="col-lg-9 col-md-9 col-sm-9 col-9">
                                                        <div class="dashboard-card-title">
                                                            SMA Taruna 1
                                                        </div>
                                                        <div class="dashboard-card-subtitle">
                                                            02 April 21 
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                </div>
            </div>



        </div>
    </div>

@endsection
