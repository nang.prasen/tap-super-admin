@extends('layouts.super-admin')
@section('content')

<div class="section-content section-dashboard-detail" data-aos="fade-up" data-aos-delay="500">
  <div class="container-fluid">
      <!-- Breandcrumb -->
      <section class="store-breadcrumbs">
          <div class="row">
              <div class="col-lg-12">
                  <nav aria-label="breadcrumb">
                      <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="{{url('instansi/dashboard')}}">Dashboard</a></li>
                          <li class="breadcrumb-item active" aria-current="page">Detail Buku</li>
                      </ol>
                  </nav>
              </div>
          </div>
      </section>
      <!-- Judil Detail Buku -->
      <div class="row">
          <div class="col-lg-9">
              <h5 class="dashboard-title">
                  Detail Buku
              </h5>
              <p class="dashboard-subtitle">
                  Sagittis, gravida hac sagittis adipiscing feugiat bibendum adipiscing
              </p>
          </div>
      </div>
      <!--Content Detail Buku-->
      <section class="section-inventaris">
          <div class="row">
          <div class="col-lg-3">
              <div class="book-rekapitulasi">
                  <img src="{{ url($catalogdetail->cover)}}" alt="" class="img-detail-book" >
              </div>
              <br/>
              <div class="container">
                  <p>
                      Harga Buku
                  </p>
                  <h6>
                      Rp {{$catalogdetail->price}}
                  </h6>
                  <button class="btn btn-tersedia"> Tersedia di Inventaris Buku
                  </button>
                  <hr>
              </div>
          </div>
          <div class="col-lg-9">
              <div class="container">
                  <div class="book-rekapitulasi-detail">
                      <div class="dashboard-heading">
                          <h6 class="dashboard-title">
                              {{$catalogdetail->title}}
                          </h6>
                          <p class="dashboard-subtitle" >
                              {{ $catalogdetail->name}}
                          </p>
                          <div class="container border">
                              <h5 class=" sinopsis-title "">
                                  Sinopsis Buku
                              </h5>
                              <p>
                                  {{ $catalogdetail->description}}

                              </p>
                          </div>
                          <br>
                          <div class="container border">
                              <h5 class=" sinopsis-title " >
                                  Tentang Buku
                              </h5>
                              <table class="table table-striped">
                                  <tbody class="body-table">
                                      <tr >
                                          <th scope="row">ID BUKU</th>
                                          <td>{{ $catalogdetail->id}}</td>
                                      </tr>
                                      <tr>
                                          <th scope="row">ISBN</th>
                                          <td>{{ $catalogdetail->isbn}}</td>
                                      </tr>
                                      <tr>
                                          <th scope="row">Tanggal Terbit</th>
                                          <td>{{ $catalogdetail->publication_year}}</td>
                                      </tr>
                                      <tr>
                                          <th scope="row">Penerbit</th>
                                          <td>{{ $catalogdetail->publisher->name}}</td>
                                      </tr>
                                      <tr>
                                          <th scope="row">Halaman</th>
                                          <td>{{ $catalogdetail->page}} halaman</td>
                                      </tr>
                                      <tr>
                                          <th scope="row">Bahasa</th>
                                          <td>Indonesia</td>
                                      </tr>
                                      <tr>
                                          <th scope="row">Kategori</th>
                                          <td >
                                              <div class="icon-kategori" >
                                                  <p> <img src="/images/icon/masjid.svg" alt=""> {{$catalogdetail->category->name}}
                                                  </p>
                                              </div>
                                          </td>
                                      </tr>
                                  </tbody>
                              </table>
                          </div>
                          <br/>
                      </div>

                  </div>
              </div>
          </div>

      </div>
      </section>
      
  </div>
</div>
@endsection

