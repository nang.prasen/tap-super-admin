@extends('layouts.super-admin')
@section('content')

<div id="page-content-wrapper">
    <!--Content Dashboard-->
    <!-- ........................ -->
    <div class="section-content section-inventaris-buku" data-aos="fade-up" data-aos-delay="500">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-6">
                            <div class="dashboard-heading">
                                <h5 class="dashboard-title" style="margin-top: 32px;">
                                    Riwayat RAB Anda
                                </h5>
                                <p class="dashboard-subtitle">
                                    Daftar riwayat RAB yang pernah dibuat
                                </p>
                            </div>
                        </div>
                        <div class="col-6 button-right">
                            <div class="dashboard-heading" style="align-items: baseline;">
                                <button class="btn" style="margin-top: 32px; color: #3685C8;"><img
                                        src="super-admin/images/icon/love.svg" alt=""> <a href="{{route('rab.create')}}">
                                        Buat RAB</a>
                                    </button>
                                <button class="btn" style="margin-top: 32px; color: #3685C8"><img
                                        src="super-admin/images/icon/add.svg" alt=""> <a href="{{route('catalog.mitra')}}">Katalog</a> 
                                    </button>

                            </div>
                        </div>
                    </div>

                    <div class="dashboard-heading">
                        <div class="container-fluid table">
                            <table class="table table-striped table-hover" id="table-inventaris">
                                <thead class="head-table">
                                    <tr>
                                        <th>No</th>
                                        <th>Nama RAB</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody class="body-table">
                                    @foreach ($rabmitra as $item)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{$item->name}}</td>
                                            <td>
                                                <!-- Default dropleft button -->
                                                <div class="btn-group dropleft">
                                                    <button type="button" class="btn btn-secondary fa fa-ellipsis-v"
                                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                                    </button>
                                                    <div class="dropdown-menu">
                                                        <!-- Dropdown menu links -->
                                                        <a class="dropdown-item" href="{{route('rab.detail',$item->id)}}">
                                                            <img src="super-admin/images/icon/inv-03.svg" alt=""> Detail</a>
                                                        <a class="dropdown-item" href="{{route('rab.export',$item->id)}}"><img src="super-admin/images/icon/inv-02.svg"
                                                                alt=""> Cetak</a>

                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    
                                    
                                </tbody>
                            </table>
                            <br>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

</div>
@endsection
