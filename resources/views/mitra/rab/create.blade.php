@extends('layouts.super-admin')
@section('content')
    <form action="{{route('rab.insert')}}" method="post">
        @csrf
        <div class="section-content section-dashboard-detail" data-aos="fade-up" data-aos-delay="500">
            <div class="container-fluid">
                <!-- Breandcrumb -->
                <section class="store-breadcrumbs">
                    <div class="row">
                        <div class="col-lg-12">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="">Dashboard</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Pembuatan RAB</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </section>
                <!-- Judil Detail Buku -->
                <div class="row">
                    <div class="col-lg-9">
                        <h5 class="dashboard-title">
                            Buat Rencana Anggaran Belanjamu sekarang
                        </h5>
                        <p class="dashboard-subtitle">
                            Pilih beberapa buku untuk di masukkan ke Anggaran Belanja
                        </p>
                    </div>
                </div>
                <!--Content Detail Pengaturan Buku Detail-->
                <section form-control>
                    <div class="container">
                        <h5 class="dashboard-title"> Detail Rencana Anggaran Belanja
                        </h5>
                        <hr>
                        <form>
                            <div class="form-group row">
                                <label for="input_bukupaket" class="col-sm-4 col-form-label">Nama RAB</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="name"
                                        placeholder="Masukan nama RAB Anda" id="name" required>
                                </div>
                            </div>
                        </form>
                        <br>
                    </div>
                </section>
                <br>
                <section>
                    <div class="container">
                        <h5 class="dashboard-title">
                            Pilih Buku
                        </h5>
                        <hr>
                        {{-- filter --}}
                        {{-- <div class="row">
                            <div class="col-md-6">
                                <div class="dropdown">
                                    <select name="filter_publisher" id="filter_publisher"
                                    class="form-control @error('publisher_id') is invalid @enderror" tabindex="1">
                                    <option value="#">--pilih penerbit--</option>
                                    @foreach ($publisher as $id => $name)
                                        <option value="{{ $id }}">{{ $name }}</option>
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                        </div> --}}
                        {{-- end filter --}}
                        <br>
                        <!-- Table Bootstrap -->
                        <table class="table table-striped rab" id="table-tambah-pengguna">
                            <thead>
                                <tr>
                                    <th class="active">
                                        <input id="check-all" type="checkbox" class="select-all checkbox"
                                            value="select-all" />
                                    </th>
                                    <th class="success">Judul Buku</th>
                                    <th class="warning">ISBN</th>
                                    <th class="warning">Harga</th>
                                    <th class="danger">Penerbit</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($books as $item)
                                    <tr>
                                        <td class="active">
                                            <input name="book_ids[]" class="select-item checkbox" type="checkbox"
                                                value="{{ $item->id }}">
                                        </td>
                                        <td>{{ $item->title }}</td>
                                        <td>{{ $item->isbn }}</td>
                                        <td>@currency($item->price)</td>
                                        <td>
                                            {{ isset($item->publisher->name) ? $item->publisher->name : 'Belum ada' }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </section>
                <section form-control>

                    <div class="container-fluid battom-nav">
                        <button class="btn btn-white"><img src="{{ asset('instansi/images/icon/plus_blue.svg') }}"alt="">Batal </button>
                        <button class="btn btn-blue "><img src="{{ asset('instansi/images/icon/katalog-active.svg') }}"alt=""> Buat RAB</button>
                    </div>
                </section>
            </div>
        </div>
    </form>
@endsection
@push('after-scripts')
<script>
    $(function(){
        var table = $('.rab').DataTable({
            processing: true,
            serverSide: true,
            ajax:{
                url :"{{route('get.books')}}",
            },
            columns:[
                {data: 'checkbox', name: 'checkbox', orderable: false, searchable: false
                {data:'title',name:'title'},
                {data:'isbn',name:'isbn'},
                {data:'price',name:'price'},
                {data:'publisher',name:'publisher.name'},
            ]
        });
        // $('#filter_publisher').change(function () {
        //    table.draw(); 
        // });
    });
</script>

    {{-- <script>
    $(function() {

        //button select all or cancel
        $("#select-all").click(function() {
            var all = $("input.select-all")[0];
            all.checked = !all.checked
            var checked = all.checked;
            $("input.select-item").each(function(index, item) {
                item.checked = checked;
            });
        });

        //button select invert
        $("#select-invert").click(function() {
            $("input.select-item").each(function(index, item) {
                item.checked = !item.checked;
            });
            checkSelected();
        });

        //button get selected info
        $("#selected").click(function() {
            var items = [];
            $("input.select-item:checked:checked").each(function(index, item) {
                items[index] = item.value;
            });
            if (items.length < 1) {
                alert("no selected items!!!");
            } else {
                var values = items.join(',');
                console.log(values);
                var html = $("<div></div>");
                html.html("selected:" + values);
                html.appendTo("body");
            }
        });

        //column checkbox select all or cancel
        $("input.select-all").click(function() {
            var checked = this.checked;
            $("input.select-item").each(function(index, item) {
                item.checked = checked;
            });
        });

        //check selected items
        $("input.select-item").click(function() {
            var checked = this.checked;
            console.log(checked);
            checkSelected();
        });

        //check is all selected
        function checkSelected() {
            var all = $("input.select-all")[0];
            var total = $("input.select-item").length;
            var len = $("input.select-item:checked:checked").length;
            console.log("total:" + total);
            console.log("len:" + len);
            all.checked = len === total;
        }
    });
</script> --}}
@endpush
