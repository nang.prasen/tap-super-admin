@extends('layouts.super-admin')
@section('content')


    <!-- Page Content -->
    <div id="page-content-wrapper">
        <div class="section-content section-dashboard-home" data-aos="fade-up" data-aos-delay="500">
            <!-- Banner -->
            <div class="carousel col-lg-12">
                <section class="store-carousel">
                    <div class="container-slider">
                        <div class="row">
                            <div class="col-lg-12" data-aos="zoom-in">
                                <div class="carousel slide" id="storeCarousel" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        <li class="active" data-target="#storeCarousel" data-slide-to="0"></li>
                                        <li data-target="#storeCarousel" data-slide-to="1"></li>
                                    </ol>
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <img src="instansi/images/banner/Banner-Landscape-1.jpg" alt=""
                                                class=" w-100" />
                                        </div>
                                        <div class="carousel-item">
                                            <img src="instansi/images/banner/Banner-Landscape-2.jpg" alt=""
                                                class=" w-100" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
            <br>
            <!-- Navigation -->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-6">
                        <!-- <div class="input-group rounded">
                            <input type="search" class="form-control rounded" placeholder="Search" aria-label="Search"
                                aria-describedby="search-addon" />
                            <span class="input-group-text border-0" id="search-addon">
                                <i class="fa fa-search"></i>
                            </span>
                        </div>  -->
                    </div>
                    <div class="col-lg-6" style="text-align: right;">
                        <button class="btn btn-white" style="text-decoration: none;"><img
                                src="instansi/images/icon/love.svg" alt=""> <a href="book.request"> Permintaan
                                Buku</a> </button>
                        </button>
                        <button class="btn btn-white"><img src="instansi/images/icon/study-blue.svg" alt="">
                            <a href="book.inv"> Rencana Pengadaan</a>
                        </button>
                    </div>
                </div>
            </div>

            <br>
            <!-- Content -->
            <div class="container-fluid">

                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="dashboard-heading">
                                <h5 class="dashboard-title">
                                    Kategori Buku
                                </h5>
                                <p class="dashboard-subtitle">
                                    Banyak pilihan buku berdasarkan kategori
                                </p>
                            </div>
                        </div>
                        <div class="col-md-6" style="text-align: right;">
                            <p pr-20>
                                <!-- <a href="#">Lihat Semua</a> -->
                            </p>
                        </div>
                    </div>

                    <div class="dashboard-content justify-content-center align-content-center">
                        <div class="row">
                            @foreach ($category as $item)
                                <div class=" col-lg-2 col-md-6 col-sm-6 col-6">
                                    <a href="{{ route('catalog.show.category', $item->slug) }}" class="button"
                                        style="text-decoration: none; text-overflow: clip; ">
                                        <div class="card-kategori mb-2">
                                            <div class="card-body">
                                                <img src="{{ url($item->image) }}" alt="Logo">
                                                <div class="dashboard-card-title" style="text-overflow: ellipsis;">
                                                    {{ $item->name }}
                                                </div>

                                                <div class="dashboard-card-subtitle">
                                                    {{ $item->book_count }}
                                                </div>

                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>

                <br>

                <!-- Rekomendasi untuk koleksi instansi -->

                <div class="dashboard-heading ">
                    <h5 class="dashboard-title">
                        Rekomendasi untuk koleksi instansi
                    </h5>
                    <p class="dashboard-subtitle">
                        Rekomendasi buku terbaik dari kami
                    </p>

                    <section class="book-carousel mb-50 ">
                        <div class="container-fluid table">
                            <br>
                            <div class="row">
                                <div class="col-md-6">
                                    <h2>
                                        Rekomendasi Buku Terbaru
                                    </h2>
                                </div>
                                <div class="col-md-6" style="text-align: right;">
                                    <p pr-30>
                                        <a href="#">Lihat Semua</a>
                                    </p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div id="carouselExampleInterval" class="carousel slide" data-ride="carousel">
                                        <div class="carousel-inner">

                                            <div class="carousel-item active" data-interval="10000">
                                                <div class="row">
                                                    @foreach ($catalogs as $item)
                                                        <div class="col-lg-2 col-md-3 col-sm-4 col-4">
                                                            <a href="{{route('catalog.detail',$item->id)}}"><img src="{{ $item->cover }}"
                                                                    class=" w-100" alt="..."></a>
                                                            <h4>{{ Str::limit($item->title, 10) }}</h4>
                                                            <p class="limit-text-1line">{{ $item->author }}
                                                            </p>
                                                            <div class="icon-category">
                                                                <p class="mt-auto mb-auto p-2">{{ $item->category->name }}
                                                                </p>
                                                            </div>
                                                        </div>
                                                    @endforeach

                                                </div>

                                            </div>

                                            <a class="carousel-control-prev" href="#carouselExampleInterval" role="button"
                                                data-slide="prev">
                                                <img src="instansi/images/icon/left-slider.svg" alt="">
                                                <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="carousel-control-next" href="#carouselExampleInterval" role="button"
                                                data-slide="next">
                                                <img src="instansi/images/icon/right-slider.svg" alt="">
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-6">
                                        <h2>
                                            Rekomendasi Buku Paket
                                        </h2>
                                    </div>
                                    <div class="col-md-6" style="text-align: right;">
                                        <p pr-20>
                                            <a href="#">Lihat Semua </a>
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div id="carouselExampleInterval2" class="carousel slide" data-ride="carousel">
                                            <div class="carousel-inner">

                                                <div class="carousel-item active" data-interval="10000">
                                                    <div class="row">
                                                        @foreach ($books as $item)
                                                            <div class="col-lg-2 col-md-3 col-sm-4 col-4">
                                                                <a href="{{route('catalog.detail',$item->id)}}"><img src="{{ $item->cover }}"
                                                                        class=" w-100" alt="..."></a>
                                                                <h4>{{ $item->title }}</h4>
                                                                <p class="limit-text-1line">{{ $item->author }}
                                                                </p>
                                                                <div class="icon-category">
                                                                    <p class="mt-auto mb-auto p-2">
                                                                        {{ $item->category->name }}</p>
                                                                </div>
                                                            </div>
                                                        @endforeach

                                                    </div>
                                                </div>

                                            </div>

                                            <a class="carousel-control-prev" href="#carouselExampleInterval2" role="button"
                                                data-slide="prev">
                                                <img src="instansi/images/icon/left-slider.svg" alt="">
                                                <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="carousel-control-next" href="#carouselExampleInterval2" role="button"
                                                data-slide="next">
                                                <img src="instansi/images/icon/right-slider.svg" alt="">
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </div>
                                        <br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

            </div>
        </div>
    </div>

@endsection
