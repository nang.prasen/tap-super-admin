@extends('layouts.super-admin')
@section('content')

<div id="page-content-wrapper">
    <!--Content Dashboard-->
    <!-- ........................ -->
    <div class="section-content section-inventaris-buku" data-aos="fade-up" data-aos-delay="500">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-6">
                            <div class="dashboard-heading">
                                <h5 class="dashboard-title" style="margin-top: 32px;">
                                    Detail RAB {{$rabmitradetail->name}}
                                </h5>
                                <p class="dashboard-subtitle">
                                    Daftar buku dari Rencana Anggaran Belanja yang Anda Buat
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="dashboard-heading">
                        <div class="container-fluid table">
                            <table class="table table-striped table-hover" id="table-inventaris">
                                <thead class="head-table">
                                    <tr>
                                        <th>No</th>
                                        <th>Judul Buku</th>
                                        <th>ISBN</th>
                                        <th>Harga</th>
                                    </tr>
                                </thead>
                                <tbody class="body-table">
                                    @foreach ($rabmitradetail->detail as $item)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{$item->book->title}}</td>
                                            <td>{{$item->book->isbn}}</td>
                                            <td>@currency($item->book->price) </td>
                                        </tr>
                                    @endforeach
                                    
                                    
                                </tbody>
                            </table>
                            <br>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

</div>
@endsection
