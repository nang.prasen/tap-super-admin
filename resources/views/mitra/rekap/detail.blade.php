@extends('layouts.super-admin')
@section('content')
    <div id="page-content-wrapper">
        <!--Content Dashboard-->

        <div class="section-content aos-init" data-aos="fade-up" data-aos-delay="500">
            <div class="container-fluid">
                <!-- Breandcrumb -->
                <section class="store-breadcrumbs">
                    <div class="row">
                        <div class="col-lg-12">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Rekapitulasi</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Detail Transaksi</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </section>
                <!-- Judul -->
                <div class="row justify-content-between">
                    <div class="col-lg-6">
                        <div class="dashboard-heading">
                            <h5 class="dashboard-title">
                                Detail Transaksi
                            </h5>
                        </div>
                    </div>
                </div>
                <!--Aksi Tambah Pengguna-->
                <br>
                <div class="container-fluid " style="background-color: white; padding-top: 10px;">
                    <h5 class=" dashboard-title " style="color: #23120B; font-weight: 500; font-size: 20px; ">
                        Data Transaksi
                    </h5>
                    <hr>
                    <table class="table table-striped">
                        <tbody>
                            <tr class="d-flex">
                                <th class="col-3">Nama Instansi</th>
                                <td class="col-5">
                                    <p>
                                        {{$detailtransaksi->name}}
                                    </p>
                                </td>
                                <td class="col-4"> </td>
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">Alamat</th>
                                <td class="col-5">
                                    <p>
                                        {{$detailtransaksi->address}}
                                    </p>
                                </td>
                                <td> </td>
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">Tanggal Pendaftaran</th>
                                <td class="col-5">
                                    <p>
                                        {{$detailtransaksi->registration_date}}
                                    </p>
                                </td>
                                <td> </td>
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">Tanggal Pengiriman Aplikasi</th>
                                <td class="col-5">
                                    <p>
                                        {{$detailtransaksi->delivery_app_date}}
                                    </p>
                                </td>
                                <td> </td>
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">Domain</th>
                                <td class="col-5">
                                    <p>
                                        {{$detailtransaksi->domain}}
                                    </p>
                                </td>
                                <td class="col-4"> </td>
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">Nama Aplikasi</th>
                                <td class="col-5">
                                    <p>
                                        {{$detailtransaksi->nameapp}}
                                    </p>
                                </td>
                                <td class="col-4"> </td>
                            </tr>

                            <tr class="d-flex">
                                <th class="col-3">Status Aplikasi</th>
                                <td class="col-5">
                                    @if ($detailtransaksi->status_extension_app == 1)
                                        <p>APK</p>
                                    @else
                                        Playstore
                                    @endif
                                </td>
                                <td class="col-4"> </td>
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">Link Aplikasi</th>
                                <td class="col-5">
                                    <p>
                                        {{$detailtransaksi->link_app}}
                                    </p>
                                </td>
                                <td class="col-4"> </td>
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">Versi Aplikasi</th>
                                <td class="col-5">
                                    <p>
                                        {{$detailtransaksi->version_app}}
                                    </p>
                                </td>
                                <td class="col-4"> </td>
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">Status</th>
                                <td class="col-5">
                                    <p>
                                        {{$detailtransaksi->status_progress_app}}
                                    </p>
                                </td>
                                <td class="col-4"> </td>
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">Mitra</th>
                                <td class="col-5">
                                    <p>
                                        Polo
                                    </p>
                                </td>
                                <td class="col-4"> </td>
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">Perwakilan</th>
                                <td class="col-5">
                                    PMJ
                                </td>
                                <td class="col-4"> </td>
                            </tr>
                        </tbody>
                    </table>
                    <br>

                </div>
                <br>
                <div class="container-fluid " style="background-color: white; padding-top: 10px;">
                    <h5 class=" dashboard-title " style="color: #23120B; font-weight: 500; font-size: 20px; ">
                        Informasi Transaksi
                    </h5>
                    <hr>
                    <table class="table table-striped">
                        <tbody>
                            <tr class="d-flex">
                                <th class="col-3">Nilai PO</th>
                                <td class="col-5">
                                    <p>
                                        @isset($detailtransaksi->transaction->preorder)
                                            {{$detailtransaksi->transaction->preorder}}
                                        @endisset
                                        Belum ada PO
                                         {{-- {{isset($detailtransaksi->transaction->preorder) ? $detailtransaksi->transaction->preorder:'Rp' }} --}}
                                    </p>
                                </td>
                                <td class="col-4"> </td>
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">Tanggal Masuk SPK</th>
                                <td class="col-5">
                                    <p>
                                        {{isset($detailtransaksi->transaction->spk_date) ? $detailtransaksi->transaction->spk_date:'Belum ada'}}
                                    </p>
                                </td>
                                <td class="col-4"> </td>
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">Tanggal Masuk RAB</th>
                                <td class="col-5">
                                    <p>
                                        {{isset($detailtransaksi->transaction->rab_date) ? $detailtransaksi->transaction->rab_date:'Belum ada'}}
                                        
                                    </p>
                                </td>
                                <td class="col-4"> </td>
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">Status Pembayaran</th>
                                <td class="col-5">
                                    <p>
                                        {{isset($detailtransaksi->transaction->status) ? $detailtransaksi->transaction->status:'Belum ada'}}
                                    </p>
                                </td>
                                <td class="col-4"></td>
                            </tr>
                            <tr class="d-flex">
                                <th class="col-3">Tanggal Pengiriman Paket Transaksi</th>
                                <td class="col-5">
                                    <p>
                                        {{isset($detailtransaksi->transaction->delivery_packet_date) ? $detailtransaksi->transaction->delivery_packet_date:'Belum ada'}}
                                    </p>
                                </td>
                                <td class="col-4"> </td>
                            </tr>
                        </tbody>
                    </table>
                    <br>
                </div>
                <hr>
                <div class="container-fluid justify-content-end"
                    style="background-color: white;padding: 5px 10px 5px 0; text-align: right; margin-bottom: 30px;">
                    <a href="{{route('mitra.index')}}" class="btn" style="color: #3685C8; ">
                        <i class="fa fa-angle-left"></i> Kembali</a>

                </div>
            </div>
        </div>
    </div>
@endsection
