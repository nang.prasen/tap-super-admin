@extends('layouts.super-admin')
@section('content')
<div id="page-content-wrapper">
    <!--Content Dashboard-->

    <div class="section-content aos-init" data-aos="fade-up" data-aos-delay="500">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="dashboard-heading">
                        <h5 class="dashboard-title">Aksi Admin</h5>
                        <p class="dashboard-subtitle">
                            Aksi penting yang bisa admin lakukan
                        </p>
                    </div>
                    <div class="dashboard-heading">
                        <div class="container-fluid table">
                            <br>
                            <table class="
                        table table-striped table-hover
                        dataTable
                        no-footer table-responsive
                      " id="table-paginate" role="grid">
                                <thead class="head-table">
                                    <tr role="row">
                                        <th class="sorting sorting_asc" tabindex="0"
                                            aria-controls="table-analisa" rowspan="1" colspan="1"
                                            aria-sort="ascending"
                                            aria-label="No: activate to sort column descending"
                                            style="width: 28.1875px">
                                            No
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="table-analisa"
                                            rowspan="1" colspan="1"
                                            aria-label="Nama Instansi: activate to sort column ascending"
                                            style="width: 183.547px">
                                            Nama Instansi
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="table-analisa"
                                            rowspan="1" colspan="1"
                                            aria-label="Alamat: activate to sort column ascending"
                                            style="width:300.547px">
                                            Alamat
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="table-analisa"
                                            rowspan="1" colspan="1"
                                            aria-label="Tanggal Pendaftaran: activate to sort column ascending"
                                            style="width: 18.547px">
                                            Tanggal Pendaftaran
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="table-analisa"
                                            rowspan="1" colspan="1"
                                            aria-label="Tangal Pengiriman Aplikasi: activate to sort column ascending"
                                            style="width: 183.547px">
                                            Tanggal Pengiriman Aplikasi
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="table-analisa"
                                            rowspan="1" colspan="1"
                                            aria-label="Domain: activate to sort column ascending"
                                            style="width: 183.547px">
                                            Domain
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="table-analisa"
                                            rowspan="1" colspan="1"
                                            aria-label="Nama Aplikasi: activate to sort column ascending"
                                            style="width: 183.547px">
                                            Nama Aplikasi
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="table-analisa"
                                            rowspan="1" colspan="1"
                                            aria-label="Status Aplikasi: activate to sort column ascending"
                                            style="width: 183.547px">
                                            Status Aplikasi
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="table-analisa"
                                            rowspan="1" colspan="1"
                                            aria-label="Link Aplikasi: activate to sort column ascending"
                                            style="width: 183.547px">
                                            Link Aplikasi
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="table-analisa"
                                            rowspan="1" colspan="1"
                                            aria-label="Versi Aplikasi: activate to sort column ascending"
                                            style="width: 183.547px">
                                            Versi Aplikasi
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="table-analisa"
                                            rowspan="1" colspan="1"
                                            aria-label="Status: activate to sort column ascending"
                                            style="width: 183.547px">
                                            Status
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="table-analisa"
                                            rowspan="1" colspan="1"
                                            aria-label="Mitra: activate to sort column ascending"
                                            style="width: 183.547px">
                                            Mitra
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="table-analisa"
                                            rowspan="1" colspan="1"
                                            aria-label="Perwakilan: activate to sort column ascending"
                                            style="width: 183.547px">
                                            Perwakilan
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="table-analisa"
                                            rowspan="1" colspan="1"
                                            aria-label="Transaksi: activate to sort column ascending"
                                            style="width: 40.25px">
                                            Transaksi
                                        </th>
                                    </tr>
                                </thead>
                                <tbody class="body-table">
                                    @foreach ($rekap as $item)
                                        <tr class="odd">
                                            <td class="sorting_1">{{$loop->iteration}}</td>
                                            <td>{{$item->name}}</td>
                                            <td>{{$item->address}}</td>
                                            <td>{{$item->registration_date}}</td>
                                            <td>{{$item->delivery_app_date}}</td>
                                            <td>{{$item->domain}}</td>
                                            <td>{{$item->nameapp}}</td>
                                            <td>
                                                @if ($item->status_extension_app == 1)
                                                    <p>APK</p>
                                                @elseif($item->status_extension_app == 0)
                                                    <p>Playstore</p>
                                                @endif
                                            </td>
                                            <td>
                                                <p style="text-overflow: ellipsis;overflow: hidden;display: block;white-space: nowrap;width: 170px;">
                                                    {{$item->link_app}}
                                                </p>
                                            </td>
                                            <td>{{$item->version_app}}</td>
                                            <td>
                                                <div class="text-success">{{$item->status_progress_app}}</div>
                                            </td>
                                            {{-- @foreach ($item->agent as $mitra)
                                                @foreach ($mitra->member as $item)
                                                    <td>{{$item->name}}</td>
                                                @endforeach
                                            <td>{{$mitra->name}}</td>
                                            @endforeach --}}
                                            {{-- <td>{{$item->agent_member}}</td>
                                            <td>{{$item->name_agent}}</td> --}}
                                            {{-- <td>{{$item->agent->implode('name', ', ')}}</td> --}}
                                            {{-- @foreach ($item->agent as $e => $mitra) --}}
                                                {{-- <?php $mitra = $mitra[0]; ?> --}}
                                                {{-- <td>
                                                    <p>{{isset($mitra->member->name) ? $mitra->member->name:''}}</p>
                                                </td> --}}
                                                @foreach ($item->agentmember as $mitra)
                                                <td>
                                                    <p>{{isset($mitra->name) ? $mitra->name:''}}</p>
                                                </td>
                                                <td>
                                                    <p>{{isset($mitra->agent->name) ? $mitra->agent->name:''}}</p>
                                                </td>
                                                @endforeach
                                            {{-- @endforeach --}}
                                            {{-- <td>{{$item->agent->implode('name', ', ')}}</td> --}}
                                            {{-- <td>{{$item->agent->name}}</td> --}}
                                            {{-- <td>{{isset($item->agent->member->name) ? $item->agent->member->name:''}}</td> --}}
                                            {{-- <td>{{isset($item->agent->name) ? $item->agent->name:'Perwakilan Belum Ada'}}</td> --}}
                                           
                                            <td>
                                                <!-- Default dropleft button -->
                                                <a class="btn btn-blue" type="button" href="{{route('mitra.show',$item->id)}}"> <i
                                                    class="fa fa-dollar"></i> Detail Transaksi</a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    

                                </tbody>
                            </table>
                            <br>
                        </div>
                    </div>
                    <br />
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

