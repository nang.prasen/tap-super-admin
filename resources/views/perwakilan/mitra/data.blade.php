@extends('layouts.super-admin')
@section('content')
<div class="section-content" data-aos="fade-up" data-aos-delay="500">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="row justify-content-between">
                    <div class="col-lg-6">
                        <div class="dashboard-heading">
                            <h5 class="dashboard-title">
                                Daftar Mitra Saya
                            </h5>
                            <p class="dashboard-subtitle">
                               Daftar Mitra Yang Bekerja
                            </p>
                        </div>
                    </div>
                    {{-- <div class="col-lg-6" style="text-align: right;">
                        <div class="dashboard-heading">
                            <a class="btn btn-blue" href="#" ><img src="super-admin/images/icon/plus_white.svg" aria-hidden="true"> Tambah Mitra</a>
                            
                        </div>
                    </div> --}}
                </div>
                <div class="dashboard-heading">
                    <div class="container-fluid table" >
                        
                        <br>
                        <table class="table table-striped " id="table-tambah-pengguna">
                            <thead class="head-table">
                                <tr>
                                    <th>No</th>
                                    <th>ID</th>
                                    <th>Nama Mitra </th>
                                    <th>Alamat</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody class="body-table">
                                @foreach ($agent as $ag)
                                    @foreach ($ag->member as $item)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{$item->agent_member_id}}</td>
                                            <td>{{$item->name}}</td>
                                            <td>{{$item->address}}</td>
                                            </td>
                                            <td>
                                                <!-- Default dropleft button -->
                                                <div class="btn-group dropleft">
                                                    <button type="button" class="btn btn-secondary fa fa-ellipsis-v" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                                    <div class="dropdown-menu">
                                                        <!-- Dropdown menu links -->
                                                        <a class="dropdown-item " href="{{route('perwakilan.show',$item->id)}}"> <img src="super-admin/images/icon/profile.svg" alt=""> Detail Mitra</a>
                                                        
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endforeach
                                
                                
                            </tbody>
                        </table>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
