@extends('layouts.super-admin')
@section('content')
<div id="page-content-wrapper">
    <!--Content Dashboard-->
    <!-- ........................ -->
    <div class="section-content section-dashboard-bukuterbaru" data-aos="fade-up" data-aos-delay="500">
        <div class="container-fluid">
       <!-- Breandcrumb -->
               <section class="store-breadcrumbs">
                   <div class="row">
                       <div class="col-lg-12">
                           <nav aria-label="breadcrumb">
                               <ol class="breadcrumb">
                                   <li class="breadcrumb-item"><a href="/dashboard.html">Profile</a></li>
                                   <li class="breadcrumb-item active" aria-current="page">Data Mitra</li>
                               </ol>
                           </nav>
                       </div>
                   </div>
               </section>
           <!-- Judul -->
           <div class="row justify-content-between">
              
           </div>
           <!--Aksi Tambah Pengguna-->
           <br>
           <div class="container-fluid table" >
               <div class="">
                   <h5 class=" content-title data" >
                   Data Mitra
                   <a href="{{route('perwakilan.edit', $perwakilan->id)}}"><button class="btn btn-white data" ><img src="{{asset('super-admin/images/icon/pen_blue.svg')}}" aria-hidden="true"> Edit</button></a>
                   
               </h5> 
                  
               </div>
                      
               <hr>
               <table class="table table-striped ">
                   <tbody class="body-table">
                       <tr class="d-flex">
                           <th class="col-3">Avatar</th>
                           <td class="col-5"> 
                              <img class="rounded-circle mr-2 profile-picture" src="" alt="">
                           </td>
                           <td class="col-4"> </td>
                       </tr>
                       <tr class="d-flex">
                            <th class="col-3">ID Mitra</th>
                            <td class="col-5"> 
                                <p>{{ $perwakilan->agentId }}</p>
                            </td>
                            <td class="col-4"> </td>
                        </tr>
                        <tr class="d-flex">
                            <th class="col-3">Nama Mitra</th>
                            <td class="col-5"> 
                                <p>{{ $perwakilan->name }}</p>
                            </td>
                            <td class="col-4"> </td>
                        </tr>
                        <tr class="d-flex">
                            <th class="col-3">Alamat</th>
                            <td class="col-5"> 
                                <p>{{ $perwakilan->address }}</p>
                            </td>
                            <td class="col-4"> </td>
                        </tr>
                       
                </tbody>
               </table>
               <br>
           </div>
           <br>

           <div class="container-fluid table" >
               <h5 class=" content-title ">
                   Informasi Akun
               </h5> 
               <hr>
               <table class="table table-striped">
                   <tbody class="body-table">
                        <tr class="d-flex">
                            <th  class="col-3">Nama Akun</th>
                            <td class="col-5">
                                <p> {{Auth::user()->name}}</p>      
                            </td>
                            <td> </td>
                        </tr>
                        <tr class="d-flex">
                           <th class="col-3">E-mail</th>
                           <td class="col-5">
                               {{Auth::user()->email}}
                           </td>
                           <td> </td>
                       </tr>
                       <tr class="d-flex">
                           <th class="col-3">Departemen</th>
                           <td class="col-5"> Perwakilan</p>
                           </td>
                           <td> </td>
                       </tr>
                   </tbody>
               </table>
               <br>
           </div>
           <hr>
       <!-- <div class="container-fluid battom-nav">
           <button class="btn btn-white" ><img src="super-admin/images/icon/stop_blue.svg" aria-hidden="true"> Batal</button>
           <button class="btn btn-disabled" ><img src="super-admin/images/icon/plus_white.svg" aria-hidden="true"> Tambah  Pengguna</button>
            -->
           <!-- jika aktif -->
           <!-- <button class="btn btn-blue" ><img src="super-admin/images/icon/plus_white.svg" aria-hidden="true"> Tambah  Pengguna</button>
                                                       
       </div> -->
   </div>
   </div>
</div>
@endsection