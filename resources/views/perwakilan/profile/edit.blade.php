@extends('layouts.super-admin')
@section('content')
    <div id="page-content-wrapper">
        <!--Content Dashboard-->
        <!-- ........................ -->
        <div class="section-content section-dashboard-bukuterbaru" data-aos="fade-up" data-aos-delay="500">
            <div class="container-fluid">
                <!-- Breandcrumb -->
                <section class="store-breadcrumbs">
                    <div class="row">
                        <div class="col-lg-12">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="/dashboard.html">Daftar Pengguna</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Detail Pengguna</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </section>
                <br>
                <form action="{{ route('perwakilan.update',$perwakilan->id) }}" method="POST">
                    @method('PUT')
                    @csrf
                    <div class="container-fluid table">
                        <div class="">
                            <h5 class=" content-title data">
                                Data Pengguna
                            </h5>
    
                        </div>
    
                        <hr>
                        <table class="table table-striped ">
                            <tbody class="body-table">
    
                                <tr class="d-flex">
                                    <th class="col-3">Nama Mitra</th>
                                    <td class="col-5">
                                        <input type="text" class="form-control" id="NamaLengkap" aria-describedby="nama" name="name"
                                            value="{{ $perwakilan->name }}">
                                    </td>
                                    <td> </td>
                                </tr>
    
                                <tr class="d-flex">
                                    <th class="col-3">Alamat Lengkap</th>
                                    <td class="col-5">
                                        <input type="text" class="form-control" id="inputAddress" name="address"
                                            value="{{ $perwakilan->address }}">
                                    </td>
                                    <td></td>
                                </tr>
                        </table>
                        <br>
                    </div>
                    <br>
    
                    <hr>
                    <div class="container-fluid battom-nav">
                        <button class="btn btn-white"><img src="{{ asset('super-admin/images/icon/stop_blue.svg') }}"
                                aria-hidden="true"> Batal</button>
                        <button class="btn btn-blue"><i class="fa fa-save" aria-hidden="true"></i> Update
                            Perubahan</button>
    
                    </div>
                </form>


            </div>
        </div>

    </div>
@endsection
