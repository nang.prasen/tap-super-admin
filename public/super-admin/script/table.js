// Buku Terlaris
$(document).ready(function() {
    var table = $('#table').DataTable({
        lengthChange: false,
        dom:
        // 'Bfrtip',
            "<'row'<'col-md-3'f><'col-md-6'><'col-md-3'B>>" +
            "<'row'<'col-md-12'tr>>" +
            "<'row'<col-md-12'>>",
        buttons: [{
            extend: 'csvHtml5',
            text: ' Export   <i class="fa  fa-cloud-download"></i>',
            titleAttr: 'CSV'
        }, ],

        language: {
            searchPlaceholder: "Search",
            search: "",

        },



    });

    table.buttons().container()
        .appendTo('#table_wrapper .col-md-5:eq(0)');
});
// Buku Masuk
$(document).ready(function() {
    var table = $('#buku-masuk').DataTable({
        lengthChange: false,
        dom:
        // 'Bfrtip',
            "<'row'<'col-md-3'f><'col-md-6'><'col-md-3'B>>" +
            "<'row'<'col-md-12'tr>>" +
            "<'row'<col-md-12'>>",
        buttons: [{
            extend: 'csvHtml5',
            text: ' Export   <i class="fa  fa-cloud-download"></i>',
            titleAttr: 'CSV'
        }, ],

        language: {
            searchPlaceholder: "Search",
            search: "",

        },



    });

    table.buttons().container()
        .appendTo('#table_wrapper .col-md-5:eq(0)');
});

// Buku Terlaris
$(document).ready(function() {
    var table = $('#table-analisa').DataTable({
        lengthChange: false,
        dom:
        // 'Bfrtip',
            "<'row'<'col-md-3'f><'col-md-6'><'col-md-3'B>>" +
            "<'row'<'col-md-12'tr>>" +
            "<'row'<col-md-12'>>",
        buttons: [{
            extend: 'csvHtml5',
            text: ' Export   <i class="fa  fa-cloud-download"></i>',
            titleAttr: 'CSV'
        }, ],

        language: {
            searchPlaceholder: "Search",
            search: "",

        },



    });

    table.buttons().container()
        .appendTo('#table_wrapper .col-md-5:eq(0)');
});

// Inventaris - Inventaris-1
$(document).ready(function() {
    var table = $('#table-inventaris').DataTable({
        lengthChange: false,
        dom:
        // 'Bfrtip',
            "<'row'<'col-md-3'f><'col-md-6'><'col-md-3'B>>" +
            "<'row'<'col-md-12'tr>>" +
            "<'row'<'col-sm-3'i><'col-sm-3'><'col-sm-3'p>>",
        buttons: [{
                extend: 'csvHtml5',
                text: ' Export   <i class="fa  fa-cloud-download"></i>',
                titleAttr: 'CSV'
            },
            // {
            //     text: 'Alert',
            //     action: function(e, dt, node, config) {
            //         alert('Activated!');
            //         this.disable(); // disable button
            //     }
            // }
        ],


        language: {
            searchPlaceholder: "Search",
            search: "",

        },



    });

    table.buttons().container()
        .appendTo('#table_wrapper .col-md-5:eq(0)');
});


// Tambah Pengguna
$(document).ready(function() {
    var table = $('#table-tambah-pengguna').DataTable({
        lengthChange: false,
        dom:
        // 'Bfrtip',
            "<'row'<'col-md-3'f><'col-md-6'><'col-md-3'B>>" +
            "<'row'<'col-md-12'tr>>" +
            "<'row'<'col-sm-3'i><'col-sm-3'><'col-sm-3'p>>",
        buttons: [{
            extend: 'csvHtml5',
            text: ' Export   <i class="fa  fa-cloud-download"></i>',
            titleAttr: 'CSV'
        }, ],

        language: {
            searchPlaceholder: "Search",
            search: "",

        },

    });

    table.buttons().container()
        .appendTo('#table_wrapper .col-md-5:eq(0)');
});

// Distribusi Buku ke instansi
$(document).ready(function() {
    var table = $('#distribution-agencies').DataTable({
        lengthChange: false,
        dom:
        // 'Bfrtip',
            "<'row'<'col-md-3'f><'col-md-6'><'col-md-3'B>>" +
            "<'row'<'col-md-12'tr>>" +
            "<'row'<'col-sm-3'i><'col-sm-3'><'col-sm-3'p>>",
        buttons: [{
            extend: 'csvHtml5',
            text: ' Export   <i class="fa  fa-cloud-download"></i>',
            titleAttr: 'CSV'
        }, ],

        language: {
            searchPlaceholder: "Search",
            search: "",

        },

    });

    table.buttons().container()
        .appendTo('#table_wrapper .col-md-5:eq(0)');
});

// Pengaturan Buku Paket
$(document).ready(function() {
    var table = $('#pengaturan-buku').DataTable({
        lengthChange: false,
        dom:
        // 'Bfrtip',
            "<'row'<'col-md-2'f><'col-md-7'><'col-md-3'B>>" +
            "<'row'<'col-md-12'tr>>" +
            "<'row'<'col-sm-3'i><'col-sm-3'><'col-sm-3'p>>",
        buttons: [{
                extend: 'csvHtml5',
                text: ' Export   <i class="fa  fa-cloud-download"></i>',
                titleAttr: 'CSV'
            },
            // {
            //     text: '<i class=fa  fa-cloud-download"></i> Tambah Pengguna',
            //     action: function(e, dt, node, config) {
            //         alert('Activated!');
            //         this.disable(); // disable button
            //     }
            // }
        ],


        language: {
            searchPlaceholder: "Search",
            search: "",

        },



    });

    table.buttons().container()
        .appendTo('#table_wrapper .col-md-5:eq(0)');
});
// Pengaturan Buku Paket Detail
$(document).ready(function() {
    var table = $('#pengaturan-buku-detail').DataTable({
        lengthChange: false,
        dom:
        // 'Bfrtip',
            "<'row'<'col-md-3'f><'col-md-6'><'col-md-3'B>>" +
            "<'row'<'col-md-12'tr>>" +
            "<'row'<'col-sm-3'i><'col-sm-3'><'col-sm-3'p>>",
        buttons: [{
            extend: 'csvHtml5',
            text: ' Export   <i class="fa  fa-cloud-download"></i>',
            titleAttr: 'CSV'
        }, ],


        language: {
            searchPlaceholder: "Search",
            search: "",

        },



    });

    table.buttons().container()
        .appendTo('#table_wrapper .col-md-5:eq(0)');
});

// Pengaturan Buku Paket Bagikan
$(document).ready(function() {
    $('#pengaturan-buku-bagikan').DataTable({
        dom:
        // 'Bfrtip',
            "<'row'<'col-md-3'f><'col-md-6'><'col-md-3'B>>" +
            "<'row'<'col-md-12'tr>>" +
            "<'row'<'col-sm-3'i><'col-sm-3'><'col-sm-3'p>>",
        buttons: [{
            extend: 'csvHtml5',
            text: ' Export   <i class="fa  fa-cloud-download"></i>',
            titleAttr: 'CSV'
        }, ],
        language: {
            searchPlaceholder: 'Search',
            search: "",

        },
        columnDefs: [{
            orderable: false,
            className: 'select-checkbox',
            targets: 0
        }],
        select: {
            style: 'os',
            selector: 'td:first-child'
        },
        order: [
            [1, 'asc']
        ]
    });
});

// distribuso buku
$(document).ready(function() {
    $('#distribusi-buku').DataTable({
        dom:
        // 'Bfrtip',
            "<'row'<'col-md-3'f><'col-md-6'><'col-md-3'B>>" +
            "<'row'<'col-md-12'tr>>" +
            "<'row'<'col-sm-3'i><'col-sm-3'><'col-sm-3'p>>",
        buttons: [{
            extend: 'csvHtml5',
            text: ' Export   <i class="fa  fa-cloud-download"></i>',
            titleAttr: 'CSV'
        }, ],
        language: {
            searchPlaceholder: 'Search',
            search: "",

        },
        columnDefs: [{
            orderable: false,
            className: 'select-checkbox',
            targets: 0
        }],
        select: {
            style: 'os',
            selector: 'td:first-child'
        },
        order: [
            [1, 'asc']
        ]
    });
});

// User Penerbit